﻿"use strict";
addEventListener("DOMContentLoaded", function ExportScope() {

    document.getElementById("export").onclick = buildExportData;

    // Shared globals
    var maxWidth = 0;
    var maxHeight = 0;
    var floorMap = [];
    var viewer = null;

    function buildExportData() {
        try {
            var root = {
                version: 1,
                width: 0,
                height: 0,
                floors: model.floors.map(function (floor, i) { return buildFloorExportData(floor, i) }),
                startSet: [],
                endSet: [],
                collectibles: [],
                transports: []
            };
            root.width = maxWidth + 15; // Buffer over the widest center-point found
            root.height = maxHeight + 15;

            buildStartEndExportData(root.startSet, root.endSet);
            buildCollectiblesExportData(root.collectibles);
            buildTransportsExportData(root.transports);

            var binaryDataBlob = new Blob([JSBinaryUtil.serialize(binaryFormatV1, root)], { type: "application/octet-stream" });

            navigator.msSaveBlob(binaryDataBlob, "New Maze.bin");

            if (!viewer || viewer.closed) {
                onmessage = function (e) {
                    if (e.data.ready == true) {
                        e.source.postMessage({ payload: binaryDataBlob }, "*");
                    }
                    onmessage = null;
                }
                viewer = open("binaryViewer.htm", "Viewer");
            }
            else
                viewer.postMessage({ payload: binaryDataBlob }, "*");
        }
        catch (ex) {
            alert("Export failed!\n\n" + ex);
        }

        maxWidth = 0;
        maxHeight = 0;
        floorMap = []; // Release this memory
    }

    function buildFloorExportData(floor, i) {
        var cellMap = {}; // Index it by group to get another map of [original cell index -> { newIndex: new cell index, walls: array matching wall connectors}]
        var cells = [];
        var walls = [];
        var borders = [];
        floor.groups.forEach(function (group, groupIndex) {
            var oldNewCellMap = {};
            var groupTransform = group.exportTransformStateAsMatrix();
            if (group.type ==  1) { // Rect
                group.cells.forEach(function (cell, cellIndex) {
                    if (!cell.deleted) {
                        // Absolutize the center (cell.location is already in proper units)
                        var transformedCenterPoint = groupTransform.multiply(cell.location);
                        if (transformedCenterPoint[0] > maxWidth)
                            maxWidth = transformedCenterPoint[0];
                        if (transformedCenterPoint[1] > maxHeight)
                            maxHeight = transformedCenterPoint[1];
                        oldNewCellMap[cellIndex] = { 
                            newIndex: (cells.push( { x: transformedCenterPoint[0], y: transformedCenterPoint[1], connectors: [] }) - 1), // New length - 1 is index.
                            walls: [null,null,null,null] // Establish the default [unprocessed] state for the walls.
                        };
                    }
                });
            }
            else if (group.type == 2) { // Circle
                group.cells.forEach(function (cell, cellIndex) {
                    if (!cell.deleted) {
                        // Calculate and transform the circle cell's center point...
                        var transformedCenterPoint = groupTransform.multiply(
                            [ (cell.location[0] * 10) * Math.cos( cell.location[1] + ((cell.location[2] - cell.location[1])/2) ), // X = hyp(on the center line) * cos(theta(midway between start and end angles)) 
                              (cell.location[0] * 10) * Math.sin( cell.location[1] + ((cell.location[2] - cell.location[1])/2) ) ]);
                        if (transformedCenterPoint[0] > maxWidth)
                            maxWidth = transformedCenterPoint[0];
                        if (transformedCenterPoint[1] > maxHeight)
                            maxHeight = transformedCenterPoint[1];
                        // Center cells have no parent and no siblings connectors. Forstall any problems downstream by marking these connector positions as "already processed" (-1)
                        oldNewCellMap[cellIndex] = {
                            newIndex: (cells.push({ x: transformedCenterPoint[0], y: transformedCenterPoint[1], connectors: [] }) - 1),
                            walls: cell.connectors.map(function (c, connIndex) { if ((cellIndex == 0) && (connIndex < 3)) return -1; else return null; })
                        };
                    }
                });
            }
            cellMap[groupIndex] = oldNewCellMap;
        });
        // All cells are now in the cells array with their center-points calculated
        // Next pass needs to interlink all the cells using their new index in the cells array.
        // Start with the custom connectors... they are already linked up, but have special wall requirements
        if (floor.customConnectors.candidate1 || floor.customConnectors.candidate2)
            alert("NOTE:\n\nThere are un-joined custom connectors--these will not be exported until they are joined.");
        floor.customConnectors.joined.forEach(function (join) {
            // Validate that this is a legitimate place for a join...
            var fromGroup = model.groupById(join.from.group);
            var fromCell = fromGroup.cells[join.from.cell];
            var toGroup = model.groupById(join.to.group);
            var toCell = toGroup.cells[join.to.cell];
            if (fromCell.deleted || toCell.deleted) {
                alert("Failed to Export: the custom connector #" + join.id + " includes a deleted cell");
                throw new Error("Failed Export");
            }
            var fromMatrix = fromGroup.exportTransformStateAsMatrix();
            var toMatrix = toGroup.exportTransformStateAsMatrix();
            // Custom border walls: (mirrors same logic used to draw the custom connectors
            // Note: "getConnectorCoord" is polymorphic (implemented both on rects as in circle groups)
            var wallEdge = fromGroup.getConnectorCoord(fromMatrix, fromCell, join.from.connectorIndex, false);
            wallEdge = wallEdge.concat(toGroup.getConnectorCoord(toMatrix, toCell, join.to.connectorIndex, !join.flipped));
            // Line1 = x1,y1 (from), x2, y2 (to)
            var wallEdge2 = fromGroup.getConnectorCoord(fromMatrix, fromCell, join.from.connectorIndex, true);
            wallEdge2 = wallEdge2.concat(toGroup.getConnectorCoord(toMatrix, toCell, join.to.connectorIndex, join.flipped));
            // Line1 & 2 are border walls (permanent).
            borders.push(wallEdge);
            borders.push(wallEdge2);
            // The shared wall will bisect these two lines at their midpoint.
            var bisect = midPoint(wallEdge, [wallEdge[2], wallEdge[3]]);
            bisect = bisect.concat(midPoint(wallEdge2, [wallEdge2[2], wallEdge2[3]]));
            validateTwoWayConnectivityAndAddConnection(cellMap, cells, (walls.push(bisect) - 1), fromCell, join.from.connectorIndex, toCell, join.to.connectorIndex);
        });
        // Add the connectors for all the other cells in all the other groups into either the borders or walls list and link them up appropriately.
        floor.groups.forEach(function (group, groupIndex) {
            var groupTransform = group.exportTransformStateAsMatrix();
            group.cells.forEach(function (cellRef, cellIndex) {
                if (!cellRef.deleted) {
                    // Get cell info from cellmap...
                    var cMap = cellMap[groupIndex][cellIndex];
                    cMap.walls.forEach(function (wallInfo, sideIndex) {
                        if (wallInfo == null) { // For all non-processed edges... (allows for perf optimization--avoid re-processing cells)
                            // Build the wall--either a border (if deleted connector), or a shared wall...
                            var wallEdge = group.getWallGeometry(groupTransform, cellRef, sideIndex);
                            if (cellRef.connectors[sideIndex].deleted) { // It's a border
                                borders.push(wallEdge);
                                // If there is a connected cell (at all) sharing this border line and it is not deleted, then mark off the reflecting connector as processed already
                                // Ensures that only one border is created between two cells that share a border (but aren't connected).
                                markAndAddRecipricalConnector(cellMap, cells, -1, groupIndex, cellIndex, cellRef, sideIndex);
                            }
                            else // It's not a border--valid connection; hookup the cell on the other end of the connector to this one.
                                markAndAddRecipricalConnector(cellMap, cells, (walls.push(wallEdge) - 1), groupIndex, cellIndex, cellRef, sideIndex);
                        }
                    });
                }
            });
        });
        floorMap.push(cellMap); // Save this for a later pass...
        return {
            cells: cells,
            walls: walls,
            borders: borders
        };
    }

    function midPoint(p0, p1) {
        var mp = [];
        mp.push((p0[0] + p1[0]) / 2);
        mp.push((p0[1] + p1[1]) / 2);
        return mp;
    }

    function groupNameToIndex(x) { return model.groupMap[x].groupIndex; }

    function validateTwoWayConnectivityAndAddConnection(cellMap, cellsList, sharedWallIndex, cell1, cell1ConnectionIndex, cell2, cell2ConnectionIndex) {
        // cell1 --> cell2
        var cell1Connector = cell1.connectors[cell1ConnectionIndex];
        if (cell1Connector.deleted) {
            alert("Two-way connector validation: cell connector is linked, but marked as deleted; should not be processed as a connection");
            throw new Error("Connector Validation Failure");
        }
        if (cell2 != model.groupById( cell1Connector.group ).cells[ cell1Connector.cell ]) {
            alert("Two-way connector validation: cell which connects to group#" + cell1Connector.group + ",cell#" + cell1Connector.cell + " is not reciprocated; this will lead to invalid map data");
            throw new Error("Connector Validation Failure");
        }
        // cell2 --> cell1
        var cell2Connector = cell2.connectors[cell2ConnectionIndex];
        if (cell2Connector.deleted) {
            alert("Two-way connector validation: cell connector is linked, but marked as deleted; should not be processed as a connection");
            throw new Error("Connector Validation Failure");
        }
        if (cell1 != model.groupById(cell2Connector.group).cells[cell2Connector.cell]) {
            alert("Two-way connector validation: cell which connects to group#" + cell2Connector.group + ",cell#" + cell2Connector.cell + " is not reciprocated; this will lead to invalid map data");
            throw new Error("Connector Validation Failure");
        }
        var cell1CellMap = cellMap[groupNameToIndex(cell2Connector.group)][cell2Connector.cell];
        var cell2CellMap = cellMap[groupNameToIndex(cell1Connector.group)][cell1Connector.cell];
        // Make sure the connectors haven't already been processed...
        if ((cell1CellMap.walls[cell1ConnectionIndex] != null) || (cell2CellMap.walls[cell2ConnectionIndex] != null)) {
            alert("Two-way connector validation: Connectors already processed. Data corruption error.");
            throw new Error("Connector Validation Failure");
        }
        // Find the cell object in the cellMap, and 'check off' the connector index.
        cell1CellMap.walls[cell1ConnectionIndex] = sharedWallIndex;
        cell2CellMap.walls[cell2ConnectionIndex] = sharedWallIndex;
        // Find the cell object in the cells list and add the connection to cell1
        cellsList[ cell1CellMap.newIndex ].connectors.push( { cellIndex: cell2CellMap.newIndex , wallIndex: sharedWallIndex } );
        cellsList[ cell2CellMap.newIndex ].connectors.push( { cellIndex: cell1CellMap.newIndex , wallIndex: sharedWallIndex } );
    }

    function markAndAddRecipricalConnector(cellMap, cellsList, sharedWallIndex, cellRefGroupIndex, cellRefCellIndex, cellRef, cellRefConnectorIndex) {        
        var cellRefConnector = cellRef.connectors[cellRefConnectorIndex];
        var fromCellMap = cellMap[cellRefGroupIndex][cellRefCellIndex];
        // First, 'mark off' the wall on the origin cellRef in the map (regardless of whether the connector is present/valid.
        fromCellMap.walls[cellRefConnectorIndex] = sharedWallIndex;
        // If there is a connected cell sharing this wall edge and it is not deleted, then mark off the reflecting connector as processed already
        // Ensures that only one wall edge is created between two cells that share it (but aren't connected).
        if (cellRefConnector.cell >= 0) { // -1 is the connector flag that the cell has no connector (it's a border cell with no cells beyond)
            var toGroup = model.groupById(cellRefConnector.group);
            var toCell = toGroup.cells[cellRefConnector.cell];
            if (!toCell.deleted) {
                var toCellMap = cellMap[groupNameToIndex(cellRefConnector.group)][cellRefConnector.cell];
                // Find the connector from toCell back to cellRef...
                for (var i = 0; (i < toCell.connectors.length) && ((groupNameToIndex(toCell.connectors[i].group) != cellRefGroupIndex) || (toCell.connectors[i].cell != cellRefCellIndex)) ; i++);
                if (i == toCell.connectors.length) {
                    alert("Two-way connector validation: A connector back to the cellRef was not found! Data corruption error.");
                    throw new Error("Connector Validation Failure");
                }
                toCellMap.walls[i] = sharedWallIndex; // Avoid creating duplicate border walls where possible (and easily detectable thanks to linked border cells)
                if (sharedWallIndex >= 0) { // a.k.a. not a border wall
                    // Add the connector from toCell->cellRef in the new cell list
                    cellsList[ toCellMap.newIndex].connectors.push( { cellIndex: fromCellMap.newIndex , wallIndex: sharedWallIndex } );
                    // Now add the original cellRef->toCell connector in the new cell list
                    cellsList[ fromCellMap.newIndex].connectors.push( { cellIndex: toCellMap.newIndex , wallIndex: sharedWallIndex } );
                }
            }
        }
        else if (sharedWallIndex >= 0) { // The data model indicated that there was no connector (else clause), but the sharedWallIndex seems to indicate that there was a non-border here--oops!
            alert("Border edge validation: A connector is not available, yet a shared wall index was provided! Data corruption error.");
            throw new Error("Connector Validation Failure");
        }
    }

    function buildStartEndExportData(startList, endList) {
        model.floors.forEach(function (floor, floorIndex) {
            floor.groups.forEach(function (group, groupIndex) {
                group.cells.forEach(function (cell, cellIndex) {
                    if (cell.startCell)
                        startList.push({ floorIndex: floorIndex, cellIndex: floorMap[floorIndex][groupIndex][cellIndex].newIndex });
                    if (cell.endCell)
                        endList.push({ floorIndex: floorIndex, cellIndex: floorMap[floorIndex][groupIndex][cellIndex].newIndex });
                });
            });
        });
    }

    function buildCollectiblesExportData(collectibleList) {
        var typeMap = {
            "cheese":      0,
            "key":         1,
            "gate":        2,
            "trap":        3,
            "switch":      4,
            "fan":         5,
            "earthquake":  6,
            "dyn_light":   20,
            "dyn_firefly": 21,
            "dyn_snake":   22,
            "dyn_fog":     23
        };
        var arrangementMap = {
            "inline": 0,
            "close":  1,
            "medium": 2,
            "far":    3
        };
        var assertivenessMap = {
            "none":   0,
            "low":    1,
            "medium": 2,
            "high":   3
        };

        model.collectibles.forEach(function (coll) {
            collectibleList.push({
                type: typeMap[coll.typeEnum],
                arrangement: arrangementMap[coll.arrangementEnum],
                distance: coll.located, // 1-100
                size: coll.size,
                agility: assertivenessMap[coll.assertivenessEnum]
            });
        });
    }

    function buildTransportsExportData(transportList) {
        if (model.transportManager.groupList.length > 0)
            alert("NOTE:\n\nThere are un-joined transport groups--these will not be exported until they are joined.");
        model.transportManager.joinedList.forEach(function (joinedTransport) {
            var floorNumA = null;
            var floorNumB = null;
            var cellsA = [];
            var cellsB = [];
            Object.keys(joinedTransport.groupA.cellMap).forEach(function (groupId) {
                
                if (floorNumA == null) // First time
                    floorNumA = model.groupMap[groupId].floorIndex;
                else if (floorNumA != model.groupMap[groupId].floorIndex) {
                    alert("Transport Export Failure: groups from different floors found in transport group A");
                    throw new Error("Transport Export Error");
                }
                joinedTransport.groupA.cellMap[groupId].forEach(function (cellId) {
                    cellsA.push(floorMap[floorNumA][model.groupMap[groupId].groupIndex][cellId].newIndex);
                });
            });
            Object.keys(joinedTransport.groupB.cellMap).forEach(function (groupId) {
                if (floorNumB == null) // First time
                    floorNumB = model.groupMap[groupId].floorIndex;
                else if (floorNumB != model.groupMap[groupId].floorIndex) {
                    alert("Transport Export Failure: groups from different floors found in transport group B");
                    throw new Error("Transport Export Error");
                }
                joinedTransport.groupB.cellMap[groupId].forEach(function (cellId) {
                    cellsB.push(floorMap[floorNumB][model.groupMap[groupId].groupIndex][cellId].newIndex);
                });
            });
            transportList.push({
                select: joinedTransport.cellSetCount,
                type: joinedTransport.type,
                mirror: joinedTransport.mirror,
                floorLink: joinedTransport.floorRelationship,
                groupAFloorIndex: floorNumA,
                groupBFloorIndex: floorNumB,
                groupA: cellsA,
                groupB: cellsB
            });
        });    
    }

    // Binary format definition
    var binaryFormatV1 = {
        keyOrder: ["version", "width", "height", "floors", "startSet", "endSet", "collectibles", "transports"],
        // Format version (1 == this version, the only current version)
        version: "uint8",
        // Maze dimensions
        width: "uint16", // Size (in "units")
        height: "uint16",
        // Floors
        floors: [{
            keyOrder: ["cells", "walls", "borders"],
            // Each floor has a cell list...
            cells: [{
                keyOrder: ["x", "y", "connectors"],
                // Global center coordinate point (x,y)
                x: "float32",
                y: "float32",
                // Cell has a list of connectors
                connectors: [{
                    keyOrder: ["cellIndex", "wallIndex"],
                    // index into this floor's cell list (this list) for the related connected cell
                    cellIndex: "uint16",
                    // index into the shared wall segment list (no borders, since cannot connect to a border--there's no cell)
                    wallIndex: "uint16",
                }]
            }],
            // Wall segment (general)
            walls: [["float32"], // If "line" type, then length of inner array is 4 floats: start x,y -> end x,y (absolute values), if "arc" then 8 floats (bezier) start x,y, control points1 & 2 and end point.
                "wallList"],
            // Path of external wall segments (includes custom bridgewalls)
            borders: "wallList",
        }],
        //  Start set (possible starting cell locations)
        startSet: [{
            keyOrder: ["floorIndex", "cellIndex"],
            // floor index of this start cell option
            floorIndex: "uint8",
            // Cell index of the start cell
            cellIndex: "uint16"
        }, "floorAndCellIndex"],
        // End set
        endSet: "floorAndCellIndex",
        //  Collectibles
        collectibles: [{
            keyOrder: ["type", "arrangement", "distance", "size", "agility"],
            // the collectible type {cheese,key,gate,trap,switch,fan,earthquake,dyn_light,dyn_firefly,dyn_snake,dyn_fog}
            type: "uint8",
            // arrangement  0)inline,1)close,2)medium,3)far - how close to the solution path should the collectible reside...
            arrangement: "uint8",
            // How far along the solution path this collectible should be placed (1-100)
            distance: "uint8",
            // Number of cells this should extend to
            size: "uint8",
            // agility: 0)none,1)low,2)medium,3)high - how agile/fast will the dynamic thing moves...
            agility: "uint8"
        }],
        // Transport groups
        transports: [{
            keyOrder: ["select", "type", "mirror", "floorLink", "groupAFloorIndex", "groupBFloorIndex", "groupA", "groupB"],
            // Number of cells to join from the groups
            select: "uint16",
            // type  1 = Stairs, 2= Portal
            type: "uint8",
            // mirror (For floorRelationship == 2 or 3, true if the final selected transports will be correspond to the same relative cell position between floors (like actual stairs/elevators))
            mirror: "bool",
            // floorRelationship 1 = same floor, 2 = adjacent floors, 3 = disparate floors (separate and not adjacent)
            floorLink: "uint8",
            // GROUP A's floor index
            groupAFloorIndex: "uint8",
            // GROUP B's floor index
            groupBFloorIndex: "uint8",
            // GROUP A's participating cell indexes
            groupA: ["uint16"],
            // GROUP B's participating cell indexes
            groupB: ["uint16"]
        }]
    };

});