// |a|c|e|
// |b|d|f|
// |0|0|1|
function Matrix32(a,b,c,d,e,f) {
   this.a = a ? a : 1;
   this.b = b ? b : 0;
   this.c = c ? c : 0;
   this.d = d ? d : 1;
   this.e = e ? e : 0;
   this.f = f ? f : 0;
}
Matrix32.prototype.multiply = function () {
   if (arguments.length == 0)
      throw new TypeError("Please provide a matrix to multiply");
   if (arguments.length == 1) {
      // This is a matrix or array multiplication
      if (Array.isArray(arguments[0])) {
         if ((arguments[0].length < 2) || (arguments[0].length > 3))
            throw new TypeError("Can't multiply an array of that length");
         if (arguments[0].length == 2)
            return [
               (this.a * arguments[0][0]) + (this.c * arguments[0][1]) + this.e,
               (this.b * arguments[0][0]) + (this.d * arguments[0][1]) + this.f
            ];
         else
            return [
               (this.a * arguments[0][0]) + (this.c * arguments[0][1]) + (this.e * arguments[0][2]),
               (this.b * arguments[0][0]) + (this.d * arguments[0][1]) + (this.f * arguments[0][2]),
               arguments[0][2]
            ];
      }
      if (arguments[0] instanceof Matrix32) {
         return new Matrix32(
            (this.a * arguments[0].a) + (this.c * arguments[0].b), // a
            (this.b * arguments[0].a) + (this.d * arguments[0].b), // b
            0,
            (this.a * arguments[0].c) + (this.c * arguments[0].d), // c
            (this.b * arguments[0].c) + (this.d * arguments[0].d), // d
            0,
            (this.a * arguments[0].e) + (this.c * arguments[0].f) + this.e, // e
            (this.b * arguments[0].e) + (this.d * arguments[0].f) + this.f, // f
            1
         );
      }
   }
}
Matrix32.prototype.selfScale = function (scaleX, scaleY) {
   // a c e   x 0 0    ax cy e
   // b d f   0 y 0  = bx dy f
   // 0 0 1   0 0 1    0  0  1
   this.a *= scaleX;
   this.b *= scaleX;
   this.c *= scaleY;
   this.d *= scaleY;
}
Matrix32.prototype.selfRotate = function (radians) {
   // a c e    c() -s() 0    ac()+cs() -as()+cc() e
   // b d f    s() c()  0  = bc()+ds() -bs()+dc() f
   // 0 0 1    0   0    1    0         0          1
   var storedA = this.a;
   var storedB = this.b;
   var cos = Math.cos(radians);
   var sin = Math.sin(radians);
   this.a = (this.a * cos) + (this.c * sin);
   this.b = (this.b * cos) + (this.d * sin);
   this.c = (-storedA * sin) + (this.c * cos);
   this.d = (-storedB * sin) + (this.d * cos);
}
Matrix32.prototype.selfTranslate = function (transX, transY) {
   // a c e  1 0 n   a c an+cm+e
   // b d f  0 1 m = b d bn+dm+f
   // 0 0 1  0 0 1   0 0 1
   this.e = (this.a * transX) + (this.c * transY) + this.e;
   this.f = (this.b * transX) + (this.d * transY) + this.f;
}
Matrix32.prototype.toString = function () {
   var a = String(this.a);
   var b = String(this.b);
   var c = String(this.c);
   var d = String(this.d);
   var e = String(this.e);
   var f = String(this.f);
   var maxCol1 = Math.max(a.length,b.length);
   var maxCol2 = Math.max(c.length,d.length);
   var maxCol3 = Math.max(e.length,f.length);
   var row1 = "|" + a;
   if (a.length < maxCol1 ) {
      for (var i = 0; i < (maxCol1 - a.length); i++)
         row1 += " ";
   }
   row1 += "|" + c;
   if (c.length < maxCol2 ) {
      for (var i = 0; i < (maxCol2 - c.length); i++)
         row1 += " ";
   }
   row1 += "|" + e;
   if (e.length < maxCol3 ) {
      for (var i = 0; i < (maxCol3 - e.length); i++)
         row1 += " ";
   }
   row1 += "|";
   var row2 = "|" + b;
   if (b.length < maxCol1 ) {
      for (var i = 0; i < (maxCol1 - b.length); i++)
         row2 += " ";
   }
   row2 += "|" + d;
   if (d.length < maxCol2 ) {
      for (var i = 0; i < (maxCol2 - d.length); i++)
         row2 += " ";
   }
   row2 += "|" + f;
   if (f.length < maxCol3 ) {
      for (var i = 0; i < (maxCol3 - f.length); i++)
         row2 += " ";
   }
   row2 += "|";
   var row3 = "|0";
   if (1 < maxCol1 ) {
      for (var i = 0; i < (maxCol1 - 1); i++)
         row3 += " ";
   }
   row3 += "|0";
   if (1 < maxCol2 ) {
      for (var i = 0; i < (maxCol2 - 1); i++)
         row3 += " ";
   }
   row3 += "|1";
   if (1 < maxCol3 ) {
      for (var i = 0; i < (maxCol3 - 1); i++)
         row3 += " ";
   }
   row3 += "|";
   return row1 + "\n" + row2 + "\n" + row3;
}
