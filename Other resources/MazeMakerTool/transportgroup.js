﻿"use strict";
addEventListener("DOMContentLoaded", function TransportScope() {
    // Groups do not actual draw anything themselves--they are abstract for the UI only

    // [Quazi-globals (accessor property state)]    
    var visibilityControllerDivEl = document.getElementById("knobsForTransportGroups");
    var combinedListSelectEl = document.getElementById("tranportSelect");
    var toggleTransportCellMembershipButtonEl = document.getElementById("toggleTransportToGroup");
    var numberTransportsInJointGroupInputEl = document.getElementById("numTransports");
    var maxTransportsInJointGroupSpanEl = document.getElementById("maxTransports");
    var transportTypeSelectEl = document.getElementById("transportType");
    var mirrorStateCheckboxEl = document.getElementById("useMirroring");

    var activeIndex = null; // OK in this scope because there is ever only one instance of the manager...

    // Returns an empty array if valid. If not valid, returns and array where the indexes are:
    // -1 if the trial groups [optionally provided] failed validation
    // 0-n index of the provided joinedCellLists that failed validation.
    function validateAllJoinedCellsWithMultiMembership(joinedCellList, /*optional*/trialGroups) {
        var participatingTransGroupIdMap = {};
        var results = [];
        // Add the groups from pre-existing joined transports to the participating list...
        joinedCellList.forEach(function (joined) {
            participatingTransGroupIdMap[joined.groupA.id] = joined.groupA.cellCount; // Stash the total amount of cells participating in the group (currently)
            participatingTransGroupIdMap[joined.groupB.id] = joined.groupB.cellCount; // as the value of the groupId's map.
        });
        if (trialGroups) {
            // Add these and run the first tests against these groups...
            trialGroups.forEach(function (group) { participatingTransGroupIdMap[group.id] = group.cellCount; });
            // First tests - first one to fail stops testing the trial group.
            if (!trialGroups.every(function (group) { return group.validateJoinPrereq(participatingTransGroupIdMap); })) {
                results.push(-1); // Well, it didn't validate, so pull it out of the participating list for the next set...
                trialGroups.forEach(function (group) { delete participatingTransGroupIdMap[group.id]; });
            }
        }
        for (var i = 0, len = joinedCellList.length; i < len; i++) {
            if (!joinedCellList[i].groupA.validateJoinPrereq(participatingTransGroupIdMap) ||
                !joinedCellList[i].groupB.validateJoinPrereq(participatingTransGroupIdMap)) {
                // One of the sub-groups in this joined group failed..
                results.push(i);
                // Remove these groups from the participating lists and continue...(to find all validation failure points in one go...
                delete participatingTransGroupIdMap[joinedCellList[i].groupA.id];
                delete participatingTransGroupIdMap[joinedCellList[i].groupB.id];
            }
        }
        return results;
    }

    var TransportManagerFunctions = {
        // Validation MUST occur when any of the following things happen:
        // 1. Any floor is added or deleted from the list of floors
        // 2. Any group is deleted from a floor's list of groups (adds are ok)
        // 3. Any group is re-structured (cells re-arranged as a result of adding/removing cells)
        validateAndUpdateUI: function () { // One consolidated place for validating the state of the transport manager
            // Assumes nothing about the state of the transport manager
            this.unselect(); // Ensure that nothing is selected before validating...
            activeIndex = null;
            var i = 0;
            var originalGroupListLength = this.groupList.length; // Save this to avoid double-validating the groups potentially transferred from the joinedList...
            while (i < this.joinedList.length) {
                var resCode = this.joinedList[i].validate(); // -1=rem groupA, -2=rem groupB, -3=rem both, -4=split only, 1=valid
                if ((resCode == -1) || (resCode == -4)) // Keep groupB
                    this.groupList.push(this.joinedList[i].groupB);
                if ((resCode == -2) || (resCode == -4)) // Keep groupA
                    this.groupList.push(this.joinedList[i].groupA);
                if (resCode < 0) // Drop the joint transport completely...
                    this.joinedList.splice(i, 1); // Out it goes!
                else // otherwise, it's still valid (though it may have been tweaked a bit.
                    i++;
            }
            // Of those that are left, run a check on cell multi-group inclusions
            var failedIndexes = validateAllJoinedCellsWithMultiMembership(this.joinedList);
            i = failedIndexes.length;
            while (i > 0) { // Reverse-iterate (since I'm removing things from the array based on static indexes)
                i--; // Now pointing at the index into the failedIndexed array
                this.groupList.push(this.joinedList[i].groupA);
                this.groupList.push(this.joinedList[i].groupB);
                // Drop the joined group...
                this.joinedList.splice(i, 1);
            }
            // validate the rest of the transport groups (not just validated above)
            for (; originalGroupListLength > 0; originalGroupListLength--) {
                var group = this.groupList.shift(); // Pull off the front...
                if (group.validate()) // If it doesn't validate, it doesn't get re-added.
                    this.groupList.push(group);
            }
            this.updateUI();
        },
        // Ensures that the floor data structures are properly sorted while re-creating the UI for the select menu at the same time. (Does not re-draw the floor)
        updateUI: function () {
            combinedListSelectEl.innerHTML = "";
            // Since this will change the ordering of the virtual list, need to first unselect anything selected from the current list before modifying it
            this.unselect();
            if (this.groupList.length > 0) {
                // Rebuild the Listbox and the (first the groups list)
                var tailArray = [];
                var currentFloorId = model.floor.id;
                var groupListLen = this.groupList.length;
                var newIndex = activeIndex;
                var computedIndex = null;
                var optGroupThisFloor = document.createElement("optgroup");
                var optGroupOtherFloor = document.createElement("optgroup");
                optGroupThisFloor.label = "Open Groups (this floor)";
                optGroupOtherFloor.label = "Groups (other floors)";
                for (var i = 0; i < groupListLen; i++) { // I need to use 'i' later on...
                    var transGroup = this.groupList[i];
                    var opt = new Option(transGroup.name, transGroup.id, false, activeIndex == i);
                    if (currentFloorId == this.groupList[i].floorId) {
                        if (i == activeIndex) // I can already compute with certainty what the new index will be...
                            newIndex = this.groupList.length - groupListLen; // (since groupList.length hasn't recieved the new item yet, this is an index)
                        this.groupList.push(transGroup); // Add dup to the end...(will remove original later)
                        optGroupThisFloor.appendChild(opt);
                    }
                    else {
                        if (i == activeIndex)
                            computedIndex = tailArray.length;
                        tailArray.push(transGroup);
                        optGroupOtherFloor.appendChild(opt);
                    }
                }
                if (computedIndex != null)
                    activeIndex = (this.groupList.length - groupListLen) + computedIndex;
                else
                    activeIndex = newIndex;
                // Add the tail to the end of the groupList with dups...
                tailArray.forEach(function (item) { this.groupList.push(item); }, this);
                // Rip out the first half of the array (the old sorting), leaving the new sorting in place with the right length
                this.groupList.splice(0, groupListLen);
                // Insert the optGroup elements if necessary
                if (optGroupThisFloor.hasChildNodes())
                    combinedListSelectEl.appendChild(optGroupThisFloor);
                if (optGroupOtherFloor.hasChildNodes())
                    combinedListSelectEl.appendChild(optGroupOtherFloor);
            }
            // Then the joined transport groups, if any
            if (this.joinedList.length > 0) {
                var optGroup = document.createElement("optgroup");
                optGroup.label = "Joined Groups";
                this.joinedList.forEach(function (joinedGroup) {
                    optGroup.appendChild(new Option(joinedGroup.name, joinedGroup.id, false, activeIndex == i));
                    i++; // Note: i is the virtual list index of the combined lists
                });
                combinedListSelectEl.appendChild(optGroup);
            }
            // Update relevant further selected UI...
            if (activeIndex == null)
                visibilityControllerDivEl.removeAttribute("data-show");
            else {
                // Send the select and updateUI command to the appropriate selected object
                if (activeIndex < this.groupList.length) {
                    this.groupList[activeIndex].select();
                    this.groupList[activeIndex].updateUI();
                }
                else {
                    this.joinedList[activeIndex - this.groupList.length].select();
                    this.joinedList[activeIndex - this.groupList.length].updateUI();
                }
            }
        },
        unselect: function () {
            if (activeIndex != null) {
                // De-select the current index's selected thing
                if (activeIndex < this.groupList.length)
                    this.groupList[activeIndex].unselect();
                else
                    this.joinedList[activeIndex - this.groupList.length].unselect();
            }
        },
        // Forces (without validation) the activeIndex to the specified position (or null), selects that index, updates the UI, and redraws the floor.
        select: function (unconditionalNewIndex) {    
            activeIndex = unconditionalNewIndex;
            // Select that item
            if (activeIndex != null) {
                // Select the current group
                if (activeIndex < this.groupList.length)
                    this.groupList[activeIndex].select();
                else
                    this.joinedList[activeIndex - this.groupList.length].select();
            }
            this.updateUI();
            // Re-draw the whole floor.
            model.floor.redraw();
        },
        validateFloorDelete: function(deletedFloorId) {
            // Review the joined list first (to unjoin any groups containing the target floor)
            var i = 0;
            var dirty = false; // Tracks whether there will be changes that require a UI update
            while (i < this.joinedList.length) {
                var joinedTransport = this.joinedList[i];
                if ((joinedTransport.groupA.floorId == deletedFloorId) || (joinedTransport.groupB.floorId == deletedFloorId)) { // Split its groups out (one or both may be deleted later)
                    // Well, I need to make an update!! If I haven't already done so, unselect now while the state of the old list is preserved
                    if (!dirty)
                        this.unselect();
                    this.groupList.push(joinedTransport.groupA);
                    this.groupList.push(joinedTransport.groupB);
                    this.joinedList.splice(i, 1);
                    dirty = true;
                    // Don't increment i
                } 
                else
                    i++;
            }
            // Now review the groupList
            i = 0;
            while (i < this.groupList.length) {
                var transGroup = this.groupList[i];
                if (transGroup.floorId == deletedFloorId) {
                    if (!dirty)
                        this.unselect();
                    this.groupList.splice(i, 1);
                    dirty = true;
                }
                else
                    i++;
            }
            if (dirty)
                this.select(null);
        },
        addGroup: function () {
            this.unselect();
            this.groupList.unshift(new TransportGroup()); // Add to the top of the list (so that it's sure to be grouped with the top items of the current floor)
            this.select(0);
        },
        removeListBoxItem: function () {
            if (activeIndex != null) {
                // Something's going to change in the list...
                this.unselect();
                if (activeIndex < this.groupList.length) {
                    // First, clear the group's cells
                    this.groupList[activeIndex].clearAll();
                    // Remove it, and try to select another group
                    this.groupList.splice(activeIndex, 1); // Removes it completely
                    if (this.groupList.length == 0)
                        this.select(null);
                    else
                        this.select(0); // Select the top of the list (easiest to find)
                }
                else {
                    var joinedIndex = activeIndex - this.groupList.length;
                    // First, clear the group's cells
                    this.joinedList[joinedIndex].clearAll();
                    // Remove it from the joined list
                    this.joinedList.splice(joinedIndex, 1);
                    if (this.joinedList.length == 0)
                        this.select(null);
                    else
                        this.select(this.groupList.length); // Which is also the first element in the joinedList array
                }
            }
        },
        toggleCellMembership: function () {
            if ((activeIndex != null) && (activeIndex < this.groupList.length) && (this.groupList[activeIndex].floorId == model.floor.id))
                this.groupList[activeIndex].addOrRemoveCurrentCell();
        },
        join: function () {
            // HTML5 select element DOM additions not yet supported, so this is more complicated that it should be
            // Collect all [multiple] selected items (if any)
            var selectedIndicies = [];
            for (var i = 0, len = this.groupList.length; i < len; i++) { // Only review any options that are in the groupList range (not any joined options)
                if ((combinedListSelectEl.options[i].selected) && (this.groupList[i].cellCount > 0)) // Must have at least one cell in the group for this to make sense
                    selectedIndicies.push(i);
            }
            if (selectedIndicies.length == 0)
                return; // Don't bother interrupting the user in this simple case of exploration.
            if (selectedIndicies.length != 2)
                return alert("Please select two groups both of which MUST have at least one cell in the group"); // Nothing to do.
            if (validateAllJoinedCellsWithMultiMembership(this.joinedList, [this.groupList[selectedIndicies[0]],this.groupList[selectedIndicies[1]]]).length > 0)
                return alert("All cells (in both groups), with multi-group membership, require each joined (or potentially-joined) member group to have equal or greater number of cells as the number of groups to which the cell belongs."); 
            this.unselect();
            // Remove them from the group list (last one first so that the indexes stored are still preserved)
            var groupB = this.groupList.splice(selectedIndicies[1], 1)[0]; // Splice returns the removed item wrapped in an array
            var groupA = this.groupList.splice(selectedIndicies[0], 1)[0]; // Splice returns the removed item wrapped in an array
            this.joinedList.unshift(new JoinedTransports(groupA, groupB)); // Added to the list!
            // Set the activeIndex to point to this new item (causing it to update the UI)
            this.select(this.groupList.length);
        },
        unjoin: function () {
            if ((activeIndex == null) || (activeIndex < this.groupList.length))
                return; // Nothing or a group selected
            this.unselect();
            // Rip out the object from the joined list...
            var joinedOb = this.joinedList.splice(activeIndex - this.groupList.length, 1)[0]; // Splice returns the removed item wrapped in an array
            this.groupList.push(joinedOb.groupA);
            this.groupList.push(joinedOb.groupB);
            this.select(null);
        }
    };

    // TransportManager(optional serializedTransportManager) {
    //    <<PUBLIC>> -- serialized stuff
    //    groupList: []             // List of TransportGroup objects
    //    joinedList: []            // List of JoinedTransports
    //    activeIndex: # or null    // Index into a virtual combined list (the active thing, since multiple can be selected)
    //    <<private>>
    //    joined: {} or null        // Returns the JoinedTransports object if it's selected by the activeIndex, null otherwise
    //    group: {} or null         // Returns the TransportGroup object if it's selected by the activeIndex, null otherwise
    // }

    Object.defineProperty(window, "TransportManager", {
        enumerable: true,
        value: function TransportManagerConstructor(serializedTransportManager) {
            // Initialize accessors here
            activeIndex = serializedTransportManager ? serializedTransportManager.activeIndex : null;
            return Object.create(TransportManagerFunctions, {
                joinedList: { // These are listed second in the UI
                    enumerable: true,
                    value: serializedTransportManager ? serializedTransportManager.joinedList.map(function (serializedJoinedTransports) {
                        return new JoinedTransports(serializedJoinedTransports);
                    }) : []
                },
                groupList: { // These are listed first in the UI
                    enumerable: true,
                    value: serializedTransportManager ? serializedTransportManager.groupList.map(function (serializedTransportGroup) {
                        return new TransportGroup(serializedTransportGroup);
                    }) : []
                },
                activeIndex: {
                    enumerable: true,
                    get: function () { return activeIndex; },
                    set: function (x) {
                        // This method of setting the activeIndex enforces validation.
                        var length = this.joinedList.length + this.groupList.length;
                        if (x != null) {
                            x = Math.max(0, Math.min(x, length - 1));
                            if (x >= length) return; // Not a valid index
                        }
                        if (x == activeIndex) return; // Redundant set
                        this.unselect();
                        this.select(x);
                    }
                },
                // private
                joined: {
                    get: function() { 
                        if ((activeIndex != null) && (activeIndex >= this.groupList.length))
                            return this.joinedList[activeIndex - this.groupList.length];
                        else
                            return null;
                    }
                },
                group: {
                    get: function() {
                        if ((activeIndex != null) && (activeIndex < this.groupList.length))
                            return this.groupList[activeIndex];
                        else
                            return null;
                    }
                }
            });
        }
    });

    // -----

    function calculateFloorRelationship(f1Id, f2Id) { // TODO: this must be re-evaluated when floors change...
        if (f1Id == f2Id)
            return 1; // Same floors
        // Different floors--how different?
        var otherFloorIndex = -1;
        for (var i = 0, len = model.floors.length; i < len; i++) {
            var currentFloorId = model.floors[i].id;
            if ((currentFloorId == f1Id) || (currentFloorId == f2Id)) {
                if (otherFloorIndex == -1) // First find
                    otherFloorIndex = i;
                else
                    return (Math.abs(otherFloorIndex - i) == 1) ? 2 : 3;
            }
        }
    }

    var JoinedTransportsFunctions = {
        // return codes from joint transport validation
        // * -1 - groupA is not valid--remove it (completely) and split up the joint transport
        // * -2 - groupB is not valid--remove it (completely) and split up the joint transport
        // * -3 - both groups are invalid remove them both and split up the joint transport
        // * -4 - split up the joint transport
        // * 1 - validated (though adjustments may have been made to the joint transport data)
        validate: function () {
            // Validate A, then validate B.
            // Finally evalutate the assumptions required to keep these groups joined
            // If any of the assumptions fail, then return failure code indicating that this JointTransport should be broken apart.
            var failureCode = -3; // Start out assuming everything's invalid...
            if (this.groupB.validate())
                failureCode = -1; // I'm good, but A is probably bad...
            if (this.groupA.validate())
                failureCode++; // Either -3 -> -2 or -1 -> 0
            if (failureCode < 0)
                return failureCode; // This joint group isn't valid.
            // Minimum cell count requirement
            var recalcedMaxCells = Math.min(this.groupA.cellCount, this.groupB.cellCount);
            if (recalcedMaxCells < 1)
                return -4;
            // From here on out, the joint transport will remain intact, but its values may be adjusted...
            // Cell and max-cell re-validation...
            if (recalcedMaxCells != this.cellsMax) {
                this.cellsMax = recalcedMaxCells;
                if (this.cellSetCount > recalcedMaxCells)
                    this.cellSetCount = recalcedMaxCells; // Causes a UI update even if this isn't in the current view :( Ah well.
            }
            // Floor relationship revalidation...
            var recalcedFloorRelationship = calculateFloorRelationship(this.groupA.floorId, this.groupB.floorId);
            if (recalcedFloorRelationship != this.floorRelationship) {
                this.floorRelationship = recalcedFloorRelationship;
                if ((this.type == 1) && (this.floorRelationship != 2))
                    this.type = 2; // Switch back to portal // Causes a UI update even if this isn't in the current view :( Ah well.
            }
            // Mirror relationship revalidation...
            if (this.mirror && ((this.floorRelationship == 1) || !this.groupA.mirrorabilityCheck(this.groupB)))
                this.mirror = false; // Causes a UI update even if this isn't in the current view :( Ah well.
            return 1;
        },
        updateUI: function () {
            // Ensure my options are visible...
            visibilityControllerDivEl.setAttribute("data-show", "joined");
            numberTransportsInJointGroupInputEl.value = this.cellSetCount;
            maxTransportsInJointGroupSpanEl.innerHTML = this.cellsMax;
            transportTypeSelectEl.value = this.type;
            mirrorStateCheckboxEl.checked = this.mirror;
        },
        unselect: function () {
            this.groupA.unselect();
            this.groupB.unselect();
        },
        select: function () {
            this.groupA.select();
            this.groupB.select();
        },
        clearAll: function () {
            this.groupA.clearAll();
            this.groupB.clearAll();
        }
    };

    // JoinedTransports(serializedJoinedTransportsOrGroupA, optional groupB) {
    //    <<PUBLIC>> -- serialized stuff
    //    id: ""                // Unique Joined number (mixed from both groups)
    //    groupA: {}            // Transport Group A
    //    groupB: {}            // Transport Group B (moved out of the list)
    //    cellSetCount: #       // Number of actual cells to join
    //    cellsMax: #           // Derived from the min(groupA and B cell count)
    //    type: #               // 1 = Stairs, 2= Portal
    //    mirror: bool          // For floorRelationship == 2 or 3, true if the final selected transports will be correspond to the same relative cell position between floors (like actual stairs/elevators)
    //    floorRelationship: #  // 1 = same floor, 2 = adjacent floors, 3 = disparate floors (separate and not adjacent)
    //    <<PRIVATE>>
    //    name: ""              // Generated display name
    // }

    Object.defineProperty(window, "JoinedTransports", {
        enumerable: true,
        value: function JoinedTransportsConstructor(serializedJoinedTransportsOrGroupA, groupB) {
            var serializedJoinedTransport = groupB ? null : serializedJoinedTransportsOrGroupA;
            // Setup accessors
            var cellSetCount = serializedJoinedTransport ? serializedJoinedTransport.cellSetCount : 1; // Min-requirement for entry into the group.
            var type = serializedJoinedTransport ? serializedJoinedTransport.type : 2; // Portal-types require little-to-no validation requirements
            var mirror = serializedJoinedTransport ? serializedJoinedTransport.mirror : false;
            return Object.create(JoinedTransportsFunctions, {
                id: {
                    enumerable: true,
                    value: serializedJoinedTransport ? serializedJoinedTransport.id : (serializedJoinedTransportsOrGroupA.id + "|" + groupB.id)
                },
                groupA: {
                    enumerable: true,
                    value: serializedJoinedTransport ? new TransportGroup(serializedJoinedTransportsOrGroupA.groupA) : serializedJoinedTransportsOrGroupA
                },
                groupB: {
                    enumerable: true,
                    value: serializedJoinedTransport ? new TransportGroup(serializedJoinedTransportsOrGroupA.groupB) : groupB
                },
                cellSetCount: {
                    enumerable: true,
                    get: function () { return cellSetCount; },
                    set: function (x) {
                        x = Math.max(1, Math.min(x, this.cellsMax));
                        if (x == cellSetCount) return;
                        cellSetCount = x;
                        numberTransportsInJointGroupInputEl.value = cellSetCount;
                    }
                },
                cellsMax: {
                    enumerable: true,
                    writable: true,
                    value: serializedJoinedTransport ? serializedJoinedTransport.cellsMax : Math.min(serializedJoinedTransportsOrGroupA.cellCount, groupB.cellCount)
                },
                type: {
                    enumerable: true,
                    get: function () { return type; },
                    set: function (x) {
                        x = Math.max(1, Math.min(2, x));
                        if (x == type) return;
                        if ((x == 1) && (this.floorRelationship == 2)) // Adjacency
                            type = x;
                        else
                            type = 2;
                        transportTypeSelectEl.value = type;
                    }
                },
                mirror: {
                    enumerable: true,
                    get: function () { return mirror; },
                    set: function (x) {
                        if (x == mirror) return;
                        if (x && (this.floorRelationship != 1) && this.groupA.mirrorabilityCheck(this.groupB))
                            mirror = true;
                        else
                            mirror = false;
                        mirrorStateCheckboxEl.checked = mirror;
                    }
                },
                floorRelationship: { // 1 = same floor, 2 = adjacent floors, 3 = disparate floors (separate and not adjacent)
                    enumerable: true,
                    writable: true,
                    value: serializedJoinedTransport ? serializedJoinedTransport.floorRelationship : calculateFloorRelationship(serializedJoinedTransportsOrGroupA.floorId, groupB.floorId)
                },
                // private
                name: {
                    get: function () {
                        return this.id + ((this.type == 1) ? " (Stairs) " : " (Portal) ") + this.cellSetCount + "/" + this.cellsMax;
                    }
                }
            });
        }
    });
    
    // -----

    // Returns true if the iteration was uninterrupted, false otherwise.
    function forEachCellInMap(cellMap, callback) {
        var cellGroupIds = Object.keys(cellMap);
        for (var i = 0, len = cellGroupIds.length; i < len; i++) {
            var cellGroupOb = model.groupById(cellGroupIds[i]);
            if (!cellGroupOb)
                continue; // Oops this group was deleted (and validation hasn't started yet), just skip it.
            var cellIdList = cellMap[cellGroupIds[i]];
            for (var j = 0, jLen = cellIdList.length; j < jLen; j++) {
                if (!cellGroupOb.cells[cellIdList[j]])
                    continue; // Hit a cell that was removed (and validation hasn't started yet), skip it (no harm done)
                if (callback(cellGroupOb.cells[cellIdList[j]]) === false) // If any callback returns false, this iterator fails immediately with the same return code
                    return false;
            }
        }
        return true;
    }

    var TransportGroupFunctions = {
        // Return codes:
        // * false - The group is no longer valid (group's floor is gone) signal to auto-remove this group
        // * true - The group is still valid, though one or more cells in the group may have been removed (because the cell's containing group is gone, the cell is gone, or does not back-reference this group)
        validate: function () {
            // First--does the floor associated with this transport group still exist?
            // linear search of floors (called for each group) is O(n^2), but there's no floor map, and the number of floors is expected to be relatively small
            if (!model.floors.some(function (floor) { return floor.id == this.floorId; }, this))
                return false; // Critical validation failure
            // Everything else is recoverable
            // Found the floor in the array of floors... Check that all the groups are still valid...
            var groupIdList = Object.keys(this.cellMap);
            var groupObjectList = groupIdList.map(function (groupId) { return model.groupById(groupId); }); // may return null if the group can't be found...
            groupObjectList.forEach(function (group, i) {
                var transportGroupCellList = this.cellMap[groupIdList[i]];
                if (!group) { // The group to which the cell(s) belonged has been deleted. Cleanup the cellMap and return......
                    this.cellCount -= transportGroupCellList.length; // Reduce the group's cell count by the number of items about to be removed...
                    delete this.cellMap[groupIdList[i]];
                    return;
                }
                // The group exists; ensure each cell referenced exists...
                var groupCellLength = group.cells.length;
                var filteredList = transportGroupCellList.filter(function (cellIndex) {
                    if (cellIndex >= groupCellLength) // Ooops, it was deleted...
                        return false; // Drop this cell's index...
                    // it's in the list, but does it still belong to this transport group? Skip this if not, include it if so...
                    return group.cells[cellIndex].hasTransportGroupMembership(this.id); // Pass this [the transport group]'s id
                }, this);
                if (filteredList.length != transportGroupCellList.length) {
                    this.cellCount -= (transportGroupCellList.length - filteredList.length);
                    // Update the index of cells associated with this mapped group
                    if (filteredList.length == 0) // Nothing in the group remains...
                        delete this.cellMap[groupIdList[i]];
                    else
                        this.cellMap[groupIdList[i]] = filteredList; // Update the list with what remains after validation
                }
            }, this);
            // Everything's up-to-date.
            return true; // Validation successful
        },
        // Returns true if the pre-reqs are satisfied, false otherwise...
        validateJoinPrereq: function (relevantGroupsMap) { // This map has all the groups that are currently joined and the number of cells in each group as the key's value
            return forEachCellInMap(this.cellMap, function (cell) {
                var cellsTransGroupIds = Object.keys(cell.transGroup);
                var groupCount = cellsTransGroupIds.length;
                if (groupCount > 1) { // Only applicable if the cell has more than one group membership...
                    // All relevant transport groups (total combined overlapping on this cell) must have the min cell count for the number of groups to which this cell belongs
                    // This ensures that fallback transport cell selection will never be left without a valid choice (less strict than disallowing multiple 
                    // group membership for a single cell at all!)
                    for (var i = 0, len = cellsTransGroupIds.length; i < len; i++) {
                        // If it's relevant (a joined group, not just any group) and the cell count does not meet the min requirements, then... 
                        if (relevantGroupsMap[cellsTransGroupIds[i]] && (relevantGroupsMap[cellsTransGroupIds[i]] < groupCount))
                            return false;
                    }
                    return true; // Good!
                }
            });
        },
        updateUI: function () {
            // Ensure my option (toggle cell add/remove) is only visible when its associated floor is active
            if (this.floorId == model.floor.id)
                visibilityControllerDivEl.setAttribute("data-show", "group");
            else
                visibilityControllerDivEl.removeAttribute("data-show");
            // Ensure the button is up to date...
            if (model.cell)
                model.cell.updateUI(); // Does a little more work than necessary, but not too much more.
            else
                toggleTransportCellMembershipButtonEl.innerHTML = "Group Member: No";
        },
        addOrRemoveCurrentCell: function () {
            var cell;
            if (cell = model.cell) { // There's a cell selected, and this transport group is selected
                cell.toggleTransportGroup(this); // Updates the cellMap AND cellCount as well...
                model.transportManager.updateUI();
            }
        },
        mirrorabilityCheck: function (otherTransGroup) {
            // Both this transport Group and the otherGroup must match...
            if (this.cellCount != otherTransGroup.cellCount) // The transport groups must have the same number of cells (for starters)...
                return false;
            // Match up all groups in each's cellMap...
            // Convert group ids to actual groups...(skipping any groups that can't be found...
            var myCellsList = [];
            var otherCellsList = [];
            var myGroups = Object.keys(this.cellMap).map(function (id) {
                myCellsList.push(this.cellMap[id]); // This becomes an array-of-arrays
                return model.groupById(id);     // myGroups 
            }, this);
            var otherGroups = Object.keys(otherTransGroup.cellMap).map(function (id) {
                otherCellsList.push(this.cellMap[id]);
                return model.groupById(id);
            }, otherTransGroup);
            // Group counts must be the same
            if (myGroups.length != otherGroups.length)
                return false;
            // If every group in myGroups matches some other group in the otherGroups, then mirrorability is OK.
            return myGroups.every(function (myG, i) {
                if (!myG) // It's null...
                    return true; // This can't be checked for mirrorability since it doesn't exist...It's OK to skip it.
                // Search through the other group's list looking for similar group characteristics; O(n^2) if no matches found...
                return otherGroups.some(function (otherG, j) {
                    if (!otherG) // It's null...
                        return false; // Not found here
                    if (myG.type != otherG.type)
                        return false; // Types don't match. Try the next one...
                    if (((myG.type == 1) && ((myG.rows != otherG.rows) || (myG.cols != otherG.cols))) ||
                        ((myG.type == 2) && ((myG.rings != otherG.rings) || (myG.distribution != otherG.distribution))))
                        return false; // Groups "shapes" doen't match. Try the next one...
                    var myCells = myCellsList[i];
                    var otherCells = otherCellsList[j];
                    if (myCells.length != otherCells.length)
                        return false; // Number of transport groups in the target group don't match. Try the next one...
                    // Cell indexes in the lists can be in different orders, but everything must match to pass...(another O(n^2) compare)
                    if (!myCells.every(function (myCellId) { return otherCells.indexOf(myCellId) >= 0; }))
                        return false; // The transport group's cell indexes are not all matching...
                    // This other group matches the qualifications for mirrorability with my group. 
                    // One down, the rest to go (all groups have to match up)
                    // Remove this matching other group from the selection criteria (it can only be matched once)
                    otherGroups.splice(j, 1);
                    return true; // Matched
                });
            });
        },
        unselect: function () {
            forEachCellInMap(this.cellMap, function (cell) { cell.currentTransGroup = null; });
        },
        select: function () {
            var groupId = this.id;
            forEachCellInMap(this.cellMap, function (cell) { cell.currentTransGroup = groupId; });
        },
        clearAll: function () {
            var groupId = this.id;
            forEachCellInMap(this.cellMap, function (cell) {
                // Leave the this.currentTransGroup untouched. It will be selected shortly
                if (Object.keys(cell.transGroup).length == 1)
                    cell.transGroup = null; // Full clear since this was the only transport Group
                else
                    delete cell.transGroup[groupId]; // Remove this key.
            });
        }
    };

    // TransportGroup(optional serializedTransportGroup) {
    //    <<PUBLIC>> -- serialized stuff
    //    id: #         // Model-unique id
    //    floorId: #    // The floor ID (not index) of the floor associated with this transport group
    //    cellMap: {}   // Key'd by the groupId, value is an array of cell indexes (groupId, since that's the most efficient way to find the cells on any floor/group index)
    //    cellCount: #  // Number of cells (total) in the map.
    //    <<PRIVATE>>
    //    name: ""      // Generated name of the group.
    // }

    Object.defineProperty(window, "TransportGroup", {
        enumerable: true,
        value: function TransportGroupConstructor(serializedTransportGroup) {
            // Setup accessors
            return Object.create(TransportGroupFunctions, {
                id: {
                    enumerable: true,
                    value: serializedTransportGroup ? serializedTransportGroup.id : model.transportGroupID++
                },
                floorId: {
                    enumerable: true,
                    value: serializedTransportGroup ? serializedTransportGroup.floorId : model.floor.id
                },
                cellMap: {
                    enumerable: true,
                    value: serializedTransportGroup ? serializedTransportGroup.cellMap : {}
                },
                cellCount: {
                    enumerable: true,
                    writable: true,
                    value: serializedTransportGroup ? serializedTransportGroup.cellCount : 0
                },
                // private
                name: {
                    get: function () { return "Group " + this.id + " (cells: " + this.cellCount + ")"; }
                }
            });
        }
    });

    // Hook up the UI to the model

    // New transport Group
    document.getElementById("newTransportGroup").onclick = function () { model.transportManager.addGroup(); };
    
    // Change selected item (group or joined group
    combinedListSelectEl.onchange = function () { model.transportManager.activeIndex = (this.selectedIndex < 0) ? null : this.selectedIndex; };

    // Delete transport group
    document.getElementById("transportDelete").onclick = function () { model.transportManager.removeListBoxItem(); };

    // Toggle cell to selected (open) transport group
    toggleTransportCellMembershipButtonEl.onclick = function () { model.transportManager.toggleCellMembership(); };

    // Toggle cell to selected (open) transport group (via keyboard)
    document.addEventListener("keyup", function (e) { if (e.key == "b") model.transportManager.toggleCellMembership(); });

    // Join groups
    document.getElementById("bindGroups").onclick = function () { model.transportManager.join(); };

    // Un-join joined groups
    document.getElementById("unbindBoundGroups").onclick = function () { model.transportManager.unjoin(); };

    // Change joined transport number of active cells
    numberTransportsInJointGroupInputEl.onchange = function () { var j; if (j = model.transportManager.joined) j.cellSetCount = parseInt(this.value); };

    // Change joined transport type
    transportTypeSelectEl.onchange = function () { var j; if (j = model.transportManager.joined) j.type = parseInt(this.value); };

    // Change joined transport mirror state
    mirrorStateCheckboxEl.onchange = function () { var j; if (j = model.transportManager.joined) j.mirror = this.checked; };
});

