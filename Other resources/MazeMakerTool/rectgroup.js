﻿"use strict";
addEventListener("DOMContentLoaded", function RectGroupScope() {

    // [Quazi-globals (accessor property state)]    
    var rectGroupPrototype = null;   // Created only once, the first time...
    var columnsInputEl = document.getElementById("rectWidth");
    var rowsInputEl = document.getElementById("rectHeight");
    var cellNumSpanEl = document.getElementById("currentCell");

    function toView(num) { return Math.floor(model.pixelPerUnit * num); }

    function drawCell(cell, isSelected) {
        var outline = [                       // __X___ __Y___ _index_
            toView(cell.location[0] - 4.8),   // left          0
            toView(cell.location[1] - 4.8),   //        top    1
            toView(9.6),                      //               2      width/height (cell is square)
            toView(cell.location[0] + 4.8),   // right         3
            toView(cell.location[1] + 4.8)];  //        bottom 4
        if (cell.linkCandidate > 0) {
            // Render a background to signal this cell as a "marked" cell for potential linking
            // 1 = purple, 2 = blue
            ctx.fillStyle = (cell.linkCandidate == 1) ? "purple" : "blue";
            ctx.fillRect(outline[0], outline[1], outline[2], outline[2]);
        }
        if (cell.deleted) {
            if (model.showDeleted && (model.showCellOutline || isSelected)) {
                ctx.strokeStyle = isSelected ? "lime" : "#444";
                ctx.strokeRect(outline[0], outline[1], outline[2], outline[2]);
            }
            if (model.showDeleted && model.showCenter) {
                ctx.strokeStyle = "#444";
                ctx.beginPath(); // Draw an 'X' over the box.
                ctx.moveTo(outline[0], outline[1]);
                ctx.lineTo(outline[3], outline[4]);
                ctx.moveTo(outline[3], outline[1]);
                ctx.lineTo(outline[0], outline[4]);
                ctx.stroke();
            }
            return;
        }
        // Not deleted...
        var center = [toView(cell.location[0]), toView(cell.location[1])];        
        if (model.showConnectors) { /////////////////////////////////////// DRAW CONNECTORS
            var inside = [                      // __X___ __Y___ _index_
                toView(cell.location[0] - 3.5),   // left          0
                toView(cell.location[1] - 3.5),   //        top    1
                toView(cell.location[0] + 3.5),   // right         2
                toView(cell.location[1] + 3.5)];  //        bottom 3
            var deleted = [cell.connectors[0].deleted, cell.connectors[1].deleted, cell.connectors[2].deleted, cell.connectors[3].deleted];
            var selected = [false, false, false, false];
            selected[cell.currentConnector] = true;
            // Draw top
            ctx.strokeStyle = selected[0] ? "lime" : "#888";
            ctx.lineWidth = (selected[0] || deleted[0]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[0]) { ctx.moveTo(inside[0], inside[1]); ctx.lineTo(inside[2], inside[1]); }
            else { ctx.moveTo(center[0], center[1]); ctx.lineTo(center[0], inside[1]); }
            ctx.stroke();
            // Draw right
            ctx.strokeStyle = selected[1] ? "lime" : "#888";
            ctx.lineWidth = (selected[1] || deleted[1]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[1]) { ctx.moveTo(inside[2], inside[1]); ctx.lineTo(inside[2], inside[3]); }
            else { ctx.moveTo(center[0], center[1]); ctx.lineTo(inside[2], center[1]); }
            ctx.stroke();
            // Draw left
            ctx.strokeStyle = selected[2] ? "lime" : "#888";
            ctx.lineWidth = (selected[2] || deleted[2]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[2]) { ctx.moveTo(inside[0], inside[1]); ctx.lineTo(inside[0], inside[3]); }
            else { ctx.moveTo(center[0], center[1]); ctx.lineTo(inside[0], center[1]); }
            ctx.stroke();
            // Draw bottom
            ctx.strokeStyle = selected[3] ? "lime" : "#888";
            ctx.lineWidth = (selected[3] || deleted[3]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[3]) { ctx.moveTo(inside[0], inside[3]); ctx.lineTo(inside[2], inside[3]); }
            else { ctx.moveTo(center[0], center[1]); ctx.lineTo(center[0], inside[3]); }
            ctx.stroke();
            ctx.lineWidth = 1;
        }
        if (model.showCenter) { /////////////////////////////////////////// DRAW CENTER
            ctx.fillStyle = "#888";
            ctx.beginPath();
            ctx.arc(center[0], center[1], toView(1), 0, Math.PI * 2, true);
            ctx.fill();
        }
        if (model.showCellOutline || isSelected) { //////////////////////// DRAW CELL SHAPE
            ctx.strokeStyle = isSelected ? "lime" : "#888";
            ctx.strokeRect(outline[0], outline[1], outline[2], outline[2]);
        }
        if (model.showCellToggles) { ////////////////////////////////////// DRAW START/ END
            if (cell.startCell) {
                ctx.fillStyle = "green";
                ctx.beginPath();
                ctx.arc(center[0], center[1], toView(2), 0, Math.PI * 2, true);
                ctx.fill();
            }
            if (cell.endCell) {
                ctx.fillStyle = "red";
                ctx.beginPath();
                ctx.arc(center[0], center[1], toView(1.5), 0, Math.PI * 2, true);
                ctx.fill();
            }
            if (cell.transGroup) {
                ctx.strokeStyle = (cell.currentTransGroup != null) ? "lime" : "yellow";
                ctx.beginPath();
                ctx.arc(center[0], center[1], toView(2.5), 0, Math.PI * 2, true);
                ctx.stroke();
                var keys = Object.keys(cell.transGroup);
                if (cell.currentTransGroup != null) {
                    ctx.strokeStyle = "lime";
                    ctx.strokeText((keys.length > 1) ? ("<" + cell.currentTransGroup + ">") : keys[0], center[0]-7, center[1]+3);
                }
                else
                    ctx.strokeText((keys.length > 1) ? ("{" + keys.length + "}") : keys[0], center[0]-7, center[1]+3);
            }
        }
    }

    function ensureRectGroupPrototype() {
        if (rectGroupPrototype != null) return;
        rectGroupPrototype = Group.newPrototypeSubclass();
        rectGroupPrototype.updateUI = function () {
            // Call the super() method on this prototype's prototype
            Object.getPrototypeOf(rectGroupPrototype).updateUI.call(this);
            rowsInputEl.value = this.rows;
            columnsInputEl.value = this.cols;
            cellNumSpanEl.innerHTML = this.getPresentationIdfromCellId(this.currentCell);
            this.cells[this.currentCell].updateUI();
        };
        rectGroupPrototype.redraw = function (isSelected) { // Assumes that the canvas has been previously cleared.
            this.pushTransformState();
            for (var i = 0, len = this.cells.length; i < len; i++)
                drawCell(this.cells[i], isSelected && (this.currentCell == i));
            this.popTransfromState();
        };
        rectGroupPrototype.redrawCell = function (isSelected, optionalIndex) { // Handles clearing/redrawing of just the current cell or provided specific cell index.
            var cell = this.cells[(typeof optionalIndex == "number") ? optionalIndex : this.currentCell];
            this.pushTransformState();
            ctx.clearRect(toView(cell.location[0] - 5.1), toView(cell.location[1] - 5.1), toView(10.2), toView(10.2)); // Clear the old grid region...
            drawCell(cell, isSelected)
            this.popTransfromState();
        };
        rectGroupPrototype.isConnectorDeleted = function (i, targetIndex, oldColNum, r, c) {
            // The connector will be considered deleted (true) if:
            // 1. The target is definately a wall/edge of the group
            // 2. The existing cell is currently considered deleted (after previous sameness verification)
            // 3. The target being evaluated has already been checked for sameness verification, thus it's deleted status is reliable and it is deleted
            // 4. The target hasn't been checked, but would meet the sameness requirements and is deleted
            return (targetIndex == -1) || 
                this.cells[i].deleted || 
                (!!this.cells[targetIndex] && this.cells[targetIndex].deleted &&
                    ((targetIndex < i) || ((targetIndex > i) && ((c == (targetIndex % oldColNum)) && (r == Math.floor(targetIndex / oldColNum))))));
        };
        rectGroupPrototype.modifyGrid = function (oldRow, oldCol, newRow, newCol) {
            // cells are: [ [cols], [cols], [cols] ] (like <table> rows/cols structure
            // Note all actions apply to the "end" of the list of rows/cells
            for (var r = 0; r < newRow; r++) {
                for (var c = 0; c < newCol; c++) {
                    var i = (newCol * r) + c;
                    var sameCell = ((c == (i % oldCol)) && (r == Math.floor(i / oldCol))); // If the current row/col match the row/column using the old calculation...
                    if (!this.cells[i])
                        this.cells[i] = new Cell(i); // Cell ID matches the index for simplicity
                    else
                        this.cells[i].clean(i, sameCell); // Re-assign the id
                    // Set/update the cell-center (uses the generic "location" array in the cell for this)
                    this.cells[i].location[0] = c * 10; // 10 units per cell
                    this.cells[i].location[1] = r * 10;
                    // Connectors -- 0-based index or -1
                    // Preserving cell deleted state: you're deleted, your target is deleted (less then your index, it's a safe reference to check. greater than, and you need to confirm that the destination cell will be 'same' before consulting
                    // Add top-connector [0]
                    var target = (r == 0) ? -1 : (newCol * (r - 1) + c);
                    this.cells[i].addConnector(this.id, target, this.isConnectorDeleted(i, target, oldCol, r-1, c));
                    // Add right connector [1]
                    target = (c == (newCol - 1)) ? -1 : i + 1; // next-door neighbor
                    this.cells[i].addConnector(this.id, target, this.isConnectorDeleted(i, target, oldCol, r, c+1));
                    // Add left connector [2]
                    target = (c == 0) ? -1 : (i - 1); // left-neighbor
                    this.cells[i].addConnector(this.id, target, this.isConnectorDeleted(i, target, oldCol, r, c-1));
                    // Add bottom connector [3]
                    target = (r == (newRow - 1)) ? -1 : (newCol * (r + 1) + c); // below index
                    this.cells[i].addConnector(this.id, target, this.isConnectorDeleted(i, target, oldCol, r+1, c));
                    // This preserves the state of previously deleted cells as long as the cell is the same before/after (reconstructing their connector bindings) it
                    // does not preserve one-off connector deletions...
                }
            }
            // Clear out any excess cells
            if ((newRow * newCol) < (oldRow * oldCol))
                this.cells.splice((newRow * newCol), (oldRow * oldCol)); // Over-splices, but that's OK :)
            model.transportManager.validateAndUpdateUI(); // Validate the transport manager since the group is modified
        };
        rectGroupPrototype.groupToggle = function (isRow) {
            // Toggles the delete status of a series of cells either an entire row or column
            var directionConnectorIndex = isRow ? 1 : 3; // right/bottom extention first
            var round = 1;
            var currCell = this.cells[this.currentCell];
            while ((currCell != null) && (round <= 2)) {
                // Get the next cell -- extend along this direction as long as the connector is within this group and not -1 (boundary)
                currCell = ((this.id == currCell.connectors[directionConnectorIndex].group) && (currCell.connectors[directionConnectorIndex].cell != -1)) ? this.cells[currCell.connectors[directionConnectorIndex].cell] : null;
                if (currCell)
                    currCell.toggleDeleted(currCell.id);
                else {
                    directionConnectorIndex = isRow ? 2 : 0;
                    currCell = this.cells[this.currentCell]; // Reset current cell...
                    round++;
                }
            }
            this.cells[this.currentCell].toggleDeleted(); // Delete the cell I'm on (last to avoid being overdrawn by adjacent cell deletions)
        };
        rectGroupPrototype.getPresentationIdfromCellId = function (cellId) { // 0-based cell id
            // For addressing, use zero-based index..
            var r = Math.floor(cellId / this.cols);
            var c = cellId % this.cols;
            return "r" + (r + 1) + "c" + (c + 1) + "#" + cellId;
        };
        // Supports the custom connectors (formerly "bridgewalls") between cells in the same (or different) group.
        // Given a cell reference, together with a connector index (which "side" to connect to), a "opposite" 
        // indicator (allowing to specify one end of the side or the other), and the "isSource" flag (moving to the point
        // if source, or drawing a line to the point if not)
        rectGroupPrototype.drawCustomConnector = function (cellRef, connectorIndex, opposite, isSource) {
            var draw = isSource ? ctx.moveTo.bind(ctx) : ctx.lineTo.bind(ctx);
            this.pushTransformState();
            switch (connectorIndex) {
                case 0: /* top */   draw(toView(opposite ? (cellRef.location[0] + 4.8) : (cellRef.location[0] - 4.8)), toView(cellRef.location[1] - 4.8)); break;
                case 1: /* right */ draw(toView(cellRef.location[0] + 4.8), toView(opposite ? (cellRef.location[1] + 4.8) : (cellRef.location[1] - 4.8))); break;
                case 2: /* left */  draw(toView(cellRef.location[0] - 4.8), toView(opposite ? (cellRef.location[1] - 4.8) : (cellRef.location[1] + 4.8))); break;
                case 3: /* bottom*/ draw(toView(opposite ? (cellRef.location[0] - 4.8) : (cellRef.location[0] + 4.8)), toView(cellRef.location[1] + 4.8)); break;
            }
            this.popTransfromState();
        };
        // Support the export functionality by returning custom connector coordinates in the same order/algorithm as used above
        rectGroupPrototype.getConnectorCoord = function (transformMatrix, cellRef, sideIndex, oppositeFlag) {
            switch (sideIndex) {
                case 0: /* top */   return transformMatrix.multiply([(oppositeFlag ? (cellRef.location[0] + 5) : (cellRef.location[0] - 5)), (cellRef.location[1] - 5)]);
                case 1: /* right */ return transformMatrix.multiply([(cellRef.location[0] + 5), (oppositeFlag ? (cellRef.location[1] + 5) : (cellRef.location[1] - 5))]);
                case 2: /* left */  return transformMatrix.multiply([(cellRef.location[0] - 5), (oppositeFlag ? (cellRef.location[1] - 5) : (cellRef.location[1] + 5))]);
                case 3: /* bottom*/ return transformMatrix.multiply([(oppositeFlag ? (cellRef.location[0] - 5) : (cellRef.location[0] + 5)), (cellRef.location[1] + 5)]);
            }
        };
        rectGroupPrototype.getWallGeometry = function (transformMatrix, cellRef, sideIndex) {
            var pStart = this.getConnectorCoord(transformMatrix, cellRef, sideIndex, false);
            return pStart.concat(this.getConnectorCoord(transformMatrix, cellRef, sideIndex, true));
        };
    };

    // RectGroup(optional serializedGroup) : Group {
    //    // PUBLIC (serialized stuff)
    //    id: #                 // Group id (globally unique)
    //    type: 1               // Rect type
    //    currentCell: #        // There will always be a current cell while this group exists
    //    cells: []             // Array of rectable--assumed cells.
    //    rows: #               // Number of rows in the Rect group
    //    cols: #               // Number of cells in the Rect group
    //    // PRIVATE (not serialized)
    //    name: "GridXX"
    //    <<--------------------------->>
    //    // INHERITED FROM GROUP
    //    rotate: #             // Rotation offset
    //    translateX: #         // Translation in X axis
    //    translateY: #         // Translation in Y axis
    //    stretchX: #           // Stretch X direction
    //    stretchY: #           // Stretch Y direction
    // }

    Object.defineProperty(window, "RectGroup", {
        enumerable: true,
        value: function RectGroupConstructor(serializedRectGroup, isDuplicate) {
            ensureRectGroupPrototype();
            // Initialize accessors here
            var currentCell = serializedRectGroup ? serializedRectGroup.currentCell : 0
            var cols = serializedRectGroup ? serializedRectGroup.cols : 1; // Start with one cell only
            var rows = serializedRectGroup ? serializedRectGroup.rows : 1;
            var id = (serializedRectGroup && !isDuplicate) ? serializedRectGroup.id : model.groupID++; // Needed here to initialize a new (first time) cell's connectors with this group's id.
            // Compose the two instances together... (RectGroups _are_ Groups + other stuff)
            return Object.defineProperties(new Group(rectGroupPrototype, serializedRectGroup), {
                id: {
                    enumerable: true,
                    value: id
                },
                type: {
                    enumerable: true,
                    value: 1    // Always.
                },
                cells: {
                    enumerable: true,
                    value: serializedRectGroup ? serializedRectGroup.cells.map(function(cell) { 
                        return new Cell(cell, isDuplicate, id); 
                    }) : [new Cell(0, isDuplicate, id)].map(function (cell) {
                        cell.location.push(0); // x-center = 0
                        cell.location.push(0); // y-center = 0
                        for (var i = 0; i < 4; i++) // Add in [top, right, left, bottom] connectors
                            cell.addConnector(id, -1, true);
                        return cell;
                    })
                },
                currentCell: {
                    enumerable: true,
                    get: function () { return currentCell; },
                    set: function (x) {
                        x = Math.max(0, Math.min(x, this.cells.length - 1));
                        if (currentCell == x) return;
                        this.redrawCell(false);
                        currentCell = x;
                        this.redrawCell(true); // Make the current cell selected
                        cellNumSpanEl.innerHTML = this.getPresentationIdfromCellId(currentCell);
                        this.cells[currentCell].updateUI();
                    }
                },
                rows: {
                    enumerable: true,
                    get: function () { return rows; },
                    set: function (x) {
                        x = Math.max(1, x);
                        if (x == rows) return;
                        this.modifyGrid(rows, cols, x, cols);
                        rows = x;
                        rowsInputEl.value = rows;
                        if (currentCell >= this.cells.length) // Move it to the middle...
                            currentCell = Math.floor((rows * cols) / 2);
                        model.floor.redraw();
                    }
                },
                cols: {
                    enumerable: true,
                    get: function () { return cols; },
                    set: function (x) {
                        x = Math.max(1, x);
                        if (x == cols) return;
                        this.modifyGrid(rows, cols, rows, x);
                        cols = x;
                        columnsInputEl.value = cols;
                        if (currentCell >= this.cells.length) // Move it to the middle...
                            currentCell = Math.floor((rows * cols) / 2);
                        model.floor.redraw();
                    }
                },
                name: {
                    get: function() { return "Grid" + this.id; }
                }
            });
        }
    });

    // Hook up the UI to the model

    // Change col num
    columnsInputEl.onchange = function () { if (model.group) model.group.cols = parseInt(this.value); };

    // Decrement col count
    document.getElementById("rectWidthLess").onclick = function () { if (model.group) model.group.cols--; };

    // Increment col count
    document.getElementById("rectWidthMore").onclick = function () { if (model.group) model.group.cols++; };

    // Toggles cells in column
    document.getElementById("rectWidthCellToggle").onclick = function () { if (model.group) model.group.groupToggle(false); }

    // Change row num
    rowsInputEl.onchange = function () { if (model.group) model.group.rows = parseInt(this.value); };

    // Decrement row count
    document.getElementById("rectHeightLess").onclick = function () { if (model.group) model.group.rows--; };
    
    // Increment row count
    document.getElementById("rectHeightMore").onclick = function () { if (model.group) model.group.rows++; };

    // Toggles cells in column
    document.getElementById("rectHeightCellToggle").onclick = function () { if (model.group) model.group.groupToggle(true); }
});