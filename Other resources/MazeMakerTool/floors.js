﻿"use strict";
addEventListener("DOMContentLoaded", function FloorScope() {

    // [Quazi-globals (accessor property state)]
    var managerSectionEl = document.getElementById("cellGroupManagement");
    var groupSelectEl = document.getElementById("choose");
    var groupTypeSelectEl = document.getElementById("newType");
    var cellNameSpanEl = document.getElementById("currentCell");

    function createDerivedGroupByType(type, /*optional*/serialized, /*optional*/isDuplicate) {
        switch (type) {
            case 1: return new RectGroup(serialized, isDuplicate);
            case 2: return new CircleGroup(serialized, isDuplicate);
        }
    }

    var FloorFunctions = {
        updateUI: function () {
            groupSelectEl.innerHTML = "";
            // Re-build all the groups
            for (var i = 0, len = this.groups.length; i < len; i++)
                groupSelectEl.add(new Option(this.groups[i].name, this.groups[i].id, false, i == this.currentGroup));
            if ((this.groups.length > 0) && (this.currentGroup != null)) {
                this.groups[this.currentGroup].updateUI();
                // Make the options available...
                managerSectionEl.setAttribute("data-selected", (this.groups[this.currentGroup].type == 1) ? "grid" : "circ");
            }
            else { // Empty list or none selected
                managerSectionEl.setAttribute("data-selected", "none");
                cellNameSpanEl.innerHTML = "--none selected--";
            }
            // Update the custom connectors...
            this.customConnectors.updateUI();
        },
        addGroup: function (typeID) {
            this.groups.unshift(createDerivedGroupByType(typeID));
            // Sync the model's groupMap since this alters where things are relative to this floor...
            this.syncModelGroupMap();
            if (this.currentGroup == 0)
                this.updateUI(); // Make sure that a UI update will still happen even if the current group doesn't change
            this.currentGroup = 0; // Force the newly added group to be selected
            // Paint the new group on this floor
            this.redraw();
        },
        cloneGroup: function() {
            if (this.currentGroup == null) return; // Must have a group selected
            var sourceGroup = this.groups[this.currentGroup];
            var clonedGroup = createDerivedGroupByType(sourceGroup.type, sourceGroup.createSerializedCopy(), true);
            this.customConnectors.isolateClonedGroup(clonedGroup, sourceGroup.id);
            this.groups.unshift(clonedGroup);
            // Sync the model's groupMap since this alters where things are relative to this floor...
            this.syncModelGroupMap();
            if (this.currentGroup == 0)
                this.updateUI(); // Make sure that a UI update will still happen even if the current group doesn't change
            this.currentGroup = 0; // Force the newly added group to be selected
            // Paint the new group on this floor
            this.redraw();
        },
        delGroup: function() {
            if (this.currentGroup == null) return;
            // Remove the group from the model's map
            model.mapDel(this.groups[this.currentGroup].id);
            // Do maintenance on the group array...
            this.groups.splice(this.currentGroup, 1);
            if (this.groups.length == 0)
                this.currentGroup = null;
            else if (this.currentGroup == this.groups.length) // out of bounds
                this.currentGroup--;
            else
                this.updateUI(); // Just rebuild as the currentGroup now points to something else...
            this.syncModelGroupMap();
            model.transportManager.validateAndUpdateUI(); // Validate the transport manager that the group is removed
            this.redraw();
        },
        syncModelGroupMap: function (/*optional*/options/*{clearAll:bool,floorIndex:#}*/) { // The model may call this too to sync things when floors change.
            if (!options)
                options = {};
            if (options.clearAll) // Happens if a floor is about to vanish!
                return this.groups.forEach(function (group) { model.mapDel(group.id); });
            // First figure out what the current floor index is if not provided...
            if (!options.floorIndex) {
                for (var i = 0, len = model.floors.length; i < len; i++) {
                    if (this === model.floors[i]) { 
                        options.floorIndex = i; 
                        break; 
                    }
                }
            }
            // Update each of this floor's groups with the new index info...
            for (var i = 0, len = this.groups.length; i < len; i++)
                model.mapUpdate(this.groups[i].id, options.floorIndex, i);
        },
        customConnectorsPostDuplicateUpdate: function () {
            this.customConnectors.updateGroupIds(this.groups);
        },
        redraw: function () {
            // Floor re-draws will always clear everything and start from a blank slate...
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            for (var i = 0, len = this.groups.length; i < len; i++)
                this.groups[i].redraw(i == this.currentGroup);
            // Draw any custom connectors
            this.customConnectors.redraw();
        },
        // Returns a reference to a group within the current floor (assumed or null)
        groupById: function (groupId) {
            var candidateGroup = model.groupMap[groupId];
            if (!candidateGroup) return null;
            if (model.floors[candidateGroup.floorIndex] !== this) return null;
            return this.groups[candidateGroup.groupIndex];
        }
    };

    // Floor(optional serializedFloor) {
    //    <<PUBLIC>> -- serialized stuff
    //    id: #                     // Floor number
    //    groups: []                // Array of cell groups
    //    currentGroup: # or null   // Index to the current active group or null if none active
    //    customConnectors: {}      // Custom Connection object (see customconnector.js)
    //    <<PRIVATE>> -- not serialized
    // }
    
    Object.defineProperty(window, "Floor", {
        enumerable: true,
        value: function FloorConstructor(serializedFloor, isDuplicate) {
            // Initialize accessors here
            var currentGroup = serializedFloor ? serializedFloor.currentGroup : null;
            // Return the instance object
            return Object.create(FloorFunctions, {
                id: {
                    enumerable: true,
                    value: (serializedFloor && !isDuplicate) ? serializedFloor.id : model.floorID++
                },
                groups: {
                    enumerable: true,
                    value: serializedFloor ? serializedFloor.groups.map(function (group) { return createDerivedGroupByType(group.type, group, isDuplicate); }) : []
                },  // NOTE: construction/duplication will ensure post-facto that syncModelGroupMap is called to validate the state.
                currentGroup: {
                    enumerable: true,
                    get: function () { return currentGroup; },
                    set: function (x) {
                        if (x != null) // Expecting numbers or null
                            x = Math.min(this.groups.length - 1, Math.max(0, x)); // Force the incoming value into range...
                        if (currentGroup == x) return; // number or null matches for equality...
                        currentGroup = x;
                        this.updateUI();
                        this.redraw();
                    }
                },
                customConnectors: {
                    enumerable: true,
                    value: serializedFloor ? new CustomConnector(serializedFloor.customConnectors, isDuplicate) : new CustomConnector()
                }
            });
        }
    });

    // Hook up the UI to the model
    
    // New cell group
    document.getElementById("new").onclick = function () { model.floor.addGroup(parseInt(groupTypeSelectEl.value)); };

    // Change cell group
    document.getElementById("choose").onchange = function () { model.floor.currentGroup = (this.selectedIndex < 0) ? null : this.selectedIndex; };

    // Delete cell group
    document.getElementById("deleteGroup").onclick = function () { model.floor.delGroup(); }

    // Clone cell group
    document.getElementById("cloneGroup").onclick = function () { model.floor.cloneGroup(); }
});