﻿"use strict";
addEventListener("DOMContentLoaded", function CollectibleScope() {
    // Groups do not actual draw anything themselves--they are abstract for the UI only

    // [Quazi-globals (accessor property state)]    
    var rootDisplayDivEl = document.getElementById("collectibleExtras");
    var arrangementSelectEl = document.getElementById("arrangementType");
    var locatedInputEl = document.getElementById("locatedType");
    var dynamicSizeInputEl = document.getElementById("dynamicSize");
    var assertivenessSelectEl = document.getElementById("assertiveness");

    var CollectibleFunctions = {
        updateUI: function () {
            rootDisplayDivEl.setAttribute("data-show", (this.typeEnum.indexOf("dyn_") != -1) ? "all" : "some");
            arrangementSelectEl.selectedIndex = this.arrangementIndex;
            locatedInputEl.value = this.located;
            dynamicSizeInputEl.value = this.size;
            assertivenessSelectEl.selectedIndex = this.assertivenessIndex;
        }
    };

    function typeToFriendly (type) {
        switch (type) {
            case "dyn_light": return "light";
            case "dyn_firefly": return "firefly";
            case "dyn_snake": return "snake";
            case "dyn_fog": return "fog";
            default: return type;
        }
    }

    var arrangementMap = ["inline","close","medium","far"];
    arrangementMap.inline = 0;
    arrangementMap.close = 1;
    arrangementMap.medium = 2;
    arrangementMap.far = 3;

    var assertivenessMap = ["none","low","medium","high"];
    assertivenessMap.none = 0;
    assertivenessMap.low = 1;
    assertivenessMap.medium = 2;
    assertivenessMap.high = 3;

    // Collectible(serializedGroup or typestring) {
    //    <<PUBLIC>> -- serialized stuff
    //    typeEnum: ""          // the collectible type {cheese,key,gate,trap,switch,fan,earthquake,dyn_light,dyn_firefly,dyn_snake,dyn_fog}
    //    arrangementEnum: ""   // 0)inline,1)close,2)medium,3)far - how close to the solution path should the collectible reside...
    //    located: #            // How far along the solution path this collectible should be placed
    //    size: #               // Number of cells this should extend to
    //    assertivenessEnum: "" // 0)none,1)low,2)medium,3)high - how agile/fast will the dynamic thing move...
    //    <<PRIVATE>> -- not serialized
    //    friendlyName: ""      // Version of the type suitable for showing to the screen.
    //    arrangementIndex: #   // index of the arrangement enum
    //    assertivenessIndex: # // index of the assertiveness enum
    // }

    Object.defineProperty(window, "Collectible", {
        enumerable: true,
        value: function CollectibleConstructor(serializedCollectibleOrTypeString) {
            // Instance-scoped variables (not global)
            var typeEnum = null;
            if (typeof serializedCollectibleOrTypeString == "string") { 
                typeEnum = serializedCollectibleOrTypeString;
                serializedCollectibleOrTypeString = null;
            }
            else
                typeEnum = serializedCollectibleOrTypeString.typeEnum;
            var arrangementEnum = serializedCollectibleOrTypeString ? serializedCollectibleOrTypeString.arrangementEnum: "inline";
            var located = serializedCollectibleOrTypeString ? serializedCollectibleOrTypeString.located : 50;
            var size = serializedCollectibleOrTypeString ? serializedCollectibleOrTypeString.size : 1
            var assertivenessEnum = serializedCollectibleOrTypeString ? serializedCollectibleOrTypeString.assertivenessEnum: "low";
            // Initialize accessors here
            return Object.create(CollectibleFunctions, {
                typeEnum: {
                    enumerable: true,
                    value: typeEnum
                },
                arrangementEnum: {
                    enumerable: true,
                    get: function () { return arrangementEnum; },
                    set: function (x) {
                        if (typeof arrangementMap[x] == "undefined") throw Error("Bad enum value!");
                        arrangementEnum = x;
                        arrangementSelectEl.selectedIndex = this.arrangementIndex;
                    }
                },
                located: {
                    enumerable: true,
                    get: function () { return located; },
                    set: function (x) {
                        x = Math.min(99, Math.max(0, parseInt(x))); // Restrict to 0-99 integers inclusive
                        if (x == located) return;
                        located = x;
                        locatedInputEl.value = located;
                    }
                },
                size: {
                    enumerable: true,
                    get: function () { return size; },
                    set: function (x) {
                        x = Math.max(1, parseInt(x)); // Restrict to 1+ integers
                        if (x == size) return;
                        size = x;
                        dynamicSizeInputEl.value = size;
                    }
                },
                assertivenessEnum: {
                    enumerable: true,
                    get: function () { return assertivenessEnum; },
                    set: function (x) {
                        if (typeof assertivenessMap[x] == "undefined") throw Error("Bad enum value!");
                        assertivenessEnum = x;
                        assertivenessSelectEl.selectedIndex = this.assertivenessIndex;
                    }
                },
                // Private (not enumerable)
                friendlyName: {
                    get: function () { return typeToFriendly(typeEnum); }
                },
                arrangementIndex: {
                    get: function () { return arrangementMap[arrangementEnum]; },
                    set: function (x) { this.arrangementEnum = arrangementMap[x]; }
                },
                assertivenessIndex: {
                    get: function () { return assertivenessMap[assertivenessEnum]; },
                    set: function (x) { this.assertivenessEnum = assertivenessMap[x]; }
                }
            });
        }
    });

    // arrangement change
    arrangementSelectEl.onchange = function () { if (model.collectible) model.collectible.arrangementIndex = this.selectedIndex; };

    // located closer
    document.getElementById("locatedCloser").onclick = function () { if (model.collectible) model.collectible.located -= 5; };

    // located change
    locatedInputEl.onchange = function () { if (model.collectible) model.collectible.located = this.value; };

    // located further
    document.getElementById("locatedFurther").onclick = function () { if (model.collectible) model.collectible.located += 5; };

    // less size
    document.getElementById("lessCells").onclick = function () { if (model.collectible) model.collectible.size -= 1; };

    // size change
    dynamicSizeInputEl.onchange = function () { if (model.collectible) model.collectible.size = this.value; };

    // more size
    document.getElementById("moreCells").onclick = function () { if (model.collectible) model.collectible.size += 1; };

    // assertiveness change
    assertivenessSelectEl.onchange = function () { if (model.collectible) model.collectible.assertivenessIndex = this.selectedIndex; };
});
