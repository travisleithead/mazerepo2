﻿"use strict";
addEventListener("DOMContentLoaded", function CustomConnectorScope() {

    // [Quazi-globals (accessor property state)]
    var setLink1ButtonEl = document.getElementById("setLink1");
    var link1LabelSpanEl = document.getElementById("link1id");
    var setLink2ButtonEl = document.getElementById("setLink2");
    var link2LabelSpanEl = document.getElementById("link2id");
    var linkedCellsSelectEl = document.getElementById("joinedCells");
    var swapLinkedCheckboxEl = document.getElementById("swapLink");

    // Returns null, or the validated cell reference object given a candidate { group: #, cell: #, connectorIndex?: # }
    function getValidCellFromCandidate(candidate, expectedCandidateIDorLinkedCandidate) { // Either a number (candidate num) or the opposite linked end of a linked custom connector
        var typeOfExpected = typeof expectedCandidateIDorLinkedCandidate;
        if (!candidate || !expectedCandidateIDorLinkedCandidate || ((typeOfExpected != "number") && (typeOfExpected != "object")))
            return null;
        var group = model.floor.groupById(candidate.group);
        if (!group)
            return null;
        if (candidate.cell >= group.cells.length) // Index out-of-bounds (group's cells shrunk)
            return null;
        var candidateCell = group.cells[candidate.cell];
        if ((typeOfExpected == "number") && (candidateCell.linkCandidate != expectedCandidateIDorLinkedCandidate)) // the only validation step for candidate IDs
            return null;
        else if (typeOfExpected == "object") {
            var connectorCompare = candidateCell.connectors[candidate.connectorIndex];
            if (!connectorCompare)
                return null;
            if ((expectedCandidateIDorLinkedCandidate.group != connectorCompare.group) || (expectedCandidateIDorLinkedCandidate.cell != connectorCompare.cell))
                return null;
        }
        return candidateCell;
    }

    function getGroupByOldId(groupList, oldGroupId) { return groupList[model.groupMap[oldGroupId].groupIndex]; }

    // By default, any connectors which do not validate are restored. A forceRestore boolean flag ensures that the connector is restored when possible
    // Returns true if no restoration was needed, false otherwise.
    function validateConnector(connector, forceRestore) {
        // The following handle null first-parameter just fine...
        var cellFrom = getValidCellFromCandidate(connector.from, connector.to);
        var cellTo = getValidCellFromCandidate(connector.to, connector.from);
        if ((!cellFrom || !cellTo) || forceRestore) { // If one or the other (or both?) cells aren't valid, OR we're forcing a restore anyway...
            if (cellFrom) { // Restore
                var connectorFrom = cellFrom.connectors[connector.from.connectorIndex];
                connectorFrom.group = connector.from.restoreGroup;
                connectorFrom.cell = connector.from.restoreCell;
            }
            if (cellTo) { // Restore
                var connectorTo = cellTo.connectors[connector.to.connectorIndex];
                connectorTo.group = connector.to.restoreGroup;
                connectorTo.cell = connector.to.restoreCell;
            }
            return false; // Had to restore something (validation has failed)
        }
        return true; // validation succeeded
    }

    var CustomConnectorFunctions = {
        updateUICustomConnectorList: function () {
            linkedCellsSelectEl.innerHTML = "";
            var len = this.joined.length;
            linkedCellsSelectEl.add(new Option((len == 0) ? "No Joined Cells" : "None Selected", -1, false, this.selectedIndex == null));
            // Re-build the linked connectors list
            for (var i = 0; i < len; i++)
                linkedCellsSelectEl.add(new Option(this.joined[i].name, i, false, i == this.selectedIndex));
            // Update the associated checkbox as well...
            swapLinkedCheckboxEl.disabled = (this.selectedIndex == null);
            swapLinkedCheckboxEl.checked = (this.selectedIndex != null) && this.joined[this.selectedIndex].flipped;
        },
        updateUICandidateButton: function (isOne) {
            // Update the buttons with their values from the map
            var linkLabelSpanEl = isOne ? link1LabelSpanEl : link2LabelSpanEl;
            var candidate = isOne ? this.candidate1 : this.candidate2;
            var setLinkButtonEl = isOne ? setLink1ButtonEl : setLink2ButtonEl;
            linkLabelSpanEl.innerHTML = (candidate == null) ? "unset" : ("G" + candidate.group + "c" + candidate.cell);
            if (candidate == null)
                setLinkButtonEl.removeAttribute("data-set");
            else
                setLinkButtonEl.setAttribute("data-set", "true");
        },
        updateUI: function () {
            this.updateUICustomConnectorList();
            this.updateUICandidateButton(true);
            this.updateUICandidateButton(false);
        },
        updateGroupIds: function (myGroupList) {
            // Called after duplicating a floor to swap the group ids stored in the newly cloned custom connectors to appropriate group ids matching
            // the new floor's group ids (which are unique from the last floor, even after duplication)
            if (this.candidate1)
                this.candidate1.group = getGroupByOldId(myGroupList, this.candidate1.group).id;
            if (this.candidate2)
                this.candidate2.group = getGroupByOldId(myGroupList, this.candidate2.group).id;
            this.joined.forEach(function (join) {
                // Update the group and restoreGroup on this joined custom connector
                join.from.group = getGroupByOldId(myGroupList, join.from.group).id;
                join.to.group = getGroupByOldId(myGroupList, join.to.group).id;
                join.from.restoreGroup = getGroupByOldId(myGroupList, join.from.restoreGroup).id;
                join.to.restoreGroup = getGroupByOldId(myGroupList, join.to.restoreGroup).id;
                // Update the specific cell in the group with it's converted group id.
                // From's cell will point to To's cell and vice-versa
                getGroupByOldId(myGroupList, join.from.group).cells[join.from.cell].connectors[join.from.connectorIndex].group = join.to.group;
                getGroupByOldId(myGroupList, join.to.group).cells[join.to.cell].connectors[join.to.connectorIndex].group = join.from.group;
            });
        },
        isolateClonedGroup: function (clonedGroup, oldGroupId) {
            // Called to un-link any custom connectors from a newly cloned group
            // If any of the joined groups referred to the the clonedGroup Id, then those cells in the cloned Group must be isolated (un-linked)
            if (this.candidate1 && (this.candidate1.group == oldGroupId))
                clonedGroup.cells[this.candidate1.cell].linkCandidate = 0; // No longer a candidate.
            if (this.candidate2 && (this.candidate2.group == oldGroupId))
                clonedGroup.cells[this.candidate2.cell].linkCandidate = 0;
            this.joined.forEach(function (join) {
                var connector = null;
                if (join.from.group == oldGroupId)
                    connector = clonedGroup.cells[join.from.cell].connectors[join.from.connectorIndex];
                if (join.to.group == oldGroupId)
                    connector = clonedGroup.cells[join.to.cell].connectors[join.to.connectorIndex];
                if (connector) {
                    connector.cell = -1;
                    connector.deleted = true; // Group was already forced to the new group Id as part of the clone operation (the joined list was the only place with a record of what it linked to)
                }
            });
        },
        redraw: function () {
            // Whenever the floor is re-drawn, it is highly likely that something substantial about the groups in the floor have changed
            // (necessitating the re-draw in the first place). As such, a redraw is the right time to re-validate all of the CustomConnectors.
            // Validation of the custom connectors could lead to removals of existing links if it is found that the referencing cell no longer
            // is linked.
            var modelChanged = false;
            var i = 0;
            while (i < this.joined.length) { // The array itself MAY BE MODIFIED (items removed--never added)
                var connector = this.joined[i];
                if (!validateConnector(connector)) { // Something needed to be restored...
                    modelChanged = true;
                    this.joined.splice(i, 1); // Inline remove this connector (linked set).
                    continue; // Don't increment i.
                }
                // After post-validation that the connector is still good, I can re-aquire these references directly (sans validation)
                var cellFrom = model.floor.groupById(connector.from.group).cells[connector.from.cell];
                var cellTo = model.floor.groupById(connector.to.group).cells[connector.to.cell];
                if (model.showCellOutline || (i == this.selectedIndex)) {
                    var groupFrom = model.floor.groupById(connector.from.group);
                    var groupTo = model.floor.groupById(connector.to.group);
                    ctx.beginPath(); // First line
                    ctx.strokeStyle = (i == this.selectedIndex) ? "lime" : "#888";
                    groupFrom.drawCustomConnector(cellFrom, connector.from.connectorIndex, false, true); // source
                    groupTo.drawCustomConnector(cellTo, connector.to.connectorIndex, !connector.flipped, false); // destination
                    ctx.stroke();
                    ctx.beginPath(); // Second line
                    groupFrom.drawCustomConnector(cellFrom, connector.from.connectorIndex, true, true); // source
                    groupTo.drawCustomConnector(cellTo, connector.to.connectorIndex, connector.flipped, false); // destination
                    ctx.stroke();
                }
                i++;
            }
            // Also validate the candidates (if set)
            if (!getValidCellFromCandidate(this.candidate1, 1))
                this.candidate1 = null;
            if (!getValidCellFromCandidate(this.candidate2, 2))
                this.candidate2 = null;
            if (modelChanged) {
                // Always adjust the selected index to "not selected" state...
                if (this.selectedIndex != null)
                    this.selectedIndex = null; // Causes an updateUICustomConnectorList as a side-effect
                else
                    this.updateUICustomConnectorList();
            }
        },
        setLink: function (isOne) {
            var c = model.cell;
            if (c) {
                var gId = model.group.id;
                var candidate = isOne ? this.candidate1 : this.candidate2;
                var newcandidate = null;
                if (!candidate || (candidate.group != gId) || (candidate.cell != c.id))
                    newcandidate = { group: gId, cell: c.id };
                // else // There is a candidate and it matches exactly the existing candidate...
                //    candidate = null;
                if (isOne)
                    this.candidate1 = newcandidate;
                else
                    this.candidate2 = newcandidate;
                model.floor.redraw();
            }
        },
        join: function () {
            if (!this.candidate1 || !this.candidate2)
                return;
            var candidateCell1 = getValidCellFromCandidate(this.candidate1, 1);
            var candidateCell2 = getValidCellFromCandidate(this.candidate2, 2);
            if (!candidateCell1 || !candidateCell2) { // Failed the validity test as a candidate (not likely at this point, but just making sure)
                this.candidate1 = null;
                this.candidate2 = null;
                model.floor.redraw();
                return;
            }
            // Ensure a connector is selected
            if ((candidateCell1.currentConnector == null) || (candidateCell2.currentConnector == null))
                return alert("Please select a connector within one of these cells");
            this.candidate1.connectorIndex = candidateCell1.currentConnector;
            this.candidate2.connectorIndex = candidateCell2.currentConnector;
            var link = new CustomConnectorDataConstructor(this.candidate1, this.candidate2);
            // Update the connectors within each actual cell, saving the old links (and un-delete them if they were previously deleted)..
            var conn1 = candidateCell1.connectors[candidateCell1.currentConnector];
            this.candidate1.restoreGroup = conn1.group;
            conn1.group = this.candidate2.group;        // Note: refers to the destination's link
            this.candidate1.restoreCell = conn1.cell;
            conn1.cell = this.candidate2.cell;          // Note: refers to the destination's link
            conn1.deleted = false;
            var conn2 = candidateCell2.connectors[candidateCell2.currentConnector];
            this.candidate2.restoreGroup = conn2.group;
            conn2.group = this.candidate1.group;
            this.candidate2.restoreCell = conn2.cell;
            conn2.cell = this.candidate1.cell;
            conn2.deleted = false;
            // Bookkeeping
            this.joined.push(link);
            this.selectedIndex = this.joined.length - 1; // Select the newly added link.
            this.candidate1 = null;
            this.candidate2 = null;
            this.updateUICustomConnectorList();
            model.floor.redraw();
        },
        unjoin: function () {
            if (this.selectedIndex == null)
                return;
            // The cells should be available, but you never know...
            var link = this.joined[this.selectedIndex];
            validateConnector(link, true); // This restores the connectors
            // Make the previously linked items now candidates (for easy "re-linking")
            this.candidate1 = link.from;
            this.candidate2 = link.to;
            // Remove the actual link
            this.joined.splice(this.selectedIndex, 1); // Inline remove this linked set.
            this.selectedIndex = null;
            this.updateUICustomConnectorList();
            model.floor.redraw();
        }
    };

    // CustomConnector(optional serializedCustomConnector) {
    //    <<PUBLIC>> -- serialized stuff
    //    candidate1: {} or null     // Where candidate is { group: #, cell: # } (Group Id, Cell Id)
    //    candidate2: {} or null     // Where candidate is { group: #, cell: # }
    //    joined: [ {                // List of linked cells (CustomConnectorData objects)
    //      id: #                    // Unique link id
    //      from: {                  // Group/cell to join from...
    //        group: #               // Origin group # (id)
    //        cell: #                // Origin cell # (id)
    //        connectorIndex: #      // Connector in origin cell (index) - ADDED at join time
    //        restoreGroup: #        // (For restore/undo) this cell's original connector group - ADDED at join time
    //        restoreCell: #         // (For restore/undo) this cell's original connector cell - ADDED at join time
    //      },
    //      to: { ... }              // Group/cell to join to...
    //      flipped: bool
    //      name (PRIVATE): string
    //    } ]                        // Array of cell groups
    //    selectedIndex: # or null   // Index to the joined array or null if none selected
    // }

    Object.defineProperty(window, "CustomConnector", {
        enumerable: true,
        value: function CustomConnectorConstructor(serializedCustomConnector, isDuplicate) {
            // Initialize accessors here
            var candidate1 = serializedCustomConnector ? serializedCustomConnector.candidate1 : null;
            var candidate2 = serializedCustomConnector ? serializedCustomConnector.candidate2 : null;
            var selectedIndex = serializedCustomConnector ? serializedCustomConnector.selectedIndex : null;
            // Return the instance object
            return Object.create(CustomConnectorFunctions, {
                candidate1: {
                    enumerable: true,
                    get: function () { return candidate1; },
                    set: function (x) {
                        // Either gets null (clear the old cell if any-->not linked) or an object (store it)
                        // If I was storing something, it's either now linked, or dropped (optionally in favor of another object)
                        if (candidate1) {
                            var cell = getValidCellFromCandidate(candidate1, 1);
                            if (cell)
                                cell.linkCandidate = 0;
                        }
                        candidate1 = x;
                        if (candidate1) {
                            var newCell = model.floor.groupById(candidate1.group).cells[candidate1.cell];
                            // Both candidate 1 and 2 cannot be referring to the same cell...
                            if (newCell.linkCandidate == 2)
                                this.candidate2 = null; // Set candidate 2 to null
                            newCell.linkCandidate = 1;
                        }
                        this.updateUICandidateButton(true);
                    }
                },
                candidate2: {
                    enumerable: true,
                    get: function () { return candidate2; },
                    set: function (x) {
                        if (candidate2) {
                            var cell = getValidCellFromCandidate(candidate2, 2);
                            if (cell)
                                cell.linkCandidate = 0;
                        }
                        candidate2 = x;
                        if (candidate2) {
                            var newCell = model.floor.groupById(candidate2.group).cells[candidate2.cell];
                            // Both candidate 1 and 2 cannot be referring to the same cell...
                            if (newCell.linkCandidate == 1)
                                this.candidate1 = null; // Set candidate 1 to null
                            newCell.linkCandidate = 2;
                        }
                        this.updateUICandidateButton(false);
                    }
                },
                selectedIndex: {
                    enumerable: true,
                    get: function () { return selectedIndex; },
                    set: function (x) {
                        if (x != null) // Expecting numbers or null
                            x = Math.min(this.joined.length - 1, Math.max(0, x)); // Force the incoming value into range...
                        if (x == selectedIndex) return;
                        selectedIndex = x;
                        this.updateUICustomConnectorList();
                    }
                },
                joined: {
                    enumerable: true,
                    value: serializedCustomConnector ? serializedCustomConnector.joined.map(function (serializedLink) {
                        return new CustomConnectorDataConstructor(serializedLink, null, isDuplicate);
                    }) : []
                }
            });
        }
    });

    function CustomConnectorDataConstructor(serializedDataOrLinkBindingA, linkBindingB, isDuplicate) {
        // Pass either:
        // 1) serializedData (and no second param) with optional third param...
        // 2) linkBindingA and linkingBindingB (in params 1 & 2), etc.
        var serializedData = !linkBindingB ? serializedDataOrLinkBindingA : null;
        Object.defineProperties(this, {
            id: { 
                enumerable: true,
                value: (serializedData && !isDuplicate) ? serializedData.id : model.linkID++
            },
            from: {
                enumerable: true,
                value: serializedData ? serializedData.from : serializedDataOrLinkBindingA
            },
            to: {
                enumerable: true,
                value: serializedData ? serializedData.to : linkBindingB
            },
            flipped: {
                enumerable: true,
                writable: true,
                value: serializedData ? serializedData.flipped : false
            },
            name: {
                get: function () {
                    return "(G" + this.from.group + "c" + this.from.cell + ")<-" + this.id + "->(G" + this.to.group + "c" + this.to.cell + ")";
                }
            }
        });
    }

    // Hook up the UI to the model

    // [Try to] link two candidates...
    document.getElementById("link").onclick = function () { model.floor.customConnectors.join(); };

    // Set/clear candidate1
    setLink1ButtonEl.onclick = function () { model.floor.customConnectors.setLink(true); };

    // Set/clear candidate2
    setLink2ButtonEl.onclick = function () { model.floor.customConnectors.setLink(false); };

    // Change selected joined link
    linkedCellsSelectEl.onchange = function () { 
        if ((this.selectedIndex == -1) || (this.value == -1))
            model.floor.customConnectors.selectedIndex = null;
        else
            model.floor.customConnectors.selectedIndex = parseInt(this.value);
        model.floor.redraw();
    };

    // Delete selected joined link
    document.getElementById("deleteJoinedCell").onclick = function () { model.floor.customConnectors.unjoin(); };

    // Change line connections on joined custom connectors
    swapLinkedCheckboxEl.onchange = function () {
        var ref = model.floor.customConnectors;
        ref.joined[ref.selectedIndex].flipped = !ref.joined[ref.selectedIndex].flipped;
        model.floor.redraw();
    };
});