﻿"use strict";
addEventListener("DOMContentLoaded", function GroupScope() {
    // Groups do not actual draw anything themselves--they are abstract for the UI only

    // [Quazi-globals (accessor property state)]    
    var RADIANPERDEGREE = 2 * Math.PI / 360;
    var rotateInputEl = document.getElementById("rotate");
    var transXInputEl = document.getElementById("translateX");
    var transYInputEl = document.getElementById("translateY");
    var stretchXInputEl = document.getElementById("stretchX");
    var stretchYInputEl = document.getElementById("stretchY");

    var GroupFunctions = {
        updateUI: function () {
            rotateInputEl.value = Math.floor(this.rotate / RADIANPERDEGREE);
            transXInputEl.value = this.translateX;
            transYInputEl.value = this.translateY;
            stretchXInputEl.value = this.stretchX;
            stretchYInputEl.value = this.stretchY;
        },
        pushTransformState: function () {
            ctx.save()
            ctx.translate((this.translateX * model.pixelPerUnit), (this.translateY * model.pixelPerUnit));
            ctx.rotate(this.rotate);
            ctx.transform(this.stretchX, 0, 0, this.stretchY, 0, 0);
        },
        popTransfromState: function () {
            ctx.restore();
        },
        exportTransformStateAsMatrix: function () {
            var transformState = new Matrix32();
            // Setup the transform to match the group's current transform...
            transformState.selfTranslate(this.translateX, this.translateY);
            transformState.selfRotate(this.rotate);
            transformState.selfScale(this.stretchX, this.stretchY);
            return transformState;
        },
        createSerializedCopy: function () {
            // Clone this group (adjusting the x/y offsets prior to re-consituting so that it re-appears offset). Note: the clone is not an actual Group instance yet!!
            var copiedClone = JSON.parse(JSON.stringify(this));
            copiedClone.translateX += 10; // Offset the clone by 10 (to make it visible)
            copiedClone.translateY += 10;
            return copiedClone;
        }      
    };

    // Group(optional serializedGroup) { // BASE CLASS
    //    <<PUBLIC>> -- serialized stuff
    //    rotate: #             // Rotation offset
    //    translateX: #         // Translation in X axis
    //    translateY: #         // Translation in Y axis
    //    stretchX: #           // Stretch X direction
    //    stretchY: #           // Stretch Y direction
    // }

    Object.defineProperty(window, "Group", {
        // Not enumerable (not really "public") -- Use Constructors for derived groups instead (e.g., 
        value: function GroupConstructor(derivedProtoObject, serializedGroup) {
            if (!derivedProtoObject)
                throw Error("Required parameter missing; hint: use the result of Group.newPrototypeSubclass()");
            // Instance-scoped variables (not global)
            var rotate = serializedGroup ? serializedGroup.rotate: 0;
            var translateX = serializedGroup ? serializedGroup.translateX : 10;
            var translateY = serializedGroup ? serializedGroup.translateY : 10;
            var stretchX = serializedGroup ? serializedGroup.stretchX : 1;
            var stretchY = serializedGroup ? serializedGroup.stretchY : 1;
            // Initialize accessors here
            return Object.create(derivedProtoObject, {
                rotate: {
                    enumerable: true,
                    get: function() { return rotate; },
                    set: function(x) {
                        if (rotate == x) return;
                        if (x >= (2 * Math.PI))
                            x %= (2 * Math.PI);
                        if (x < 0)
                            x = (x % (2 * Math.PI)) + (2 * Math.PI);
                        rotate = x;
                        rotateInputEl.value = Math.round(rotate / RADIANPERDEGREE); // Round, since the division might be reall, really, really close to the integer number, but not quite...
                        model.floor.redraw();
                    }
                },
                translateX: {
                    enumerable: true,
                    get: function() { return translateX; },
                    set: function (x) {
                        if (translateX == x) return;
                        translateX = x;
                        transXInputEl.value = translateX;
                        model.floor.redraw();
                    }
                },
                translateY: {
                    enumerable: true,
                    get: function() { return translateY; },
                    set: function (x) {
                        if (translateY == x) return;
                        translateY = x;
                        transYInputEl.value = translateY;
                        model.floor.redraw();
                    }
                },
                stretchX: {
                    enumerable: true,
                    get: function() { return stretchX; },
                    set: function (x) {
                        x = Math.max(0, x);
                        if (stretchX == x) return;
                        stretchX = x;
                        stretchXInputEl.value = stretchX;
                        model.floor.redraw();
                    }
                },
                stretchY: {
                    enumerable: true,
                    get: function () { return stretchY; },
                    set: function (x) {
                        x = Math.max(0, x);
                        if (stretchY == x) return;
                        stretchY = x;
                        stretchYInputEl.value = stretchY;
                        model.floor.redraw();
                    }
                }
            });
        }
    });
    // Bootstrap method for setting up proper derived prototype objects (e.g., RectGroups, etc.)
    Object.defineProperty(Group, "newPrototypeSubclass", {
        enumerable: true,
        value: function () { return Object.create(GroupFunctions); }
    });

    // Hook up the UI to the model

    // Change rotation
    rotateInputEl.onchange = function () { if (model.group) model.group.rotate = parseFloat(this.value) * RADIANPERDEGREE; };

    // Rotation CW
    document.getElementById("rotateMore").onclick = function () { if (model.group) model.group.rotate += (Math.PI / 8); }; // 1/8th of a turn

    // Rotation CCW
    document.getElementById("rotateLess").onclick = function () { if (model.group) model.group.rotate -= (Math.PI / 8); }; // 1/8th of a turn

    // Change translationX
    transXInputEl.onchange = function () { if (model.group) model.group.translateX = parseFloat(this.value); };

    // Translation X bump up
    document.getElementById("transXMore").onclick = function () { if (model.group) model.group.translateX += 10; }; // 1 full cell-length

    // Translation X bump down
    document.getElementById("transXLess").onclick = function () { if (model.group) model.group.translateX -= 10; }; // 1 full cell-length

    // Change translationY
    transYInputEl.onchange = function () { if (model.group) model.group.translateY = parseFloat(this.value); };

    // Translation Y bump up
    document.getElementById("transYMore").onclick = function () { if (model.group) model.group.translateY += 10; }; // 1 full cell-length

    // Translation Y bump dpwm
    document.getElementById("transYLess").onclick = function () { if (model.group) model.group.translateY -= 10; }; // 1 full cell-length

    // StretchX
    stretchXInputEl.onchange = function () { if (model.group) model.group.stretchX = parseFloat(this.value); };

    // Stretch X wider
    document.getElementById("stretchXMore").onclick = function () { if (model.group) model.group.stretchX *= 1.1; }; // 10% wider

    // Stretch X narrower
    document.getElementById("stretchXLess").onclick = function () { if (model.group) model.group.stretchX /= 1.1; }; // 10% narrower

    // StretchY
    stretchYInputEl.onchange = function () { if (model.group) model.group.stretchY = parseFloat(this.value); };

    // Stretch Y wider
    document.getElementById("stretchYMore").onclick = function () { if (model.group) model.group.stretchY *= 1.1; }; // 10% wider

    // Stretch Y narrower
    document.getElementById("stretchYLess").onclick = function () { if (model.group) model.group.stretchY /= 1.1; }; // 10% narrower

    // Keyboard: navigate the group's cells connectors
    document.addEventListener("keydown", function (e) {
        if (model.group) {
            var connector = null;
            switch (e.key) {
                case "i": connector = model.cell.connectors[0]; break; // UP / Parent
                case "l": connector = model.cell.connectors[1]; break; // RIGHT / CW
                case "k": connector = model.cell.connectors[3]; break; // DOWN / Child -- Only chooses one branch (no alternate down branches)
                case "j": connector = model.cell.connectors[2]; break; // LEFT / CCW
                default: return; // Not the key I'm looking for...
            }
            if (connector.cell == -1) return; // No link (edge boundary)
            if (connector.group != model.group.id) // Links to a different group...
                model.floor.currentGroup = model.groupMap[connector.group].groupIndex;
            // Now we're in the same group for sure--switch to this cell...
            model.group.currentCell = connector.cell;
        }
    });

    function createKeyboardValueSpinner(onspinCallback) {
        return function keyboardValueSpinner(e) {
            var step = e.shiftKey ? 0.5 : 1;
            switch (e.key) {
                case "Up":
                case "Right": onspinCallback(parseFloat(this.value) + step); e.stopPropagation(); break;
                case "Down":
                case "Left": onspinCallback(parseFloat(this.value) - step); e.stopPropagation(); break;
            }
        }
    }
    
    // Increment/decrement Rotation by 1 (or 0.5 if shift held)
    rotateInputEl.onkeydown = createKeyboardValueSpinner(function (newValue) { if (model.group) model.group.rotate = newValue * RADIANPERDEGREE; });

    // Increment/decrement X-translation by 1 (or 0.5 if shift held)
    transXInputEl.onkeydown = createKeyboardValueSpinner(function (newValue) { if (model.group) model.group.translateX = newValue; });

    // Increment/decrement Y-translation by 1 (or 0.5 if shift held)
    transYInputEl.onkeydown = createKeyboardValueSpinner(function (newValue) { if (model.group) model.group.translateY = newValue; });

    // Increment/decrement X-stretch by 1 (or 0.5 if shift held)
    //stretchXInputEl.onkeydown = createKeyboardValueSpinner(function (newValue) { if (model.group) model.group.stretchX = newValue; });

    // Increment/decrement Y-stretch by 1 (or 0.5 if shift held)
    //stretchYInputEl.onkeydown = createKeyboardValueSpinner(function (newValue) { if (model.group) model.group.stretchY = newValue; });
});