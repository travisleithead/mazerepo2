﻿"use strict";
addEventListener("DOMContentLoaded", function CellScope() {

    // [Quazi-globals (accessor property state)]    
    var startToggleInputEl = document.getElementById("toggleStart");
    var endToggleInputEl = document.getElementById("toggleEnd");
    var transToggleButtonEl = document.getElementById("toggleTransportToGroup");
    var currentConnectorSpanEl = document.getElementById("connectorCurrent");
    var connectorUIArray = ["\ue0e4", "\ue0e3", "\ue0e2", /* 3+ */ "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5", "\ue0e5"];

    function toggleConnectorDeletedPair(cell, connectorIndex) {
        var connector = cell.connectors[connectorIndex];
        connector.deleted = !connector.deleted || (connector.cell == -1); // While an exterior wall, the connector remains deleted regardless of the toggle
        if (connector.cell != -1) { // It points to another cell...
            // Try to also update the cell this connector points to...
            var otherGroup = model.floor.groupById(connector.group);
            var otherCell = otherGroup.cells[connector.cell];
            var otherConnectors = otherCell.connectors;
            for (var i = 0, len = otherConnectors.length; i < len; i++) {
                if ((otherConnectors[i].cell == cell.id) && (otherConnectors[i].group == model.group.id)) {
                    otherConnectors[i].deleted = connector.deleted; // Gets the same status.
                    otherGroup.redrawCell(false, otherCell.id);
                    return;
                }
            }
        }
    }

    function updateUI_startCell(startCellVal) { startToggleInputEl.value = "Start Cell: " + (startCellVal ? "On" : "Off"); }
    function updateUI_endCell(endCellVal) { endToggleInputEl.value = "End Cell: " + (endCellVal ? "On" : "Off"); }
    function updateUI_transportCell(transGroupMap) {
        // If the current transport group selected (if any) matches one of the groups in this cell, then it's "ON" otherwise not.
        var selectedGroup;
        var newState = "No"; // default
        if (transGroupMap && (selectedGroup = model.transportManager.group) && transGroupMap[selectedGroup.id])
            newState = "Yes";
        transToggleButtonEl.innerHTML = "Group Member: " + newState;
    }

    var CellPrototype = {
        updateUI: function () {
            // Sync the toggle states
            updateUI_startCell(this.startCell);
            updateUI_endCell(this.endCell);
            updateUI_transportCell(this.transGroup);
            // Update Current cell id
            currentConnectorSpanEl.innerHTML = (this.currentConnector == null) ? "" : connectorUIArray[this.currentConnector];
        },
        addConnector: function(group, cell, deleted) { // 'cell' is -1 if not connected to anything.
            this.connectors.push({ // Lightweight "Connector" object (data only)
                group: group,
                cell: cell,
                deleted: deleted
            });
        },
        toggleDeleted: function(optionalCellIdToRedraw) {
            this.deleted = !this.deleted;
            for (var i = 0, len = this.connectors.length; i < len; i++)
                toggleConnectorDeletedPair(this, i);
            model.group.redrawCell(optionalCellIdToRedraw ? false : true, optionalCellIdToRedraw); // If providing an optional redraw cell, assume that it shouldn't be drawn selected.
        },
        toggleConnectorDeleted: function() {
            if (this.currentConnector == null) return;
            toggleConnectorDeletedPair(this, this.currentConnector);
            model.group.redrawCell(true);
        },
        toggleConnector: function(index, forceOn) { // 0-3, 3 has cyclical quality
            var connectIndex = this.currentConnector;
            if (connectIndex >= 3)
                connectIndex = (++connectIndex == this.connectors.length) ? 3 : connectIndex; // Forces to 3 so that the next 'if' clause resets it to null
            if (connectIndex == index)
                this.currentConnector = forceOn ? index : null;
            else
                this.currentConnector = (connectIndex > 3) ? connectIndex : index;
            // Update connector UI
            currentConnectorSpanEl.innerHTML = (this.currentConnector == null) ? "" : connectorUIArray[this.currentConnector];
            model.group.redrawCell(true);
        },
        toggleStartEnd: function(isStart) {
            if (isStart)
                updateUI_startCell(this.startCell = !this.startCell);
            else
                updateUI_endCell(this.endCell = !this.endCell);
            model.group.redrawCell(true);
        },
        toggleTransportGroup: function (group) {
            // If the cell is not a member of any transport group or is a member of another transport group, but not this one...
            if (!this.hasTransportGroupMembership(group.id)) {
                // ADD IT (first to the cell's inventory, then to the group's inventory)
                if (!this.transGroup)
                    this.transGroup = {};
                this.transGroup[group.id] = true; // Add this group to the cell's transport group map.
                this.currentTransGroup = group.id; // This will always be a "current" view, in order to reach this code.
                group.cellCount++;
                if (group.cellMap[model.group.id]) // Key'd by the cell's containing group Id, it's a list of cell ids in this group that are members...
                    group.cellMap[model.group.id].push(this.id);
                else
                    group.cellMap[model.group.id] = [this.id];
            }
            else { // The cell's already a member of this group, toggle it off
                if (Object.keys(this.transGroup).length == 1)
                    this.transGroup = null; // Full clear since this was the only transport Group
                else
                    delete this.transGroup[group.id]; // Remove this key.
                this.currentTransGroup = null; // Since one group cannot have duplicate cell entries, this won't be selected
                group.cellCount--;
                if (group.cellMap[model.group.id].length == 1) // Cleanup the key itself...
                    delete group.cellMap[model.group.id];
                else { // There will still be another cell in the group's list after this is deleted
                    var array = group.cellMap[model.group.id];
                    array.splice(array.indexOf(this.id), 1);
                }
            }
            // Note: The caller initiates the updateUI call...
            model.group.redrawCell(true);
        },
        hasTransportGroupMembership: function (transportGroupId) {
            return (this.transGroup && this.transGroup[transportGroupId]);
        },
        clean: function (newId, preserveDeletedState) {
            this.id = newId;
            // location will be updated/reset by caller
            this.startCell = false;
            this.endCell = false;
            this.linkCandidate = 0; // Force this to not participating (if it was before)--the cell's ID is no longer valid for the old link
            this.currentTransGroup = null;
            this.transGroup = null;
            this.currentConnector = null;
            this.connectors.splice(0, 50); // Clear all (50 should be plenty)
            if (!preserveDeletedState)
                this.deleted = false;
        },
        trimForeignConnectors: function (originGroupId) {
            for (var i = 0, len = this.connectors.length; i < len; i++) {
                if (this.connectors[i].group != originGroupId) {
                    this.connectors[i].cell = -1; // Force a terminal connection here
                    this.connectors[i].group = originGroupId; // Force same-group association
                    this.connectors[i].deleted = true; // Indicate that this connection is deleted
                }
            }
        }
    };

    // Cell(optional serializedCell) {
    //    // PUBLIC (serialized stuff)
    //    id: #                         // Relative (unique to the group) identifier
    //    location: [...]               // Array of #'s (interpretation depends on the group-type using the cell: for Rect it is x/y coordinate pair of center of the cell (in units), for Circle it is: ring # (not in units)/cw-start/cw-end)
    //    startCell: bool               // Is this cell a starting cell candidate?
    //    endCell: bool                 // Is this cell an ending cell candidate?
    //    linkCandidate: #              // 0, 1, 2 (1 = candidate 1, 2 = candidate 2, 0 = not a candidate)
    //    currentTransGroup: # or null  // Which transgroup is active
    //    transGroup: {} or null        // Transport group map (key'd by transport groupId, value is true in all cases)
    //    currentConnector: # or null   // index into the connectors array whichever one is selected (or null for not)
    //    connectors: [ {               // Array of connectors (order interpretation up to the containing group)
    //      group: #                    // Unique group id of the cell to which this connector links
    //      cell: #                     // The cell id to which this connector references (-1 if not linked)
    //      deleted: bool               // Is this connector deleted/removed or not? (Toggles vs. actually deleting)
    //    }... ]
    //    deleted: bool                 // Whether this cell is deleted
    // }

    Object.defineProperty(window, "Cell", {
        enumerable: true,
        value: function CellConstructor(serializedCellOrId, isDuplicate, newGroupId) {
            var initialId = 0;
            if (typeof serializedCellOrId == "number") {
                initialId = serializedCellOrId;
                serializedCellOrId = null;
            }
            else
                initialId = serializedCellOrId.id; // Load the serialized ID.
            // Initialize accessors here
            var currentConnector = serializedCellOrId ? serializedCellOrId.currentConnector : null;
            return Object.create(CellPrototype, {
                id: {
                    enumerable: true,
                    writable: true,
                    value: initialId
                },
                location: {
                    enumerable: true,
                    value: serializedCellOrId ? serializedCellOrId.location : [] // read/write array - specific group-types must initialize the contents of this array.
                },
                startCell: {
                    enumerable: true,
                    writable: true,
                    value: (serializedCellOrId && !isDuplicate) ? serializedCellOrId.startCell : false      // START cells are not duplicated when cloning floors
                },
                endCell: {
                    enumerable: true,
                    writable: true,
                    value: (serializedCellOrId && !isDuplicate) ? serializedCellOrId.endCell : false        // END cells are not duplicated when cloning floors
                },
                linkCandidate: {
                    enumerable: true,
                    writable: true,
                    value: serializedCellOrId ? serializedCellOrId.linkCandidate : 0
                },
                currentTransGroup: {
                    enumerable: true,
                    writable: true,
                    value: (serializedCellOrId && !isDuplicate) ? serializedCellOrId.currentTransGroup: null    // Selected Transport group is not duplicated when cloning floors
                },
                transGroup: {
                    enumerable: true,
                    writable: true,
                    value: (serializedCellOrId && !isDuplicate) ? serializedCellOrId.transGroup : null          // Transport group inclusions are not duplicated when cloning floors (because the core data structures are not floor specific)
                },
                currentConnector: {
                    enumerable: true,
                    get: function () { return currentConnector; },
                    set: function (x) {
                        if (x != null)
                            x = Math.min(this.connectors.length - 1, Math.max(0, x));
                        if (x == currentConnector) return;
                        currentConnector = x;
                        this.updateUI();
                        model.group.redrawCell(true);
                    }
                },
                connectors: {
                    enumerable: true,
                    // A duplicated cell (from another floor) needs to re-bind it's "group" association now...(linked group associations are fixed--by overwriting--just after construction)
                    value: isDuplicate ? serializedCellOrId.connectors.map(function (conn) { conn.group = newGroupId; return conn; }) : 
                        (serializedCellOrId ? serializedCellOrId.connectors : [])
                },
                deleted: {
                    enumerable: true,
                    writable: true,
                    value: serializedCellOrId ? serializedCellOrId.deleted : false
                }
            });
        }
    });

    // Hook up the UI to the model

    // Delete/undelete cell
    document.getElementById("cellDelete").onclick = function () { var c = null; if (c = model.cell) c.toggleDeleted(); };

    // Keyboard: delete/undelete cell
    document.addEventListener("keyup", function (e) { var c = null; if ((e.key == "m") && (c = model.cell)) c.toggleDeleted(); });

    // Toggle start cell
    startToggleInputEl.onclick = function () { var c = null; if (c = model.cell) c.toggleStartEnd(true); };

    // Toggle end cell
    endToggleInputEl.onclick = function () { var c = null; if (c = model.cell) c.toggleStartEnd(false); };

    // Keyboard: toggle start/end cell
    var toggleStartEndKeyMap = { "z": true, "/": true }; // Does a quick key check (faster) than doing a model.cell check (slower--due to multiple getters)
    document.addEventListener("keyup", function (e) {
        var c = null;
        if (toggleStartEndKeyMap[e.key] && (c = model.cell)) {
            if (e.key == "z") c.toggleStartEnd(true);
            if (e.key == "/") c.toggleStartEnd(false);
        }
    });

    // Toggle parent connector
    document.getElementById("connectorUp").onclick = function () { var c = null; if (c = model.cell) c.toggleConnector(0); };

    // Toggle CW connector / Right
    document.getElementById("connectorCW").onclick = function () { var c = null; if (c = model.cell) c.toggleConnector(1); };

    // Toggle CCW connector / Left
    document.getElementById("connectorCCW").onclick = function () { var c = null; if (c = model.cell) c.toggleConnector(2); };

    // Toggle any down connectors
    document.getElementById("connectorDown").onclick = function () { var c = null; if (c = model.cell) c.toggleConnector(3); };

    // Delete/undelete connector
    document.getElementById("toggleConnectorDelete").onclick = function () { var c = null; if (c = model.cell) c.toggleConnectorDeleted(); };

    // Keyboard: delete/undelete connector
    var toggleConnectorDeletedKeyMap = { "w": true, "a": true, "s": true, "d": true };
    document.addEventListener("keyup", function (e) {
        var c = null;
        if (toggleConnectorDeletedKeyMap[e.key] && (c = model.cell)) {
            switch (e.key) {
                case "w": c.toggleConnector(0, true); c.toggleConnectorDeleted(); break; // Up / parent
                case "a": c.toggleConnector(2, true); c.toggleConnectorDeleted(); break; // Left / CCW
                case "s": c.toggleConnector(3, true); c.toggleConnectorDeleted(); break; // Down / children
                case "d": c.toggleConnector(1, true); c.toggleConnectorDeleted(); break; // Right / CW
            }
        }
    });
});