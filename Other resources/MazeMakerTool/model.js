﻿"use strict";
addEventListener("DOMContentLoaded", function ModelScope() {

    // [Quazi-globals (accessor property state)]
    var currentFloor = 0;
    var pixelPerUnit = 4;
    var currentCollectible = null;
    var showCellOutline = true;
    var showConnectors = true;
    var showCenter = true;
    var showCellToggles = true;
    var showDeleted = true;
    var floorSelectEl = document.getElementById("floors");
    var duplicateCheckboxEl = document.getElementById("dupFloor");
    var whichCollectibleSelectEl = document.getElementById("stationaryItemType");
    var collectibleListSelectEl = document.getElementById("collectiblesList");
    var collectibleExtraGroupDivEl = document.getElementById("collectibleExtras");
    var pixelSizeTextInputEl = document.getElementById("viewRatio");
    var showItemsCheckboxEl = document.getElementById("showItems");
    var showConnectorsCheckboxEl = document.getElementById("showConnectors");
    var showCenterCheckboxEl = document.getElementById("showCenter");
    var showTogglesCheckboxEl = document.getElementById("showStartEnd");
    var showDeletedCheckboxEl = document.getElementById("showDeletedCells");
    var ctxTopRuler = document.getElementById("topRuler").getContext('2d');
    var ctxLeftRuler = document.getElementById("leftRuler").getContext('2d');
    window.ctx = document.getElementById("mainCanvas").getContext('2d'); // Expose this truly globally!
    var dirtyCanvas = true;

    // Helpers
    
    function updateCanvasDimensions() {
        ctxTopRuler.canvas.width = ctxTopRuler.canvas.offsetWidth;
        ctxTopRuler.canvas.height = ctxTopRuler.canvas.offsetHeight;
        ctxLeftRuler.canvas.width = ctxLeftRuler.canvas.offsetWidth;
        ctxLeftRuler.canvas.height = ctxLeftRuler.canvas.offsetHeight;
        ctx.canvas.width = ctx.canvas.offsetWidth;
        ctx.canvas.height = ctx.canvas.offsetHeight;
        dirtyCanvas = false;
    }

    function applyRulerContextStyles(c) {
        c.fillStyle = "#222";
        c.lineWidth = 1;
        c.strokeStyle = "#888";
        c.font = "8pt 'Segoe UI'";
    }

    function createImmutableMapReference(fi, gi) {
        return Object.create(null, { 
            floorIndex: {
                enumerable: true, // NOT WRITABLE or CONFIGURABLE
                value: fi
            },
            groupIndex: {
                enumerable: true, // NOT WRITABLE or CONFIGURABLE
                value: gi
            }
        });
    }

    function updateUIHelper_refreshFloorSelect() {
        // Re-build the floor select control to match the new arrangement
        floorSelectEl.innerHTML = "";
        for (var i = model.floors.length - 1; i >= 0; i--)
            floorSelectEl.add(new Option("Floor " + (i + 1) + " (id#" + model.floors[i].id + ")", i, false, (currentFloor == i)));
    }

    function updateUIHelper_refreshCollectibleSelect() {
        // Re-build the floor select control to match the new arrangement
        collectibleListSelectEl.innerHTML = "";
        for (var i = 0, len = model.collectibles.length; i < len; i++)
            collectibleListSelectEl.add(new Option((i + 1) + ": " + model.collectibles[i].friendlyName, i, false, (currentCollectible == i)));
    }

    function updateUIHelper_hideCollectibleDetails() {
        collectibleExtraGroupDivEl.setAttribute("data-show", "none");
    }
    // Instance prototype methods

    // 'this' will be the model object...
    var ModelFunctions = {
        updateUI: function () { // Recursive
            // Sync all the UI
            // Ensure the floor selectors is up-to-date
            updateUIHelper_refreshFloorSelect();
            duplicateCheckboxEl.checked = this.duplicateFloor;
            // Ensure pixels-per-unit is updated
            pixelSizeTextInputEl.value = pixelPerUnit;
            // Ensure view-states are accurate
            showItemsCheckboxEl.checked = showCellOutline;
            showConnectorsCheckboxEl.checked = showConnectors;
            showCenterCheckboxEl.checked = showCenter;
            showTogglesCheckboxEl.checked = showCellToggles;
            this.floor.updateUI();
            this.transportManager.updateUI(); // Update the transport group manager as well
            // Make the collectibles management current
            updateUIHelper_refreshCollectibleSelect();
            if (currentCollectible != null)
                this.collectibles[currentCollectible].updateUI();
        },
        insFloor: function (isAbove) { // Doesn't switch to the new floor.
            var f = null;
            if (this.duplicateFloor)
                f = new Floor(JSON.parse(JSON.stringify(this.floor)), true);
            else
                f = new Floor();
            this.floors.splice(currentFloor + (isAbove ? 1 : 0), 0, f);
            if (!isAbove)
                currentFloor++; // Pushing in item before the current index shifts the index up.
            // Since the floor order is potentially changing, refresh each of the existing floor's groupMap info
            this.floors.forEach(function (floor, index) { floor.syncModelGroupMap({ floorIndex: index }); });
            updateUIHelper_refreshFloorSelect();
            if (this.duplicateFloor)
                f.customConnectorsPostDuplicateUpdate(); // Ensure the old joined/selected groups are transitioned to the brand new group IDs after duplication
            this.transportManager.validateAndUpdateUI(); // Validate the transport manager that the floor is added
        },
        delFloor: function () { // May switch floors
            if (this.floors.length == 1) return; // Can't delete the last floor.
            // First clear out the existing floor's groupMap info
            this.floor.syncModelGroupMap({ clearAll: true });
            // Remove the floor
            this.floors.splice(currentFloor, 1);
            if (currentFloor == this.floors.length)
                currentFloor--; // Can't have an out-of-range currentFloor
            // Order may have changed, refresh this data in the groupMap...
            this.floors.forEach(function (floor, index) { floor.syncModelGroupMap({ floorIndex: index }); });
            updateUIHelper_refreshFloorSelect();
            this.transportManager.validateAndUpdateUI(); // Validate the transport manager that the floor is removed
            this.floor.updateUI();
            this.floor.redraw();
        },
        redraw: function () { // Optional mouse coordinates...
            if (dirtyCanvas) 
                updateCanvasDimensions();
            // Redraw the content in the current floor only.
            this.floor.redraw();
        },
        refreshRulers: function (mouseX, mouseY) {
            // Redraw the top ruler
            applyRulerContextStyles(ctxTopRuler);
            ctxTopRuler.clearRect(0, 0, ctxTopRuler.canvas.width, ctxTopRuler.canvas.height);
            for (var pos = pixelPerUnit * 10, max = ctxTopRuler.canvas.width; pos < max; pos += (pixelPerUnit * 10)) {
                ctxTopRuler.beginPath();
                ctxTopRuler.moveTo(pos, 0);
                ctxTopRuler.lineTo(pos, 16);
                ctxTopRuler.stroke();
                ctxTopRuler.strokeText((pos / pixelPerUnit), pos + 2, 15);
            }
            if (mouseX) {
                ctxTopRuler.beginPath();
                ctxTopRuler.strokeStyle = "lime";
                ctxTopRuler.lineWidth = 2;
                ctxTopRuler.moveTo(mouseX, 0);
                ctxTopRuler.lineTo(mouseX, 20);
                ctxTopRuler.stroke();
            }
            // Draw the left ruler
            applyRulerContextStyles(ctxLeftRuler);
            ctxLeftRuler.clearRect(0, 0, ctxLeftRuler.canvas.width, ctxLeftRuler.canvas.height);
            for (var pos = pixelPerUnit * 10, max = ctxLeftRuler.canvas.height; pos < max; pos += (pixelPerUnit * 10)) {
                ctxLeftRuler.beginPath();
                ctxLeftRuler.moveTo(0, pos);
                ctxLeftRuler.lineTo(15, pos);
                ctxLeftRuler.stroke();
                ctxLeftRuler.strokeText((pos / pixelPerUnit), 2, pos + 12);
            }
            if (mouseY) {
                ctxLeftRuler.beginPath();
                ctxLeftRuler.strokeStyle = "lime";
                ctxLeftRuler.lineWidth = 2;
                ctxLeftRuler.moveTo(0, mouseY);
                ctxLeftRuler.lineTo(20, mouseY);
                ctxLeftRuler.stroke();
            }
        },
        addCollectible: function () {
            this.collectibles.unshift(new Collectible(whichCollectibleSelectEl.value)); // String overload of the Constructor...
            currentCollectible = 0; // Shifts onto the front, so this will always be true here.
            updateUIHelper_refreshCollectibleSelect();
            this.collectibles[currentCollectible].updateUI();
        },
        delCollectible: function () {
            if (currentCollectible == null) return; // Nothing selected
            this.collectibles.splice(currentCollectible, 1);
            if (this.collectibles.length == 0) { // Last item deleted
                this.currentCollectible = null; // unselects and updates UI.
                return;
            }
            if (currentCollectible == this.collectibles.length)
                currentCollectible--;
            updateUIHelper_refreshCollectibleSelect();
            this.collectibles[currentCollectible].updateUI();
        },
        // APIs for managing the groupMap data in a consistent way
        mapDel: function (existingId) {
            if (!this.groupMap[existingId]) throw new Error("id doesn't exist! Something when wrong in maintaining the groupMap?");
            delete this.groupMap[existingId];
        },
        mapUpdate: function (existingId, newFloorIndex, newGroupIndex) { // Also adds values that are missing.
            var potentialObjectToUpdate = this.groupMap[existingId];
            if (potentialObjectToUpdate && (potentialObjectToUpdate.floorIndex == newFloorIndex) && (potentialObjectToUpdate.groupIndex == newGroupIndex))
                return; // Nothing to do
            else
                this.groupMap[existingId] = createImmutableMapReference(newFloorIndex, newGroupIndex);
        },
        // Returns a reference to a group within the model (assumed or throws)
        groupById: function (groupId) {
            var candidateGroup = this.groupMap[groupId];
            if (!candidateGroup) return null;
            return this.floors[candidateGroup.floorIndex].groups[candidateGroup.groupIndex];
        }
    };

    // The model drives UI updates

    // Model(optional serializedModel) {
    //    // PUBLIC (serialized stuff)
    //    floorID: #                // Globally unique next available index
    //    groupID: #                // Globally unique next available index
    //    linkID: #                 // Globally unique next available index
    //    transportGroupID: #       // Globally unique next available index
    //    floors: []                // Array of Floor
    //    currentFloor: #           // index into floors array
    //    duplicateFloor: bool      // State of the duplicate floor checkbox
    //    transportManager: {}      // Transport Manager (see transportgroup.js)
    //    collectibles: []          // Array of Collectible (stuff to put on the path through the maze)
    //    currentCollectible: # or null // index into the collectibles array
    //    pixelPerUnit: num         // Zoom state for rendering
    //    showCellOutline: bool     // Render the cell outline
    //    showConnectors: bool      // Render the cell's connectors
    //    showCenter: bool          // Render the cell's center point
    //    showCellToggles: bool     // Render start/ end/ transport cells
    //    showDeleted: bool         // Render deleted cells
    //    groupMap: {}              // For every Group in a Floor, this stores the relevant {floorIndex:#, groupIndex:#} keyed by the group ID (unique)
    //    // PRIVATE (not serialized)
    //    floor: {}                 // Direct ref to this floor (for UI handlers to use for convenience in global scope)
    //    group: {} or null         // Direct ref to an active group or null if there is none
    //    cell: {} or null          // Direct ref to the active cell of an active group or null if there is none
    //    collectible: {} or null   // Direct ref to the selected collectible or null if there is none
    // }

    Object.defineProperty(window, "Model", {
        enumerable: true,
        value: function ModelConstructor(serializedModel) {
            // Initialize accessors here
            currentFloor = serializedModel ? serializedModel.currentFloor : 0;
            pixelPerUnit = serializedModel ? serializedModel.pixelPerUnit : 4;
            currentCollectible = serializedModel ? serializedModel.currentCollectible : null;
            showCellOutline = serializedModel ? serializedModel.showCellOutline : true;
            showConnectors = serializedModel ? serializedModel.showConnectors : true;
            showCenter = serializedModel ? serializedModel.showCenter : true;
            showCellToggles = serializedModel ? serializedModel.showCellToggles : true;
            showDeleted = serializedModel ? serializedModel.showDeleted : true;
            setImmediate(function () {
                if (model.floors.length == 0)
                    model.floors.push(new Floor()); // Done async so that the Floor object can refer to the "model" global variable and get its ID.
                model.redraw();
                model.refreshRulers();
                model.updateUI();
            });
            // Return the instance object
            return Object.create(ModelFunctions, {
                floors: {
                    enumerable: true, // enumerable stuff are the things I want to have serialized
                    value: serializedModel ? serializedModel.floors.map(function(floor) { return new Floor(floor); }) : [] // Default floor added async after creation....
                },
                floorID: {
                    enumerable: true,
                    writable: true,
                    value: serializedModel ? serializedModel.floorID : 1
                },
                groupID: {
                    enumerable: true,
                    writable: true,
                    value: serializedModel ? serializedModel.groupID : 1
                },
                linkID: {
                    enumerable: true,
                    writable: true,
                    value: serializedModel ? serializedModel.linkID : 1
                },
                transportGroupID: {
                    enumerable: true,
                    writable: true,
                    value: serializedModel ? serializedModel.transportGroupID : 1
                },
                currentFloor: {
                    enumerable: true,
                    get: function() { return currentFloor; },
                    set: function (newFloorIndex) {
                        // Force the incoming value into range
                        newFloorIndex = Math.min(this.floors.length - 1, Math.max(0, newFloorIndex));
                        if (currentFloor == newFloorIndex) return;
                        currentFloor = newFloorIndex;
                        // Update the Selected Floor index.
                        floorSelectEl.selectedIndex = (this.floors.length - 1 - currentFloor); // Since I inserted the options "upside-down" must index in reverse as well.
                        this.floor.updateUI();
                        this.transportManager.updateUI(); // Update the transport group manager as well
                        this.floor.redraw();
                    }
                },
                duplicateFloor: {
                    enumerable: true,
                    writable: true,
                    value: serializedModel ? serializedModel.duplicateFloor : true
                },
                transportManager: {
                    enumerable: true,
                    value: serializedModel ? new TransportManager(serializedModel.transportManager) : new TransportManager()
                },
                collectibles: {
                    enumerable: true,
                    value: serializedModel ? serializedModel.collectibles.map(function (col) { return new Collectible(col); }) : []
                },
                currentCollectible: {
                    enumerable: true,
                    get: function() { return currentCollectible; },
                    set: function(x) {
                        if (x == null) {
                            currentCollectible = null;
                            if (this.collectibles.length == 0)
                                collectibleListSelectEl.innerHTML = "";
                            updateUIHelper_hideCollectibleDetails();
                            return;
                        }
                        // Force the incoming value into range...
                        x = Math.min(this.collectibles.length - 1, Math.max(0, x));
                        if (currentCollectible == x) return;
                        currentCollectible = x;
                        // Update the selected collectible in the list...
                        collectibleListSelectEl.selectedIndex = x;
                        // Force a re-draw of that collectible's data
                        this.collectibles[currentCollectible].updateUI();
                    }
                },
                pixelPerUnit: {
                    enumerable: true,
                    get: function () { return pixelPerUnit; },
                    set: function (newPixelsPerUnit) {
                        newPixelsPerUnit = Math.max(1, newPixelsPerUnit);
                        if (pixelPerUnit == newPixelsPerUnit) return;
                        pixelPerUnit = newPixelsPerUnit;
                        pixelSizeTextInputEl.value = newPixelsPerUnit;
                        dirtyCanvas = true;
                        this.redraw();
                        this.refreshRulers();
                    }
                },
                showCellOutline: {
                    enumerable: true,
                    get: function () { return showCellOutline; },
                    set: function (x) {
                        if (x == showCellOutline) return;
                        showCellOutline = x;
                        this.floor.redraw();
                    }
                },
                showConnectors: {
                    enumerable: true,
                    get: function () { return showConnectors; },
                    set: function (x) { 
                        if (x == showConnectors) return;
                        showConnectors = x;
                        this.floor.redraw();
                    }
                },
                showCenter: {
                    enumerable: true,
                    get: function () { return showCenter; },
                    set: function (x) { 
                        if (x == showCenter) return;
                        showCenter = x;
                        this.floor.redraw();
                    }
                },
                showCellToggles: {
                    enumerable: true,
                    get: function () { return showCellToggles; },
                    set: function (x) {
                        if (x == showCellToggles) return;
                        showCellToggles = x;
                        this.floor.redraw();
                    }
                },
                showDeleted: {
                    enumerable: true,
                    get: function () { return showDeleted; },
                    set: function (x) {
                        if (x == showDeleted) return;
                        showDeleted = x;
                        this.floor.redraw();
                    }
                },
                groupMap : {
                    enumerable: true,
                    // Convert the mutable serialized map structure into an immutable structure
                    value: serializedModel ? Object.keys(serializedModel.groupMap).reduce(function (newMap, keyToAdd) {
                        newMap[keyToAdd] = createImmutableMapReference(serializedModel.groupMap[keyToAdd].floorIndex, serializedModel.groupMap[keyToAdd].groupIndex);
                        return newMap;
                    }, {}) : {}
                },
                // END PUBLIC
                floor: {
                    get: function () { return this.floors[currentFloor]; }
                },
                group: {
                    get: function () { return (this.floor.currentGroup != null) ? this.floor.groups[this.floor.currentGroup] : null; }
                },
                cell: { // If a group is available, a cell will be selected
                    get: function () { var g = this.group; return (g != null) ? g.cells[g.currentCell] : null; }
                },
                collectible: {
                    get: function () { return (this.currentCollectible != null) ? this.collectibles[this.currentCollectible] : null; }
                }
            });
        }
    });

    // Hook up the UI to the model

    // Serialize the app state and export
    document.getElementById("savebutton").onclick = function () {
        // Save (export) the maze state (as-is)
        navigator.msSaveBlob(new Blob([JSON.stringify(model)]), "New Maze.maze");
    };

    // Load the app state
    document.getElementById("fileopen").onchange = function () {
        // Replace the current model with a loaded one.
        if (this.files[0] && (this.files[0].name.indexOf(".maze") != -1)) {
            var reader = new FileReader();
            reader.onload = function () {
                var realizedModelObject = null;
                try {
                    realizedModelObject = JSON.parse(reader.result)
                }
                catch (ex) {
                    alert("file was corrupted");
                    return;
                }
                model = new Model(realizedModelObject);
            }
            reader.readAsText(this.files[0]);
        }
    };

    // Floor down
    document.getElementById("goDown").onclick = function () { model.currentFloor--; };

    // Swap floors
    floorSelectEl.onchange = function () { model.currentFloor = parseInt(this.value); };

    // Floor up
    document.getElementById("goUp").onclick = function () { model.currentFloor++; };

    // Delete floor
    document.getElementById("floorDelete").onclick = function () { model.delFloor(); };

    // Duplicate floor option
    duplicateCheckboxEl.onclick = function () { model.duplicateFloor = this.checked; };

    // New floor above
    document.getElementById("createAbove").onclick = function () { model.insFloor(true); };

    // New floor below
    document.getElementById("createBelow").onclick = function () { model.insFloor(false); };

    // New collectible
    document.getElementById("newCollectible").onclick = function () { model.addCollectible(); };

    // Change collectible
    document.getElementById("collectiblesList").onchange = function () { model.currentCollectible = (this.selectedIndex < 0) ? null : this.selectedIndex; };

    // Delete collectible
    document.getElementById("collectibleDelete").onclick = function () { model.delCollectible(); };

    // Zoom change
    pixelSizeTextInputEl.onchange = function () { model.pixelPerUnit = parseFloat(this.value); };

    // Zoom more
    document.getElementById("viewLarger").onclick = function () { model.pixelPerUnit++; };

    // Zoom less
    document.getElementById("viewSmaller").onclick = function () { model.pixelPerUnit--; };

    // Show cells
    showItemsCheckboxEl.onclick = function () { model.showCellOutline = this.checked; };

    // Show connectors
    showConnectorsCheckboxEl.onclick = function () { model.showConnectors = this.checked; };

    // Show center of cells
    showCenterCheckboxEl.onclick = function () { model.showCenter = this.checked; };

    // Show start/end/transport cell groups
    showTogglesCheckboxEl.onclick = function () { model.showCellToggles = this.checked; };

    // Show deleted cells (at all)
    showDeletedCheckboxEl.onclick = function () { model.showDeleted = this.checked; };

    // Keyboard floor-switches
    document.addEventListener("keyup", function (e) {
        if (e.key == "-")
            model.currentFloor--;
        if (e.key == "=")
            model.currentFloor++;
    });

    // Window resizes
    addEventListener("resize", function () { dirtyCanvas = true; model.redraw(); model.refreshRulers(); });

    // Mouse location on the ruler update
    ctx.canvas.addEventListener("mousemove", function (e) { model.refreshRulers(e.offsetX, e.offsetY); });

    onbeforeunload = function (e) { e.returnValue = "Your progress will be lost! Be sure you've saved it..."; }
});

// One-time load
addEventListener("load", function init() { window.model = new Model(); });