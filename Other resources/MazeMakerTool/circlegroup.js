﻿"use strict";
addEventListener("DOMContentLoaded", function CircGroupScope() {

    // [Quazi-globals (accessor property state)]    
    var circleGroupPrototype = null;   // Created only once, the first time...
    var ringsInputEl = document.getElementById("circRings");
    var distributionSelectEl = document.getElementById("distribution");
    var cellNumSpanEl = document.getElementById("currentCell");


    function toView(num) { return Math.floor(model.pixelPerUnit * num); }

    function drawCell(cell, isSelected) {
        if (cell.location[0] == 0)
            return drawCenterCell(cell, isSelected); // Everything about the center cell is special-cased logic.
        var spoke = [
				    toView(cell.location[0] * 10),            // [0] center line (radius)
				    toView((cell.location[0] * 10) - 4.5),    // [1] parent line (radius)
				    toView((cell.location[0] * 10) + 4.5)];   // [2] child line (radius)
        var start = cell.location[1];   // Clockwise-starting angle (at the 12 o'clock position: left-side)
        var end = cell.location[2];     // Clockwise-ending angle   (at the 12 o'clock position: right-side)
        var proportionalGap = ((end - start) * 0.05); // 5% of the actual angle spread (absolute amount depends on radius)
        start += proportionalGap;
        end -= proportionalGap;
        var mid = start + ((end - start) / 2); // "center" angle
        var cellCenterX = spoke[0] * Math.cos(mid);     // Computed X coordinate pair based on angle/radius
        var cellCenterY = spoke[0] * Math.sin(mid);     // Computed Y coordinate pair based on angle/radius
        if (cell.linkCandidate > 0) {
            // Render a background to signal this cell as a "marked" cell for potential linking
            // 1 = purple, 2 = blue
            ctx.fillStyle = (cell.linkCandidate == 1) ? "purple" : "blue";
            ctx.beginPath();
            ctx.arc(0, 0, spoke[1], end, start, true);
            ctx.arc(0, 0, spoke[2], start, start, true); // Draw line from last point to a point at the child-side.
            ctx.arc(0, 0, spoke[2], start, end, false);  // Child-side arc
            ctx.arc(0, 0, spoke[1], end, end, false);
            ctx.fill();
        }
        if (cell.deleted) {
            if (model.showDeleted && (model.showCellOutline || isSelected)) {
                ctx.strokeStyle = isSelected ? "lime" : "#444";
                ctx.beginPath();
			    ctx.arc(0, 0, spoke[1], end, start, true);
			    ctx.arc(0, 0, spoke[2], start, start, true); // Draw line from last point to a point at the child-side.
			    ctx.arc(0, 0, spoke[2], start, end, false);  // Child-side arc
			    ctx.arc(0, 0, spoke[1], end, end, false);
			    ctx.stroke();
            }
            if (model.showDeleted && model.showCenter) {
                ctx.strokeStyle = "#444";
                ctx.beginPath(); // Draw an 'X' over the box.
                ctx.moveTo(spoke[1] * Math.cos(start), spoke[1] * Math.sin(start)); // parent-start -> child-end
                ctx.lineTo(spoke[2] * Math.cos(end),   spoke[2] * Math.sin(end));
                ctx.moveTo(spoke[1] * Math.cos(end),   spoke[1] * Math.sin(end)); // parent-end -> child-start
                ctx.lineTo(spoke[2] * Math.cos(start), spoke[2] * Math.sin(start));
                ctx.stroke();
            }
            return;
        }
        //// Not deleted...
        if (model.showConnectors) { /////////////////////////////////////// DRAW CONNECTORS
            var insideStart = mid - ((mid - start) * .8); // 80% of half the angle (4 of 5 = 8)
            var insideEnd = mid + ((mid - start) * .8);
            var insideParent = toView((cell.location[0] * 10) - 4.0);
            var insideChild = toView((cell.location[0] * 10) + 4.0);
            var deleted = cell.connectors.map(function (connector) { return connector.deleted; });
            var selected = cell.connectors.map(function (c, i) { return i == cell.currentConnector; });
            // Draw top
            ctx.strokeStyle = selected[0] ? "lime" : "#888";
            ctx.lineWidth = (selected[0] || deleted[0]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[0]) ctx.arc(0, 0, insideParent, insideEnd, insideStart, true);
            else { ctx.moveTo(cellCenterX, cellCenterY); ctx.arc(0, 0, insideParent, mid, mid, true); }
            ctx.stroke();
            // Draw CW
            ctx.strokeStyle = selected[1] ? "lime" : "#888";
            ctx.lineWidth = (selected[1] || deleted[1]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[1]) { ctx.arc(0, 0, insideParent, insideEnd, insideEnd, true); ctx.arc(0,0, insideChild, insideEnd, insideEnd, true); }
            else ctx.arc(0, 0, spoke[0], insideEnd, mid, true);
            ctx.stroke();
            // Draw CCW
            ctx.strokeStyle = selected[2] ? "lime" : "#888";
            ctx.lineWidth = (selected[2] || deleted[2]) ? 2 : 1;
            ctx.beginPath();
            if (deleted[2]) { ctx.arc(0, 0, insideParent, insideStart, insideStart, true); ctx.arc(0, 0, insideChild, insideStart, insideStart, true); }
            else ctx.arc(0, 0, spoke[0], insideStart, mid, false);
            ctx.stroke();
            // Draw Children (variable children)
            var childCount = cell.connectors.length - 3;
            var childAngleDist = (insideEnd - insideStart) / childCount;
            for (var i = 3, len = cell.connectors.length; i < len; i++) {
                ctx.strokeStyle = selected[i] ? "lime" : "#888";
                ctx.lineWidth = (selected[i] || deleted[i]) ? 2 : 1;
                ctx.beginPath();
                var startAngle = insideStart + (childAngleDist * (i - 3));
                var endAngle = startAngle + childAngleDist;
                if (deleted[i]) ctx.arc(0, 0, insideChild, startAngle, endAngle, false);
                else { ctx.moveTo(cellCenterX, cellCenterY); ctx.arc(0, 0, insideChild, startAngle + ((endAngle - startAngle) / 2), startAngle + ((endAngle - startAngle) / 2), true); }
                ctx.stroke();
            }
            ctx.lineWidth = 1;
        }
		if (model.showCenter) { /////////////////////////////////////////// DRAW CENTER
			ctx.fillStyle = "#888";
			ctx.beginPath();
			ctx.arc(cellCenterX, cellCenterY, toView(1), 0, Math.PI * 2, true);
			ctx.fill();
		}
		if (model.showCellOutline || isSelected) { //////////////////////// DRAW CELL SHAPE
			ctx.strokeStyle = isSelected ? "lime" : "#888";
			ctx.beginPath();
			ctx.arc(0, 0, spoke[1], end, start, true);
			ctx.arc(0, 0, spoke[2], start, start, true); // Draw line from last point to a point at the child-side.
			ctx.arc(0, 0, spoke[2], start, end, false);  // Child-side arc
			ctx.arc(0, 0, spoke[1], end, end, false);
			ctx.stroke();
		}
		if (model.showCellToggles) { ////////////////////////////////////// DRAW START/ END
			if (cell.startCell) {
				ctx.fillStyle = "green";
				ctx.beginPath();
				ctx.arc(cellCenterX, cellCenterY, toView(2), 0, Math.PI * 2, true);
				ctx.fill();
			}
			if (cell.endCell) {
				ctx.fillStyle = "red";
				ctx.beginPath();
				ctx.arc(cellCenterX, cellCenterY, toView(1.5), 0, Math.PI * 2, true);
				ctx.fill();
			}
			if (cell.transGroup) {
			    ctx.strokeStyle = (cell.currentTransGroup != null) ? "lime" : "yellow";
			    ctx.beginPath();
			    ctx.arc(cellCenterX, cellCenterY, toView(2.5), 0, Math.PI * 2, true);
			    ctx.stroke();
			    var keys = Object.keys(cell.transGroup);
			    if (cell.currentTransGroup != null) {
			        ctx.strokeStyle = "lime";
			        ctx.strokeText((keys.length > 1) ? ("<" + cell.currentTransGroup + ">") : keys[0], cellCenterX-7, cellCenterY+3);
			    }
			    else
			        ctx.strokeText((keys.length > 1) ? ("{" + keys.length + "}") : keys[0], cellCenterX-7, cellCenterY+3);
			}
		}
    }
    // In the game, this cell doesn't have any rendering! But for navigation, I do need to render this cell...
    function drawCenterCell(cell, isSelected) {
        if (cell.linkCandidate > 0) {
            // Render a background to signal this cell as a "marked" cell for potential linking
            // 1 = purple, 2 = blue
            ctx.fillStyle = (cell.linkCandidate == 1) ? "purple" : "blue";
            ctx.beginPath();
            ctx.arc(0, 0, toView(4.5), 0, Math.PI * 2, true);
            ctx.fill();
        }
        if (cell.deleted) {
            if (model.showDeleted && (model.showCellOutline || isSelected)) {
                ctx.strokeStyle = isSelected ? "lime" : "#444";
                ctx.beginPath();
                ctx.arc(0, 0, toView(4.5), 0, Math.PI * 2, true);
                ctx.stroke();
            }
            if (model.showDeleted && model.showCenter) {
                var corner = toView(4.5) * Math.cos(Math.PI / 4); // 45 degrees
                ctx.strokeStyle = "#444";
                ctx.beginPath(); // Draw an 'X' over the box.
                ctx.moveTo(-corner, -corner); // upper-left -> lower right
                ctx.lineTo(corner, corner);
                ctx.moveTo(corner, -corner); // upper-right -> lower-left
                ctx.lineTo(-corner, corner);
                ctx.stroke();
            }
            return;
        }
        if (model.showConnectors) { /////////////////////////////////////// DRAW CONNECTORS
            // Center has only children (don't bother with parent/siblings)
            var deleted = cell.connectors.map(function (connector) { return connector.deleted; });
            var selected = cell.connectors.map(function (c, i) { return i == cell.currentConnector; });
            var childRadius = toView(4);
            var childCount = cell.connectors.length - 3;
            var childAngleDist = (2 * Math.PI) / childCount;
            for (var i = 3, len = cell.connectors.length; i < len; i++) {
                ctx.strokeStyle = selected[i] ? "lime" : "#888";
                ctx.lineWidth = (selected[i] || deleted[i]) ? 2 : 1;
                ctx.beginPath();
                var startAngle = childAngleDist * (i - 3);
                var endAngle = startAngle + childAngleDist;
                if (deleted[i]) ctx.arc(0, 0, childRadius, startAngle, endAngle, false);
                else { ctx.moveTo(0, 0); ctx.arc(0, 0, childRadius, startAngle + ((endAngle - startAngle) / 2), startAngle + ((endAngle - startAngle) / 2), true); }
                ctx.stroke();
            }
            ctx.lineWidth = 1;
        }
        if (model.showCenter) { /////////////////////////////////////////// DRAW CENTER
            ctx.fillStyle = "#888";
            ctx.beginPath();
            ctx.arc(0, 0, toView(1), 0, Math.PI * 2, true);
            ctx.fill();
        }
        if (model.showCellOutline || isSelected) { //////////////////////// DRAW CELL SHAPE
            ctx.strokeStyle = isSelected ? "lime" : "#888";
            ctx.beginPath();
            ctx.arc(0, 0, toView(4.5), 0, 2 * Math.PI, true);
            ctx.stroke();
        }
        if (model.showCellToggles) { ////////////////////////////////////// DRAW START/ END
            if (cell.startCell) {
                ctx.fillStyle = "green";
                ctx.beginPath();
                ctx.arc(0,0, toView(2), 0, Math.PI * 2, true);
                ctx.fill();
            }
            if (cell.endCell) {
                ctx.fillStyle = "red";
                ctx.beginPath();
                ctx.arc(0,0, toView(1.5), 0, Math.PI * 2, true);
                ctx.fill();
            }
            if (cell.transGroup) {
                ctx.strokeStyle = (cell.currentTransGroup != null) ? "lime" : "yellow";
                ctx.beginPath();
                ctx.arc(0, 0, toView(2.5), 0, Math.PI * 2, true);
                ctx.stroke();
                var keys = Object.keys(cell.transGroup);
                if (cell.currentTransGroup != null) {
                    ctx.strokeStyle = "lime";
                    ctx.strokeText((keys.length > 1) ? ("<" + cell.currentTransGroup + ">") : keys[0], -7, 3);
                }
                else
                    ctx.strokeText((keys.length > 1) ? ("{" + keys.length + "}") : keys[0], -7, 3);
            }
        }
    }
    function blackoutCell(cell) {
        if (cell.location[0] == 0)
            return blackoutCenterCell(cell);
        ctx.fillStyle = "#000";
        var inside = toView((cell.location[0] * 10) - 5.1);
        var outside = toView((cell.location[0] * 10) + 5.1);
        var start = cell.location[1];   // Clockwise-starting angle (at the 12 o'clock position: left-side)
        var end = cell.location[2];     // Clockwise-ending angle   (at the 12 o'clock position: right-side)
        ctx.beginPath();
        ctx.arc(0, 0, inside, end, start, true);    // inside arc
        ctx.arc(0, 0, outside, start, start, true); // CW-start edge
        ctx.arc(0, 0, outside, start, end, false);  // outside arc
        ctx.arc(0, 0, inside, end, end, false);     // CW-end edge
        ctx.fill();
    }
    function blackoutCenterCell(cell) {
        ctx.fillStyle = "#000"; // Black-out
        ctx.beginPath();
        ctx.arc(0, 0, toView(5.1), 0, 2 * Math.PI, true);
        ctx.fill();
    }
    // For a child cell to have a parent, it's own start/end angle must be wholly contained by the parent's start/end angle...
    // Assumes that the parentCells array is sorted (ascending) by start/end offset pairs. EndOffset is never visited (it's assumed 1-cell beyond the addressable space)
    function lookupParentIndex(parentStartOffset, parentEndOffset, parentCells,childStartAngle,childEndAngle) {
        // Directed search, starting in approximately the right index, then moving backward/forward to try to satisfy the conditions. This algorhithm "spins" the parent wheel to align with the child's parameters.
        var index = (Math.floor(childStartAngle / (2 * Math.PI))/* fraction 0..1 of angle*/ * 
            (parentEndOffset - parentStartOffset)/*number of cells to consider in the parent ring*/) /*discretized*/ + parentStartOffset;/*relative to the start offset*/
        var startIndex = index;
        var endIndex = index;
        var fuzzyBorder = (childEndAngle - childStartAngle) * .2; // 20% (for a total combined fuzzy-factor of 40%)
        // In each case don't overshoot the indexes available to the parent wheel (this is checked first)
        // Assume I overshot, move around the parent wheel CW...
        while ((startIndex < (parentEndOffset - 1)) && (parentCells[startIndex].location[1] <= (childStartAngle - fuzzyBorder))) startIndex++;
        // Now, I'm potentially over my childStartAngle threshold, so back up as far as possible (also handles the "undershot" scenario...
        while ((startIndex >= parentStartOffset) && (parentCells[startIndex].location[1] > (childStartAngle + fuzzyBorder))) startIndex--;
        // Do the same for the parentEndOffset, but so that the childEndAngle is inside the boundary...
        while ((endIndex > parentStartOffset) && (parentCells[endIndex].location[2] >= (childEndAngle + fuzzyBorder))) endIndex--;
        while ((endIndex < (parentEndOffset - 1)) && (parentCells[endIndex].location[2] < (childEndAngle - fuzzyBorder))) endIndex++;
        // There can be only one parent, if start/endIndex diverge, then this cell doesn't have a proper [inclusive] parent.
        if ((startIndex == endIndex) && (parentCells[startIndex].location[1] <= (childStartAngle + fuzzyBorder)) && (parentCells[startIndex].location[2] >= (childEndAngle - fuzzyBorder)))
            return startIndex;
        else
            return -1; // No proper parent. 
    }
    // relativeOffset is -1 for previous, +1 for next (or other relative offset desired...)
    function getPreviousNextCellInRing(thisGroup, refCell, relativeOffset) {
        var currentRing = refCell.location[0];
        if (currentRing == 0)
            return refCell; // The Center cell is a complete ring, so any requested relative offsets just land you back on the origin center cell! (Logic for wall selection still works.)
        var cellLocationIndex = refCell.id;
        if (thisGroup.cells[cellLocationIndex] != refCell) {
            // Search left/right to find it...
            for (var i = 1; ((cellLocationIndex - i) >= 0) || ((cellLocationIndex + i) < thisGroup.cells.length); i++) {
                if ((((cellLocationIndex - i) >= 0) && (thisGroup.cells[cellLocationIndex - i] == refCell)) ||
                    (((cellLocationIndex + i) < thisGroup.cells.length) && (thisGroup.cells[cellLocationIndex + i] == refCell))) {
                    cellLocationIndex = (thisGroup.cells[cellLocationIndex - i] == refCell) ? (cellLocationIndex - i) : (cellLocationIndex + i);
                    break;
                }
            }
        }
        // Try the easiest (best-case scenario)...
        if (((cellLocationIndex + relativeOffset) > 0) && ((cellLocationIndex + relativeOffset) < thisGroup.cells.length) &&
            (thisGroup.cells[cellLocationIndex + relativeOffset].location[0] == currentRing))
            return thisGroup.cells[cellLocationIndex + relativeOffset];    
        // Otherwise, the current-ring didn't match or tried to step back or forward too far
        var lowerLimitIndex = cellLocationIndex;
        var upperLimitIndex = cellLocationIndex;
        while ((upperLimitIndex < thisGroup.cells.length) && (thisGroup.cells[upperLimitIndex].location[0] == currentRing))
            upperLimitIndex++
        while ((lowerLimitIndex >= 1) && (thisGroup.cells[lowerLimitIndex].location[0] == currentRing))
            lowerLimitIndex--
        upperLimitIndex--;
        lowerLimitIndex++; // These are now indexes...
        if (Math.abs(relativeOffset) >= (upperLimitIndex - lowerLimitIndex + 1))
            throw new Error("getPrevious/NextCellInRing: requested relativeOffset is larger than the size of the ring: request would wrap or overlap the refCell index");
        // Handle any need to wrap around the extremes...
        if (((cellLocationIndex + relativeOffset < lowerLimitIndex)) || ((cellLocationIndex + relativeOffset) > upperLimitIndex)) {
            // Shorten the relativeOffset by the overlap
            relativeOffset += (cellLocationIndex - ((relativeOffset < 0) ? lowerLimitIndex : upperLimitIndex));
            // Adjust the cellLocationIndex to the extreme of the ring's range (+/-1)
            cellLocationIndex = (relativeOffset < 0) ? (upperLimitIndex + 1) : (lowerLimitIndex - 1);
        }
        return thisGroup.cells[cellLocationIndex + relativeOffset];
    }
    // Returns the bezier controls points for a curve from pStart to pEnd with pPrevious 
    // leading into pStart and pNext following pEnd such that the curve has C2 continuity
    // at the end-points.
    // The controls points are P1 and P2 such that the bezier curve can be constructed as:
    // pStart, with P1 and P2 as intermediate control points, and ending at pEnd.
    // A single array with 4 floats is returned: [ cp1x, cp1y, cp2x, cp2y ]
    function generateCurveSegmentBezierControlPoints(pPrevious, pStart, pEnd, pNext) {
        // Get two intermediate control points: 
        // 1) the quadratic control point for a curve that starts at pPrevious, intersects
        //    pStart, and ends at pEnd, and...
        // 2) the same quadratic control point for a curve segment starting at pStart, 
        //    intersecting pEnd and finishing at pNext.
        var qCP1 = quadraticControlPointFrom3Knots(pPrevious, pStart, pEnd);
        var qCP2 = quadraticControlPointFrom3Knots(pStart, pEnd, pNext);
        return midPoint(qCP1, pEnd).concat(midPoint(pStart, qCP2));
    }

    // http://stackoverflow.com/questions/6711707/draw-a-quadratic-b%c3%a9zier-curve-through-three-given-points
    // pc is the center point between p0 and p2.
    function quadraticControlPointFrom3Knots(p0, pc, p2) {
        var cp = [];
        cp.push((2 * pc[0]) - (p0[0] / 2) - (p2[0] / 2));
        cp.push((2 * pc[1]) - (p0[1] / 2) - (p2[1] / 2));
        return cp;
    }
    function midPoint(p0, p1) {
        var mp = [];
        mp.push((p0[0] + p1[0]) / 2);
        mp.push((p0[1] + p1[1]) / 2);
        return mp;
    }

    function ensureCircleGroupPrototype() {
        if (circleGroupPrototype != null) return;
        circleGroupPrototype = Group.newPrototypeSubclass();
        circleGroupPrototype.updateUI = function () {
            // Call the super() method on this prototype's prototype
            Object.getPrototypeOf(circleGroupPrototype).updateUI.call(this);
            ringsInputEl.value = this.rings;
            distributionSelectEl.selectedIndex = (this.distribution - 1);
            cellNumSpanEl.innerHTML = this.getPresentationIdfromCellId(this.currentCell);
            this.cells[this.currentCell].updateUI();
        };
        circleGroupPrototype.redraw = function (isSelected) { // Assumes that the canvas has been previously cleared.
            this.pushTransformState();
            for (var i = 0, len = this.cells.length; i < len; i++)
                drawCell(this.cells[i], isSelected && (this.currentCell == i));
            this.popTransfromState();
        };
        circleGroupPrototype.redrawCell = function (isSelected, optionalIndex) { // Handles clearing/redrawing of just the current cell or the provided specific cell index
            var cell = this.cells[(typeof optionalIndex == "number") ? optionalIndex : this.currentCell];
            this.pushTransformState(); // Implemented on Group's prototype...
            blackoutCell(cell);
            drawCell(cell, isSelected)
            this.popTransfromState();
        };
        circleGroupPrototype.modifyCircle = function (sameCell) {
            // There is always "the center spoke".
            this.cells[0].connectors.splice(3, 50); // Ensure all child connectors are cleared from the center spoke.
            this.cells[0].addConnector(this.id, -1, true); // Add an empty dummy connector for the purposes of the detection portion of the below algorithm...
            var previousRingStartOffset = 0; // The location in the cells array where the last ring starts
            var currentRingStartOffset = 1; // The location in the cells array currently being re-evaluated.
            // cells are added CW in concentric rings
            for (var r = 1, maxRing = this.rings; r <= maxRing; r++) {
                var curcumference = Math.PI * r * 20; // (10 units per cell * r) * 2 * PI (pre-computing the two constants).
                var numcells = (this.distribution == 3/*LOOSE*/) ? Math.floor(curcumference / 10) : Math.ceil(curcumference / 10); // Round up or down depending on packing preference
                if ((this.distribution == 1/*AXIS*/) && (r > 1)) { // Ring index must be 2+ (so that there's a prior ring to base the alignment off of)
                    var cellsPerPreviousRing = currentRingStartOffset - previousRingStartOffset;
                    numcells = cellsPerPreviousRing * Math.round((curcumference / cellsPerPreviousRing) / 10); // (circumference / cells-per-previous-ring) = unitsPerCurrentCell -- ratio of projected unit lengh to 10 and round
                }
                var cellAngularWidth = (2 * Math.PI) / numcells;
                var angle = 0; // Tracks the currnet angle around the new ring (in radians)
                // Visit all the cells in the new ring, hooking up sibling/parent connectors
                for (var i = currentRingStartOffset, len = currentRingStartOffset + numcells; i < len; i++) {
                    if (!this.cells[i])
                        this.cells[i] = new Cell(i); // Cell ID matches the index for simplicity
                    else
                        this.cells[i].clean(i, sameCell); // Re-assign the id
                    // Set/update the cell's location info (ring, cw-start angle, cw-end angle)
                    this.cells[i].location[0] = r;      // What ring?
                    this.cells[i].location[1] = angle;  // Start angle?
                    this.cells[i].location[2] = (angle += cellAngularWidth); // Advance the angle around the circle and store it as the end angle...
                    // Connectors -- 0-based index or -1
                    // Add parent-connector [0] to me...
                    var target = lookupParentIndex(previousRingStartOffset, currentRingStartOffset, this.cells, this.cells[i].location[1], angle);
                    this.cells[i].addConnector(this.id, target, (target == -1) || (sameCell && this.cells[target] && this.cells[target].deleted));
                    // Also add child connector to designated target...
                    if (target != -1) {
                        if (this.cells[target].connectors[3].cell == -1) // This parent cell previously had no children...This will be the first, so replace the previous placeholder...
                            this.cells[target].connectors.splice(3, 1);
                        this.cells[target].addConnector(this.id, i, this.cells[i].deleted);
                    }
                    // Add CW connector [1]
                    target = (i == (len - 1)) ? currentRingStartOffset : i + 1; // last cell in the circle points to the first cell in the circle...
                    this.cells[i].addConnector(this.id, target, (target == -1) || (sameCell && this.cells[target] && this.cells[target].deleted));
                    // Add CCW connector [2]
                    target = (i == currentRingStartOffset) ? len - 1 : i - 1; // first cell in the circle projects a target of the last cell index...
                    this.cells[i].addConnector(this.id, target, (target == -1) || (sameCell && this.cells[target] && this.cells[target].deleted));
                    // Add at least one child connector (it may be replaced later)
                    this.cells[i].addConnector(this.id, -1, true);
                    // This preserves the state of previously deleted cells and their connector bindings...
                }
                previousRingStartOffset = currentRingStartOffset;
                currentRingStartOffset += numcells;
            }
            // Clear out any excess cells
            if (this.cells.length > currentRingStartOffset)
                this.cells.splice(currentRingStartOffset, (this.cells.length - currentRingStartOffset));
            model.transportManager.validateAndUpdateUI(); // Validate the transport manager since the group is modified
        };
        circleGroupPrototype.toggleRingDelete = function () {
            if (this.currentCell != 0) { // Only do this for non-center cells
                var stopCell = this.cells[this.currentCell]; // Stop on my cell
                var currCell = this.cells[stopCell.connectors[1].cell];
                while (currCell != null) {
                    currCell.toggleDeleted((stopCell === currCell) ? undefined : currCell.id); // Will pass undefined for the final delete call, which will be the starting cell :)
                    currCell = ((this.id == currCell.connectors[1].group) && (currCell !== stopCell)) ? this.cells[currCell.connectors[1].cell] : null;
                }
            }
            else
                this.cells[0].toggleDeleted(); // delete the center cell otherwise...
        };
        circleGroupPrototype.getPresentationIdfromCellId = function (cellId) { // 0-based cell id
            return "r" + this.cells[cellId].location[0] + "#" + cellId;
        };
        // Supports the custom connectors (formerly "bridgewalls") between cells in the same (or different) group.
        // Given a cell reference, together with a connector index (which "side" to connect to), a "opposite" 
        // indicator (allowing to specific one end of the side or the other), and the "isSource" flag (moving to the point
        // if source, or drawing a line to the point if not)
        circleGroupPrototype.drawCustomConnector = function (cellRef, connectorIndex, opposite, isSource) {
            var start = cellRef.location[1];   // Clockwise-starting angle (at the 12 o'clock position: left-side)
            var end = cellRef.location[2];     // Clockwise-ending angle   (at the 12 o'clock position: right-side)
            var proportionalGap = ((end - start) * 0.05); // 5% of the actual angle spread (absolute amount depends on radius)
            start += proportionalGap;
            end -= proportionalGap;
            // moving to the correct spot and drawing a line to the same spot are both accomplished using 'arc' (which connects a line from the previous point in the sub-path if one exists).
            this.pushTransformState();
            if (connectorIndex == 0) // parent
                ctx.arc(0, 0, toView((cellRef.location[0] * 10) - 4.5), opposite ? start : end, opposite ? start : end, true);
            else if (connectorIndex <= 2) // CW and CCW
                ctx.arc(0, 0, toView(opposite ? ((cellRef.location[0] * 10) + 4.5) : ((cellRef.location[0] * 10) - 4.5)), (connectorIndex == 1) ? end : start, (connectorIndex == 1) ? end : start, true);
            else if (connectorIndex >= 3) { // Multiple children (including the center circle)
                var childAngleDist = (end - start) / /*childCount*/ (cellRef.connectors.length - 3);
                var startAngle = start + (childAngleDist * (connectorIndex - 3));
                var usedSide = opposite ? /*end angle*/(startAngle + childAngleDist) : startAngle;
                ctx.arc(0, 0, toView((cellRef.location[0] * 10) + 4.5), usedSide, usedSide, true);
            }
            this.popTransfromState();
        };
        // Support the export functionality by returning custom connector coordinates in the same order/algorithm as used above...
        // Note: "extendedReach" is an extra boolean flag for retrieving the previous(-1)/next(1) point along the curved path for sideIndex 0 or >=3.
        circleGroupPrototype.getConnectorCoord = function (transformMatrix, cellRef, sideIndex, oppositeFlag, extendedReach) {
            var start = cellRef.location[1];   // Clockwise-starting angle (at the 12 o'clock position: left-side)
            var end = cellRef.location[2];     // Clockwise-ending angle   (at the 12 o'clock position: right-side)
            var hypotenuse = (cellRef.location[0] * 10) + (((sideIndex == 0) || ((sideIndex <= 2) && !oppositeFlag)) ? -5 : 5);
            if (sideIndex == 0) { // parent
                if (extendedReach) {
                    var extendedCellRef = getPreviousNextCellInRing(this, cellRef, (oppositeFlag ? -1 : 1));
                    start = extendedCellRef.location[1];
                    end = extendedCellRef.location[2];
                }
                return transformMatrix.multiply([hypotenuse * Math.cos(oppositeFlag ? start : end), hypotenuse * Math.sin(oppositeFlag ? start : end)]);
            }
            else if (sideIndex <= 2) { // CW and CCW
                //var hypotenuse = (oppositeFlag ? ((cellRef.location[0] * 10) + 5) : ((cellRef.location[0] * 10) - 5)); // See above--logic should be the same.
                return transformMatrix.multiply([hypotenuse * Math.cos((sideIndex == 1) ? end : start), hypotenuse * Math.sin((sideIndex == 1) ? end : start)]);
            }
            else if (sideIndex >= 3) { // Multiple children
                var childAngleDist = (end - start) / /*childCount*/ (cellRef.connectors.length - 3);
                var startAngle = start + (childAngleDist * (sideIndex - 3));
                var usedSide = oppositeFlag ? /*end angle*/(startAngle + childAngleDist) : startAngle;
                if (extendedReach) {
                    // Add some fuzzyness to account for small round-off error in floating-point
                    if ((oppositeFlag && ((usedSide + childAngleDist - 0.00000001) > end)) || (!oppositeFlag && ((usedSide - childAngleDist + 0.00000001) < start))) {
                        // Get the next/previous cell's coordinates, children count and spacing, and then get the point just beyond the start or just before the end.
                        var extendedCellRef = getPreviousNextCellInRing(this, cellRef, (oppositeFlag ? 1 : -1));
                        var extendedChildAngleDist = (extendedCellRef.location[2] - extendedCellRef.location[1]) / (extendedCellRef.connectors.length - 3);
                        usedSide = oppositeFlag ? (extendedCellRef.location[1] + extendedChildAngleDist) : (extendedCellRef.location[2] - extendedChildAngleDist);
                    }
                    else // Use existing points within the curve
                        usedSide = (oppositeFlag ? (usedSide + childAngleDist) : (usedSide - childAngleDist));
                }
                return transformMatrix.multiply([hypotenuse * Math.cos(usedSide), hypotenuse * Math.sin(usedSide)]);
            }
        };
        circleGroupPrototype.getWallGeometry = function (transformMatrix, cellRef, sideIndex) {
            // 0 = parent (an arc)
            // 1 = CW side (a line)
            // 2 = CCW side (a line)
            // 3+ = children (arcs)
            if ((sideIndex == 0) || (sideIndex >= 3)) {
                // Get 4 points to construct a bezier curve. Point prior to the starting point of the arc, arc starting point, arc ending point, and point beyond the end of the arc.
                var pPrevious = this.getConnectorCoord(transformMatrix, cellRef, sideIndex, true, true);
                var pStart = this.getConnectorCoord(transformMatrix, cellRef, sideIndex, true, false);
                var pEnd = this.getConnectorCoord(transformMatrix, cellRef, sideIndex, false, false);
                var pNext = this.getConnectorCoord(transformMatrix, cellRef, sideIndex, false, true);
                return pStart.concat(generateCurveSegmentBezierControlPoints(pPrevious, pStart, pEnd, pNext), pEnd);
            }
            else {
                // Sideindex == 1 || 2
                var pStart = this.getConnectorCoord(transformMatrix, cellRef, sideIndex, false);
                return pStart.concat(this.getConnectorCoord(transformMatrix, cellRef, sideIndex, true));
            }
        };
    };

    // CircleGroup(optional serializedGroup) : Group {
    //    // PUBLIC (serialized stuff)
    //    id: #                 // Group id (globally unique)
    //    type: 2               // Circ type
    //    currentCell: #        // There will always be a current cell while this group exists
    //    cells: []             // Array of rectable--assumed cells.
    //    rings: #              // Number of rings in the Circle group
    //    distribution: #       // 1-Axis Aligned, 2-Tight-Packed, 3-Loose Packed
    //    // PRIVATE (not serialized)
    //    name: "CircXX"
    //    <<--------------------------->>
    //    // INHERITED FROM GROUP
    //    rotate: #             // Rotation offset
    //    translateX: #         // Translation in X axis (also the circle's center point).
    //    translateY: #         // Translation in Y axis (also the circle's center point).
    //    stretchX: #           // Stretch X direction
    //    stretchY: #           // Stretch Y direction
    // }

    Object.defineProperty(window, "CircleGroup", {
        enumerable: true,
        value: function CircleGroupConstructor(serializedCircleGroup, isDuplicate) {
            ensureCircleGroupPrototype();
            // Initialize accessors here
            var currentCell = serializedCircleGroup ? serializedCircleGroup.currentCell : 0
            var rings = serializedCircleGroup ? serializedCircleGroup.rings : 0; // Start with zero rings
            var distribution = serializedCircleGroup ? serializedCircleGroup.distribution : 1; // Abitrary default
            var id = (serializedCircleGroup && !isDuplicate) ? serializedCircleGroup.id : model.groupID++; // Needed here to initialize a new (first time) cell's connectors with this group's id.
            // Compose the group instance with this Circle group
            return Object.defineProperties(new Group(circleGroupPrototype, serializedCircleGroup), {
                id: {
                    enumerable: true,
                    value: id
                },
                type: {
                    enumerable: true,
                    value: 2    // Always.
                },
                cells: {
                    enumerable: true,
                    value: serializedCircleGroup ? serializedCircleGroup.cells.map(function(cell) { 
                        return new Cell(cell, isDuplicate, id); 
                    }) : [new Cell(0, isDuplicate, id)].map(function (cell) {
                        cell.location.push(0); // Ring-0
                        cell.location.push(0); // CW-Start angle
                        cell.location.push(2 * Math.PI); // CW-End angle (360-degree cell size!)
                        cell.addConnector(id, -1, true); // No parent (it is the top-level parent)
                        cell.addConnector(id, -1, true); // No CW sibling
                        cell.addConnector(id, -1, true); // No CCW sibling
                        cell.addConnector(id, -1, true); // No child siblings
                        // Children will be added later.
                        return cell;
                    })
                },
                currentCell: {
                    enumerable: true,
                    get: function () { return currentCell; },
                    set: function (x) {
                        x = Math.max(0, Math.min(x, this.cells.length - 1));
                        if (currentCell == x) return;
                        this.redrawCell(false);
                        currentCell = x;
                        this.redrawCell(true); // Make the current cell selected
                        cellNumSpanEl.innerHTML = this.getPresentationIdfromCellId(currentCell);
                        this.cells[currentCell].updateUI();
                    }
                },
                rings: {
                    enumerable: true,
                    get: function () { return rings; },
                    set: function (x) {
                        x = Math.max(0, x);
                        if (x == rings) return;
                        rings = x;
                        this.modifyCircle(true); // Changes in ring size (linear direction) will keep the cells in the same relative order, so they are considered 'same'
                        ringsInputEl.value = rings;
                        if (currentCell >= this.cells.length) // Move it to the middle...
                            currentCell = 0; // Cell zero is the middle cell
                        model.floor.redraw();
                    }
                },
                distribution: {
                    enumerable: true,
                    get: function () { return distribution; },
                    set: function (x) {
                        x = Math.max(1, x);
                        if (x == distribution) return;
                        distribution = x;
                        this.modifyCircle(false);
                        distributionSelectEl.selectedIndex = (x - 1);
                        if (currentCell >= this.cells.length) // Move it to the middle...
                            currentCell = 0;
                        model.floor.redraw();
                    }
                },
                name: {
                    get: function() { return "Circle" + this.id; }
                }
            });
        }
    });

    // Hook up the UI to the model

    // Change rings num
    ringsInputEl.onchange = function () { if (model.group) model.group.rings = parseInt(this.value); };

    // Decrement rings count
    document.getElementById("circRingsLess").onclick = function () { if (model.group) model.group.rings--; };

    // Increment rings count
    document.getElementById("circRingsMore").onclick = function () { if (model.group) model.group.rings++; };

    // Toggle delete status on the current ring
    document.getElementById("circRingsCellCoggle").onclick = function () { if (model.group) model.group.toggleRingDelete(); };

    // Change distribution type
    distributionSelectEl.onchange = function () { if (model.group) model.group.distribution = parseInt(this.value); };
});