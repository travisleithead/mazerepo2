﻿var MazeRender = (function () {

    //////////////////////////////////////////
    // Rendering code
    //////////////////////////////////////////

    var draw = function () {
        // Draw each group
        Settings.SetContextForBackgroundFill(ctx);
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        // Draw only the inside walls in the first pass...
        ctx.beginPath();
        Settings.SetContextForInsideWall(ctx);
        drawWallState = WALLSTATE_INSIDE;
        for (var i = 0, iLen = groups.length; i < iLen; i++)
            drawGroup(groups[i]);
        ctx.stroke();
        // Draw only the boundary walls in the second pass....
        ctx.beginPath();
        Settings.SetContextForBoundaryWall(ctx);
        drawWallState = WALLSTATE_BOUNDARY;
        for (var i = 0, iLen = groups.length; i < iLen; i++)
            drawGroup(groups[i]);
        ctx.stroke();
        // Draw other info
    };
    var drawGroup = function (g) {
        // Draw absolute edges
        if (g.y == RENDERGROUPTYPE_RECT) {
            for (var i = 0, iLen = g.c.length; i < iLen; i++)
                drawRectCellAbs(g.c[i]);
        }
        else if (g.y == RENDERGROUPTYPE_CIRC) {
            alert("not implemented (circle rendering)");
        }
        // Draw relative edges
        ctx.save()
        ctx.translate(toPixel(g.t[X1]), toPixel(g.t[Y1]));
        ctx.rotate(g.r);
        if (g.y == RENDERGROUPTYPE_RECT) {
            for (var m = 0, mLen = g.c.length; m < mLen; m++)
                drawRectCellRel(g.c[m]);
        }
        else if (g.y == RENDERGROUPTYPE_CIRC) {
            alert("not implemented (circle rendering)");
        }
        ctx.restore();
    };
    var drawRectCellAbs = function (c) { // c = rectangle render cell
        if (c.b && (drawWallState == WALLSTATE_BOUNDARY)) { // Has bridgelines
            for (var i = 0, iLen = c.b.length; i < iLen; i += 4)
                drawLineFromArray(c.b, i);
        }
        if (c.w) { // Has custom walls
            for (var side = 0; side < NUMRECTSIDES; side++) {
                // Check the connectors to see if a side needs to be rendered (if the connector is null, then render a wall)
                if (c.w[side] && canDrawWall(c, side))
                    drawLineFromArray(c.w[side]);
            }
        }
    };
    var drawRectCellRel = function (c) {
        for (var side = 0; side < NUMRECTSIDES; side++) {
            if (c.w && c.w[side]) // This cell has a custom wall for this side, skip it (it was already [potentially] rendered)
                continue;
            if (canDrawWall(c, side)) {     // Double-up the relative point
                if (TOP == side) drawDefaultRectLineFromPoint(c.p, ARRAY_DEFAULT_TOPLINE_OFFSETS);
                else if (LEFT == side) drawDefaultRectLineFromPoint(c.p, ARRAY_DEFAULT_LEFTLINE_OFFSETS);
                else if (BOTTOM == side) drawDefaultRectLineFromPoint(c.p, ARRAY_DEFAULT_BOTTOMLINE_OFFSETS);
                else if (RIGHT == side) drawDefaultRectLineFromPoint(c.p, ARRAY_DEFAULT_RIGHTLINE_OFFSETS);
            }
        }
    };
    var drawDefaultRectLineFromPoint = function (centerPoint, offsetArray) {
        ctx.moveTo(toPixel(centerPoint[X1] + offsetArray[X1]), toPixel(centerPoint[Y1] + offsetArray[Y1]));
        // Note: centerPoint is repeated in X1, Y1 (not a bug)
        ctx.lineTo(toPixel(centerPoint[X1] + offsetArray[X2]), toPixel(centerPoint[Y1] + offsetArray[Y2]));
    };

    
    ///////// helpers ////////////////
    var toPixel = function (x) {
        return x * toPixelCache;
    };
    // images should only be scaled DOWN to avoid pixelation. This function does that based on the current zoom setting
    var toPixelImage = function (x) {
        return x * toPixelImageCache; // the latter will be a fractional scale value <= 1.
    };
    var getHSLThemeColor = function () {
        if (toColorCache == "red")          // RED (HSL) = 0deg, 87%, 44%
            return { hDeg: 0, sPercent: 87, lPercent: 44, serialized: "hsl(0,87%,44%)" };
        else if (toColorCache == "orange")  // ORANGE (HSL) = 19deg, 100%, 49%
            return { hDeg: 19, sPercent: 100, lPercent: 49, serialized: "hsl(19,100%,49%)" };
        else // Assume                      // GREEN (easy) paths levels (HSL) = 91deg, 77%, 42%
            return { hDeg: 91, sPercent: 77, lPercent: 42, serialized: "hsl(91,77%,42%)" };
    };



    var drawLineFromArray = function (a, offset) {
        if (!offset)
            offset = 0;
        ctx.moveTo(toPixel(a[offset + X1]), toPixel(a[offset + Y1]));
        ctx.lineTo(toPixel(a[offset + X2]), toPixel(a[offset + Y2]));
    };
    var canDrawWall = function (c, sideIndex) {
        if ((c.i.c[sideIndex] == NOT_CONNECTED_ORIGINAL_WALL) && (drawWallState == WALLSTATE_BOUNDARY))
            return true;
        else if ((c.i.c[sideIndex] == NOT_CONNECTED_INTERIOR_WALL) && (drawWallState == WALLSTATE_INSIDE))
            return true;
        else
            return false;
    };


    /* Path rendering */
    /********************************************************/

    // Animation state on a path object
    // * animationStatePath (created on-demand as needed)

    function stepAndRenderPath(floor) {

        var paths = floor.paths;
        for (var i = 0, len = paths.length; i < len; i++) {
            var pathSeg = paths[i];
            // For stepPathItem, the previousPathPulseTimeOffset is used if a new path item transitions to 
            // the pulse state. By resetting this "previous" value to be the "fromCell" (0th cell in the new
            // segment) animation state, the "pulse" will appear to fork at junctions in the maze. A cool
            // effect, vs. just wrapping from the end of one segment to the next.
            previousPathPulseTimeOffset = getPathPulseTimeOffsetAnimationState(pathSeg[0]); // Note: there will always be a zero'th item in every segment (and a one-th for all but the first segment)
            for (var segI = 0, segLen = pathSeg.length; segI < segLen; segI++) {
                var pathItem = pathSeg[segI];
                // Update all segments excluding those 0th position items that are forks of other segments
                if ((pathSeg.type != PATH_TYPE_FORK) || (segI != 0))
                    stepPathItem(pathItem, pathSeg.type);// Step the animation state (uses/udpates previousPathPulseTimeOffset)
                // Render the path
                if (segI != 0) {
                    drawThemedPathSegment[Settings.ThemeIndex](pathSeg[segI - 1], pathItem, pathSeg.type);
                }
            }
        }
    }
    function getPathPulseTimeOffsetAnimationState(item) {
        // Safe jump from knowing the state to getting the pulseTimeOffset, since the state transition
        // in "stepPathItem" is atomic, creating this property as well...
        if (item.animationStatePath && (ANIMATION_STATE_PATH_PULSE == item.animationStatePath.state))
            return item.animationStatePath.pulseTimeOffset;
        return 0;
    }
    // The previousPathPulseTimeOffset is used when the current pathItem has just switched
    // from the temperature state to the pulse state, and needs to determine it's initial
    // time offset which is based on a hard-coded time interval that represents an elapsed
    // time between adjacent cells in the map.
    // previousPathPulseTimeOffset is the _final_ calculated offset from the prior item
    // (including the current elapsed time), so don't add the elapsed time in again.
    // This function will update the previousPathPulseTimeOffset if the current item
    // meets the qualifications (being in the pulse state)
    function stepPathItem(pathItem, pathType) {
        // Create the animation state on-demand if it's not there...
        if (!pathItem.animationStatePath)
            pathItem.animationStatePath = createDefaultPathAnimationState(pathType);
        var myState = pathItem.animationStatePath;
        // Starting state...
        if (ANIMATION_STATE_PATH_TEMPERATURE == myState.state) {
            if (!myState.temperatureTimeRemaining) // First time rendering the temperature...
                myState.temperatureTimeRemaining = PATH_TEMPERATURE_COOLDOWN_DURATION;
            else
                myState.temperatureTimeRemaining -= currentElapsedTime;
            if (myState.temperatureTimeRemaining > 0)
                myState.intensity = myState.temperatureTimeRemaining / PATH_TEMPERATURE_COOLDOWN_DURATION;
            else // State transition time!
                myState.state = ANIMATION_STATE_PATH_PULSE;
        }
        // Second [final] state
        if (ANIMATION_STATE_PATH_PULSE == myState.state) {
            if (!myState.pulseTimeOffset) // this item must have just transitioned into the pulse state
                myState.pulseTimeOffset = previousPathPulseTimeOffset - PATH_PULSE_TIME_INTERVAL_BETWEEN_CELLS; // Subracting the time makes the pulse go from start->end (adding the time makes the pulse go in reverse--ends to start)
            else // non-first-time case
                myState.pulseTimeOffset = (myState.pulseTimeOffset + currentElapsedTime) % PATH_PULSE_ELAPSED_TIME_WRAPAROUND;
            previousPathPulseTimeOffset = myState.pulseTimeOffset; // Update the "prior" position with my current position for the next path cell(s)
            myState.intensity = getThemedPathPulseShape[Settings.ThemeIndex](myState.pulseTimeOffset);
        }
    }
    function createDefaultPathAnimationState(type) {
        return {
            state: (type != PATH_TYPE_TRANSPORT) ? ANIMATION_STATE_PATH_TEMPERATURE : ANIMATION_STATE_PATH_PULSE, // Default initial state when a path item is just about to be rendered
            intensity: 0 // 0..1 (shared by both the pulse and temperature rendering code)
        };
    }
    function resetPathFloorRenderState(floor) {
        for (var segI = 0, segLen = floor.paths.length; segI < segLen; segI++) {
            for (var i = 0, len = floor.paths[segI].length; i < len; i++) {
                // delete animation path info for each cell on the path
                delete floor.paths[segI][i].animationStatePath;
            }
        }
    }
    // Converts a time value into a 0..1 range for pulse rendering
    var getThemedPathPulseShape = [
        // Test theme
        function (time) { // [0]/---\[0.5]----[3]
            var moduloTime = time % 3000; // repeat at 3 second function span...
            if (moduloTime > 500)
                return 0;
            if (moduloTime > 250)
                return (500 - moduloTime) / 250;
            else
                return moduloTime / 250;
        }
    ];
    
    var drawThemedPathSegment = [
        function (from, to, pathType) {
            var intensity = to.animationStatePath.intensity;
            animationCtx.beginPath();
            animationCtx.lineCap = "round";
            var hsl = getHSLThemeColor();
            if (to.animationStatePath.state == ANIMATION_STATE_PATH_TEMPERATURE) {
                // Dark brown std color in hsl = (22,100%,26%)
                var hueDelta = 22 - hsl.hDeg; // from brown to theme (going to brown at 100% intensity--since intensity will work backward)
                var satDelta = 100 - hsl.sPercent;
                var liteDelta = 26 - hsl.lPercent;
                animationCtx.strokeStyle = "hsl(" + (hsl.hDeg + (hueDelta * intensity)) + "," +
                                                    (hsl.sPercent + (satDelta * intensity)) + "%," +
                                                    (hsl.lPercent + (liteDelta * intensity)) + "%)";
                animationCtx.lineWidth = toPixel(2 + (2 * intensity));
                // Note: doing any kind of shadow effect requires that the path be rendered in two phases.
            }
            // Pulse
            else if (pathType != PATH_TYPE_TRANSPORT) { // PULSE and segment is not a transport 
                animationCtx.lineWidth = toPixel(2 + intensity);
                animationCtx.strokeStyle = "hsl(" + hsl.hDeg + "," + hsl.sPercent + "%," + Math.ceil(hsl.lPercent + ((100 - hsl.lPercent) * intensity)) + "%)";
            }
            else { // PULSE and transport type
                animationCtx.lineWidth = toPixel(1.4 + intensity);
                animationCtx.strokeStyle = "hsla(" + hsl.hDeg + "," + hsl.sPercent + "%," + hsl.lPercent + "%," + intensity + ")";
            }
            animationCtx.moveTo(toPixel(from.p[X1]), toPixel(from.p[Y1]));
            animationCtx.lineTo(toPixel(to.p[X1]), toPixel(to.p[Y1]));
            animationCtx.stroke();
        }
    ];

    /* Transport animation */
    /***********************************************/

    function stepAndRenderTransports(floor) {
        if (!floor.transports)
            return; // Nothing to render, since there's no transports on this floor
        for (var i = 0, len = floor.transports.length; i < len; i++) {
            stepTransport(floor.transports[i]);
        }
    }

    // STATE = disabled -> enabled <-> active (special state 'go')
    // (transition animations between states are forced to the new state when the user changes the state pre-maturely (before the transitions ends))                         
    // TODO: This is not nearly so complicated... 
    function stepTransport(transMapOb) {
        var transOb = transMapOb.transport;        
        // Implement stepping and rendering for each state (and automatic transitions from certain states)
        if (TRANSPORT_STATE_DISABLED == transOb.s) {  // STARTING State
            if (!transOb._disabledRenderContext) // First time...
                transOb._disabledRenderContext = loadThemedTransportCtxResourcesByType[Settings.ThemeIndex]("disabled", transOb.t, {});
            // Step and..
            advanceImageStrip(transOb._disabledRenderContext); // Purposely ignoring the return value (not needed here)
            // Render it
            animationCtx.drawImageStrip(transMapOb.p[X1], transMapOb.p[Y1], transOb._disabledRenderContext);
        }
        else if (TRANSPORT_STATE_ENABLED == transOb.s) {
            if (!transOb._enabledRenderContext) { // Just transitioned...
                // Determine if this is a transition from disabled -> enabled or from active -> enabled (because they have different transition animations)
                if (transOb._disabledRenderContext) { // Last state was disabled...
                    // Remove the disabled state...
                    delete transOb._disabledRenderContext;
                    transOb._enabledRenderContext = loadThemedTransportCtxResourcesByType[Settings.ThemeIndex]("preenabled", transOb.t, { inTransition: true });
                }
                else { // Last state was active...
                    delete transOb._activeRenderContext;
                    transOb._enabledRenderContext = loadThemedTransportCtxResourcesByType[Settings.ThemeIndex]("exitactive", transOb.t, { inTransition: true });
                }
            }
            // Advance and [optionally] switch to the enabled render view...
            if (!advanceImageStrip(transOb._enabledRenderContext) && transOb._enabledRenderContext.inTransition) // If it returns false, then I need to fully switch to the enabled state
                transOb._enabledRenderContext = loadThemedTransportCtxResourcesByType[Settings.ThemeIndex]("enabled", transOb.t, {});
            animationCtx.drawImageStrip(transMapOb.p[X1], transMapOb.p[Y1], transOb._enabledRenderContext);
        }
        else if (TRANSPORT_STATE_ACTIVE == transOb.s) {
            if (!transOb._activeRenderContext) {
                delete transOb._enabledRenderContext;
                transOb._activeRenderContext = loadThemedTransportCtxResourcesByType[Settings.ThemeIndex]("preactive", transOb.t, { inTransition: true });
            }
            // Advance until the transition period is over
            if (!advanceImageStrip(transOb._activeRenderContext) && transOb._activeRenderContext.inTransition)
                transOb._activeRenderContext = loadThemedTransportCtxResourcesByType[Settings.ThemeIndex]("active", transOb.t, {});
            animationCtx.drawImageStrip(transMapOb.p[X1], transMapOb.p[Y1], transOb._activeRenderContext);
        }
        else if (TRANSPORT_STATE_GO == transOb.s) {
            transOb.s = TRANSPORT_STATE_ENABLED;
            transOb.c.transport.s = TRANSPORT_STATE_ENABLED;
            // Send a callout to switch floors if necessary
            //var data = {
            //    gotoFloor: transMapOb.transport.f,
            //    startPath: transMapOb.transport.c.path ? null : transMapOb.transport.c
            //};
            // Send callout to switch to the dst floor (and activate the path on that transport)
        }
    }
    
    function resetTransportFloorRenderState(mazeFloor) {
        // Clear render states from the transports on the provided floor (if any)
        if (mazeFloor.transports) {
            for (var i = 0; i < mazeFloor.transports.length; i++) {
                var transOb = mazeFloor.transports[i].transport;
                // Delete any render states (other than disabled)
                delete transOb._activeRenderContext;
                delete transOb._enabledRenderContext;
                transOb.s = TRANSPORT_STATE_DISABLED;
            }
        }
    }
    

    // resourceName should match the suffix in use for the id of the resources in default.html (e.g., "...transport-down-disabled" pass: "disabled")
    // returns the provided (filled-out) transport context.
    var loadThemedTransportCtxResourcesByType = [
        function (resourceName, transportType, transportCtx) {
            if ((transportType == TRANSPORT_TYPE_PORTAL_DOWN) || (transportType == TRANSPORT_TYPE_PORTAL_UP) || (transportType == TRANSPORT_TYPE_PORTAL_SAME)) 
                loadImageStripDetails(transportCtx, "template-theme0-transport-any-" + resourceName);
            else if (transportType == TRANSPORT_TYPE_STAIRS_DOWN)
                loadImageStripDetails(transportCtx, "template-theme0-transport-down-" + resourceName);
            else // TYPE_STAIRS_UP
                loadImageStripDetails(transportCtx, "template-theme0-transport-up-" + resourceName);
            return transportCtx;
        }
    ];

    /* ____________________________________________________ */
    /* Active cell */
    
    /*
    function stepAndRenderActiveCell() {
        var aCell = mapAnimationFloor.active.c;
        if (!aCell)
            return;
        if (!mapAnimationFloor.active.activeCellAnimationState)
            mapAnimationFloor.active.activeCellAnimationState = { rings: [], lastTime: 0 };
        var state = mapAnimationFloor.active.activeCellAnimationState;
        // Step the existing rings by current time and remove expired rings...        
        state.rings = state.rings.filter(function (ring) {
            ring.lifeTime += currentElapsedTime;
            if (ring.lifeTime < ACTIVE_CELL_RING_LIFETIME)
                return true;
            else
                return false;
        });
        var lastTime = state.lastTime;
        var currTime = (lastTime + currentElapsedTime) % ACTIVE_CELL_RING_LOOP_PERIOD;
        // See if I need to add any rings...
        // Y(0)     Y(0.3)  Y(0.5)            (repeat)
        //  |________|_______|_________________|
        if ((lastTime > (0.5 * ACTIVE_CELL_RING_LOOP_PERIOD)) && (currTime < (0.3 * ACTIVE_CELL_RING_LOOP_PERIOD)))
            state.rings.push({ lifeTime: 0 });
        else if ((lastTime < (0.3 * ACTIVE_CELL_RING_LOOP_PERIOD)) && (currTime >= (0.3 * ACTIVE_CELL_RING_LOOP_PERIOD)))
            state.rings.push({ lifeTime: 0 });
        else if ((lastTime < (0.5 * ACTIVE_CELL_RING_LOOP_PERIOD)) && (currTime >= (0.5 * ACTIVE_CELL_RING_LOOP_PERIOD)))
            state.rings.push({ lifeTime: 0 });
        // Adjust the last-time value...
        state.lastTime = currTime;
        // Render the rings...
        var hsl = getHSLThemeColor();
        for (var i = 0, len = state.rings.length; i < len; i++) {
            var ring = state.rings[i];
            var intensity = ring.lifeTime / ACTIVE_CELL_RING_LIFETIME;
            var radius = intensity * ACTIVE_CELL_RING_RADIUS_MAX;
            animationCtx.beginPath();
            animationCtx.moveTo(toPixel(aCell.p[X1]), toPixel(aCell.p[Y1]));
            animationCtx.arc(toPixel(aCell.p[X1]), toPixel(aCell.p[Y1]), toPixel(radius), 0, 2 * Math.PI, false);
            animationCtx.fillStyle = "hsla(" + hsl.hDeg + "," + hsl.sPercent + "%," + hsl.lPercent + "%," + (1-intensity) + ")";
            //animationCtx.lineWidth = 2;
            animationCtx.fill();
        }
    }
    */

    /* ____________________________________________________ */
    /* Start/End indicators */

    function stepAndRenderStartEnd(floor) {
        var start = floor.start;
        if (start)
            renderStartEndAtCell(start, "start");
        var end = floor.end;
        if (end)
            renderStartEndAtCell(end, "end");
    }
    function renderStartEndAtCell(cell, startEnd) {
        if (!cell[startEnd + "imageStrip"]) {
            cell[startEnd + "imageStrip"] = { stripFrameNum: 0 };
            loadImageStripDetails(cell[startEnd + "imageStrip"], "template-theme0-"+startEnd+"-flag");
        }
        animationCtx.drawImageStrip(cell.p[X1], cell.p[Y1], cell[startEnd + "imageStrip"]);
    }


    /* General helpers */
    
    // Gets the element reference for a given ID from the cache if available, otherwise does
    // a DOM request for the element and caches it before returning.
    // Note: this is build this way to prevent caching the element directly into the render
    // object, since DOM elements are not serializable (the maze won't serialize), and if
    // I restore a maze object in the middle of some render state, I want to make the image
    // element available on-demand the first time its needed.
    function getImageStripElement(id) {
        if (imageStripElementResources[id])
            return imageStripElementResources[id];
        else {
            imageStripElementResources[id] = document.getElementById(id);
            return imageStripElementResources[id];
        }
    }

    function loadImageStripDetails(ob, id) {
        var element = getImageStripElement(id);
        ob.stripElementId = id;
        // General info for handling an image strip
        ob.stripItemHeight = element.naturalHeight; // Assumes horizontal image strips
        // Allow data-* properties to be ommitted.
        var stripWidth = element.getAttribute("data-strip-width");
        ob.stripItemWidth = (stripWidth != null) ? parseInt(stripWidth) : element.naturalWidth;
        ob.stripLoop = (element.getAttribute("data-loop") == "yes") ? true : false;
        if (ob.stripLoop)
            ob.stripLoopReverse = (element.getAttribute("data-loop-reverse") == "yes") ? true : false;
        var stripDur = element.getAttribute("data-strip-duration");
        ob.stripDuration = (stripDur != null) ? (parseFloat(stripDur) * 1000) : 1000; // in seconds->miliseconds (or 1sec by default)
        // Strip animation info
        ob.stripMaxFrames = Math.floor(element.naturalWidth / ob.stripItemWidth);
        ob.stripFrameNum = 0;
    }
    function setRotationDetails(ob, fullCircleTime) {
        ob.rotateDuration = fullCircleTime;
    }
    // Returns true if the image strip can still advance, false, if the image strip is done (complete)
    function advanceImageStrip(stripDetails) {
        if (stripDetails.stripMaxFrames == 1) // A strip of one frame (not really a strip, so it cannot be "advanced")
            return false;
        if ("number" != typeof stripDetails.stripTimePoint) // First time...
            stripDetails.stripTimePoint = 0;
        else
            stripDetails.stripTimePoint += currentElapsedTime;
        // Looping image strips are more complicated :-)
        if (stripDetails.stripLoop) {
            // Will never return false (since this just loops and loops and loops and loops and...
            if (stripDetails.stripLoopReverse) {
                stripDetails.stripTimePoint %= (stripDetails.stripDuration * 2); // Allow twice the length of the duration for the reverse logic w/out an extra property...
                if (stripDetails.stripTimePoint < stripDetails.stripDuration)
                    stripDetails.stripFrameNum = Math.floor(stripDetails.stripTimePoint / stripDetails.stripDuration * stripDetails.stripMaxFrames);
                else // >= duration && < duration*2 (since mode excludes the mod-ed value itself)
                    stripDetails.stripFrameNum = Math.floor((stripDetails.stripDuration - (stripDetails.stripTimePoint - stripDetails.stripDuration)) / stripDetails.stripDuration * stripDetails.stripMaxFrames * 0.9999999); // last part ensures that when time=duration, the floor doesn't exceed the maxframes
            }
            else { // repeat
                stripDetails.stripTimePoint %= stripDetails.stripDuration; // Cycle on the length of the duration
                stripDetails.stripFrameNum = Math.floor(stripDetails.stripTimePoint / stripDetails.stripDuration * stripDetails.stripMaxFrames);
            }
            return true;
        }
        // Non-looping image strip
        else if (stripDetails.stripTimePoint < stripDetails.stripDuration) { // <= means that the time point _could_ be exactly the duration, which would break the "floor" calculation below
            // Still time to go (compute the frame number based on the duration and maxframes)
            stripDetails.stripFrameNum = Math.floor(stripDetails.stripTimePoint / stripDetails.stripDuration * stripDetails.stripMaxFrames);
            return true;
        }
        else { // non-looping, and >= strip duration
            // Set to last frame and return false (done)
            stripDetails.stripFrameNum = stripDetails.stripMaxFrames - 1;
            return false;
        }
    }
    function advanceRotation(rotationSettings) {
        if ("number" != typeof rotationSettings.rotationTimePoint)
            rotationSettings.rotationTimePoint = 0;
        else
            rotationSettings.rotationTimePoint = (rotationSettings.rotationTimePoint + currentElapsedTime % rotationSettings.rotateDuration);
        rotationSettings.rotate = (rotationSettings.rotationTimePoint / rotationSettings.rotateDuration) * 2 * Math.PI;
    }
    function renderImageStripWithRotation(x, y, rotation, imageStripState) {
        animationCtx.save();
        animationCtx.translate(toPixel(x), toPixel(y));
        animationCtx.rotate(rotation);
        animationCtx.drawImageStripTargetCentered(imageStripState);
        animationCtx.restore();
    }
    CanvasRenderingContext2D.prototype.drawImageStripTargetCentered = function(stripSettings) {
        this.drawImage(getImageStripElement(stripSettings.stripElementId),
                /* source image location */
                stripSettings.stripFrameNum * stripSettings.stripItemWidth, 0, stripSettings.stripItemWidth, stripSettings.stripItemHeight,
                /* destination image location */
                toPixelImage(-stripSettings.stripItemWidth / 2), toPixelImage(-stripSettings.stripItemHeight / 2),
                toPixelImage(stripSettings.stripItemWidth), toPixelImage(stripSettings.stripItemHeight));
    }
    CanvasRenderingContext2D.prototype.drawImageStrip = function(x, y, stripSettings) {
        this.drawImage(getImageStripElement(stripSettings.stripElementId),
                /* source image location */
                stripSettings.stripFrameNum * stripSettings.stripItemWidth, 0, stripSettings.stripItemWidth, stripSettings.stripItemHeight,
                /* destination image location */
                toPixel(x) + toPixelImage(-stripSettings.stripItemWidth / 2), toPixel(y) + toPixelImage(-stripSettings.stripItemHeight / 2),
                toPixelImage(stripSettings.stripItemWidth), toPixelImage(stripSettings.stripItemHeight));
    }


    /* Animation loop */
    /********************************************************/
    function requestAnimationFrameElapsed(func) {
        var rightNow = performance.now();
        return requestAnimationFrame(function (currentTime) {
            func(currentTime - rightNow);
        });
    }

    function animateFloors(elapsed) {
        currentElapsedTime = elapsed;
        // Schedule the next iteration (early so that elapsed time will include the time I will spend
        //  executing this code)
        continueFloorAnimation = requestAnimationFrameElapsed(animateFloors);
        if (mazeapp.maze.currentFloorIndex == null) // Render all the floors since the maze is zoomed out
            mazeapp.maze.floors.forEach(animateFloor);
        else
            animateFloor(mazeapp.maze.floor);
    }

    // This function (and its dependents) expects to have the "animationCtx" values set.
    function animateFloor(floor) {
        animationCtx = floor.canvases[1].getContext('2d');
        // Clear the canvas in preparation for this re-draw step.
        animationCtx.clearRect(0, 0, animationCtx.canvas.width, animationCtx.canvas.height);
        // Step and render various graphical elements to the animation canvas...
        //stepAndRenderActiveCell();
        stepAndRenderPath(floor);
        stepAndRenderTransports(floor);
        stepAndRenderStartEnd(floor);
        animationCtx = null;
    }


    var ctx = null;
    var groups = null;
    var toPixelCache = null;
    var toPixelImageCache = null;
    var toColorCache = null; // Hard/Medium/Easy current color
    var continueFloorAnimation = null; // Default vaule (don't animate until enabled)
    var currentElapsedTime = 0; // Set temporarily on every animation step.
    var previousPathPulseTimeOffset = 0; // Used to track an animation pulse's position for path cells that change to the pulse state.
    var animationCtx = null;
    var imageStripElementResources = {}; // Saves the relevant resource elements for painting into the maze floors

    // May or may not be needed.
    var MAP_FLAG_START = "start";
    var MAP_FLAG_END = "end";
    var MAP_FLAG_PATH = "path"; // Used?
    var MAP_FLAG_TRANSPORT = "transport";

    var RENDERGROUPTYPE_RECT = 1;
    var RENDERGROUPTYPE_CIRC = 2;
    var X1 = 0;
    var Y1 = 1;
    var X2 = 2;
    var Y2 = 3;
    var TOP = 0;
    var LEFT = 1;
    var BOTTOM = 2;
    var RIGHT = 3;
    var NUMRECTSIDES = 4;
    var HALFCELL = 5; // 5 units in a half-cell (the radius of a cell)
    var ARRAY_DEFAULT_TOPLINE_OFFSETS = [HALFCELL, -HALFCELL, -HALFCELL, -HALFCELL];
    var ARRAY_DEFAULT_LEFTLINE_OFFSETS = [-HALFCELL, -HALFCELL, -HALFCELL, HALFCELL];
    var ARRAY_DEFAULT_BOTTOMLINE_OFFSETS = [-HALFCELL, HALFCELL, HALFCELL, HALFCELL];
    var ARRAY_DEFAULT_RIGHTLINE_OFFSETS = [HALFCELL, HALFCELL, HALFCELL, -HALFCELL];
    var NOT_CONNECTED_ORIGINAL_WALL = -1;
    var NOT_CONNECTED_INTERIOR_WALL = -2;
    var NOT_CONNECTED_UNRENDERED_WALL = -3;
    var WALLSTATE_INSIDE = 1;
    var WALLSTATE_BOUNDARY = 2;
    var TRANSPORT_TYPE_STAIRS_UP = 1;
    var TRANSPORT_TYPE_STAIRS_DOWN = 2;
    var TRANSPORT_TYPE_PORTAL_UP = 3;
    var TRANSPORT_TYPE_PORTAL_DOWN = 4;
    var TRANSPORT_TYPE_PORTAL_SAME = 5;
    var ANIMATION_STATE_PATH_TEMPERATURE = 0;
    var ANIMATION_STATE_PATH_PULSE = 1;
    var TRANSPORT_STATE_DISABLED = 1;
    var TRANSPORT_STATE_ENABLED = 2;
    var TRANSPORT_STATE_ACTIVE = 3;
    var TRANSPORT_STATE_GO = 4; // Signal that the transport has been activated and should engage (render thread sees this)

    var PATH_TYPE_BEGIN = 1;
    var PATH_TYPE_FORK = 2;
    var PATH_TYPE_TRANSPORT = 3;
    var PATH_TEMPERATURE_COOLDOWN_DURATION = 2000; // 2 seconds
    var PATH_PULSE_TIME_INTERVAL_BETWEEN_CELLS = 20; // In milliseconds (larger the number, the slower the pulse will travel (but it will have less gradient stops))
    var PATH_PULSE_ELAPSED_TIME_WRAPAROUND = 600000; // Wrap every ten minutes.
    var ACTIVE_CELL_RING_LOOP_PERIOD = 1800; // milliseconds
    var ACTIVE_CELL_RING_LIFETIME = 1000; // milliseconds
    var ACTIVE_CELL_RING_RADIUS_MAX = 4.7; // maze units
    // vars dependent on constants...
    var drawWallState = WALLSTATE_INSIDE;

    mazeapp.on("mazeloaded", function initForNewMaze() {

        mazeapp.maze.on("renderingenabled", function restartAnimation() {
            // Only start the animation loop if it is not running
            if (null == continueFloorAnimation) {
                continueFloorAnimation = requestAnimationFrameElapsed(animateFloors);
                // RequestAnimationFrame gets the current time on the callback (not elapsed)
                // Since this is just starting out cold, set the elapsed time to the time to first invoke - current time
                currentElapsedTime = new Date();
            }
        });

        mazeapp.maze.on("renderingdisabled", function pauseAnimation() {
            if (continueFloorAnimation == null)
                return; // The animation loop is already paused.
            cancelAnimationFrame(continueFloorAnimation); // Stops any pending animation frame
            continueFloorAnimation = null; // Ensure the state is clean for restartAnimation
        });
    });

    return {
        resetRenderStateToInitial: function(mazeFloor) {
            resetTransportFloorRenderState(mazeFloor);
            resetPathFloorRenderState(mazeFloor);
        },
        drawMazeFloorToCanvas: function (mazeFloor) {
            toPixelCache = mazeapp.pixelPerUnit; // In case this changed... 5-7
            toPixelImageCache = mazeapp.pixelPerUnit / (HALFCELL * 2);
            toColorCache = document.body.className;
            groups = mazeFloor.render;
            ctx = mazeFloor.canvases[0].getContext('2d');
            draw();
            groups = null;
            ctx = null;
        }
    };
})();
