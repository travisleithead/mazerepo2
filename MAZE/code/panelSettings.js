(function SettingsPanelScope() {
    
    function dismissSettingsKeyHandler(e) {
        if (e.key == "Esc") {
            mazeapp.viewSettings = false;
            e.stopPropagation();
        }
    }

    function dismissSettingsPointerHandler(e) { mazeapp.viewSettings = false; }

    // Prevents the pointer-down from hitting the parent and closing the settings
    function flyoutPointerDownTrap(e) { e.stopPropagation(); }

    function resetButtonClick(e) { mazeapp.reset(); }

    function ensureSliderValueBeforeSettingsShow() { // this is the slider element
        this.value = mazeapp.pixelPerUnit;
        document.getElementById("echoSliderValue").innerHTML = this.value;
        this.addEventListener("change", sizeSliderChange);
    }

    function ensureLangValueBeforeSettingsShow() { // this is the select element
        this.value = mazeapp.lang;
    }

    function disableSliderChange() { this.removeEventListener("change", sizeSliderChange); }

    function sizeSliderChange(e) {
        mazeapp.pixelPerUnit = parseInt(this.value);
        document.getElementById("echoSliderValue").innerHTML = this.value;
    }

    function enableFlyoutTrap() { this.addEventListener("MSPointerDown", flyoutPointerDownTrap); }

    function disableFlyoutTrap() { this.removeEventListener("MSPointerDown", flyoutPointerDownTrap); }

    function makeVisible() { // This is the settings panel element
        document.addEventListener("keyup", dismissSettingsKeyHandler);
        this.addEventListener("MSPointerDown", dismissSettingsPointerHandler);
        this.removeAttribute("hidden");
    }

    function hide() {
        document.removeEventListener("keyup", dismissSettingsKeyHandler);
        this.removeEventListener("MSPointerDown", dismissSettingsPointerHandler);
        this.setAttribute("hidden", "");
    }

    mazeapp.on("appinit", function modelReady() {
        var settingEl = document.querySelector("#settings");
        var flyoutEl = document.querySelector("#settings .flyout");
        var sizeSlider = document.getElementById("sizeSlider");

        mazeapp.on("settingsshow", ensureSliderValueBeforeSettingsShow, sizeSlider);
        mazeapp.on("settingsshow", enableFlyoutTrap, flyoutEl);
        mazeapp.on("settingsshow", makeVisible, settingEl);
        mazeapp.on("settingsshow", ensureLangValueBeforeSettingsShow, document.getElementById("languageChooser"));

        mazeapp.on("settingshide", disableSliderChange, sizeSlider);
        mazeapp.on("settingshide", disableFlyoutTrap, flyoutEl);
        mazeapp.on("settingshide", hide, settingEl);

        var resetButton = document.getElementById("resetGame");
        resetButton.addEventListener('click', resetButtonClick);

        var langSelect = document.getElementById("languageChooser");
        langSelect.onchange = function onLanguageSelectElementChange(e) {
            mazeapp.lang = e.target.value;
        };

        // Hookup this panel to Windows 8's Settings Charm.
        Windows.UI.ApplicationSettings.SettingsPane.getForCurrentView().oncommandsrequested = function (e) {
            // In here, the user has requested the Settings panel... populate it with settings commands
            e.request.applicationCommands.push(new Windows.UI.ApplicationSettings.SettingsCommand("op1", mazeapp.loc["Settings"], function (e) {
                // In here, the "settings.title" option was selected by the user... launch this panel!
                mazeapp.viewSettings = true;
            }));
        };
    });
})();

var Settings = {
    ThemeIndex: 0,
    // MAP layer
    SetContextForBackgroundFill: function (ctx) { ctx.fillStyle = "rgb(249,243,170)"; }, /* light */
    SetContextForBoundaryWall: function (ctx) { ctx.strokeStyle = "#FBD081"; ctx.lineWidth = 6; ctx.lineCap = "round"; ctx.shadowOffsetX = 1; ctx.shadowOffsetY = 1; ctx.shadowBlur = 3; ctx.shadowColor = "#833100"; },
    SetContextForInsideWall: function (ctx) { ctx.strokeStyle = "#FBD081"; ctx.lineWidth = 2; ctx.shadowOffsetX = 1; ctx.shadowOffsetY = 1; ctx.shadowBlur = 2; ctx.shadowColor = "#833100"; },
    // PATH
};