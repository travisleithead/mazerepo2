"use strict";
(function gameModuleScope() {
    // mazeapp.maze
    mazeapp.on("appinit", function oneTimeSetup() {
        document.getElementById("peekbar").addEventListener("click", appbarOn);
        document.getElementById("notice").addEventListener("animationend", clearNotice);
        document.getElementById("gotoZoomButton").addEventListener("click", toggleFloorView);
        document.getElementById("gotoPauseButton").addEventListener("click", onPauseButtonClicked);

        // Ensure that the appbar is shut down anytime the maze unloads (there is a potentially dangerous timer that must be stopped). This is a no-op if the appbar is hidden at the time of unload, but this is not guaranteed, since a reset can come at any time.
        mazeapp.on("mazeunloaded", handleHideAppbar, document.getElementById("game"));
    });

    mazeapp.on("mazeloaded", function gameViewInit() {

        var floorHolder = document.querySelector("#floorHolder");

        for (var i = mazeapp.maze.floors.length - 1; i >= 0; i--) {
            var floorTemplate = document.querySelector("#template-floor").clone();
            floorTemplate.querySelector(".left-side > .floorName").locText = "Floor";
            floorTemplate.querySelector(".left-side > .floorNum").innerHTML = (i + 1);

            mazeapp.maze.floors[i].canvases = floorTemplate.querySelectorAll(".right-side canvas");

            // Setup the canvases
            var oneHundredPercentPxWidth = mazeapp.maze.width * mazeapp.pixelPerUnit;
            var oneHundredPercentPxHeight = mazeapp.maze.height * mazeapp.pixelPerUnit;
            mazeapp.maze.floors[i].canvases[0].width = oneHundredPercentPxWidth
            mazeapp.maze.floors[i].canvases[0].height = oneHundredPercentPxHeight;
            mazeapp.maze.floors[i].canvases[1].width = oneHundredPercentPxWidth;
            mazeapp.maze.floors[i].canvases[1].height = oneHundredPercentPxHeight;


            var mazeReference = floorTemplate.querySelector(".mazeReference");
            mazeReference.style.width = oneHundredPercentPxWidth + "px";
            mazeReference.style.height = oneHundredPercentPxHeight + "px";

            // Setup the active cell
            var activeSpot = floorTemplate.querySelector(".activeSpot");
            activeSpot.style.width = (mazeapp.pixelPerUnit * 10 * 3) + "px"; // 3x3 unit grid
            activeSpot.style.height = (mazeapp.pixelPerUnit * 10 * 3) + "px";
            activeSpot.addEventListener("MSPointerDown", pointerDownOnTheActiveSpot);
            // Register for changes to the active cell in order to adjust the active spot...
            mazeapp.maze.floors[i].on("activechanged", positionActiveSpot, activeSpot);
            mazeapp.maze.floors[i].on("activeon", showActiveSpot, activeSpot);
            mazeapp.maze.floors[i].on("activeoff", hideActiveSpot, activeSpot);
            // Put these new elements into the game...
            floorHolder.appendChild(floorTemplate);
            // It's convenient to set this up here while the activeSpot element is in scope...
            if (mazeapp.maze.currentFloorIndex == i) { // If this is the starting/last-resumed-state floor...
                // Enable the active spot in its place
                positionActiveSpot.call(activeSpot, {}, mazeapp.maze.floors[i].active);
                showActiveSpot.call(activeSpot);
            }
            // draw the maze canvas
            MazeRender.drawMazeFloorToCanvas(mazeapp.maze.floors[i]);
        }
        // Ensure that the currentFloorIndex (whatever it is set to) is the visible floor now that all the floors are created...
        floorHolder.scrollTop = floorTopPixel(mazeapp.maze.currentFloorIndex, false);
        // Start the selected floor out with some user-input handlers...
        mazeapp.maze.floor.canvases[1].addEventListener("MSPointerMove", pointerMoveOnTheCanvas);
        mazeapp.maze.floor.canvases[1].addEventListener("MSPointerUp", pointerUpOnTheCanvas);

        // Register for floor "selection" and "de-selection"
        mazeapp.maze.on("selected", floorSelected, floorHolder);
        mazeapp.maze.on("unselected", floorUnSelected);
        mazeapp.maze.on("zoomout", makeViewZoomedOut, floorHolder); // Fire in context of the floorHolder element...
        mazeapp.maze.on("zoomin", makeViewZoomedIn, floorHolder); // A floor will always be <selected> before the <zoomin> event triggers.
        // Register for transport actions
        mazeapp.maze.on("transportgo", followTransport)
        
        // Setup the appbar on/off handlers
        var gameEl = document.getElementById("game");
        mazeapp.maze.on("appbarshow", handleShowAppbar, gameEl);
        mazeapp.maze.on("appbarhide", handleHideAppbar, gameEl);
        var titleBarEl = document.getElementById("titleBar");
        mazeapp.maze.on("appbarshow", installTitleBarPointerTrap, titleBarEl);
        mazeapp.maze.on("appbarhide", uninstallTitleBarPointerTrap, titleBarEl);
        var appBarEl = document.getElementById("appCommandBar");
        mazeapp.maze.on("appbarshow", installAppBarPointerTrap, appBarEl);
        mazeapp.maze.on("appbarhide", uninstallAppBarPointerTrap, appBarEl);

        // Setup paused handlers for the renderer and clock state
        mazeapp.maze.on("pausedshow", onGamePaused);
        mazeapp.maze.on("pausedhide", onGameResumedFromPaused);
        // Update some static content:
        document.getElementById("titleBarGameNum").innerText = mazeapp.maze.stats.gameNum;
        var gameTitleEl = document.getElementById("titleBarGameTitle");

        gameTitleEl.locText = mazeapp.maze.stats.gameNameLocId;
        if (mazeapp.maze.exceededFastestTime) // No "fastest time" to report yet
            gameEl.setAttribute("data-post-fastest", "");
        else
            document.getElementById("statsBarBestTime").innerText = parseMinSec(mazeapp.maze.stats.timeFastest);
        mazeapp.maze.on("fastesttimeexceeded", switchToElapsedTime, gameEl);

        // Hide the multi-floor selector button if there is not more than one floor
        if (mazeapp.maze.floors.length > 1)
            document.getElementById("gotoZoom").style.display = "";
        else // Hide it
            document.getElementById("gotoZoom").style.display = "none";
    });

    mazeapp.on("mazeunloaded", function gameViewCleanup() {
        
        var floorHolderEl = document.querySelector("#floorHolder");
        // Clear the floor holder
        floorHolderEl.innerHTML = ""
        // Clear any [potential] zoomed-out state (if unloading during zoomed-out state)
        floorHolderEl.className = "";
        // TODO: Check to see if I have a leak with all the context elements that are cached
        // in the event registration path "on(..,..,ctxEl)". If so, this method needs to safely remove 
        // them all.
        var gameEl = document.getElementById("game");
        gameEl.removeAttribute("data-post-fastest"); // In case it was set (get back to a "clean" status on exit).
        gameEl.removeAttribute("data-notice"); // ditto
    });

    function floorSelected(modelEvent, isLevelSwitch) { // can use 'mazeapp.maze.floor' shortcut.
        if (isLevelSwitch) {
            var promise = modelEvent.getPromise();
            var target = this;
            target.classList.add("movefloor");
            target.addEventListener("transitionend", function onTransitionEndPhase1() {
                target.removeEventListener("transitionend", onTransitionEndPhase1);
                // Get the current value of 100% height...	
                target.animatedScrollTop = floorTopPixel(mazeapp.maze.currentFloorIndex, false);
                target.addEventListener("scrollend", function onScrollEnd() {
                    target.removeEventListener("scrollend", onScrollEnd);
                    target.classList.remove("movefloor");
                    target.addEventListener("transitionend", function onTransitionEndPhase2() {
                        target.removeEventListener("transitionend", onTransitionEndPhase2);
                        // All done.
                        promise.done();
                    });
                });
            });
        }
        // Otherwise, this floor is selected from being zoomed out.
        // Enable game play interaction
        mazeapp.maze.floor.canvases[1].addEventListener("MSPointerMove", pointerMoveOnTheCanvas);
        mazeapp.maze.floor.canvases[1].addEventListener("MSPointerUp", pointerUpOnTheCanvas);
    }

    function floorUnSelected(modelEvent, unselectedFloorIndex) {
        // Stope taking input.
        mazeapp.maze.floors[unselectedFloorIndex].canvases[1].removeEventListener("MSPointerMove", pointerMoveOnTheCanvas);
        mazeapp.maze.floors[unselectedFloorIndex].canvases[1].removeEventListener("MSPointerUp", pointerUpOnTheCanvas);
    }

    function makeViewZoomedOut(modelEvent, lastFloorIndex) {
        var promise = modelEvent.getPromise();
        var tar = this;
        tar._animationDuration = 500;
        var zoomFactor = mazeapp.maze.floors[lastFloorIndex].canvases[0].verticalFitToWindowPercentage;
        var reverseLastFloorIndex = toFloorElementIndex(lastFloorIndex);
        tar.querySelectorAll(".right-side").forEach(function (zoomerEl, i) { // This iterates the floors in reverse [logical] order
            if (i != reverseLastFloorIndex) {
                zoomerEl.msContentZoomFactor = zoomFactor; // No animation
            }
            else {
                zoomerEl._animationDuration = 250;
                zoomerEl.msAnimatedContentZoomFactor = zoomFactor;
                zoomerEl.addEventListener("zoomend", function onZoomEnd() {
                    // Zoom factor animation ended.
                    zoomerEl.removeEventListener("zoomend", onZoomEnd);
                    delete zoomerEl._animationDuration;
                    tar.classList.add("zoomOut");
                    if (lastFloorIndex == 0)
                        tar.scrollTop = floorTopPixel(lastFloorIndex, true); // Directly set the value (no animation)
                    tar.classList.add((lastFloorIndex == 0) ? "istFloor" : "nthFloor");
                    tar.addEventListener("animationend", function onAnimationEnd(e) {
                        if (e.eventPhase != Event.AT_TARGET) return;
                        tar.removeEventListener("animationend", onAnimationEnd);
                        promise.done();
                    });
                });
            }
        });
    }


    function makeViewZoomedIn(modelEvent) {
        var tar = this; // The floorHolder context element
        if (mazeapp.maze.currentFloorIndex == null)
            return console.debugAssert("Ooops! Can't depend on currentFloorIndex being null for the following operation.");
        var destinationFloor = mazeapp.maze.currentFloorIndex; // Cache between async callbacks
        var promise = modelEvent.getPromise();
        // Ensure's the selected floor is aligned (cannot guarantee snap points in Win8 for mouse/keyboard/touch pad)
        function makeViewZoomedIn_Stage1_SnapToFloor() {
            tar._animationDuration = 250;
            tar.animatedScrollTop = floorTopPixel(destinationFloor, true);
            tar.addEventListener("scrollend", makeViewZoomedIn_Stage2_PerformZoom);
        }
        function makeViewZoomedIn_Stage2_PerformZoom() {
            tar.removeEventListener("scrollend", makeViewZoomedIn_Stage2_PerformZoom);
            delete tar._animationDuration; // Reset to default.
            // Cleanup zoom out state
            tar.classList.remove("zoomOut");
            tar.classList.remove("istFloor");
            tar.classList.remove("nthFloor");
            // Setup for the zoom-in.
            tar.classList.add("zoomIn");
            tar.classList.add((destinationFloor == 0) ? "istFloor" : "nthFloor");
            tar.addEventListener("animationend", function onAnimationEnd(e) {
                if (e.eventPhase != Event.AT_TARGET) return;
                tar.removeEventListener("animationend", onAnimationEnd);
                tar.classList.remove("zoomIn");
                tar.classList.remove("istFloor")
                tar.classList.remove("nthFloor");
                if (destinationFloor == 0)
                    tar.scrollTop = floorTopPixel(destinationFloor, false);
                var reverseDestinationFloor = toFloorElementIndex(destinationFloor);
                // Remove the constrictive zoom settings
                tar.querySelectorAll(".right-side").forEach(function (zoomerEl, i) {
                    if (i != reverseDestinationFloor) {
                        zoomerEl.msContentZoomFactor = 1;
                    }
                    else {
                        zoomerEl._animationDuration = 250;
                        zoomerEl.msAnimatedContentZoomFactor = 1;
                        zoomerEl.addEventListener("zoomend", function onZoomEnd() {
                            delete zoomerEl._animationDuration;
                            zoomerEl.removeEventListener("zoomend", onZoomEnd);
                            promise.done();
                        });
                    }
                });
            });
        }
        if (Math.abs(floorTopPixel(destinationFloor, true) - tar.scrollTop) < 10) { // Approximately snapped (close enough not to animate)
            tar.scrollTop = floorTopPixel(destinationFloor, true); // Force snap (no animate)
            makeViewZoomedIn_Stage2_PerformZoom();
        }
        else
            makeViewZoomedIn_Stage1_SnapToFloor();
    }

    // Util for managing the zoom conditions...
    function floorTopPixel(floorIndex, applyZoomedOutRule) {
        if (applyZoomedOutRule && (floorIndex == 0)) // The bottom floor is never scrolled to the top in zoomed out view.
            floorIndex = 1;
        return innerHeight * toFloorElementIndex(floorIndex);
    }

    // When the active spot is enabled
    function showActiveSpot() { // 'this' is the active element
        this.style.visibility = "visible";
    }

    function hideActiveSpot() { // 'this' is the active element
        this.style.visibility = "hidden";
    }
    
    // Triggered when the mazeapp active floor changes (for each floor)
    function positionActiveSpot(modelEvent, mapCellFromSomeFloor) { // 'this' is the floor's active spot element (and the param will correspond to that floor).
        var oneAndAHalfCells = 15 * mazeapp.pixelPerUnit; // 10 + (10/2)
        this.style.left = ((mazeapp.pixelPerUnit * mapCellFromSomeFloor.p[0]) - oneAndAHalfCells) + "px";
        this.style.top = ((mazeapp.pixelPerUnit * mapCellFromSomeFloor.p[1]) - oneAndAHalfCells) + "px";
    }

    function pointerDownOnTheActiveSpot(e) {
        // If I'm zoomed out, bail out, since I won't have an actual selected floor to work against.
        // Only necessary for the active spot -- the canvas event handlers are disconnected on selection change.
        if (mazeapp.maze.currentFloorIndex == null)
            return;
        // Retarget this pointer to the canvas element for future movements (as if the pointer is dragging along the canvas.
        mazeapp.maze.floor.canvases[1].msSetPointerCapture(e.pointerId);
    }

    function pointerUpOnTheCanvas(e) {
        // Try to "activate" the cell being released.
        var cell = mazeapp.maze.floor.hitTestMaze(toPointInMazeUnits(e.offsetX, e.offsetY));
        if (!cell)
            return;
        if (cell === mazeapp.maze.floor.active) // Triggering is only possible on the active spot on release for any given cell (mouse/finger must be released on that spot to activate)
            mazeapp.maze.floor.triggerCell(cell);
        if (cell.path)
            mazeapp.maze.floor.active = cell; // Point releases on existing paths re-position the active cell at that location.
    }

    function pointerMoveOnTheCanvas(e) {
        // Ensure that a buttons is 1 (so that mouse moves without the button down don't do anything)
        if (e.buttons != 1)
            return;
        var cell = mazeapp.maze.floor.hitTestMaze(toPointInMazeUnits(e.offsetX, e.offsetY));
        if (!cell)
            return;
        if (mazeapp.maze.floor.isAdjacentToActiveCell(cell)) {
            var f = mazeapp.maze.floor;
            if (cell.path)
                f.active = cell; // Move the active cell to the current location, as long as the current location is already on a path (allow re-position of the active cell along a path by "dragging")
            else {
                f.extendPathToCell(f.active, cell);
                f.active = cell;
                checkGameEnd(cell);
            }
        }
    }

    function checkGameEnd(cell) {
        if (cell.end) {
            // Stop the clock
            mazeapp.maze.clockState = "stopped";
            // Increment the number of games solved on this level (if this was previously unsolved)...
            if (mazeapp.maze.stats.completedCount == 0) // This game has not been previously won...
                mazeapp.levels[mazeapp.currentLevel].solvedMazeCount++;
            // Update the game statistics (which updates the completedCount as well)
            mazeapp.maze.stats.updateStats(mazeapp.maze.elapsedTime);
            // Show the game-end screen
            mazeapp.viewFinished = true;
        }
    }

    // When the user activates a transport, the model fires this handler.
    function followTransport(modelEvent, floorIndex, cellRef) {
        if (floorIndex != mazeapp.maze.currentFloorIndex)
            mazeapp.maze.currentFloorIndex = floorIndex;
        mazeapp.maze.floors[floorIndex].active = cellRef;
    }

    function toFloorElementIndex(floorIndex) { return mazeapp.maze.floors.length - floorIndex - 1; }

    function toCanvasFromActiveSpotOffsets(e) { var style = e.target.style; return { offsetX: e.offsetX + parseInt(style.left), offsetY: e.offsetY + parseInt(style.top) }; }

    // Helper class (converts client coordinates into maze offset coordinates and maze local units)
    function toPointInMazeUnits(canvasX, canvasY) {
        var res = { x:0, y:0 };
        res.x = canvasX / mazeapp.pixelPerUnit;
        res.y = canvasY / mazeapp.pixelPerUnit;
        return res;
    }

    function appbarOn() { mazeapp.maze.viewAppbar = true; }

    function appbarOff() { mazeapp.maze.viewAppbar = false; }

    function dismissAppbarKeyHandler(e) {
        if (e.key == "Esc") {
            appbarOff();
            e.stopImmediatePropagation();
            // Since some of the confirmation boxes may also add a key handler (and this one is registered first), I can prevent
            // their handlers from firing by stopping the immediate propagation. Otherwise they might fire just to close their respective panels.
        }
    }

    function flyoutPointerDownTrap(e) { e.stopPropagation(); }

    // 'this' is the titleBarEl
    function installTitleBarPointerTrap() { this.addEventListener("MSPointerDown", flyoutPointerDownTrap); }
    function installAppBarPointerTrap() { this.addEventListener("MSPointerDown", flyoutPointerDownTrap); }
    function uninstallTitleBarPointerTrap() { this.removeEventListener("MSPointerDown", flyoutPointerDownTrap); }
    function uninstallAppBarPointerTrap() { this.removeEventListener("MSPointerDown", flyoutPointerDownTrap); }

    var timeoutUpdateVisibleElapsedTime = null;

    function updateElapsedTime() {
        var t = mazeapp.maze.elapsedTime;
        if (mazeapp.maze.exceededFastestTime)
            document.getElementById("timeValue").innerHTML = parseMinSec(t);
        else { // Still under the fastest time
            var tFastest = mazeapp.maze.stats.timeFastest;
            if (t <= tFastest) {
                t = tFastest - t;
                document.getElementById("timeValueToGo").innerHTML = parseMinSec(t);
            }
            else { // The current time just exceeded the fastest time!
                mazeapp.maze.exceededFastestTime = true;
                t = 0;
            }
        }
        if (t < 60000)
            timeoutUpdateVisibleElapsedTime = setTimeout(updateElapsedTime, 100); // 100ms intervals 1/10 second
        else
            timeoutUpdateVisibleElapsedTime = setTimeout(updateElapsedTime, 500); // 1/2s intervals (to avoid getting more than 1/2 second off the real time)
    }

    function switchToElapsedTime() { this.setAttribute("data-post-fastest"); }

    // 'this' is the #game element.
    function handleShowAppbar() {
        updateElapsedTime(); // Synchronously before the app bar starts appearing (it will self-queue the next time update)...
        this.setAttribute("data-appbar","");
        this.addEventListener("MSPointerDown", appbarOff);
        document.addEventListener("keyup", dismissAppbarKeyHandler);
    }

    function handleHideAppbar() {
        this.removeAttribute("data-appbar");
        this.removeEventListener("MSPointerDown", appbarOff);
        clearTimeout(timeoutUpdateVisibleElapsedTime);
        timeoutUpdateVisibleElapsedTime = null;
        document.removeEventListener("keyup", dismissAppbarKeyHandler);
    }

    function enableUserInvokeAppbar() {
        // WIN8 Integration ************************
        // Hookup Appbar to Windows 8 "edge" gesture
        var edgy = Windows.UI.Input.EdgeGesture.getForCurrentView();
        edgy.addEventListener("starting", appbarOn); // for touch
        edgy.addEventListener("completed", appbarOn); // For keyboard/mouse (also fires at the end of touch)
        edgy.addEventListener("canceled", appbarOff); // Ensures proper state management...
    }

    function disableUserInvokeAppbar() {
        var edgy = Windows.UI.Input.EdgeGesture.getForCurrentView();
        edgy.removeEventListener("starting", appbarOn); // for touch
        edgy.removeEventListener("completed", appbarOn); // For keyboard/mouse (also fires at the end of touch)
        edgy.removeEventListener("canceled", appbarOff);
    }

    function clearNotice(e) {
        if (e.eventPhase != Event.AT_TARGET) return;
        // The animation for the notice element has finished playing...
        document.querySelector("#game").removeAttribute("data-notice");
    }

    var lastFloor = null;
    function toggleFloorView(e) {
        if (mazeapp.maze.currentFloorIndex == null) // zoomed out, but new floor not selected explicitly
            mazeapp.maze.currentFloorIndex = lastFloor;
        else {
            lastFloor = mazeapp.maze.currentFloorIndex;
            mazeapp.maze.currentFloorIndex = null;
        }
        appbarOff();
    }

    function onPauseButtonClicked(e) { mazeapp.maze.viewPaused = true; }

    function onGamePaused(modelEvent) {
        mazeapp.maze.clockState = "stopped";
        mazeapp.maze.renderingEnabled = false;
    }

    function onGameResumedFromPaused(modelEvent) {
        mazeapp.maze.clockState = "running";
        mazeapp.maze.renderingEnabled = true;
    }

    // Setup the view-in/out handlers
    mazeapp.on("gameshow", function onGameShow() {
        document.querySelector("#game").removeAttribute("hidden");
        enableUserInvokeAppbar();
        mazeapp.maze.clockState = "running"; // Stopped explicitly when the end cell is encountered
        mazeapp.maze.renderingEnabled = true;
    });

    mazeapp.on("gamehide", function onGameHide() {
        document.querySelector("#game").setAttribute("hidden", "");
        disableUserInvokeAppbar();
        // clockState is already stoppped if the game ends by solving--otherwise in the case of reset, stopping the clock is irrelevant.
    });

    mazeapp.on("mazeunloaded", function onGameUnloading() {
        mazeapp.maze.renderingEnabled = false; // the rendering state must be stopped (this fires syncronously from inside the async handler)
    });
})();

