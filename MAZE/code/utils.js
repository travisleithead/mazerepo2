// General Utilities

(function anonUtilInit() {
	var DOMCollectionAdditionalProps = {
		array: { 
			enumerable: true,
			get: function() {
				return Array.prototype.slice.call(this, 0); // Creates an array out of a DOM NodeList instance
			}
		},
		forEach: {
			enumerable: true,
			value: function(callback, thisToUse) {
				return this.array.forEach(callback, thisToUse);
			}
		}
	};

	Object.defineProperties(NodeList.prototype, DOMCollectionAdditionalProps);
	Object.defineProperties(HTMLCollection.prototype, DOMCollectionAdditionalProps);

	var debugConfig = true; // TODO: Switch this to false for publication.
	Object.defineProperty(console, "debugAssert", { value: function (x) { if (debugConfig) throw new Error(x); else console.error(x); } });

    // Fancy animated form of HTMLElement.prototype.scrollTop. Fires the "scrollend" event when done.
    // Configuration can be altered by adding the following to the element:
    // * _animationTimingFunction = ""; // Allowed CSS property values except "step-start", "step-end", "steps(...)", "cubic-bezier(...)" out of lazyiness.
    // * _animationDuration = ""; // <number> in milliseconds (only)
	Object.defineProperties(HTMLElement.prototype, {
	    animatedScrollTop: {
	        enumerable: true,
	        get: function () { return this.scrollTop; },
	        set: function (px) {
	            if (typeof px != "number")
	                console.debugAssert("The property only accepts posivite numbers");
	            if (typeof this._animationTimingFunction != "string")
	                this._animationTimingFunction = "ease"; // The default animation timing function.
	            var timingCacheKey = "animationTimingFunctionCache_" + this._animationTimingFunction;
	            if (!Math[timingCacheKey]) // Build the animation timing function cache (if not already there)
	                Math[timingCacheKey] = Math.createDiscreteCubicCurve(this._animationTimingFunction);
	            if (typeof this._animationDuration != "number")
	                this._animationDuration = 1000; // The default animation duration (if not specified)
	            var elem = this;
	            if (elem._pendingFrame) // One animation running per element only...
	                cancelAnimationFrame(elem._pendingFrame);
	            elem._pendingFrame = null;
	            var relativeTimeStart = performance.now();
	            var elemStartPx = elem.scrollTop;
	            elem._pendingFrame = requestAnimationFrame(function scriptAnimation(time) {
	                var elapsedTime = time - relativeTimeStart;
	                if (elapsedTime > elem._animationDuration) {
	                    elem.scrollTop = px;
	                    elem._pendingFrame = null;
	                    var e = document.createEvent("Event");
	                    e.initEvent("scrollend", false, false);
	                    elem.dispatchEvent(e);
	                }
	                else {
	                    elem.scrollTop = elemStartPx + Math.floor((px - elemStartPx) * Math[timingCacheKey][Math.floor((elapsedTime / elem._animationDuration) * 100)]);
	                    elem._pendingFrame = requestAnimationFrame(arguments.callee);
	                }
	            });
	        }
	    },
	    msAnimatedContentZoomFactor: { // Fires "zoomend" event when done.
	        enumerable: true,
	        get: function () { return this.msContentZoomFactor; },
	        set: function (ratio) {
	            if (typeof ratio != "number")
	                console.debugAssert("The property only accepts number ratios");
	            if (typeof this._animationTimingFunction != "string")
	                this._animationTimingFunction = "ease"; // The default animation timing function.
	            var timingCacheKey = "animationTimingFunctionCache_" + this._animationTimingFunction;
	            if (!Math[timingCacheKey]) // Build the animation timing function cache (if not already there)
	                Math[timingCacheKey] = Math.createDiscreteCubicCurve(this._animationTimingFunction);
	            if (typeof this._animationDuration != "number")
	                this._animationDuration = 1000; // The default animation duration (if not specified)
	            var elem = this;
	            if (elem._pendingFrame) // One animation running per element only...
	                cancelAnimationFrame(elem._pendingFrame);
	            elem._pendingFrame = null;
	            var relativeTimeStart = performance.now();
	            var elemStartRatio = elem.msContentZoomFactor;
	            elem._pendingFrame = requestAnimationFrame(function scriptAnimation(time) {
	                var elapsedTime = time - relativeTimeStart;
	                if (elapsedTime > elem._animationDuration) {
	                    elem.msContentZoomFactor = ratio;
	                    elem._pendingFrame = null;
	                    var e = document.createEvent("Event");
	                    e.initEvent("zoomend", false, false);
	                    elem.dispatchEvent(e);
	                }
	                else {
	                    elem.msContentZoomFactor = elemStartRatio + ((ratio - elemStartRatio) * Math[timingCacheKey][Math.floor((elapsedTime / elem._animationDuration) * 100)]);
	                    elem._pendingFrame = requestAnimationFrame(arguments.callee);
	                }
	            });
	        }
	    },
	    locText: {
	        enumerable: true,
	        set: function (locID) {
	            mazeapp.locElement(locID, this);
	        }
	    }
	});


	Object.defineProperty(HTMLCanvasElement.prototype, "verticalFitToWindowPercentage", {
	    enumerable: true,
	    get: function () {
	        if (innerHeight < this.height) // TODO: This is a hack until I can center and zoom out at the same time...
	            return parseFloat((innerHeight / this.height).toFixed(2));
	        else
	            return 1; // No percentage change
	    }
	});

    // Creates (returns) a discrete cubic curve matching the predefined types or explicit control points provided
    // p2_x, p2_y, p3_x, p3_y are optional unless curveType is "custom"
	Math.createDiscreteCubicCurve = function (curveType, p2_x, p2_y, p3_x, p3_y) {
	    if (typeof curveType != "string")
	        curveType = "ease";
	    if ((curveType == "custom") && ((typeof p2_x != "number") || (typeof p2_y != "number") || (typeof p3_x != "number") || (typeof p3_y != "number")))
	        throw Error("Custom cubic bezier curves require providing Points 2 (x1,y1) and 3 (x2,y2) of the cubic curve");
	    var bezFunc = null;
	    var makeCubicBezierFunction = function (x1, y1, x2, y2) {
	        // b(t) = (1-t)^3*[0,0] +  // --> 0
	        //        3((1-t)^2)*t*[x1,y1] +
	        //        3(1-t)*(t^2)*[x2,y2] +
	        //        t^3*[1,1]  // --> [t^3,t^3]
	        // Returns a discrete [x,y] array for the bezier curve parameters.
	        return function bezier_curve(t) {
	            return [(3 * Math.pow(1 - t, 2) * t * x1) + ((3 * (1 - t)) * Math.pow(t, 2) * x2) + Math.pow(t, 3),
                    (3 * Math.pow(1 - t, 2) * t * y1) + ((3 * (1 - t)) * Math.pow(t, 2) * y2) + Math.pow(t, 3)];
	        };
	    }
	    switch (curveType) {
	        case "ease": bezFunc = makeCubicBezierFunction(0.25, 0.1, 0.25, 1); break;
	        case "ease-in": bezFunc = makeCubicBezierFunction(0.25, 0.1, 0.25, 1); break;
	        case "ease-out": bezFunc = makeCubicBezierFunction(0.25, 0.1, 0.25, 1); break;
	        case "ease-in-out": bezFunc = makeCubicBezierFunction(0.25, 0.1, 0.25, 1); break;
	        case "custom": bezFunc = makeCubicBezierFunction(p2_x, p2_y, p3_x, p3_y); break;
	        case "linear": bezFunc = function (x) { return [x, x]; }; break;
	    }
	    var curvePoints = []; // Create/clear the old cache
	    var steps = [];
	    for (var i = 0, len = 100; i < len; i++)
	        steps.push(bezFunc(i / 100));
	    // Make the x values in the steps array the array indexes for the corresponding y-values
	    var lastValue = 0;
	    for (var i = 0, len = 100; i < len; i++) {
	        var index = Math.floor((steps[i][0] * 100));
	        if (index > i) { // Repeat values of X where the X-curvature is gappy.
	            for (var s = i, stop = index; s < stop; s++)
	                curvePoints.push(lastValue);
	            i = index;
	        }
	        // index == i or i has been adjusted forward so that it equals index.
	        lastValue = steps[i][1];
	        curvePoints.push(lastValue);
	    }
	    return curvePoints;
	    // Math.cache is an array where the indexes are X-values and their value is the output Y
	}
	

})();


// Converts a number (in milliseconds) into min:sec format for display
// Minutes not shown if < 1 minutes.
// Always at least three digits X:XX (four if the param is specified X:XX.X)
function parseMinSec(milli) {
    var out = "";
    milli = parseInt(milli); // Remove extraneous fractional miliseconds
	var tenths = Math.floor((milli % 1000) / 100);
	var seconds = Math.floor(milli / 1000); // Round down, if <0, then 0.
	var minutes = Math.floor(seconds / 60);
	seconds = seconds - (minutes * 60); // Factor out the number of minutes
    if (minutes > 0)
	    out += minutes + ":";
	// pad the seconds (always double-digits)
	if ((minutes > 0) && (seconds < 10))
		out += "0";
	// Add the seconds and return
	return out + seconds + (!minutes ? "." + tenths : "");
}


// Adds numerical separators per the language constants defined in the resource strings file.
var parseScore = function (num) {
	var str = num.toString();
	// Insert commas every 3 digits (from the right)
	var i = str.length % LANG_DIGIT_GROUP_NUM; // This [conveniently] is also the starting offset (except for the zero case)
	if (i == 0)
		i = LANG_DIGIT_GROUP_NUM;
	var tail = 0;
	var out = str.substring(tail, i);
	while (i < str.length) {
		out += LANG_DIGIT_GROUP_SEPARATOR;
		tail = i;
		i += LANG_DIGIT_GROUP_NUM;
		out += str.substring(tail, i);
	}
	return out;
}

// English
var LANG_DIGIT_GROUP_NUM = 3; // Every three digits in a number get a separator 12[,]345[,]678[,]910
var LANG_DIGIT_GROUP_SEPARATOR = ",";