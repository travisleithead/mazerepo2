﻿"use strict";
// Parser-blocking script (high-priority, runs in first set of scripts)

// Model interaction:
// The model consists of this file, plus any model "helpers" that provide specific data access services
// (like the database). The model interacts with model helpers through the async promises pattern


// Data model defintions and implementation (on the mazeapp global)
// ?+* refer to their regexp meanings.  

// model: (AppModel)
// {                                                <appsuspend> / <appresume> fired only between load* and unload method calls...
//    init()                                        <appinit>{1} event
//    reset()                                       <appreinit>* event. May occur at any time.
//    currentLevel: # (index into levels array)
//    activeMaze: # or null
//    payingCustomer: bool           			 	<unlock>? event
//    lang: { "en", "pt" }                          <langchange>+
//    loc: { string table }                         
//    locElement( string, element)                  sets the localization attribute and value on the element, given the loc ID and target element
//    viewLevels: bool								<levelselectshow>* / <levelselecthide>* events
//    viewGame: bool								<gameshow> / <gamehide> events
//    viewFinished: bool                            <finishedshow> / <finishedhide> events
//    viewSettings: bool							<settingsshow> / <settingshide> events
//    levels: [                                     <levelsloaded>
//       {                                          <selected> / <unselected> events
//           solvedMazeCount: #						<solvedmazecountchange> event
//			 defaultLocked: bool
//           mazes: [
//              {                                   <activated> / <deactivated> events
//                  gameNum: #
//                  gameNameLocId: string
//                  smallCheesesAcquired: # (1-based) <smallcheesenumchange> event
//                  timeFastest: #					
//                  timeAverage: #					
//                  timeSlowest: #					// If timeSlowest is 0, then the maze hasn't been played yet (that's the indicator)
//                  completedCount: #               // Times the maze has been finished.
//                  template: string
//                  updateStats( time )             // Fires <statsupdate (fastest, slowest, sumTime, completedCount)>* event
//              }
//           ]
//       }
//    ]
//    loadMaze()                                    <mazeloaded> event. Takes the relative file name and loads it into the maze model
//    unloadMaze(callback)                          <mazeunloaded> fires before the maze object is cleared out; callback is invoked after unload completes...
//	  maze: maze or null                            // Setable only during appinit/appreinit (takes a suspended maze object)
//    pixelPerUnit: #
// }
/*
 * Constructor function for the AppModel (known as "mazeapp" hereafter)
 */
Object.defineProperty(window, "AppModel", { // enumerable, configurable, writable are assumed false if not set to true.
    value: function () {
        // Initial defaults
        var initialized = false;
        var currentLevel = 0;
        var activeMaze = null;
        var activeMazeTransition = false;
        var payingCustomer = false; // May be set to true
        var ENUM_LANG = { en: "en", pt: "pt" };
        var lang = ""; // Unset at first. (English is the default from the uninitialized dB.
        var langPending = false;
        // View control
        var viewLevels = false;
        var viewGame = false;
        var viewFinished = false;
        var viewSettings = false;
        // additional data model
        var maze = null;
        var levels = [];
        // Transition flags
        var initPending = false;
        var unloadPending = false;
        // If any of these view (except settings) is set, this first sets them all to false first, so that only one-at-a-time is showing.
        function internalHideAllViews() {
            mazeapp.viewLevels = false;
            mazeapp.viewGame = false;
            mazeapp.viewFinished = false;
            mazeapp.viewSettings = false;
        };
        // On game load, this enables the possibility of suspending
        function enableAppSuspendHandler() {
            // Register for the app-level suspend event and emit the notifications when this happens
            Windows.UI.WebUI.WebUIApplication.addEventListener("suspending", function onWinRTSuspend(e) {
                // WINRT suspension deferral (for my async operation)
                var suspendDeferral = e.suspendingOperation.getDeferral();
                mazeapp.notify("appsuspend", {
                    oncomplete: function () {
                        suspendDeferral.complete();
                    }
                });
            }, false);
        }
        // Enables resume handler
        function disableAppSuspendHandler() {
            // Register for the app-level resume event and emit the notifications when this happens
            Windows.UI.WebUI.WebUIApplication.addEventListener("resuming", function onWinRTResume() {
                mazeapp.notify("appresume");
            }, false);
        }
        // Dispatch and complete handler for appinit or appreinit notifications
        function notifyInit(eventName, nextStageCallback) {
            initialized = false; // Redundant for appinit, not for appreinit.
            initPending = true;  // Prevents initialized from being called improperly (rendundant for reset(), not for init())
            mazeapp.notify(eventName, { // BUG: should be 'this' but it's undefined in this scenario, so useing global ref instead.
                oncomplete: function appinitComplete() {
                    // Async notify about the levels being loaded and allow all handlers to setup
                    this.notify("levelsloaded", {
                        oncomplete: function levelsLoadedComplete() {
                            // With the init handlers + level handlers complete, I'm basically done.
                            initialized = true;
                            initPending = false;
                            nextStageCallback();
                        }
                    });
                }
            });
        }
        // This function is called as a result of reset (settings charm option)
        function appResetPostMazeUnload() {
            // Ensure that there is no activeMaze at this point...
            mazeapp.activeMaze = null; // May clear some UI state on the viewLevels... (done prior to appreinit which will reset the activeMaze WITHOUT side effects...
            // Now, simulate being in an uninitialized state, so that data can be set without side-effects...but without re-firing an appinit event!
            notifyInit("appreinit", function onAppReinitComplete() {    
                if (mazeapp.viewLevels)
                    levels[0].notify("selected");
                else
                    mazeapp.viewLevels = true; // May be a no-op if the levels screen is already showing...
            });
        }
        // Derive from an Event Target (on/notify feature)
        return Object.create(new Eventing(), {
            init: {
                enumerable: true,
                value: function oninit() {
                    if (initialized || initPending)
                        return;
                    initPending = true;
                    notifyInit("appinit", function setFinalStage() {
                        if (maze) {
                            // If initialization produced a maze object (SCA-serialized thing), then it was previously saved and that game should be restored immediately
                            maze = MazeModel(maze, false);
                            enableAppSuspendHandler();
                            mazeapp.notify("mazeloaded", {
                                oncomplete: function onMazeLoadedResumeComplete() {
                                    mazeapp.viewGame = true;
                                    mazeapp.maze.viewPaused = true;
                                    mazeapp.notify("appresume"); // To clear the stored maze from the DB
                                }
                            });
                        }
                        else
                            mazeapp.viewLevels = true;
                    });
                }
            },
            reset: {
                enumerable: true,
                value: function () {
                    if (initPending)
                        return;
                    if (!initialized)
                        return console.debugAssert("Reset requested, but game model not ready.");
                    initPending = true;
                    if (maze) // I need to release the maze-in-progress first...
                        this.unloadMaze(appResetPostMazeUnload);
                    else
                        appResetPostMazeUnload(); // Proceed directly to this post-stage...
                }
            },
            currentLevel: {
                enumerable: true,
                get: function() { 
                    if (levels.length == 0)
                        return console.debugAssert("Levels are not loaded, do not read the current level at this time (unpredictable things might happen)!");
                    return currentLevel;
                },
                set: function(x) {
                    if (x === currentLevel)
                        return;
                    if (!initialized) {
                        currentLevel = x;
                        return; // Store w/out side-effects.
                    }
                    if (maze)
                        return console.debugAssert("Maze Lock in effect--do not set the currentLevel");
                    // Changing the level always re-sets the activeMaze...
                    this.activeMaze = null; // May have side-effect of notify for <deactivated>....
                    this.levels[currentLevel].notify("unselected");
                    this.levels[x].notify("selected");
                    currentLevel = x;
                },
            },
            activeMaze: {
                enumerable: true,
                get: function() { return activeMaze; },
                set: function(x) {
                    if (x === activeMaze)
                        return;
                    if (!initialized) {
                        activeMaze = x;
                        return; // Store w/out side-effects.
                    }
                    if (maze)
                        return console.debugAssert("Maze Lock in effect--do not set the activeMaze");
                    if (activeMazeTransition)
                        return; // Ignore until complete
                    function setActiveMaze() {
                        activeMaze = x;
                        if (activeMaze != null) { // Then activate the item being set
                            levels[currentLevel].mazes[activeMaze].notify("activated", {
                                oncomplete: function onactivationcompleted() {
                                    activeMazeTransition = false;
                                }
                            });
                        }
                        else
                            activeMazeTransition = false;
                    }
                    activeMazeTransition = true;
                    if (activeMaze != null) // Then de-activate the previously active maze first...
                        levels[currentLevel].mazes[activeMaze].notify("deactivated", { oncomplete: setActiveMaze });
                    else
                        setActiveMaze();
                }
            },
            payingCustomer: {
                enumerable: true,
                get: function() { return payingCustomer; },
                set: function(x) {
                    if (x === payingCustomer) 
                        return;
                    if (typeof x !== "boolean")
                        return console.debugAssert("You can only set this value to a boolean--it defaults to false");
                    if (x)
                        this.notify("unlock"); // Unlock that which needs to be unlocked (no false case)
                    payingCustomer = x;
                }
            },
            lang: {
                enumerable: true,
                get: function () { return lang; },
                set: function (newlang) {
                    if (langPending)
                        return;
                    if (newlang == lang)
                        return;
                    if (!ENUM_LANG[newlang])
                        return console.debugAssert("Lang value not valid");
                    lang = newlang;
                    langPending = true; // Don't allow overlapping language change notifications.
                    this.notify("langchange", {
                        oncomplete: function langChanageNotificationComplete() {
                            langPending = false;
                        }
                    });
                }
            },
            loc: {
                enumerable: true,
                writable: true,
                value: null         // Set (and re-set) in the <langchange> event handler.
            },
            locElement: {
                enumerable: true,
                value: function (locID, elem) {
                    if (!elem || !elem.tagName)
                        return console.debugAssert("2nd parameter to locElement() must be an element");
                    if (typeof locID != "string")
                        return console.debugAssert("1st parameter to locElement() must be a string");
                    if (!this.loc)
                        return console.debugAssert("Localization table not loaded yet");
                    if (typeof this.loc[locID] == "undefined")
                        return console.debugAssert("1st parameter ("+ locID +") to locElement() was not a localization ID found in the loc table for language \"" + lang + "\"");
                    if ((elem.tagName == "INPUT") || (elem.tagName == "TEXTAREA")) {
                        elem.setAttribute("data-resv", locID);
                        elem.value = this.loc[locID];
                    }
                    else {
                        elem.setAttribute("data-res", locID);
                        elem.innerText = this.loc[locID];
                    }
                }
            },
			viewLevels: {
				enumerable: true,
				get: function () { return viewLevels; },
				set: function (x) {
					if (x == viewLevels)
						return;
					if (x) {
					    internalHideAllViews();
						this.notify("levelselectshow");
					}
					else
						this.notify("levelselecthide");
					viewLevels = x; // MUST come after the (re-entrant) call to internalHideAllViews in order to avoid 2 inverted notifications.
				}
			},
			viewGame: {
				enumerable: true,
				get: function () { return viewGame; },
				set: function (x) {
					if (x == viewGame)
						return;
					if (x) {
					    internalHideAllViews();
					    this.notify("gameshow");
					}
					else
					    this.notify("gamehide");
					viewGame = x; // MUST come after the (re-entrant) call to internalHideAllViews in order to avoid 2 inverted notifications.
				}
			},
			viewFinished: {
				enumerable: true,
				get: function () { return viewFinished; },
				set: function (x) {
					if (x == viewFinished)
						return;
					if (x) {
					    internalHideAllViews();
					    this.notify("finishedshow");
					}
					else
					    this.notify("finishedhide");
					viewFinished = x;
				}
			},
			viewSettings: {
				enumerable: true,
				get: function () { return viewSettings; },
				set: function (x) {
					if (x == viewSettings)
						return;
					if (x)
					    this.notify("settingsshow");
					else
					    this.notify("settingshide");
					viewSettings = x;
				}
			},
			levels: {
			    enumerable: true,
			    get: function() { return levels; },
			    set: function (x) {
			        if (initialized)
			            return console.debugAssert("It is inappropriate to set the levels at this time");
			        levels = x;
			    }
			},
			loadMaze: {
			    enumerable: true,
			    value: function () {
			        if (maze)
			            return; // Multiple calls when there is already a loaded maze fail silently
			        if (!initialized)
			            return console.debugAssert("Attempted load from template too early!");
			        maze = {}; // Temporary, to function as "maze lock" and "in-progress" maze load...
			        var xhr = new XMLHttpRequest();
			        xhr.onload = function onLoadGameTemplateViaXHR(e) {
			            maze = MazeModel(JSON.parse(this.response), true);
			            enableAppSuspendHandler();
			            mazeapp.notify("mazeloaded", { oncomplete: function onMazeLoadedComplete() { mazeapp.viewGame = true; } });
			        };
			        xhr.open("GET", "/templates/" + levels[currentLevel].mazes[activeMaze].template);
			        xhr.send();
			    }
			},
			unloadMaze: {
			    enumerable: true,
			    value: function (onunloadedcallback) {
			        if (!maze || unloadPending)
			            return; // Multiple calls fail silently
			        unloadPending = true;
			        disableAppSuspendHandler();
			        this.notify("mazeunloaded", {
			            oncomplete: function () {
			                maze = null; // Clear out the maze after all the handlers have run their unload logic (which may depend on accessing the maze object still...)
			                unloadPending = false;
			                if (typeof onunloadedcallback == "function")
			                    onunloadedcallback();
			            }
			        });
			    }
			},
			maze: {
				enumerable: true,
				get: function () { return maze; },
				set: function (suspendedMaze) {
				    if (initialized && maze)
				        return console.debugAssert("Do not directly set the maze object--use loadMaze instead--but first unload the current maze");
				    if (initialized && !maze)
				        return console.debugAssert("Do not directly set the maze object--use loadMaze instead");
				    // !initialized -- (appinit event handler...)
				    maze = suspendedMaze; // may be null
				}
			},
			pixelPerUnit: {
			    enumerable: true,
			    writable: true,
                value: null // Value will be set when loading the resume state (see the managerDatabase.js)
			}
		});
	}
});


// * levelIndex should be zero-based
Object.defineProperty(window, "LevelModel", {
	value: function(solvedMazesCount, defaultLocked) {
		return Object.create(new Eventing(), {
			solvedMazeCount: {
				enumerable: true,
				get: function() { return solvedMazesCount; },
				set: function(x) {
					if (x < 0) 
						return console.debugAssert("Can't solve negative mazes");
					solvedMazesCount = x;
					this.notify("solvedmazecountchange");
				}
			},
			defaultLocked: {
				enumerable: true,
				value: defaultLocked
			},
			mazes: {
				enumerable: true,
				value: []
			}
		});
	}
});


Object.defineProperty(window, "TileModel", {
    value: function (gameNumber, gameNameLoc, cheeseNum, fastTime, slowTime, sumTime, playCount, templateFile) {
        var updateStatsLock = false;
		return Object.create(new Eventing(), {
			gameNum: {
				enumerable: true,
				value: gameNumber
			},
			gameNameLocId: {
				enumerable: true,
				value: gameNameLoc
			}, 
			smallCheesesAcquired: {
				enumerable: true,
				get: function () { return cheeseNum; },
				set: function (x) { cheeseNum = x; this.notify("smallcheesenumchange"); }
			},
			timeFastest: {
				enumerable: true,
				get: function () { return fastTime; }
			},
			timeAverage: {
				enumerable: true,
				get: function () { return playCount ? sumTime / playCount: 0; }
			},
			timeSlowest: {
				enumerable: true,
				get: function () { return slowTime; }
			},
			completedCount: {
			    enumerable: true,
			    get: function () { return playCount; }
			},
			template: {
				enumberable: true,
				value: templateFile
			},
			updateStats: {
			    enumerable: true,
			    value: function (newTime) {
			        if (updateStatsLock)
			            return;
			        updateStatsLock = true;
			        if (fastTime == 0) // Should only happen on the first play.
			            fastTime = newTime + 1; // Be sure that the next conditional will always run
			        if (newTime < fastTime)
			            fastTime = newTime;
			        if (newTime > slowTime)
			            slowTime = newTime;
			        sumTime += newTime;
			        playCount++;
			        this.notify("statsupdate", {
			            args: [fastTime, slowTime, sumTime, playCount], oncomplete: function () {
			                updateStatsLock = false;
			            }
			        });
			    }
			}
		});
	}
});

// See also MazeModel definition in the mazeEngine.js file.

// Define the model (first before DOMContentLoaded)
// so that other scripts can hook-up event handlers to the model
// for state transitions
var mazeapp = new AppModel();


document.addEventListener("DOMContentLoaded", function initModel() {
    mazeapp.init();
});