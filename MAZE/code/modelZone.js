"use strict";
// Defines a Model: HitTestModel() available on the global

// [Constructor(serializedZone), // Note this constructor form doesn't create new instance, but rather in-place upgrades the serialized object into a Zone-typed object
//  Constructor(verticalCenter, horizontalCenter, quarterHeight, quarterWidth)] // This constructor (please call with 'new') creates a new Zone object
// interface Zone {
//    readonly attribute sequence<sequence<Zone>> zones; // Double-array of Zones (quadrants)
//    readonly attribute sequence<Cell>?          cells;
//    readonly attribute float                    horizontalCenter;
//    readonly attribute float                    verticalCenter;
//    readonly attribute float                    quarterWidth;
//    readonly attribute float                    quarterHeight;
//    void add(Cell cell);
//    Cell? hitTest(unsigned long floor, sequence<unsigned long> xyUnitCoordinatePair);
// };

(function ZoneScope() {
    var MAX_POINTS_PER_ZONE = 10;
    var RADIUS = 5; // 10 units is the typical diameter of a cell, so this is the 'fat finger' hit test radius.
    var HALF_RADIUS = 2.5;
    var X = 0;
    var Y = 1;
    var UPPER = 0;
    var LOWER = 1;
    var LEFT = 0;
    var RIGHT = 1;

    // the point p is the user's point, and indexes is an array of 2+ indexes into the this.points array.
    // Return the winner.
    distanceTestContest = function (p, candidateCells) {
        // Compute euclidean distance to find the "closest point" within the bounding box
        var minDist = 100; // Just has to be as bigger than 2*RADIUS
        var minIndex = -1;
        for (var i = 0, len = candidateCells.length; i < len; i++) {
            var candidatePoint = candidateCells[i].location;
            // Get absolute distance (any direction)
            var x = Math.abs(candidatePoint[X] - p[X]);
            var y = Math.abs(candidatePoint[Y] - p[y]);
            var dist = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
            if (dist < minDist) {
                minDist = dist;
                minIndex = i;
            }
        }
        // One index will have emerged victorious! Return it's data object.
        return candidateCells[minIndex];
    }

    // Tests whether point is on or inside a bounding box of "radius" around the cell. returns true/false
    function pointInCellBounds(point, cell) {
        if (point[Y] < (cell.location[Y] - RADIUS))
            return false;
        if (point[Y] > (cell.location[Y] + RADIUS))
            return false;
        if (point[X] < (cell.location[X] - RADIUS))
            return false;
        if (point[X] > (cell.location[X] + RADIUS))
            return false;
        return true; // It must be inside!
    }

    // Creates the Zone if it doesn't already exist for the given distribution location
    function directCellToZones(zone, cell) {
        // Note, some cells may belong to multiple zones
        // (this is OK--ensures that hit test radius tolerance won't miss important points close to an adjoining zone)
        var isUpper = cell.location[Y] <= (zone.y + RADIUS);
        var isLower = cell.location[Y] >= (zone.y - RADIUS);
        var isLeft = cell.location[X] <= (zone.x + RADIUS);
        var isRight = cell.location[X] >= (zone.x - RADIUS);
        if (isUpper && isLeft) {
            if (!zone.zones[UPPER][LEFT])
                zone.zones[UPPER][LEFT] = new Zone(zone.y - zone.quarterHeight, zone.x - zone.quarterWidth, zone.quarterHeight / 2, zone.quarterWidth / 2);
            zone.zones[UPPER][LEFT].add(cell);
        }
        if (isUpper && isRight) {
            if (!zone.zones[UPPER][RIGHT])
                zone.zones[UPPER][RIGHT] = new Zone(zone.y - zone.quarterHeight, zone.x + zone.quarterWidth, zone.quarterHeight / 2, zone.quarterWidth / 2);
            zone.zones[UPPER][RIGHT].add(cell);
        }
        if (isLower && isLeft) {
            if (!zone.zones[LOWER][LEFT])
                zone.zones[LOWER][LEFT] = new Zone(zone.y + zone.quarterHeight, zone.x - zone.quarterWidth, zone.quarterHeight / 2, zone.quarterWidth / 2);
            zone.zones[LOWER][LEFT].add(cell);
        }
        if (isLower && isRight) {
            if (!zone.zones[LOWER][RIGHT])
                zone.zones[LOWER][RIGHT] = new Zone(zone.y + zone.quarterHeight, zone.x + zone.quarterWidth, zone.quarterHeight / 2, zone.quarterWidth / 2);
            zone.zones[LOWER][RIGHT].add(cell);
        }
    }

    var ZonePrototype = Object.create(Object.prototype, {
        // Adds a cell into this zone. If the number of cells in this zone exceed the MAX_POINTS_PER_ZONE, the zone sub-divides itself.
        add: {
            enumerable: true,
            value: function add(cell) {
                // If this is not a leaf node in the tree (it's a Zone sub-division), then continue to push the cell down toward a leaf-node
                if (this.cells == null)
                    return directCellToZones(this, cell);
                this.cells.push(cell);
                // In addition to the points limit, also ensure that the zone isn't too small before sub-dividing.
                // (the zone's horizontal or vertical dimension must be larger than the hit-test diameter to continue)
                if ((this.cells.length > MAX_POINTS_PER_ZONE) && ((this.quarterWidth > HALF_RADIUS) || (this.quarterHeight > HALF_RADIUS))) {
                    for (var i = 0, len = this.cells.length; i < len; i++)
                        directCellToZones(this, this.cells[i]);
                    this.cells = null; // Indicates that this zone is no longer a leaf node in the Zone hierarchy
                }
            }
        },
        // Tries to find a cell given a candidate point (tolerance is applied). If no point can be located, returns null.
        hitTest: {
            enumerable: true,
            value: function hitTest(floorNum, p) {
                if (this.cells == null) { // I'm not a leaf node...
                    var verticalIndex = (p[Y] > this.y) ? 1 : 0;
                    var horizontalIndex = (p[X] > this.x) ? 1 : 0;
                    if (!this.zones[verticalIndex][horizontalIndex]) // If the zone doesn't exist, then we immediatley know there's no hit test possible
                        return null;
                    return this.zones[verticalIndex][horizontalIndex].hitTest(floorNum, p);
                }
                // Iterate the candiates and select the closest cell if any
                var candidates = [];
                // The following is the linear search that this entire Zone system is meant to scope. Enjoy
                for (var i = 0, len = this.cells.length; i < len; i++) {
                    if ((this.cells[i].floor == floorNum) && pointInCellBounds(p, this.cells[i]))
                        candidates.push(i);
                }
                if (candidates.length == 0) // No winners today...
                    return null;
                if (candidates.length == 1) // A no-contest winner!
                    return candidates[0];
                // else there are at least two potential winners...
                return distanceTestContest(p, candidates);
            }
        }
    });

    Object.defineProperty(window, "Zone", {
        // Constructor (with a serialized Zone) in-place upgrades the instance (rather then creating a new object based on it)
        value: function Zone(verticalCenterOrSerializedZone, horizontalCenter, quarterHeight, quarterWidth) {
            var zones = [[null, null], [null, null]];
            var cells = [];
            var instance = this;
            if (typeof verticalCenterOrSerializedZone != "number") { // Presume this is a serialized cell
                // Use the provided instance instead of one created by 'new'
                instance = verticalCenterOrSerializedZone;
                // Test for "Zone-ness"; the result must match the current OM exactly...
                if ((Object.keys(instance).length != 6) || 
                    !Array.isArray(instance.zones) ||
                    ((instance.cells != null) && !Array.isArray(instance.cells)) ||
                    typeof instance.x != "number" ||
                    typeof instance.y != "number" ||
                    typeof instance.quarterWidth != "number" ||
                    typeof instance.quarterHeight != "number")
                    throw new Error("Could not convert [serialized] object to a Zone");
                // Make it a Zone type object...
                Object.setPrototypeOf(instance, ZonePrototype);
                // Prepare to re-define the existing values with proper strict-mode safetys.
                zones = instance.zones;
                cells = instance.cells;
                verticalCenterOrSerializedZone = instance.y;
                horizontalCenter = instance.x;
                quarterHeight = instance.quarterHeight;
                quarterWidth = instance.quarterWidth;
                // The first Zone recursively makes the additional Zones and their contained Cell objects as well.
                if (zones[UPPER][LEFT]) Zone(zones[UPPER][LEFT]);
                if (zones[UPPER][RIGHT]) Zone(zones[UPPER][RIGHT]);
                if (zones[LOWER][LEFT]) Zone(zones[LOWER][LEFT]);
                if (zones[LOWER][RIGHT]) Zone(zones[LOWER][RIGHT]);
                if (cells) {
                    for (var i = 0, len = cells.length; i < len; i++)
                        Cell(cells[i]);
                }
            }
            // Zone map: [upper/lower][left/right]
            //  0 [0  |  1]
            //     ---+---
            //  1 [0  |  1]
            return Object.defineProperties(instance, {
                zones:         { enumerable: true, configurable: false, writable: false, value: zones }, // null, if there is no sub-zone for the given area
                cells:         { enumerable: true, configurable: false, writable: true,  value: cells }, // List of cells, when this Zone gets children (any children), the cells value is assigned null.
                x:             { enumerable: true, configurable: false, writable: false, value: horizontalCenter },
                y:             { enumerable: true, configurable: false, writable: false, value: verticalCenterOrSerializedZone },
                quarterWidth:  { enumerable: true, configurable: false, writable: false, value: quarterWidth },
                quarterHeight: { enumerable: true, configurable: false, writable: false, value: quarterHeight },
            });
        }
    });
    Object.definePropery(Zone, "prototype", { value: ZonePrototype });
    Object.defineProperty(ZonePrototype, "constructor", { value: Zone });
})();