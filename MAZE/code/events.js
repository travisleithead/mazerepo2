﻿"use strict";
/* Eventing system for models */

/**
 * This event target object must be inherited by any object
 * that wishes to support event registration/notification.
 * Principally that will be any Model in the app
 */

/* Registrants can indicate whether their operation will need extra 
 * time to complete (because the registrant's operations is async).
 * They do this by requesting a promise in their callback, and then 
 * fullfilling that promise when their work is done.
 */

/* Notifiers can request that they be told when the event dispatch 
 * (which is async) is completed by providing an oncomplete dictionary
 * option in their notify call
 */

(function eventingModelScope() {
    // Global stuff
    function registerEventCallback(name, callback, optionalRefElement) {
        if (!this.registrars[name])
            this.registrars[name] = [];
        // Don't allow duplicate callbacks...the new registration replaces the old...
        for (var i = 0, len = this.registrars[name].length; i < len; i++) {
            if (callback === this.registrars[name][i].callback) {
                this.registrars[name][i].elem = (optionalRefElement ? optionalRefElement : undefined);
                return;
            }
        }
        this.registrars[name].push({ callback: callback, elem: optionalRefElement });
    }

    function unregisterEventCallback(name, callback) {
        if (!this.registrars[name])
            return;
        for (var i = 0, len = this.registrars[name].length; i < len; i++) {
            if (callback === this.registrars[name][i].callback) {
                this.registrars[name].splice(i, 1); // Modifies the original array
                break;
            }
        }
    }

    function makeFullfillmentDict(completeCallback) {
        return {
            complete: completeCallback,     // Method to call when all requested promises for async work have reported back that they are done.
            outstanding: 0,                 // Track the current number of outstanding promises not yet done.
            active: false                   // Signal that allows the last unfilled promise to trigger the complete callback (can't be allowed to happen before all notifications have been sent)
        };
    }

    // This is used when the notifier doesn't need to know/doesn't care
    // when the registrants are finished. It provides a consistent API
    // to the registrants so that they just always assume someone may
    // need to know and don't have to know the notifier's intensions.
    function makeFakePromiseGetter() {
        return function uselessGetPromise() { return { done: function () { } }; };
    }

    function makePromiseGetter(fullfillmentDictionary, model) {
        return function getPromise() {
            fullfillmentDictionary.outstanding++;   // Only increment if the registrant requests a promise...
            return {
                // This is the promise object...
                done: function onAsyncNotifyPromiseFullfilled() { // This is what the registrant calls when the async work is done.
                    fullfillmentDictionary.outstanding--;
                    if ((fullfillmentDictionary.outstanding == 0) && fullfillmentDictionary.active) // This is the last unfullfilled promise...
                        fullfillmentDictionary.complete.call(model);
                }
            };
        };
    }

    function makeEventObject(nam, targt, optionalFullfillmentOptions) {
        return {    // The event object (provided as param 0 to all registrars) 
            target: targt,
            name: nam,
            getPromise: optionalFullfillmentOptions ? makePromiseGetter(optionalFullfillmentOptions, targt) : makeFakePromiseGetter()
        };
    }

    // Used to swap to synchronous event if already in an async callback
    var innotify = false;

    function notifyByNameForceSync(name, options) {
        innotify = true;
        notifyByName(name, options);
        innotify = false;
    }

    // dict can have { args: [stuff...], oncomplete: function }
    // The oncomplete function, if provided, will always be called asynchronously.
    function notifyByName(name, options) {
        // Normalize the options
        var opts = options ? options : {};
        // Quick exit? Registrants that want to be notified must register before
        // the notify call of they will not be counted on the roster. Dynamicly
        // added registrars (during notification) are not processed for this notify pass.
        if (!this.registrars[name] || (this.registrars[name].length == 0)) {
            if (typeof opts.oncomplete == "function") // This notify wants to be notified on callback complete (but there is no one to notify...)
                setImmediate(opts.oncomplete); // Async invoke
            return;
        }
        var fullfillmentOptions = opts.oncomplete ? makeFullfillmentDict(opts.oncomplete) : null;
        var eventObPlusArgs = [makeEventObject(name, this, fullfillmentOptions)].concat(opts.args ? opts.args : []);
        if (innotify) { // go sync (notify can be re-entrant)
            this.registrars[name].forEach(function syncNotify(reg) {
                reg.callback.apply(reg.elem ? reg.elem : window, eventObPlusArgs);
            });
        }
        else { // go async
            this.registrars[name].forEach(function setupAsyncNotifyOrder(registrar) {
                setImmediate(function asyncNotify(eventParamAndArgs, reg) {
                    innotify = true;
                    reg.callback.apply(reg.elem ? reg.elem : window, eventParamAndArgs);
                    innotify = false;
                }, eventObPlusArgs, registrar);
            });
        }
        // Setup the state for an oncomplete callback, if requested.
        if (fullfillmentOptions) { // The notifier wanted a callback...
            // This is necessary to guarantee that the oncomplete callback is invoked once
            // in the case where no promises are requested by any registrars.
            setImmediate(function checkAreAllPromisesFullfilled(fullfillTracker, model) {
                if (fullfillTracker.outstanding == 0) // Either all the promises were synchronously fullfilled, or no promises were requested.
                    fullfillTracker.complete.call(this); // Make the callback now! (and that's that).
                else
                    fullfillTracker.active = true; // Allow future fullfillments to make the complete callback at that time.
            }, fullfillmentOptions, this);
        }
    }

    function EventingConstructor() {
        // Stores the event listeners
        Object.defineProperty(this, "registrars", { value: [] });
    }

    EventingConstructor.prototype = Object.create(Object.prototype, {
        // Register listeners
        on: { value: registerEventCallback },			// (name, callback, optional contextelement)
        off: { value: unregisterEventCallback },		// (name, callback)
        // Notification model
        notify: { value: notifyByName },				// (name, {args/oncomplete}) -> handlers take ({target:X,getPromise(),name:""}, args), invoked on contextelement (or window)
        notifySync: { value: notifyByNameForceSync }	// Same signature as above
    });

    Object.defineProperty(window, "Eventing", { value: EventingConstructor }); // This is the thing that get's 'new'-ed
})();