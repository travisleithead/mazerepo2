﻿"use strict";

// [Constructor(object serializedCell),
//  Constructor(sequence<float> xyLocation, unsigned long floorNum)]
// interface Cell {
//     readonly attribute sequence<float> location;
//     readonly attribute unsigned long   floor;
//              attribute Item?           item;
//     (Edge? or sequence<Edge>) follow(optional Edge? visitedEdge, optional float angleInRadians);
//     (Edge? or sequence<Edge>) roam(optional Edge? visitedEdge, optional float angleInRadians);
//     sequence<Cell> pathTo(Cell targetCell);
//   // Private
//     readonly attribute sequence<Edge> _connectors;
//     readonly attribute sequence<Edge> _paths;
// };


(function ModelCellScope() {

    // constructor - creates a new Cell object and new degree/floor data, with the cloned Cell referencing the same Edge objects contained in the 
    //               original Cell, as well as the same location data
    // * location - the x/y global coordinate of the center of this cell
    // * floor - this cells' floor number
    // * item - a cell can hold a single item this is it. (or null)
    // * follow() - (no provided angle) returns the set of available Edges (not including the visited Edge) in no particular order (or a single edge, or null).
    //              (with a provided angle) returns either: 
    //              * null if there is no Edge within a scope of 110 degrees of the provided angle.
    //              * Edge if there is one Edge that is within a scope of 110 degrees of the provided angle.
    //              * sequence<Edge> if there is more than one Edge that falls within the scope of 110 degrees of the provided angle.
    //              Note: follow respects edges that can't be followed (because they're locked)
    // * roam() - same as follow() but uses the _connectors list to roam the maze edges (all), not just the paths through the maze
    // * _connectors - the list of all potential paths (used to re-generate the _paths at some future time. This forms the raw mesh of all connected cells
    // * _paths - once a tree is generated from the mesh, the _paths are the branches [a subset] that participate in the tree

    var ARC_MIN_MARGIN = Math.PI * (11 / 36); // 55 degrees (in radians)
    var CIRCLE_MAX = Math.PI * 2;

    function pickEdgeFromCollection(cell, collection, visitedEdge, angle) {
        var degree = collection.length;
        var hasVisitedEdge = (visitedEdge != null);
        var set = [];
        if (degree == 0)
            throw new Error("Cell is not connected to any other cell");
        else if ((degree == 1) && hasVisitedEdge) { // Optimization for returning null quickly...
            if (collection[0] == visitedEdge)
                return null;
            else
                throw new Error("visitedEdge not found");
        }
        else if (typeof angle == "number") { // hasAngle == true, for any degree > 0 with or without a visitedEdge...
            // Collect wall angles within the arc angle range
            hasVisitedEdge = !hasVisitedEdge; // Flip the logic, so that this is true if not provided.
            var shift = (angle < ARC_MIN_MARGIN) ? ARC_MIN_MARGIN : (angle > (CIRCLE_MAX - ARC_MIN_MARGIN)) ? -ARC_MIN_MARGIN : 0;
            angle += shift;
            for (var i = 0; i < degree; i++) {
                if (collection[i] === visitedEdge) { // Exclude the visited edge if provided...
                    hasVisitedEdge = true;
                    continue;
                }
                var angleedge = (collection[i].angle(cell) + shift) % CIRCLE_MAX; // If shift wraps over CIRCLE_MAX, bring it around the end of the circle
                if (angleedge < 0) // If shift wraps under 0, bring it back around the circle_max boundary.
                    angleedge += CIRCLE_MAX;
                if (Math.abs(angleedge - angle) < ARC_MIN_MARGIN) // If angular distance is within the threshold
                    set.push(collection[i]);
            }
            if (!hasVisitedEdge) // If the visited edge is provided and still false, then it wasn't found in the collection and this is an error
                throw new Error("visitedEdge not found");
            return (set.length == 0) ? null : (set.length == 1) ? set[0] : set;
        }
        else if (degree == 1) // (implied) && !hasVisitedEdge && !hasAngle
            return collection[0];
        else if ((degree == 2) && hasVisitedEdge) { // && !hasAngle
            if (collection[0] === visitedEdge)
                return collection[1];
            else if (collection[1] === visitedEdge)
                return collection[0];
            throw new Error("visitedEdge not found");
        }
        else if (!hasVisitedEdge) // degree >= 2, with no visitedEdge and no angle provided
            return collection.slice(); // Return all edges as a new array (all edges are fair game).
        else { // (degree >= 2) && has a visitedEdge && no angle provided
            for (var i = 0; i < degree; i++) {
                if (collection[i] !== visitedEdge)
                    set.push(collection[i]);
            }
            if (set.length != (degree - 1))
                throw new Error("visitedEdge not found");
            return set;
        }
    }

    var CellPrototype = Object.create(null, {
        follow: {
            enumerable: true,
            value: function follow(visitedEdge, angle) {
                return pickEdgeFromCollection(this, this._paths, (typeof visitedEdge == "undefined") ? null : visitedEdge, angle);
            }
        },
        roam: {
            enumerable: true,
            value: function roam(visitedEdge, angle) {
                return pickEdgeFromCollection(this, this._connectors, (typeof visitedEdge == "undefined") ? null : visitedEdge, angle);
            }
        },
    });

    Object.defineProperty(global, "Cell", {
        value: function Cell(serializedCellOrXYCoordinatePair, floorNum) {
            var instance = this;
            var item = null;
            var paths = [];
            var connectors = [];
            if (!Array.isArray(serializedCellOrXYCoordinatePair)) {
                instance = serializedCellOrXYCoordinatePair;
                // May be called with an already-de-serialized cell, fast exit (from Zone upgrade, when a Cell is in multiple Zones)
                if (instance instanceof Cell)
                    return instance; // Done.
                // Smells like a serialized cell?
                if ((Object.keys(instance).length != 5) || 
                    !Array.isArray(instance.location) ||
                    (typeof instance.floor != "number") ||
                    ((instance.item != null) && !instance.item) ||
                    !Array.isArray(instance._connectors) ||
                    !Array.isArray(instance._paths))
                    throw new Error("Could not convert [serialized] object to a Cell");
                Object.setPrototypeOf(instance, CellPrototype);
                // Copy for re-init later...
                serializedCellOrXYCoordinatePair = instance.location;
                floorNum = instance.floor;
                item = Item(instance.item);
                paths = instance._paths; // These are a subset of the _connectors, don't bother looking here...
                connectors = instance._connectors;
                // These represent all the possible Edges (from the maze's walls + paths (if that separation has been done) -- except the Maze's borders, init separately
                for (var i = 0, len = connectors.length; i < len; i++) Edge(connectors[i]);
            }
            return Object.defineProperties(instance, {
                location:    { enumerable: true, configurable: false, writable: false, value: serializedCellOrXYCoordinatePair },
                floor:       { enumerable: true, configurable: false, writable: false, value: floorNum },
                item:        { enumerable: true, configurable: false, writable: true,  value: item },
                _paths:      { enumerable: true, configurable: false, writable: false, value: paths },
                _connectors: { enumerable: true, configurable: false, writable: false, value: connectors }
            });
        }
    });
    Object.defineProperty(Cell, "prototype", { value: CellPrototype })
    Object.defineProperty(CellPrototype, "constructor", { value: Cell });
})();