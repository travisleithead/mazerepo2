﻿"use strict";

// [Constructor((object serializedTemplate or object serializedMaze)]
// interface Maze {
//     //readonly attribute sequence<Edge>  walls; // group these by floor for convenience.
//     //readonly attribute sequence<Edge>  borders;
//     readonly attribute sequence<Object> wallFloors;
//              attribute Cell            start;  // The start cell can change when generateTree updates the start to be the initiator cell.
//     readonly attribute Cell            end;
//     readonly attribute sequence<Cell>  solutionPath;
//              attribute unsigned long   hintIndex;
//     readonly attribute Actor           mouse;  // mouse.location.floor :-)
//     readonly attribute sequence<Cell>  items; // All the cells that have special items (convenience list for rendering purposes)
//     readonly attribute sequence<Actor> actors;
//     void generateTree(Cell startCell, Cell endCell, EnumStrategy strategy);
//     readonly attribute Zone cells;  // The tree structure for holding all Cells in the Maze; provides fast hitTest capability for random coordinates pairs.
// };
// enum EnumStrategy { ".." };

(function ModelMazeScope(global) {

    // wallFloors[n] * walls   - when the maze is generated, the walls array contains all the discarded Edges (kept Edges are paths within the maze--not rendered)
    // wallFloors[n] * borders - the boundaries of the maze
    // * start   - returns a reference to the starting cell
    // * end     - returns a reference to the ending cell
    // * solutionPath - the cells the comprise the solution to the maze (including detours to get the necessary items to unlock the path forward)
    // * hintIndex - the next cell along the solution path that isn't in the mouse's history
    // * mouse - the star of the show
    // * items - list of cells which contain items in the maze. Used to see which items have been collected and to render them
    // * actors - list of actors (things that move in the maze, where they are, etc.)
    // * generateTree(startCell, endCell) - divides each cell's walls into those that form a path and those that belong to walls in the maze
    // * cells - data structure for holding all the cells in the Maze (held via Zones for fast hit-testing capability)
    var MazePrototype = Object.create(Object.prototype, {
        generateTree: {
            enumerable: true,
            value: function generateTree(startCell, endCell, strategy) {
                throw new Error("Not implemented");
            }
        }
    });

    Object.defineProperty(global, "Maze", {
        value: function Maze(data) {
            var instance = this;
            var wallFloors = [];
            var start = null;
            var end = null;
            var solutionPath = [];
            var hintIndex = 0;
            var mouse = null;
            var items = [];
            var actors = [];
            var cells = null;
            // This is either a new maze or a serialized maze (SCA serialization, which preserves graph cycles)
            if (data.startSet) {
                // Indicator that this is a new maze template file...we're building a new maze from scratch!

            }
            else {
                // data should be a serialized Maze -- validate that it "looks like" a Maze object...
                if ((Object.keys(data).length != 9) ||
                    !Array.isArray(data.wallFloors) ||
                    !data.start || !data.end ||
                    !Array.isArray(data.solutionPath) ||
                    typeof data.hintIndex != "number" ||
                    !data.mouse ||
                    !Array.isArray(data.items) ||
                    !Array.isArray(data.actors) ||
                    !data.cells)
                    throw new Error("Could not convert [serialized] object to a Maze");
                // Passes muster!
                Object.setPrototypeOf(data, MazePrototype);
                cells = Zone(data.cells); // Reconstitute the Zone hierarchy and all the Cell objects it contains.
                start = data.start;
                end = data.end; // Start and end will be in-place updated by references from the Zone tree, no need to do them specially.
                wallFloors = data.wallFloors;
                // upgrade all the Edges in the 'borders' list (skip the 'walls', as they are done as part of Cell init's connectors)
                for (var i = 0; i < wallFloors.length; i++) {
                    for (var j = 0, len = wallFloors[i].borders.length; j < len; j++)
                        Edge(wallFloors[i].borders[j]);
                }
                solutionPath = data.solutionPath; 
                hintIndex = data.hintIndex;
                mouse = data.mouse; // TODO: upgrade the mouse Actor
                items = data.items; // Items will be upgraded within the Cells that contain them, this is a convenience list, so no need to do this specifically.
                actors = data.actors; // TODO: upgrade the other Actor objects...
                instance = data;
            }
            return Object.defineProperties(instance, {
                wallFloors:   { enumerable: true, configurable: false, writable: false, value: wallFloors },
                start:        { enumerable: true, configurable: false, writable: true,  value: start },
                end:          { enumerable: true, configurable: false, writable: false, value: end },
                solutionPath: { enumerable: true, configurable: false, writable: false, value: solutionPath },
                hintIndex:    { enumerable: true, configurable: false, writable: true,  value: hintIndex },
                mouse:        { enumerable: true, configurable: false, writable: false, value: mouse },
                items:        { enumerable: true, configurable: false, writable: false, value: items },
                actors:       { enumerable: true, configurable: false, writable: false, value: actors },
                cells:        { enumerable: true, configurable: false, writable: false, value: cells }
            });
        }
    });
    Object.defineProperty(Maze, "prototype", { value: MazePrototype });
    Object.defineProperty(MazePrototype, "constructor", { value: Maze });

    

   
    // The template file binary format key - note this must be identical to the format in the maze creation tool
    // ------------------ FORMAT SPEC --------------------------------------------
    var binaryFormatV1 = {
        keyOrder: ["version", "width", "height", "floors", "startSet", "endSet", "collectibles", "transports"],
        // Format version (1 == this version, the only current version)
        version: "uint8",
        // Maze dimensions
        width: "uint16", // Size (in "units")
        height: "uint16",
        // Floors
        floors: [{
            keyOrder: ["cells", "walls", "borders"],
            // Each floor has a cell list...
            cells: [{
                keyOrder: ["x", "y", "connectors"],
                // Global center coordinate point (x,y)
                x: "float32",
                y: "float32",
                // Cell has a list of connectors
                connectors: [{
                    keyOrder: ["cellIndex", "wallIndex"],
                    // index into this floor's cell list (this list) for the related connected cell
                    cellIndex: "uint16",
                    // index into the shared wall segment list (no borders, since cannot connect to a border--there's no cell)
                    wallIndex: "uint16",
                }]
            }],
            // Wall segment (general)
            walls: [["float32"], // If "line" type, then length of inner array is 4 floats: start x,y -> end x,y (absolute values), if "arc" then 8 floats (bezier) start x,y, control points1 & 2 and end point.
                    "wallList"],
            // Path of external wall segments (includes custom bridgewalls)
            borders: "wallList",
        }],
        //  Start set (possible starting cell locations)
        startSet: [{
            keyOrder: ["floorIndex", "cellIndex"],
            // floor index of this start cell option
            floorIndex: "uint8",
            // Cell index of the start cell
            cellIndex: "uint16"
        }, "floorAndCellIndex"],
        // End set
        endSet: "floorAndCellIndex",
        //  Collectibles
        collectibles: [{
            keyOrder: ["type", "arrangement", "distance", "size", "agility"],
            // the collectible type {cheese,key,gate,trap,switch,fan,earthquake,dyn_light,dyn_firefly,dyn_snake,dyn_fog}
            type: "uint8",
            // arrangement  0)inline,1)close,2)medium,3)far - how close to the solution path should the collectible reside...
            arrangement: "uint8",
            // How far along the solution path this collectible should be placed (1-100)
            distance: "uint8",
            // Number of cells this should extend to
            size: "uint8",
            // agility: 0)none,1)low,2)medium,3)high - how agile/fast will the dynamic thing moves...
            agility: "uint8"
        }],
        // Transport groups
        transports: [{
            keyOrder: ["select", "type", "mirror", "floorLink", "groupAFloorIndex", "groupBFloorIndex", "groupA", "groupB"],
            // Number of cells to join from the groups
            select: "uint16",
            // type  1 = Stairs, 2= Portal
            type: "uint8",
            // mirror (For floorRelationship == 2 or 3, true if the final selected transports will be correspond to the same relative cell position between floors (like actual stairs/elevators))
            mirror: "bool",
            // floorRelationship 1 = same floor, 2 = adjacent floors, 3 = disparate floors (separate and not adjacent)
            floorLink: "uint8",
            // GROUP A's floor index
            groupAFloorIndex: "uint8",
            // GROUP B's floor index
            groupBFloorIndex: "uint8",
            // GROUP A's participating cell indexes
            groupA: ["uint16"],
            // GROUP B's participating cell indexes
            groupB: ["uint16"]
        }]
    };

})(window);