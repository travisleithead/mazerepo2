﻿"use strict";
// This file manages the games database

// It is a service module to the model.

(function IndexedDBServices() {

    var CURRENT_DB_VERSION = 1.0; // major/minor
    var DB = "maze";
    var dbOpen = null; // New design: keep the database open as long as necessary (also required to be able to close it before deleting it)

	// NEW
	var STORE = {
		resumeInfo: {
			name: "resumeInfo",
			key: "id", 
			index: null, // null for no index
			schema: {
				id:					0, // number
				currentLevelIndex:	1, // number
				activeMazeIndex:	2, // number or null (if no game-in-progress)
				gameData:           3, // object or null (if no game-in-progress)
                lang:               4, // string (last selected language)
                pixPerMazeUnit:     5, // number (last "zoom" level)
                paid:               6  // bool
			}
		},
		levelMeta: {
			name: "levelMeta",
			key: "levelNum",
			index: null,
			schema: {
				levelNum:		0,	// number
				defaultLocked:  1   // bool
			}
		},
		mazes: {
			name: "mazes",
			key: null, // null for no key; use generic key generator
			index: { 
				levels: {name: "levels", prop: "levelIndex", unique: false}, 
				games: {name: "games", prop: "gameId", unique: false}
			},
			schema: { 
				levelIndex:		0,	// number
				gameId:			1,  // number (one-based)
				numSmallCheeses:2,  // number
				timeFastest:	3,  // number (miliseconds)
				timeSlowest:	4,  // number (miliseconds)
				gamesTotal:		5,  // number
				timeSum:		6,  // number (miliseconds)
				template:		7   // string
			}
		}
	};

	///////////////////////////////////////////////////////////////////
	// Multiple-use functions
	///////////////////////////////////////////////////////////////////

	function open() {
		var openPromise = { onopen: null };
		if (dbOpen) {
			setImmediate(function onInstantDBOpen() {
				if (openPromise.onopen)
					openPromise.onopen(dbOpen);
			});
			return openPromise;
		}
		// Open the database!
		var openReq = indexedDB.open(DB, CURRENT_DB_VERSION);
		openReq.onupgradeneeded = function onMigrateDatabaseOnUpgradeNeeded(e) {
			if ((e.oldVersion == 0) && (e.newVersion == 1))
				loadV0toV1DefaultData();
			populateLocalDB(e);
		};
		openReq.onerror = onOpeningIndexedDBError;
		openReq.onblocked = onOpeningIndexedDBError;
		openReq.onsuccess = function onOpeningIndexedDBSuccess(e) {
			dbOpen = e.target.result;
			if (openPromise.onopen)
				openPromise.onopen(dbOpen); // The open request's result is a DB object.
		}
		return openPromise;
	}

	// In case of error--tell the console.
	function onOpeningIndexedDBError(e) {
		return console.debugAssert(e.type + " opening DB...");
	}

	// From database v0 to database v1 upgrade. Subsequent additional releases may add more.
	function loadV0toV1DefaultData(e) {
		// TODO: Default data may come from the cloud (when the app is installed on multiple
		// machines). May need to sync to skydrive for this...?

		// If nothing else is available, then I'll load the defaults
		STORE.resumeInfo.rows = [
			["resumeInfo",  // id
             0,             // currentLevelIndex
             null,          // activeMazeIndex
             null,          // gameData
             "en",          // lang
             6,             // pixPerMazeUnit
             false          // paid
			] // the first zero is the key value to get this record from the DB.
		];
		STORE.levelMeta.rows = [
			[0, false],
			[1, false],
			[2, true], // Default locked (true)
			[3, true]
		];
		STORE.mazes.rows = [
			[0, 1,  0, 0, 0, 0, 0, "grid.map.js"],
			[0, 2,  0, 0, 0, 0, 0, "happyFace.map.js"],
			[0, 3,  0, 0, 0, 0, 0, "testMap.map.js"],
			[0, 4,  0, 0, 0, 0, 0, "quickTest.map.js"],
			[0, 5,  0, 0, 0, 0, 0, ""],
			[0, 6,  0, 0, 0, 0, 0, ""],
			[0, 7,  0, 0, 0, 0, 0, ""],
			[0, 8,  0, 0, 0, 0, 0, ""],
			[0, 9,  0, 0, 0, 0, 0, ""],
			[0, 10, 0, 0, 0, 0, 0, ""],
			[0, 11, 0, 0, 0, 0, 0, ""],
			[0, 12, 0, 0, 0, 0, 0, ""],
			[0, 13, 0, 0, 0, 0, 0, ""],
			[0, 14, 0, 0, 0, 0, 0, ""],
			[0, 15, 0, 0, 0, 0, 0, ""],
			[0, 16, 0, 0, 0, 0, 0, ""],
			[0, 17, 0, 0, 0, 0, 0, ""],
			[0, 18, 0, 0, 0, 0, 0, ""],
			[0, 19, 0, 0, 0, 0, 0, ""],
			[0, 20, 0, 0, 0, 0, 0, ""],
			[0, 21, 0, 0, 0, 0, 0, ""],
			[0, 22, 0, 0, 0, 0, 0, ""],
			[0, 23, 0, 0, 0, 0, 0, ""],
			[0, 24, 0, 0, 0, 0, 0, ""],
			[0, 25, 0, 0, 0, 0, 0, ""],
			[0, 26, 0, 0, 0, 0, 0, ""],
			[0, 27, 0, 0, 0, 0, 0, ""],
			[0, 28, 0, 0, 0, 0, 0, ""],
			[0, 29, 0, 0, 0, 0, 0, ""],
			[0, 30, 0, 0, 0, 0, 0, ""],
			[1, 1,  0, 0, 0, 0, 0, ""],
			[1, 2,  0, 0, 0, 0, 0, ""],
			[1, 3,  0, 0, 0, 0, 0, ""],
			[1, 4,  0, 0, 0, 0, 0, ""],
			[1, 5,  0, 0, 0, 0, 0, ""],
			[1, 6,  0, 0, 0, 0, 0, ""],
			[1, 7,  0, 0, 0, 0, 0, ""],
			[1, 8,  0, 0, 0, 0, 0, ""],
			[1, 9,  0, 0, 0, 0, 0, ""],
			[2, 1,  0, 0, 0, 0, 0, ""],
			[2, 2,  0, 0, 0, 0, 0, ""],
			[2, 3,  0, 0, 0, 0, 0, ""],
			[2, 4,  0, 0, 0, 0, 0, ""],
			[3, 1,  0, 0, 0, 0, 0, ""],
			[3, 2,  0, 0, 0, 0, 0, ""]
		];
	}

	// Iterate the declared DB tables (all) and place the data into the local database, 
	// building indexes, etc., as per the schema definition
    function populateLocalDB(e) {
        var db = e.target.result;
		// Populate tables (from scratch)
		Object.keys(STORE).forEach(function processStoreNames(storeName) {
			var store = STORE[storeName];
			// Create the object store
			var os = db.createObjectStore(storeName, store.key == null ? { autoIncrement: true } : { keyPath: store.key });
			// Create any indexes
			if (store.index) {
				Object.keys(store.index).forEach(function processIndexNames(indexName) {
					var index = store.index[indexName];
					os.createIndex(index.name, index.prop, index.unique ? { unique: true } : undefined)
				})
			}
			// Process the rows
			store.rows.forEach(function addRowToObjectStore(row) {
				var data = {};
				Object.keys(store.schema).forEach(function createObjectStoreRecord(schemaName) {
					data[schemaName] = row[store.schema[schemaName]];
				});
				os.put(data);
			});
		});
    }

    // Meta methods (abstract some of the Indexed DB async machinery from multiple callers)

    function openResumeInfoStoreFor(readType, modifyCallback, completedCallback) {
        open().onopen = function (db) {
            db.transaction(STORE.resumeInfo.name, readType).objectStore(STORE.resumeInfo.name).get(STORE.resumeInfo.name).onsuccess = function resumeInfoRead(e) {
                modifyCallback(e.target.result);
                if (readType == "readonly")
                    return (completedCallback ? completedCallback() : undefined);
                var req = e.target.source.put(e.target.result);
                if (completedCallback)
                    req.onsuccess = completedCallback;
            }
        }
    }

    function lookupMazeInDB(levelIndex, mazeId, onMazeLocatedCallback) {
        open().onopen = function (db) {
            var mazeStore = STORE.mazes.name;
            var gamesIndex = STORE.mazes.index.games.name;
            db.transaction(mazeStore, "readwrite").objectStore(mazeStore).index(gamesIndex).openCursor(IDBKeyRange.only(mazeId)).onsuccess = function iterateMazesToFindMatchingLevel(e) {
                var cursor = e.target.result;
                if (cursor && (cursor.value.levelIndex == levelIndex))
                    onMazeLocatedCallback(cursor);
                else if (cursor)
                    cursor.continue(); // Keep looking...
            }
        }
    }
    
    // Open the Database and async load the levels
    function populateLevelInfoToModel(promise) {
		// Open the database!
		open().onopen = function onDBOpened(db) {
			// Load the levels...
			var loadLevelsTransaction = db.transaction(STORE.mazes.name, "readonly");
			var levelStructure = [];
			loadLevelsTransaction.objectStore(STORE.mazes.name).openCursor().onsuccess = function iterateAllRecordsInMazes(e) {
				if (e.target.result) { // a cursor object
					var record = e.target.result.value;
					// Does this level already exist in my data?
					if (!levelStructure[record.levelIndex]) {
						for (var i = 0, dst = record.levelIndex; i <= dst; i++) {
							if (!levelStructure[i])
								levelStructure.push({ solvedCount: 0, m: [] });
						}
					}
				    // Now it will exist...
				    // Auto-generate the localization ID... Format: "nameX_XX"
					record.gameNameLocId = ("name" + record.levelIndex + "_" + ((record.gameId < 10) ? "0" : "") + record.gameId);
					// Is this a solved game? If so, note it...
					if (record.timeSlowest != 0)
						levelStructure[record.levelIndex].solvedCount++;
					//add it to the right m: array
					levelStructure[record.levelIndex].m.push(record);

					e.target.result.continue(); // Get the next item in the object store's cursor
				}
			}
			loadLevelsTransaction.oncomplete = function onLevelsLoadedTransactionComplete(e) {
				// Get the level meta-info
				var loadLevelMetaTransaction = db.transaction(STORE.levelMeta.name, "readonly");
				loadLevelMetaTransaction.objectStore(STORE.levelMeta.name).openCursor().onsuccess = function onLevelMetaRecordReceived(e) {
					if (e.target.result) {
						var record = e.target.result.value;
						levelStructure[record.levelNum].defLocked = record.defaultLocked;
						e.target.result.continue();
					}
				};
				loadLevelMetaTransaction.oncomplete = function onFinishLoadingLevelMetaInfoTransactionComplete(e) {
					// Convert from database objects to model objects
				    var newLevels = [];
					levelStructure.forEach(function iterateLevelStructure(dbLevelOb) {
						var level = new LevelModel(dbLevelOb.solvedCount, dbLevelOb.defLocked);
						// Sort the maze levels ascending
						dbLevelOb.m.sort(function (a, b) { return a.gameId - b.gameId; });
						dbLevelOb.m.forEach(function (dbMazeOb) {
						    level.mazes.push(new TileModel(dbMazeOb.gameId, dbMazeOb.gameNameLocId, dbMazeOb.numSmallCheeses, dbMazeOb.timeFastest, dbMazeOb.timeSlowest, dbMazeOb.timeSum, dbMazeOb.gamesTotal, dbMazeOb.template));
						});
						newLevels.push(level);
					});
					// All loaded!
					mazeapp.levels = newLevels;
					promise.done();
				};
			}
		}
	}
	
    function pushMazeStatsData(statsUpdateEvent, fastest, slowest, sumTime, totalCount) {
        lookupMazeInDB(mazeapp.currentLevel, (mazeapp.activeMaze + 1), function onMazeLocated(dBcursor) {
            dBcursor.value.timeFastest = fastest;
            dBcursor.value.timeSlowest = slowest;
            dBcursor.value.gamesTotal = totalCount;
            dBcursor.value.timeSum = sumTime;
            dBcursor.update(dBcursor.value); // If the callers needs the completion callback to finish a promise, then attach to update's onsucess handler.
        });
    }

    function pullResumeStateToModel(callback) {
        openResumeInfoStoreFor("readonly", function onPullDBStateToMazeapp(dBState) {
            mazeapp.currentLevel = dBState.currentLevelIndex;
            mazeapp.activeMaze = dBState.activeMazeIndex;
            mazeapp.maze = dBState.gameData
            mazeapp.lang = dBState.lang;
            mazeapp.pixelPerUnit = dBState.pixPerMazeUnit;
            mazeapp.payingCustomer = dBState.paid;
        }, callback);
    }

	// Start loading the level array of the model
    mazeapp.on("appinit", function onAppInit(modelEvent) {
        var promise = modelEvent.getPromise();
        populateLevelInfoToModel(promise);
    });

	// Start loading the resume state info
    mazeapp.on("appinit", function onloadResumeStateDuringFirstInit(modelEvent) {
        var promise = modelEvent.getPromise();
        pullResumeStateToModel(function onResumeStateLoaded() {
            promise.done();
        });
    });

	// If the game is reset, delete the database and re-load it.
	mazeapp.on("appreinit", function resetDB(modelEvent) {
	    var promise = modelEvent.getPromise();
		dbOpen.close();
		dbOpen = null;
		deleteDB(function onDBdeleted(e) {
		    pullResumeStateToModel(function onFirstReInitStageComplete() {
		        populateLevelInfoToModel(promise);
		    });
		});
	});

	function deleteDB(finishedCallback) {
	    var deleteRequest = indexedDB.deleteDatabase(DB);
	    deleteRequest.onblocked = function (e) { setTimeout(deleteDB, 20, finishedCallback); } // Try, try again (probably still cleaning up...
	    deleteRequest.onsuccess = finishedCallback;
	}

    // Synchronize the database with the current selected level
    // Stores (primarily) the "currentLevelIndex"
	mazeapp.on("levelsloaded", function addLevelSelectionChangeWatches() {
		mazeapp.levels.forEach(function addLevelHandler(levelModel) {
            // Update the DB when the selected level changes (restores the game to the previously played level)
		    levelModel.on("selected", function onStoreSelectedLevel(modelEvent) {
		        var promise = modelEvent.getPromise();
		        openResumeInfoStoreFor("readwrite", function gotResumeState(resumeState) {
		            resumeState.currentLevelIndex = mazeapp.currentLevel;
		        }, function finished() { promise.done(); });
		    });
		});
	});

	function pushSmallCheesesUpdate(modelEvent) {
	    lookupMazeInDB(mazeapp.currentLevel, (mazeapp.activeMaze + 1), function onMazeLocated(cursor) {
	        cursor.value.numSmallCheeses = modelEvent.target.smallCheesesAcquired;
	        cursor.update(cursor.value);
	    });
	}

	mazeapp.on("mazeloaded", function registerForSyncMazeStatsData(modelEvent) {
        // Store the activeMaze object to the DB...
	    var promise = modelEvent.getPromise();
	    openResumeInfoStoreFor("readwrite", function onGotResumeInfo(resumeInfo) {
	        resumeInfo.activeMazeIndex = mazeapp.activeMaze;
	    }, function finished() { promise.done(); });
        // Register for statsupdates and other stats-related changes
	    mazeapp.maze.stats.on("statsupdate", pushMazeStatsData);
	    mazeapp.maze.stats.on("smallcheesenumchange", pushSmallCheesesUpdate);
	});

	mazeapp.on("mazeunloaded", function unregisterForSyncMazeStatsData(modelEvent) {
	    var promise = modelEvent.getPromise();
	    openResumeInfoStoreFor("readwrite", function onGotResumeInfo(resumeInfo) {
	        resumeInfo.activeMazeIndex = null; // Clear the active maze.
	    }, function finished() { promise.done(); });
        // Unhook the status stuff...
	    mazeapp.maze.stats.off("statsupdate", pushMazeStatsData);
	    mazeapp.maze.stats.off("smallcheesenumchange", pushSmallCheesesUpdate);
	});

    // When a game is solved, remove any potentially suspended game info
    // Forces the "maze" to null even before the actual data model "maze" is cleaned up...(so that you can't restore from resume a game that is already finished)
	mazeapp.on("appresume", function clearSuspendedMazeData(modelEvent) {
	    var promise = modelEvent.getPromise();
	    openResumeInfoStoreFor("readwrite", function gotResumeInfoForClearSuspendedMazeData(resumeState) {
	        resumeState.gameData = null;
	    }, function finsihed() { promise.done(); });
	});
    
    // Synchronize any prepared game after a suspension is in progress
    // Save the "maze" data.
	mazeapp.on("appsuspend", function onAppSuspend(modelEvent) {
	    var promise = modelEvent.getPromise();
	    openResumeInfoStoreFor("readwrite", function gotResumeInfo(resumeInfo) {
	        resumeInfo.gameData = mazeapp.maze ? mazeapp.maze.toSCASerializable() : null; // May be null (but hopefully isn't)
	    }, function finished() { promise.done(); });
	});

    // Synchronize the state of the settings panel when it is dismissed
    // Save the "pixelPerUnit" / "lang" updates. (Clear maze data is handled via "appreinit".)
	mazeapp.on("settingshide", function onSettingsDismissed(modelEvent) {
	    var promise = modelEvent.getPromise();
	    openResumeInfoStoreFor("readwrite", function gotResumeInfo(resumeInfo) {
	        resumeInfo.lang = mazeapp.lang;
	        resumeInfo.pixPerMazeUnit = mazeapp.pixelPerUnit;
	    }, function finished() { promise.done(); });
	});

    // Save the "payingCustomer" state-change to the database
	mazeapp.on("unlock", function onUnlock(modelEvent) {
	    openResumeInfoStoreFor("readwrite", function gotResumeInfo(resumeInfo) {
	        resumeInfo.paid = mazeapp.payingCustomer;
	    });
	}); 
})();



// Localization engine (applied to all elements that have static (or dynamic language content)

// Handle switching languages..
mazeapp.on("langchange", function onlangchange(e) {
    var finishedPromise = e.getPromise();
    var xhr = new XMLHttpRequest(); 
    xhr.open("GET", "/resources/" + mazeapp.lang + ".resjson");
    xhr.onload = function onResJSONLoaded() {
        var stringTable = JSON.parse(xhr.responseText);
        // Swap out the live localizable elements...
        document.querySelectorAll("[data-res]").forEach(function localizeInnerTextOfElement(element) { element.innerHTML = stringTable[element.getAttribute("data-res")]; });
        document.querySelectorAll("[data-resv]").forEach(function localizeValueAttribOfElement(element) { element.value = stringTable[element.getAttribute("data-resv")]; });
        // Save the string table for dynamic scenarios
        mazeapp.loc = stringTable;
        finishedPromise.done();
    }
    xhr.send();
});