"use strict";
// { //maze
//    stats: ref to mazeapp.levels[x].mazes[y]      // Back reference (quick access) to the game stats for this maze
//    startFloorIndex: #
//    endFloorIndex: #
//    currentFloorIndex: #                          // <zoomout (last floor index)> / <zoomin> / <selected (?bool)> / <unselected (old floor index)> (set to null to trigger zoom out)
//    width: #                                      // in game model units
//    height: #                                     // in game model units
//    elapsedTime: # (milliseconds)                 // Total time in the game while the clock is running (readonly)
//    exceededFastestTime: bool                     // Indicates whether the elapsed time has passed the fastest recorded time. Fires <fastesttimeexceeded>
//    clockState: { "running", "stopped", "reset" } // fires <clockstart> / <clockstop> events
//    viewAppbar: bool                              // fires <appbarshow> / <appbarhide> events
//    viewPaused: bool                              // fires <pausedshow> / <pausedhide> events
//    renderingEnabled: bool                        // fires <renderingenabled> / <renderingdisabled> events
//    toSCASerializable()
//    floor: floorOb (current floor ref) readonly
//    floors: [                                     // fires <transportgo (floor#, mapRef)>
//        {
//            canvases: [
//               elem...                            // The canvas elements associated with this floor 0..1
//            ],
//			  map: [
//               {                  
//                  p: [x1, y1]
//                  c: [ connections ]
//                  (optional)
//                  path: pathSegRef or undefined
//                  start: bool
//                  end: bool
//                  transport: {
//                      f: #
//                      c: targetCellRef
//                      t: # (type enum)
//                      s: # (state enum)
//                      path: pathSegRef
//                  }
//               }
//            ],
//            render: [
//               {
//                  r: # (rotation amount--in radians)
//                  t: [x1, y1] (translation amount)
//					y: # (type flag [1=rect, 2=circle])
//				    c: [...] (interpreted based on the render group type)
//               }
//            ],
//            transports: [
//  		     {
//                  f: # (floor index)
//					c: mapRef (ref to the target map cell object on the indicated floor)
//					t: # (TRANSPORT.TYPE.*)
//					s: # (TRANSPORT.STATE.*)
//            ],
//            paths: [
//               [ mapRef, ...	// Array of references to map cells
//               ]
//            ],
//            start: mapRef or null
//            end:   mapRef or null
//            active: mapRef or null                // Fires <activechanged (mapRef)>, <activeon>, <activeoff> events.
//            hitTestMaze( point )                  // Returns the closest mapRef within a half-cell radius or null if nothing was hit by the point
//            isAdjacentToActiveCell ( mapRef )     // Returns true if the provided mapRef is adjacent to the active cell, false otherwise
//            extendPathToCell( fromPathCell, toCell )    // Connects the path data model from the existing cell (with a path) to the new cell
//            triggerCell( mapRef )                 // Checks if the cell has any triggers, and if so, activates their behavior.
//            reinitFloor()                         // Restores the floor to it's initial state (render state, paths, active, etc.)
//            toSCASerializable()
//        }
//    ]
// }

(function MazeModelHelperScope() {
	// Constants
	var ENSURE1_0_FLOOR_ROUNDS_DOWN   = -0.0000001;
	var NOT_CONNECTED_ORIGINAL_WALL   = -1;
	var NOT_CONNECTED_INTERIOR_WALL   = -2;
	var NOT_CONNECTED_UNRENDERED_WALL = -3;
	var TRANSPORT = {
		TYPE: {
			STAIRS_UP:   1,
			STAIRS_DOWN: 2,
			PORTAL_UP:   3,
			PORTAL_DOWN: 4,
			PORTAL_SAME: 5
		},
		STATE: {
			DISABLED: 1,
			ENABLED:  2,
			ACTIVE:   3,
			GO:       4  // Signal that the transport has been activated and should engage (render thread sees this)
		}
	};
	var PATH = {
	    TYPE: {
	        BEGIN: 1,
	        FORK: 2,
	        TRANSPORT: 3
	    }
	};
	var RENDERGROUPTYPE_RECT = 1;
	var RENDERGROUPTYPE_CIRC = 2;
	var X1 = 0;
	var Y1 = 1;


	function resolveStartOrEndCell(template, startOrEnd) {
		// Structure of template's start/end input:
		// template = {
		//   end: [
		//     { 
		//       c: [ cell candidate indexes ]
		//     }
		//   ],
		//   start: {
		//     c: [ cell candidate indexes ]
		//   }
		if (startOrEnd == "end") // Narrow down the end-choices from an array of floors to a single floor (to match start choices)	
			template[startOrEnd] = template[startOrEnd][Math.floor(Math.random() * template[startOrEnd].length + ENSURE1_0_FLOOR_ROUNDS_DOWN)];
		// Start and end are now the same structure... Narrow down the choices from among the start/end candidates index list
		var startOrEndCellIndex = template[startOrEnd].c[Math.floor(Math.random() * template[startOrEnd].c.length + ENSURE1_0_FLOOR_ROUNDS_DOWN)];
		// Find an appropriate edge to use as the start side (if possible)
		// The final chosen side should be a wall edge (unless it has none)
		var startOrEndFloorIndex = template[startOrEnd].f;
		var targetCell = template.floors[startOrEndFloorIndex].map[startOrEndCellIndex];
		targetCell[startOrEnd] = true; // Sets "start"/"end" flag on the target map cell
		// CONVERT: .start/end.c from index to direct reference, and store it on the right floor.
		template.floors[startOrEndFloorIndex][startOrEnd] = targetCell; // Store the target cell on floor.start
		var startOrEndConnectors = targetCell.c;
		for (var side = 0, sideLen = startOrEndConnectors.length; side < sideLen; side++) {
			if (startOrEndConnectors[side] == NOT_CONNECTED_ORIGINAL_WALL) {
				startOrEndConnectors[side] = NOT_CONNECTED_UNRENDERED_WALL;
				break; // Just select one side :)
			}
		}
		// Cleanup original start/end data on the template...
		delete template[startOrEnd];
		// Now, floor[x].start/end: ref_to_start/end_cell, map cell annotated with "start"/"end" (boolean) as well.
		return startOrEndFloorIndex;
	}

	function resolveCrossFloorConnections(template) {
		// Structure of the template's transport data:
		// template = {
		//   group: [		// Always pairs of objects
		//     {			// even groups are origin "from" groups, while odd groups are destination "to" groups
		//       c: [ prospective transport index ]
		//       n: #		// Maximum number of final transports to produce
		//       m: bool    // Flag indicating whether this transport is mirrored/symmetric
		//       f: #       // Floor index of this group's prospective cell indexes
		//       t: #       // Transport type (1=stairs +1/-1, 2 = portal)
		//     }
		//   ]
		// }
		// First select the cells from the transport groups that should be bound together
		for (var i = 0, iLen = template.group.length; i < iLen; i += 2) {
			var fromGroup = template.group[i];
			// Reduce the number of prospective transports to the specified number n
			while (fromGroup.c.length > fromGroup.n)
				fromGroup.c.splice(Math.floor(Math.random() * fromGroup.c.length + ENSURE1_0_FLOOR_ROUNDS_DOWN), 1); // Randomly remove an index
			var toGroup = template.group[i + 1];
			if (fromGroup.m) { // Mirror?
				toGroup.c = fromGroup.c; // Copy the index set over exactly...
			}
			else { // asymetric...
				while (toGroup.c.length > toGroup.n)
					toGroup.c.splice(Math.floor(Math.random() * toGroup.c.length + ENSURE1_0_FLOOR_ROUNDS_DOWN), 1); // Randomly remove an index
			}
			// The remaining indexes are no longer candidates, they are actual connectors, add them into the appropriate map info, and mark the render cells
			for (var a = 0, aLen = fromGroup.c.length, fromFloor = fromGroup.f, toFloor = toGroup.f; a < aLen; a++) {
				var dstCellIndex = a; // Assumes mirroring (initially)
				if (!fromGroup.m)
					dstCellIndex = Math.floor(Math.random() * toGroup.c.length + ENSURE1_0_FLOOR_ROUNDS_DOWN);
				// Add the transport info to the map object
				var fromCell = template.floors[fromFloor].map[fromGroup.c[a]];
				var toCell = template.floors[toFloor].map[toGroup.c[dstCellIndex]];
				// Remove the selected destination cell so that it doesn't come up again (for the random() selection above)
				if (!fromGroup.m)
					toGroup.c.splice(dstCellIndex, 1);
				// Link the transports
				fromCell.transport = { c: toCell, f: toFloor, s: TRANSPORT.STATE.DISABLED };
				toCell.transport = { c: fromCell, f: fromFloor, s: TRANSPORT.STATE.DISABLED };
				// Convert the type and save it to the transports array.
				if (fromGroup.t == 1) { // stairs
					fromCell.transport.t = (toFloor > fromFloor) ? TRANSPORT.TYPE.STAIRS_UP : TRANSPORT.TYPE.STAIRS_DOWN;
					toCell.transport.t = (toFloor > fromFloor) ? TRANSPORT.TYPE.STAIRS_DOWN : TRANSPORT.TYPE.STAIRS_UP;
				}
				else { // portal
					fromCell.transport.t = (toFloor > fromFloor) ? TRANSPORT.TYPE.PORTAL_UP : (toFloor < fromFloor) ? TRANSPORT.TYPE.PORTAL_DOWN : TRANSPORT.TYPE.PORTAL_SAME;
					toCell.transport.t = (toFloor > fromFloor) ? TRANSPORT.TYPE.PORTAL_DOWN : (toFloor < fromFloor) ? TRANSPORT.TYPE.PORTAL_UP : TRANSPORT.TYPE.PORTAL_SAME;
				}
				// Ensure there is a "transports" array, if not, create it.
				if (!template.floors[fromFloor].transports)
					template.floors[fromFloor].transports = [];
				if (!template.floors[toFloor].transports)
					template.floors[toFloor].transports = [];
				template.floors[fromFloor].transports.push(fromCell);
				template.floors[toFloor].transports.push(toCell);
			}
		}
		delete template.group;
		// the old "group" data is removed completely
		// a new "transports"[] array with references to the map object that are the transports on this floor
		// map objects have a "transport" object with:
		// * "f" (floor index)
		// * "c" (ref to the target map cell object on the indicated floor)
		// * "t" (TRANSPORT.TYPE.*)
		// * "s" (TRANSPORT.STATE.*)
	}

	function applyGrowingTreeAlgorithm(template, builderFloor, selectionPolicy) {
		if ((selectionPolicy < 1) || (selectionPolicy > 8))
			return console.debugAssert("Chosen selection policy number is outside the appropriate range");
		var list = [];
		var newItemsListBoundary = 0; // Upper-bound index based on list.length * LISTBOUNDARY_NEW_PERCENT
		var oldItemsListBoundary = 0; // Lower-bound index based on list.length * LISTBOUNDARY_OLD_PERCENT

		var WALL_CANDIDATE = 1;
		var WALL_ABSENT = 0; // falsey value
		// NOTE: Careful, don't let these two boundaries overlap. (It will cause some negative index computations herein)
		var LISTBOUNDARY_NEW_PERCENT = 0.85; // first 15% of the list is considered the recently added cells (list is a stack, with the top of the stack at the end)
		var LISTBOUNDARY_OLD_PERCENT = 0.15; // Last 15% of the list is considered the older cells
		var OCCASIONAL_RANDOMNESS_INITIAL_PROBABILITY = 0.05; // 5% chance
		var OCCASIONAL_RANDOMNESS_PROBABILITY_INCREASE_PER_SELECTION = 0.05; // increment by 5% per iteration, reaches a 50% probability in 10 tries. On average, "occasional" means 1-in-10.
		var occasionalRandomness = OCCASIONAL_RANDOMNESS_INITIAL_PROBABILITY; // Start with 5% chance, and increase over time...

		list.pick = function (optionalOverrideSelectionPolicy) {
			if (this.length == 0)
				return console.debugAssert("The game should not be attempting to pick from an empty list!");
			// Re-compute index markers based on length of the list (ensure that the boundaries are current (since push-es don't update this data)
			newItemsListBoundary = Math.round(this.length * LISTBOUNDARY_NEW_PERCENT); // Shift the percent to the tail of the list (top of the stack)
			if (newItemsListBoundary >= this.length)
				newItemsListBoundary = this.length - 1;
			oldItemsListBoundary = Math.round(this.length * LISTBOUNDARY_OLD_PERCENT);
			if (oldItemsListBoundary >= this.length)
				oldItemsListBoundary = this.length - 1;
			// Cells are added to the end (push) in all cases.
			switch (optionalOverrideSelectionPolicy ? optionalOverrideSelectionPolicy : selectionPolicy) {
				case 1:	// Use most recent (like: recursive backtracker) // HARDER on HAPPY Face
					return this.pop();				
				case 2:	// Pick fully random (like: Prim's algorithm)
					return this.splice(Math.floor(Math.random() * this.length + ENSURE1_0_FLOOR_ROUNDS_DOWN), 1)[0]; // splice returns an array
				case 3: // Use oldest cell (low river factor)
					return this.shift();
				case 4: // Usually most recent (but occasionally random)
						// Try for random selection
						if (Math.random() < occasionalRandomness) {
							// Jackpot!
							occasionalRandomness = OCCASIONAL_RANDOMNESS_INITIAL_PROBABILITY;
							return this.getCellWrapper(2);
						}
						else {
							occasionalRandomness += OCCASIONAL_RANDOMNESS_PROBABILITY_INCREASE_PER_SELECTION; // Increase my chances next time by this percent
							return this.getCellWrapper(1);
						}
				case 5: // Usually oldest cell (but occasionally random)
						if (Math.random() < occasionalRandomness) {
							// Jackpot!
							occasionalRandomness = OCCASIONAL_RANDOMNESS_INITIAL_PROBABILITY;
							return this.getCellWrapper(2);
						}
						else {
							occasionalRandomness += OCCASIONAL_RANDOMNESS_PROBABILITY_INCREASE_PER_SELECTION; // Increase my chances next time by this percent
							return this.getCellWrapper(3);
						}
				case 6: // Random recent cell
					return this.splice(Math.round(Math.random() * (this.length - newItemsListBoundary)) + newItemsListBoundary, 1)[0];
				case 7: // Random oldest cell
					return this.splice(Math.round(Math.random() * oldItemsListBoundary), 1)[0];
				case 8: // Random middle-of-the-pack
					return this.splice(Math.round(Math.random() * (newItemsListBoundary - oldItemsListBoundary)) + oldItemsListBoundary, 1)[0];
			}
		}

		function processCell(cell, optionalOverrideBuilderFloor) {
			if (cell.made)
				return; // Prevent infinite recursion when recursively invoked on a cell with a 3d connector (will only go 1 level deep)
			var tempWrapper = { 
				floor: (typeof optionalOverrideBuilderFloor == "undefined") ? builderFloor : optionalOverrideBuilderFloor, 
				cell: cell, 
				wallCandidates: [] 
			};
			// Circ-ref to get from cell->wrapper (it's explicitly broken later...)
			cell.builderInfo = tempWrapper;
			for (var side = 0, sideLen = cell.c.length; side < sideLen; side++)
				tempWrapper.wallCandidates.push((cell.c[side] > NOT_CONNECTED_ORIGINAL_WALL) ? WALL_CANDIDATE : WALL_ABSENT); // Because the other walls are already accounted for
			cell.made = true; // Can't put on the temp wrapper since this is needed even after the temp wrapper is removed (on cell exit from list)
			// Will remove/convert the wallCandidates when this cell comes out of the list...
			list.push(tempWrapper); // Temporary wrapper
			// cells with 3-dimensional connectors (multi-floor/portals) have a guaranteed connection; always insert such cells as a pair
			// The 3-dimensial aspect is thus not a participant in the normal carving algorithm (becasue the 3d path is carved at insertion)
			if (cell.transport) { // Has a 3d connector // TODO: transport landing cells will need some special case treatment to promote them for higher-than-ususal re-selection (so that they don't often end-up as leaf branches)
				// Add the connected-to cell also (and mark it as "carved"/made)
				processCell(cell.transport.c, cell.transport.f); // Recursion fun!
			}
		}

		function finalize(tempCellInfo) {
			// Convert the wallCandidates info into the connections array (c)
			for (var side = 0, sideLen = tempCellInfo.wallCandidates.length; side < sideLen; side++) {
				if (tempCellInfo.wallCandidates[side]) // if wall candidate (now a sureity)
					tempCellInfo.cell.c[side] = NOT_CONNECTED_INTERIOR_WALL; // "remove" the official index (this is now a true wall and will be rendered as such)
				else if (tempCellInfo.cell.c[side] > NOT_CONNECTED_ORIGINAL_WALL) // A connected wall, CONVERT the index into an actual ref to the map object
					tempCellInfo.cell.c[side] = template.floors[tempCellInfo.floor].map[tempCellInfo.cell.c[side]];
				// Otherwise, leave it -- an original wall.
			}
			// Remove the circ-ref to allow the wrapper to be cleaned up
			delete tempCellInfo.cell.builderInfo;
		}

		function generateListOfUnMadeAdjacentCells(cell) {
			var candidates = [];
			for (var side = 0, sideLen = cell.c.length; side < sideLen; side++) {
				if ((cell.c[side] > NOT_CONNECTED_ORIGINAL_WALL) && !template.floors[builderFloor].map[cell.c[side]].made) // "made" is set when a cell is carved into
					candidates.push(side);
			}
			return candidates;
		}

		function findSideConnectedTo(sourceCell, connectedToCell) {
			for (var side = 0, sideLen = sourceCell.c.length; side < sideLen; side++) {
				if ((sourceCell.c[side] > NOT_CONNECTED_ORIGINAL_WALL) && (template.floors[builderFloor].map[sourceCell.c[side]] === connectedToCell))
					return side;
			}
		}

		// Note: the builder starts off on the exit floor (the floor with the end cell)
		// Don't add the start cell to the list--let it be located "naturally" via the algorithm.
		processCell(template.floors[builderFloor].end);
		// This is the "growing tree algorithm" as detailed at http://www.astrolog.org/labyrnth/algrithm.htm#perfect
		while (list.length > 0) {
			var info = list.pick();
			builderFloor = info.floor; // Set the "working" floor appropriately to the retrieved cell
			var cell = info.cell;
			var possibleUnmadeAdjacentCells = generateListOfUnMadeAdjacentCells(cell);
			// Try to "carve-in" to an adjacent "unmade" cell
			if (possibleUnmadeAdjacentCells.length > 0) {
				// Carve in; select a random side from the possible adjacent cells...
				var side = possibleUnmadeAdjacentCells[Math.floor(Math.random() * possibleUnmadeAdjacentCells.length + ENSURE1_0_FLOOR_ROUNDS_DOWN)]; // Ensure a perfect 1.0 (random value) won't mess-up the index.
				var carveToCell = template.floors[builderFloor].map[cell.c[side]];
				// Mark the wall from this cell and the cell linked by "side" in the adjacent cell as WALL_ABSENT
				info.wallCandidates[side] = WALL_ABSENT;
				// process the cell (since it was previously un-made).
				processCell(carveToCell);
				carveToCell.builderInfo.wallCandidates[findSideConnectedTo(carveToCell, cell)] = WALL_ABSENT;
				// Re-insert the info cell too...unless it now has no more possible unmade adjacent cells...
				if (possibleUnmadeAdjacentCells.length == 1) // Which is now zero after this operation...
					finalize(info);
				else
					list.push(info); // Don't "insert" as that is only for new unmade cells (this one is already "made")
			}
			else
				finalize(info); // Done with this cell.
		}
	}
    
	Object.defineProperty(window, "MazeModel", {
		value: function MazeModelConstructor(maze, isTemplate) {
			// For templates, reduce the start/end cell candidates for this specific maze (randomly)
			var startFloorIndex = isTemplate ? resolveStartOrEndCell(maze, "start") : maze.startFloorIndex;
			var endFloorIndex = isTemplate ? resolveStartOrEndCell(maze, "end") : maze.endFloorIndex;
			var currentFloorIndex = isTemplate ? startFloorIndex : maze.currentFloorIndex;
			var currentFloorIndexInTransition = false; // Ensures multiple floor index changes don't overlap.
			var baseElapsedTime = isTemplate ? 0 : maze.elapsedTime;
			var exceededFastestTime = isTemplate ? ((mazeapp.levels[mazeapp.currentLevel].mazes[mazeapp.activeMaze].timeSlowest == 0) ? true : false) : maze.exceededFastestTime;
			var realtimeSinceClockStarted = 0; // So that the elapsed time doesn't have to be continously updated
			var clockState = "stopped";
			var appbarShowing = false;
			var pausedShowing = false;
			var pausedInTransition = false;
			var rendering = false;
			var zoomedOut = false; // Start zoomed in.
			if (isTemplate) {
				resolveCrossFloorConnections(maze);
				applyGrowingTreeAlgorithm(maze, endFloorIndex, 1); // TODO: the selection policy should be embedded into each template!!
			}
			return Object.create(new Eventing(), {
			    stats: { // Read-only
			        enumerable: true,
                    value: mazeapp.levels[mazeapp.currentLevel].mazes[mazeapp.activeMaze]
			    },
			    startFloorIndex: { // Read-only
					enumerable: true,
					get: function() { return startFloorIndex; }
				},
				endFloorIndex: { // Read-only
					enumerable: true,
					get: function() { return endFloorIndex; }
				},
				currentFloorIndex: {  // <zoomout> / <zoomin> / <selected> / <unselected> (set to null to trigger zoom out)
					enumerable: true,
					get: function() { return currentFloorIndex; },
					set: function (x) {
                        if (((typeof x != "number") && (x !== null)) || currentFloorIndexInTransition || (currentFloorIndex === x)) // Last clause protects the null == null case primarily (see additional check below)
                            return;
                        if ((x === null) && (this.floors.length < 2))
                            return console.debugAssert("There is no zooming out when there is only one floor!");
                        if (x === null) { // Then there is a current selected floor index...
                            currentFloorIndexInTransition = true;
                            this.notify("unselected", { args: [currentFloorIndex] });
                            this.notify("zoomout", {
                                args: [currentFloorIndex],
                                oncomplete: function () { currentFloorIndexInTransition = false; }
                            });
                            currentFloorIndex = x;
                            return;
                        }
					    // This is a request to set a floor index number...
                        x = Math.min(this.floors.length - 1, x); // Force values over the floor length max to the max value...
                        x = Math.max(0, x); // Force values under 0 to zero...
                        if (currentFloorIndex == x)
                            return; // Only floor changes within the valid range are honored
					    // Now we're committed
                        currentFloorIndexInTransition = true;
                        if (typeof currentFloorIndex == "number") { // Changing floors...
                            this.notify("unselected", { args: [currentFloorIndex] });
                            currentFloorIndex = x; // Because notify can sometimes be synchronous (when re-entered from an existing callback).
                            this.notify("selected", { args: [true], oncomplete: function () { currentFloorIndexInTransition = false; } });
                        }
                        else { // was 'null'
                            this.notify("selected", { args: [false] });
                            currentFloorIndex = x;
                            this.notify("zoomin", { oncomplete: function () { currentFloorIndexInTransition = false; } });
                        }
					}
				},
				floor: { // Convenience shortcut to get the current floor object.
					enumerable: true,
					get: function() { 
						return this.floors[currentFloorIndex];
					}
				},
				floors: { // Creates the FloorModel objects directly.
					enumerable: true,
					value: maze.floors.map(function (mazeFloor) { return new FloorModel(mazeFloor, isTemplate, maze.width, maze.height); })
				},
				renderingEnabled: {
				    enumerable: true,
				    get: function () { return rendering; },
				    set: function (x) {
				        if (rendering == x)
				            return;
				        if (typeof x != "boolean")
				            console.debugAssert("renderingEnabled is a boolean property");
				        if (x)
				            this.notify("renderingenabled");
				        else
				            this.notify("renderingdisabled");
				        rendering = x;
				    }
				},
				width: { // read-only
					enumerable: true,
					value: maze.width
				},
				height: { // read-only
					enumerable: true,
					value: maze.height
				},
				exceededFastestTime: {
				    enumerable: true,
				    get: function () { return exceededFastestTime; },
				    set: function (x) {
				        if (typeof x != "boolean")
				            return console.debugAssert("exceededFastestTime expects a boolean value");
				        if (x == exceededFastestTime)
				            return; // ignore
				        exceededFastestTime = x;
				        if (exceededFastestTime)
				            this.notify("fastesttimeexceeded");
				    }
				},
				elapsedTime: { // read-only
				    enumerable: true,
				    get: function () {
				        if (clockState == "stopped")
				            return baseElapsedTime;
				        else
				            return baseElapsedTime + (performance.now() - realtimeSinceClockStarted); // Computes on-the-fly
				    }
				},
				clockState: {
				    enumerable: true,
				    get: function() { return clockState; },
				    set: function (x) {
				        if ((x != "running") && (x != "stopped") && (x != "reset"))
				            return console.debugAssert("Bad value assignment to the clockState");
                        // Resetting the clock causes a running clock to stop, but does not impact a stopped clock
				        var resetRequest = false;
				        if (x == "reset") {
				            baseElapsedTime = 0;
				            resetRequest = true;
				            x = "stopped";
				        }
                        // At this point there is only two states: stopped/running
				        if (x == clockState)
				            return;
                        // The state changed
				        clockState = x;
				        if (clockState == "running") {
				            realtimeSinceClockStarted = performance.now();
				            this.notify("clockstart");
				        }
				        else {
				            if (!resetRequest) // (regular stop request) leave the baseElapsedTime alone otherwise...
                                baseElapsedTime += (performance.now() - realtimeSinceClockStarted);
				            this.notify("clockstop");
				        }
				    }
				},
				viewAppbar: {
				    enumerable: true,
				    get: function () { return appbarShowing; },
				    set: function (x) {
				        if (typeof x != "boolean")
				            return console.debugAssert("Boolean value expected!");
				        if (appbarShowing === x)
				            return;
				        appbarShowing = x;
				        if (appbarShowing)
				            this.notify("appbarshow");
				        else
				            this.notify("appbarhide");
				    }
				},
				viewPaused: {
				    enumerable: true,
				    get: function () { return pausedShowing; },
				    set: function (x) {
				        if (typeof x != "boolean")
				            return console.debugAssert("Boolean value expected!");
				        if (pausedInTransition || (pausedShowing === x))
				            return;
				        pausedInTransition = true;
				        pausedShowing = x;
				        var params = {
				            oncomplete: function () {
				                pausedInTransition = false;
				            }
				        };
				        if (pausedShowing)
				            this.notify("pausedshow", params);
				        else
				            this.notify("pausedhide", params);
				    }
				},
    		    toSCASerializable: {
				    enumerable: true,
				    value: function () {
                        return { // This becomes the "maze" which can be used to re-construct this object later...
				            startFloorIndex: startFloorIndex,
				            endFloorIndex: endFloorIndex,
				            currentFloorIndex: currentFloorIndex,
				            width: this.width,
				            height: this.height,
				            elapsedTime: this.elapsedTime, // Call the getter
				            exceededFastestTime: exceededFastestTime,
                            floors: this.floors.map(function (floor) { return floor.toSCASerializable(); })
				        };
				    }
				}
			});
		}
	});

    // ---------------------------------------------------------------------------------------------------

    //////////////////////////////////////////
    // Path operations
    //////////////////////////////////////////
    /*
    // Returns a list of neighbors that have paths (caller will need to 
    // decide which one of the neighbors to choose based on proximity)
	function getNeighborsWithPath(mapOb) {
	    var resultsList = [];
	    for (var i = 0, len = mapOb.c.length; i < len; i++) {
	        // if the connected neighbor is on a path
	        if ((typeof mapOb.c[i] == "object" /* "numbers" will be walls *//*) && mapOb.c[i].path)
	            resultsList.push(mapOb.c[i]);
	    }
	    return resultsList;
	}*/
    /*
    // Returns null if the list was empty, otherwise returns an object
    // from the map cell list that is closest (in euclidean space) to the point.
	function findClosestMapCellToPoint(mapCellList, point) {
	    // Easy, if there's only zero or one neighbor
	    if (mapCellList.length == 0)
	        return null;
	    if (mapCellList.length == 1)
	        return mapCellList[0];
	    // There's two or more...
	    var minDist = Number.MAX_VALUE;
	    var candidate = null;
	    for (var i = 0, len = mapCellList.length; i < len; i++) {
	        var candidatePoint = mapCellList[i].p; // [x,y]
	        // Get absolute distance (any direction)
	        var x = Math.abs(candidatePoint[X1] - point.x);
	        var y = Math.abs(candidatePoint[Y1] - point.y);
	        var dist = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	        if (dist < minDist) {
	            minDist = dist;
	            candidate = mapCellList[i];
	        }
	    }
	    // One candiate is now selected
	    return candidate;
	}*/
    /*
    // fromCell will be a cell with an existing path
	function addPathSegment(fromCell, toCell, currentFloor) {
	    // Check if the fromCell is at the "edge" of a path segment (the end)
	    // (the "path" is a ref to a path segment (array), of which, each array index contains a reference to a mapObject)
	    if (fromCell.path[fromCell.path.length - 1] == fromCell) { // Tack on the new cell to the end of the segment (it becomes the new end)
	        toCell.path = fromCell.path;
	        fromCell.path.push(toCell);
	    }
	    else { // The path intersection was in the middle (or start) of a segment
	        // Start a new path segment from fromCell to toCell
	        // Note the fromCell already has a path it belongs to. Rather than make the cell own multiple paths,
	        // we simply add it into the new segment (but it points to a different segment to which it belongs),
	        // and the toCell gets the pointer to the new Segment. Since we're ever only interested in the "last"
	        // cell, this works fine for this algorithm. For rendering, a cell's path ownership is irrelevant (which makes this algorithm OK)
	        var newPathSegment = [];
	        newPathSegment.push(fromCell);
	        newPathSegment.push(toCell);
	        newPathSegment.type = PATH.TYPE.FORK;
	        toCell.path = newPathSegment;
	        currentFloor.paths.push(newPathSegment);
	    }
	    currentFloor.active = toCell;
	}*/

    // This unconditionally starts a new path on the target floor's paths object at the given mapOb
    // These starting paths are special in that they are known to not be forks. Therefore, they are annotated as such for rendering purposes.
    function startPath(paths, mapOb) {
        if (!mapOb || !mapOb.c)
            console.debugAssert("startPath must take a mapRef object parameter");
        var startPath = [mapOb];
        startPath.type = PATH.TYPE.BEGIN;
        paths.push(startPath);
        // annotate the starting cell with its path.
        mapOb.path = startPath;
    }

	function createTransportPath(srcCell) {
	    var transPathSeg = [];
	    transPathSeg.type = PATH.TYPE.TRANSPORT;
	    srcCell.transport.path = transPathSeg;
	    var PATH_LENGTH_UNITS = 6;
	    var dstCell = srcCell.transport.c;
	    var floorDelta = dstCell.transport.f - srcCell.transport.f; // (dst->trans->f is the srcFloor) backwards so that higher floors have a negative value...
	    var yCoordRelativeToSrc = (dstCell.p[Y1] + (floorDelta * mazeapp.maze.height)) - srcCell.p[Y1]; // Takes into account the floor difference (floors increasing as they stack horizontally above)
	    var xCoordRelativeToSrc = dstCell.p[X1] - srcCell.p[X1];
	    var absXLen = Math.abs(xCoordRelativeToSrc);
	    var absYLen = Math.abs(yCoordRelativeToSrc);
	    var numSegmentItems = Math.floor(Math.sqrt(Math.pow(absXLen, 2) + Math.pow(absYLen, 2)) / PATH_LENGTH_UNITS);
	    var xInc = xCoordRelativeToSrc / numSegmentItems;
	    var yInc = yCoordRelativeToSrc / numSegmentItems;
	    var activeFloor = mazeapp.maze.currentFloorIndex;
	    var baseRefX = srcCell.p[X1];
	    var baseRefY = srcCell.p[Y1];
	    for (var i = 0; i <= numSegmentItems; i++) { // Note: <= is valid here--I want to include both the start item point as well as the end item point.
	        // Create psuedo-cell object (not a real cell in the map)
	        transPathSeg.push({
	            p: [/*X1*/ baseRefX + (xInc * i),
                    /*Y1*/ baseRefY + (yInc * i)]
	        });
	        if (((baseRefY + (yInc * i)) < 0) || ((baseRefY + (yInc * i)) > mazeapp.maze.height)) { // The virtual path ambled off the top/bottom of the canvas...
	            mazeapp.maze.floors[activeFloor].paths.push(transPathSeg); // Commit it to the current floor
                // Create a new path group (for the next floor)
	            transPathSeg = [];
	            transPathSeg.type = PATH.TYPE.TRANSPORT;
	            // Adjust the vertical base reference, now that I'm in a new floor...
	            baseRefY = (yInc > 0) ? baseRefY - mazeapp.maze.height : baseRefY + mazeapp.maze.height;
	            // Push the previous coordinate onto the path
	            transPathSeg.push({
	                p: [/*X1*/ baseRefX + (xInc * (i - 1)),
                        /*Y1*/ baseRefY + (yInc * (i - 1))]
	            });
	            // ... and push the current path onto this path segment also...
	            transPathSeg.push({
	                p: [/*X1*/ baseRefX + (xInc * i),
                        /*Y1*/ baseRefY + (yInc * i)]
	            });
	            // The above segment is rendered twice--once in the old floor, and once in the new floor's canvas.
	            // Advance the floor
	            activeFloor = (yInc > 0) ? activeFloor - 1 : activeFloor + 1; // If positive increment then go "down" a floor.
	        }
	    }
	    mazeapp.maze.floors[activeFloor].paths.push(transPathSeg); // Commit the final (only?) segment to the current floor
	    // Mark the final segment path on the tail end of the path segment
	    srcCell.transport.c.transport.path = transPathSeg; // Might be a logically different transPathSeg than the one that started--but this is OK.
	}
    

    // Helper Object constructor for the isPointOnPath API's return value.
    /*
	function PathHitResult(resValue) {
	    this.value = resValue;
	}
	var pathHitResultConstants = { // All readonly, non-configurable
	    PointMissedPath: { enumerable: true, value: 0 }, // point did not hit OR extend an existing path
	    PointHitPath: { enumerable: true, value: 1 }, // point hit exactly on a path
	    PointAtEnd: { enumerable: true, value: 2 }, // point extended/hit on an existing path and reached the exit (game ending condition)
	};
	PathHitResult.prototype = Object.create(null, pathHitResultConstants); // Put the contants on the prototype for external callers to reference...
	Object.defineProperties(PathHitResult, pathHitResultConstants); // Put the contants on the constructor function for convenience in using the constructor.
    */

    //////////////////////////////////
    // Transport operations
    //////////////////////////////////

    /*
	var transportWantsNoTriggerNotice = false; // True if any transport enters appropriate states... note: using a global because I don't want this temporary state to persisit in the data model.

    // Note: floor changes due to user-action responses on transport cells are triggered from the MazeRenderer code (after optional animations)
	function transportUserActionResponse(transportMapOb, currentFloor) {
	    // The action depends on the state of the transportMapOb
	    // NOTE: To get here, transport objects cannot be in the DISABLED state.
	    if (transportMapOb.transport.s == TRANSPORT.STATE.ENABLED) {
	        transportMapOb.transport.s = TRANSPORT.STATE.ACTIVE;
	        transportWantsNoTriggerNotice = true;
	    }
	    else if (transportMapOb.transport.s == TRANSPORT.STATE.ACTIVE) {
	        transportMapOb.transport.s = TRANSPORT.STATE.GO; // Render engine signal to engage...(render engine will switch the state back to enabled later)
	        if (!transportMapOb.transport.path) // Add a transport path from this tranportMapOb to the target...
	            createTransportPath(transportMapOb, currentFloor);
	        transportWantsNoTriggerNotice = false;
	    }
	    // else GO state is ignored...
	}
    */
    // Search transports (that are NOT the current transport), and see if they need to state-transitions based on
    // a FAILED TRIGGER action. This should be the ACTIVE state. Note, does not check if transports array exists, because this
    // method is always called on a floor with known transports.
    /*
	function transportStateTransitionNoTrigger(excludeMapOb, currentFloor) {
	    for (var i = 0, len = currentFloor.transports.length; i < len; i++) {
	        var candiateTransportOb = currentFloor.transports[i];
	        if ((candiateTransportOb != excludeMapOb) && (candiateTransportOb.transport.s == TRANSPORT.STATE.ACTIVE)) {
	            candiateTransportOb.transport.s = TRANSPORT.STATE.ENABLED;
	        }
	    }
	    transportWantsNoTriggerNotice = false; // If any *were* interested, then they were reset by the above loop.
	}*/
    /*
	function returnCellNotOnPath(mazeFloor) {
	    // Results other than PointMissedPath will keep the game play panel in "drawing mode" which 
	    // will allow the contact up pointer to 'processPointRelease' which also checks for 
	    // 'transportWantsNoTriggerNotice'. So all user-interaction cases are handled.
	    if (transportWantsNoTriggerNotice)
	        transportStateTransitionNoTrigger(null, mazeFloor); // Process every transport (by passing null)
	    return new PathHitResult(PathHitResult.PointMissedPath);
	}*/


    // Note: remember that the floor model needs to be able to SCA serialize and reconsitute for save/load
	Object.defineProperty(window, "FloorModel", {
		value: function FloorModelConstructor(mazeFloor, isTemplate, width, height) {
			var active = isTemplate ? null : mazeFloor.active;
			var paths = isTemplate ? [] : mazeFloor.paths;
			var index = new HitTestModel(mazeFloor, width, height, 5) // 5 units in a half-cell (the radius of a cell)
			if (isTemplate) {
				// Setup the render cell conversion (from cell indexes into direct cell references) for performance.
				for (var r = 0, rLen = mazeFloor.render.length; r < rLen; r++) {
					var group = mazeFloor.render[r];
					if (group.y == RENDERGROUPTYPE_RECT) {
						for (var ci = 0, cLen = group.c.length; ci < cLen; ci++) // CONVERT i from index to cell ref in the map.
							group.c[ci].i = mazeFloor.map[group.c[ci].i];
					}
					else
						return console.debugAssert("Not implemented");
				}
			    // If this is the starting floor, then setup the initial path, and the active spot to the starting cell position
				if (mazeFloor.start) {
				    startPath(paths, mazeFloor.start);
				    active = mazeFloor.start;
				}
			}
            
			return Object.create(new Eventing(), {
			    canvases: {                             // This is a convenience-holder for the game play panel (to avoid repeated searching for the right canvas to use per floor)
			        enumerable: true,
                    writable: true,
			        value: null
			    },
			    map: { // read-only
					enumerable: true,
					value: mazeFloor.map
				},
				render: { // read-only
					enumerable: true,
					value: mazeFloor.render
				}, // read-only
				transports: {
					enumerable: true,
					value: mazeFloor.transports
				}, // read-only
				paths: {
					enumerable: true,
					value: paths
				},
				start: {
					enumerable: true,
					value: mazeFloor.start ? mazeFloor.start : null
				},
				end: {
					enumerable: true,
					value: mazeFloor.end ? mazeFloor.end : null
				},
				active: {
					enumerable: true,
					get: function () { return active; },
					set: function (x) {
					    if (x == active)
					        return;
					    if (active == null) // null -> something
					        this.notify("activeon");
					    if (x == null) // something -> null
					        this.notify("activeoff");
					    active = x;
					    if (active != null)
					        this.notify("activechanged", { args: [active] });
					}
				},
				hitTestMaze: {
				    enumerable: true,
				    value: function (point) {
				        if (!point || (typeof point.x != "number") || (typeof point.y != "number"))
				            console.debugAssert("hitTestMaze requires a point object (with .x and .y as numbers)");
				        return index.get(point);
				    }
				},
				isAdjacentToActiveCell: {
				    enumerable: true,
				    value: function (refCell) {
				        if (!refCell || !refCell.c || (typeof refCell.c.length != "number"))
				            console.debugAssert("isAdjacentToActiveCell requires a mapRef object parameter");
				        for (var i = 0, len = refCell.c.length; i < len; i++) {
				            // if the connected neighbor is the active cell
				            if ((typeof refCell.c[i] == "object" /* "numbers" will be walls */) && (refCell.c[i] === active))
				                return true;
				        }
				        return false;
				    }
				},
				extendPathToCell: { // Note that the cells are not validated to be adjacent to each other!
				    enumerable: true,
				    value: function (fromPathCell, toCell) {
				        // fromCell will be a cell with an existing path (validate this), and toCell must not already have a path.
				        if (!fromPathCell || !toCell || !fromPathCell.c || !toCell.c)
				            console.debugAssert("extendPathToCell parameters must be mapRef objects");
				        if (!fromPathCell.path || toCell.path)
				            console.debugAssert("fromPathCell parameter must be part of a path (have a .path) and toCell must not be part of an existing path!");
				        // Check if the fromCell is at the "edge" of a path segment (the end)
				        // (the "path" is a ref to a path segment (array), of which, each array index contains a reference to a mapObject)
				        if (fromPathCell.path[fromPathCell.path.length - 1] == fromPathCell) { // Tack on the new cell to the end of the segment (it becomes the new end)
				            toCell.path = fromPathCell.path;
				            fromPathCell.path.push(toCell);
				        }
				        else { // The path intersection was in the middle (or start) of a segment
				            // Start a new path segment from fromCell to toCell
				            // Note the fromCell already has a path it belongs to. Rather than make the cell own multiple paths,
				            // we simply add it into the new segment (but it points to a different segment to which it belongs),
				            // and the toCell gets the pointer to the new Segment. Since we're ever only interested in the "last"
				            // cell, this works fine for this algorithm. For rendering, a cell's path ownership is irrelevant (which makes this algorithm OK)
				            var newPathSegment = [];
				            newPathSegment.push(fromPathCell);
				            newPathSegment.push(toCell);
				            newPathSegment.type = PATH.TYPE.FORK;
				            toCell.path = newPathSegment;
				            paths.push(newPathSegment);
				        }
				        // Now, if this is an item (cheese, transport, etc.) then activate it.
				        if (toCell.transport) {
				            //this.notify("transportenabled", { args: [toCell] });
				            toCell.transport.s = TRANSPORT.STATE.ENABLED;
				            toCell.transport.c.transport.s = TRANSPORT.STATE.ENABLED;
				        }   
				    }
				},
				triggerCell: {
				    enumerable: true,
				    value: function (mapOb) {
				        // Check for and trigger optional activation behaviors for certain things that can exist in a cell...
				        if (mapOb.transport && (mapOb.transport.s == TRANSPORT.STATE.ENABLED)) {
				            mazeapp.maze.notify("transportgo", { args: [mapOb.transport.f, mapOb.transport.c] });
				            if (!mapOb.transport.path) { // Add a transport path for this tranportMapOb to the target...
				                createTransportPath(mapOb);
				                // And start a real path there too...
				                startPath(mazeapp.maze.floors[mapOb.transport.f].paths, mapOb.transport.c);
				            }
				        }
				    }
				},
				reinitFloor: {
				    enumerable: true,
				    value: function () {
				        console.debugAssert("NOT YET IMPLEMENTED");
				        // First purge the rendering state (while the data state is still intact)
				        MazeRender.resetRenderStateToInitial(this);
				        // Clear the active state
				        this.active = null;
				        
				        // Clear all path-related data from the provided
				        // March through the floor's "paths" double-array, and clear the path back-pointer
				        // Remove this back-pointer into the paths data-structure to allow the path to be fully recycled.
				        this.paths.forEach(function (pathSegArray) { pathSegArray.forEach(function (cell) { delete cell.path; }); });

				        // Clear the entire array (and let it be cleaned up)
				        this.paths.splice(0, this.paths.length); // Cleared!
				        // Clear the transport cells of their cached paths if any

				        if (this.transports)
				            this.transports.forEach(function (transOb) { delete transOb.transport.path; });

				        // If this is the starting floor, then setup the first path segement...
				        if (this.start) { // Insert the starting path as happens during init...
				            startPath(this.paths, this.start);
				            this.active = this.start;
				        }
				    }
				},
				toSCASerializable: {
				    enumerable: true,
				    value: function () {
				        return {
				            map: this.map,
				            render: this.render,
				            transports: this.transports,
				            paths: this.paths,
				            start: this.start,
				            end: this.end,
				            active: this.active
				        };
				    }
				}
			});
		}
	});

})(); // End MazeModelHelperScope