﻿"use strict";

// [Constructor(object serializedEdge),
//  Constructor(sequence<float> data, boolean floorBoundary, Cell fromCell, Cell toCell)]
// interface Edge {
//     readonly attribute sequence<float> data;
//     readonly attribute boolean floorBoundary;
//              attribute boolean locked;  // Being locked implies that there is a key somewhere that has locked this. Key items link to their related Edge.
//     Cell other(Cell refCell);
//     float angle(Cell refCell);
//   PRIVATE:
//     readonly attribute Cell _cell1;
//     readonly attribute Cell _cell2;
// };

(function ModelEdgeScope() {

    var TWOPI = Math.PI * 2;

    function calcAndCacheAngles(edge, cell1, cell2) {
        var xDiff = cell2.location[0] - cell1.location[0];
        var yDiff = cell2.location[1] - cell1.location[1];
        var angle = Math.atan2(yDiff, xDiff);
        if (angle < 0) // atan2 is between -PI and PI (negative values are thus converted to values between 0 and 2*PI)
            angle += TWOPI; // Logically: flip the negative angle over the y-axis by adding it to PI, then add it to PI again.
        edge._cell1To2Angle = angle;
        angle += Math.PI; // Rotate it by 180 for the opposite angle...
        if (angle > TWOPI)
            angle -= TWOPI;
        edge._cell2To1Angle = angle;
    }

    var EdgePrototype = Object.create(Object.prototype, {
        other: {
            enumerable: true,
            value: function other(refCell) {
                switch (refCell) {
                    case this._cell1: return this._cell2;
                    case this._cell2: return this._cell1;
                    default: throw new Error("refCell not found");
                }
            }
        },
        angle: {
            enumerable: true,
            value: function angle(refCell) {
                if (this._cell1To2Angle == -1) // initial value
                    calcAndCacheAngles(this, this._cell1, this._cell2);
                switch (refCell) {
                    case this._cell1: return this._cell1To2Angle;
                    case this._cell2: return this._cell2To1Angle;
                    default: throw new Error("refCell not found");
                }
            }
        }
    });

    Object.defineProperty(global, "Edge", {
        value: function EdgeConstructor(dataOrSerializedEdge, isFloorBoundary, fromCell, toCell) {
            var instance = this;
            var locked = false;
            var c12angle = -1;
            var c21angle = -1;
            if (!Array.isArray(dataOrSerializedEdge)) {
                instance = dataOrSerializedEdge;
                if (instance instanceof Edge)
                    return instance; // Quick exit (might be "upgraded" multiple times during de-serialization)
                if ((Object.keys(instance).length != 5) ||
                    !Array.isArray(instance.data) ||
                    (typeof instance.floorBoundary != "boolean") ||
                    (typeof instance.locked != "boolean") ||
                    !instance._cell1 || !instance._cell2)
                    throw new Error("Could not convert [serialized] object to an Edge");
                Object.setPrototypeOf(instance, EdgePrototype);
                dataOrSerializedEdge = instance.data;
                isFloorBoundary = instance.floorBoundary;
                locked = instance.locked;
                fromCell = instance._cell1;
                toCell = instance._cell2;
            }
            // hidden _cell1To2Angle & _cell2To1Angle
            return Object.defineProperties(instance, {
                data:          { enumerable: true, configurable: false, writable: false, value: dataOrSerializedEdge },
                floorBoundary: { enumerable: true, configurable: false, writable: false, value: isFloorBoundary },
                locked:        { enumerable: true, configurable: false, writable: true,  value: locked },
                _cell1:        { enumerable: true, configurable: false, writable: false, value: fromCell },
                _cell2:        { enumerable: true, configurable: false, writable: false, value: toCell },
                // enum=false below...(calculated on first use)
                _cell1To2Angle: { enumerable: false, configurable: false, writable: true, value: -1 },
                _cell2To1Angle: { enumerable: false, configurable: false, writable: true, value: -1 }
            });
        }
    });
    Object.defineProperty(Edge, "prototype", { value: EdgePrototype });
    Object.defineProperty(EdgePrototype, "constructor", { value: Edge });
})();