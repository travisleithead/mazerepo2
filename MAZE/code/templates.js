// Template processor

(function processExistingTemplates() {
	var templates = document.querySelectorAll("script[data-template]");
	var divParseCtx = document.createElement("div");
	for (var i = 0, len = templates.length; i < len; i++) {
		divParseCtx.innerHTML = templates[i].textContent;
		var df = document.createDocumentFragment();
		for (var c = 0, clen = divParseCtx.childNodes.length; c < clen; c++) {
			df.appendChild(divParseCtx.firstChild);
		}
		templates[i].template = df;
	}
})();


// Template element (script) utilities

HTMLScriptElement.prototype.clone = function() {
	return this.template.cloneNode(true);
}