﻿"use strict";

mazeapp.on("appinit", function oneTimeSetup() {

    var finishedPanelEl = document.getElementById("finished2");
    document.getElementById("gotoPlayAgainButton").addEventListener("click", onPlayAgainButtonPressed);
    document.getElementById("gotoNextButton").addEventListener("click", onNextButtonPressed);
    document.getElementById("gotoResumeButton").addEventListener("click", onResumeButtonPressed);

    mazeapp.on("mazeloaded", function updateFinishedPanel() {
        // Populate the level and maze numbers...
        document.getElementById("levelStatsNumValue").innerHTML = (mazeapp.currentLevel + 1);
        var mazeNum = mazeapp.maze.stats.gameNum;
        document.getElementById("mazeStatsNumValue").innerHTML = mazeNum;

        if (mazeNum == mazeapp.levels[mazeapp.currentLevel].mazes.length)
            // Last maze on this level!
            document.getElementById("commandNext").style.display = "none";
        else {
            document.getElementById("commandNext").style.display = ""; // Just to be sure...
            document.getElementById("nextMazeNum").innerHTML = (mazeNum + 1);
        }

        mazeapp.maze.stats.on("smallcheesenumchange", syncCheeseCount, document.getElementById("cheeseSection"));

        mazeapp.maze.on("pausedshow", prepareToShowPausedPanel, finishedPanelEl);
        mazeapp.maze.on("pausedhide", hidePausedPanel, finishedPanelEl);
    });

    mazeapp.on("mazeunloaded", function cleanupStuff() {
        mazeapp.maze.stats.off("smallcheesenumchange", syncCheeseCount);
    });

    function updateElapsedTimeUI() { document.getElementById("timeStatsValue").innerHTML = parseMinSec(mazeapp.maze.elapsedTime); }

    mazeapp.on("finishedshow", function prepareToShowFinishedPanel() {
        // 'this' is the finished section element.
        updateElapsedTimeUI();
        // Populate finished stats
        document.getElementById("finishedStatsBestValue").innerHTML = parseMinSec(mazeapp.maze.stats.timeFastest);
        document.getElementById("finishedStatsAveValue").innerHTML = parseMinSec(mazeapp.maze.stats.timeAverage);
        document.getElementById("finishedStatsSlowestValue").innerHTML = parseMinSec(mazeapp.maze.stats.timeSlowest);
        document.getElementById("finishedStatsNumPlayedValue").innerHTML = mazeapp.maze.stats.completedCount;
        // Hookup controllers -- the "Menu" button has different functions at finishedshow than at pausedshow time...
        document.getElementById("gotoMenuButton").addEventListener("click", onMenuButtonPressed);
        // Make the finished panel appear...
        this.setAttribute("data-finished", "");
        this.removeAttribute("hidden");
        // Maze is still loaded (until a navigation will occur
    }, finishedPanelEl);

    mazeapp.on("finishedhide", function hideFinishedPanel() {
        this.setAttribute("data-exit", "");
        this.addEventListener("animationend", function cleanupAnimation(e) {
            // While the following handler is added/active, with multiple animations overlapping, it's possible that another animation ending (like the app-commands) will trigger this handler!
            if (e.eventPhase != Event.AT_TARGET) return; // Ignore this "other" animation
            e.target.removeEventListener("animationend", cleanupAnimation);
            e.target.removeAttribute("data-finished");
            e.target.removeAttribute("data-exit");
            e.target.setAttribute("hidden", "");
            // State is now back to square one.
            // Remove controllers
            document.getElementById("gotoMenuButton").removeEventListener("click", onMenuButtonPressed);
        });
    }, finishedPanelEl);

    // 'this' is the finished panel element
    function prepareToShowPausedPanel(modelEvent) {
        var promise = modelEvent.getPromise();
        updateElapsedTimeUI();
        // Populate the percentage complete...
        document.getElementById("progressStatsValue").innerHTML = "??%";
        // Setup handler for the menu element
        document.getElementById("gotoMenuButton").addEventListener("click", gotoMainMenuFromPaused);
        this.removeAttribute("hidden");
        this.addEventListener("animationend", function onanimationend(e) {
            if (e.eventPhase != Event.AT_TARGET) return;
            e.target.removeEventListener("animationend", onanimationend);
            // Now fully visible
            promise.done();
        });
    }

    function hidePausedPanel(modelEvent) {
        var promise = modelEvent.getPromise();
        // This handler will not exist outside of the paused state...
        document.getElementById("gotoMenuButton").removeEventListener("click", gotoMainMenuFromPaused);
        // Animate the panel out...
        this.setAttribute("data-exit2", "");
        this.addEventListener("animationend", function cleanupAnimation(e) {
            if (e.eventPhase != Event.AT_TARGET) return;
            e.target.removeEventListener("animationend", cleanupAnimation);
            e.target.removeAttribute("data-exit2");
            e.target.setAttribute("hidden", "");
            // Now fully hidden
            promise.done();
        });
    }

    // This ensures that if the game is unloaded while the paused screen is up (which can happen during a reset()), that the puased screen is torn down.
    mazeapp.on("mazeunloaded", function ensureClosureOfPausedPanel(modelEvent) {
        if (mazeapp.maze.viewPaused)
            hidePausedPanel.call(this, modelEvent);
    }, finishedPanelEl);

    function syncCheeseCount() {
        // 'this' is the cheeseSection element . TODO
    }

    // Navigation Button Handlers ----

    function onResumeButtonPressed() { mazeapp.maze.viewPaused = false; }

    function onMenuButtonPressed(e) {
        // Done with the maze now...
        mazeapp.unloadMaze(function continueToMenu() {
            mazeapp.activeMaze = null; // Clear the active maze (and associated UI state)
            mazeapp.viewLevels = true;
        });
    }
    
    function onPlayAgainButtonPressed(e) {
        mazeapp.unloadMaze(function continueToPlayAgain() {
            // Uses the same "currentLevelIndex" and "activeMaze" index.
            mazeapp.loadMaze();
        });
    }

    function onNextButtonPressed(e) {
        mazeapp.unloadMaze(function continueToSwitchActiveMaze() {
            // Maze is unloaded... now switch the activeMaze to the next level (Note--can't press this button if this is the LAST level)
            mazeapp.levels[mazeapp.currentLevel].mazes[mazeapp.activeMaze + 1].on("activated", function continueToNextMaze() {
                mazeapp.levels[mazeapp.currentLevel].mazes[mazeapp.activeMaze].off("activated", continueToNextMaze);
                mazeapp.loadMaze();
            });
            mazeapp.activeMaze++;
        });
    }

    function gotoMainMenuFromPaused(e) {
        onMenuButtonPressed(); // For now, these have the same behavior: TODO see if I can simplify after the "are you sure" blocks are in place.
    }
});