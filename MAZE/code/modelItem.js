﻿"use strict";

// [Constructor(serializedItem),
//  Constructor(Classification classifier, unsigned long variant, Edge? relatedTarget)]
// interface Item {
//    readonly attribute Classification classifier;
//    readonly attribute unsigned long  variant; // usage depends on the class
//             attribute boolean        collected;
//    readonly attribute Edge?          relatedTarget;
//    void activate();
// };
// enum Classification = { "cheese", "key", "trap", "switch", "transport", "earthquake" }

(function ModelItemScope() {
    // Cell can hold (1) of these:
    // classifier hasVariants relatedLink Notes
    // ========== =========== =========== ======
    // cheese     yes         none
    // key        yes         Edge        Keeps the edge locked until key is activated
    // trap       ?           none        Holds the actor (some actors?)
    // switch     ?           ?           Lights on/off
    // transport  yes         Edge        unique Edge, not part of any path/border segment list
    // earthquake no          none        maze re-arrange
    
    var ItemPrototype = Object.create(Object.prototype, {
        activate: {
            enumerable: true,
            value: function activate() {
                // TODO.
            }
        }
    });

    Object.defineProperty(window, "Item", {
        value: function Item(classOrSerializedItem, variant, relatedTarget) {
            var instance = this;
            var isCollected = false;
            if (typeof classOrSerializedItem != "string") {
                instance = classOrSerializedItem;
                if ((Object.keys(instance).length != 4) ||
                    (typeof instance.classifier != "string") ||
                    (typeof instance.variant != "number") ||
                    (typeof instance.collected != "boolean") ||
                    ((instance.relatedTarget != null) && (!instance.relatedTarget)))
                    throw new Error("Could not convert [serialized] object to an Item");
                Object.setPrototypeOf(instance, ItemPrototype);
                classOrSerializedItem = instance.classifier;
                variant = instance.variant;
                isCollected = instance.collected;
                relatedTarget = instance.relatedTarget;
            }
            return Object.defineProperties(instance, {
                classifier:    { enumerable: true, configurable: false, writable: false, value: classOrSerializedItem },
                variant:       { enumerable: true, configurable: false, writable: false, value: variant },
                collected:     { enumerable: true, configurable: false, writable: true,  value: isCollected },
                relatedTarget: { enumerable: true, configurable: false, writable: false, value: relatedTarget },
            });
        }
    });
    Object.defineProperty(Item, "prototype", { value: ItemPrototype });
    Object.defineProperty(ItemPrototype, "constructor", { value: Item });
})();