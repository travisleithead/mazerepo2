"use strict";
// UI controller for the level select section

mazeapp.on("appinit", function levelScreenController() {
    // Interval between maze tile creation
    var MAZETILEINSERTIONINTERVAL = 50; // miliseconds	

    // Tracks async maze tile creation timers
    var asyncCurrentLevelChange = null;

    // (Snap view) explicit level navigation controls (always active)
    function onSelectNextLevel(e) { e.target.parentNode.nextElementSibling.lastElementChild.click(); }
    function onSelectPrevLevel(e) { e.target.parentNode.previousElementSibling.lastElementChild.click(); }

    document.querySelectorAll(".levelTileDimensions .levelNav").forEach(function (levelNavEl) {
        if (levelNavEl.classList.contains("selectNextLevel"))
            levelNavEl.onclick = onSelectNextLevel;
        else if (levelNavEl.classList.contains("selectPrevLevel"))
            levelNavEl.onclick = onSelectPrevLevel;
        // If neither, then skip adding a handler, if both, then undefined (next is added)
    });

    // Active maze-tile light-dismiss (always active)
    document.getElementById("tileLayout").addEventListener("click", function onCancelActiveMaze(e) {
        mazeapp.activeMaze = null;
        e.stopPropagation();
    });

    // Handle show settings button
    document.getElementById("gotoSettingsButton").addEventListener("click", function onPressShowSettings(e) {
        mazeapp.viewSettings = true;
    });

    // Handle unlock scenario
    document.getElementById("gotoStoreButton").addEventListener("click", function onPressUnlockButton(e) {
        // TODO: Load this from the Windows Store, and use mazeEvent.getPromise() if this is async.
        mazeapp.payingCustomer = true;
    });



    ///////////////////////////////////////////////////////////////////
    // Multiple-use functions
    ///////////////////////////////////////////////////////////////////

    // Clears existing maze tiles, and optionally stops in-progress construction
    function onUnselectLevel(e) {
        // De-select this level
        this.removeAttribute("data-selected");
        // If this level was still building out its levels (async), but is now 
        // being unselected, stop the construction process
        if (asyncCurrentLevelChange) {
            clearTimeout(asyncCurrentLevelChange);
            asyncCurrentLevelChange = null;
        }
        // Clear the level tiles
        document.getElementById("tileLayout").innerHTML = "";
    }

    // Builds the maze tiles and attached appropriate handlers
    function onSelectLevelAndAsyncConstructMazeTiles(e) {
        // First ensure that the related level element is marked as selected
        this.setAttribute("data-selected", "");
        var levelModel = mazeapp.levels[mazeapp.currentLevel];
        if (levelModel.mazes.length == 0)
            return; // Nothing to build ;)
        asyncCurrentLevelChange = setTimeout(function createMazeTile(i) {
            var mazeModel = levelModel.mazes[i];
            // Get the template...
            var template = document.getElementById("template-gametile").clone();
            // Customize the game number
            template.querySelector(".frontface p span.gameActionLook").innerHTML = mazeModel.gameNum;
            // Customize the game name
            var titleElement = template.querySelector(".frontface .gameTileTitle");
            titleElement.locText = mazeModel.gameNameLocId; // So that a change in language can swap this string out without re-building the element
            // Customize the cheese-count
            template.querySelectorAll(".frontface .gameTileInfo > span").forEach(function (cheeseSpan, i) {
                cheeseSpan.className = (i < mazeModel.smallCheesesAcquired) ? "cheese" : "nocheese";
            });
            // Use the right localization strings for the Play/Print labels
            template.querySelector(".backface [data-res=Play]").locText = "Play";
            //template.querySelector(".backface [data-res=Print]").locText = "Print"; // TODO: For future printing support.

            // Add the front-face click-action handler
            var frontFace = template.querySelector(".frontface .gameFrontFaceAction");
            frontFace.onclick = onSelectMazeTile;
            frontFace.__mazeIndex = i;
            // Add the game-start click-action handler
            template.querySelector(".backface .play").onclick = function onPlayGame(e) {
                mazeapp.loadMaze();
                e.stopPropagation();
            };
            // Add this tile into the view (causing an animation to trigger...
            document.getElementById("tileLayout").appendChild(template);
            i++; // Move on to the next one.
            // If there are more tiles, asynchronously load them next...
            if (i < levelModel.mazes.length)
                asyncCurrentLevelChange = setTimeout(createMazeTile, MAZETILEINSERTIONINTERVAL, i);
            else
                asyncCurrentLevelChange = null;
        }, MAZETILEINSERTIONINTERVAL, 0);
    }

    // The model reports that a particular maze tile is activated
    // (this-value will be the maze tile being activated)
    function onMazeTileActivated(modelEvent) {
        // Flip the maze tile over
        this.setAttribute("data-flip", "");
        // Fill in the current fastest/average/slowest values before showing the status bar
        var statusContainer = document.getElementById("selectedTileStats");
        var containerValues = statusContainer.querySelectorAll("#selectedTileStats > div > span:nth-child(2)");
        containerValues[0].innerHTML = parseMinSec(modelEvent.target.timeFastest);
        containerValues[1].innerHTML = parseMinSec(modelEvent.target.timeAverage);
        containerValues[2].innerHTML = parseMinSec(modelEvent.target.timeSlowest);
        statusContainer.setAttribute("data-show", "");
    }

    // the model reports that this maze tile is deactivated
    // (this-value will be the maze tile being de-activated)
    function onMazeTileDeactivated(modelEvent) {
        // Clear the previously-activated maze tile
        this.removeAttribute("data-flip");
        // Clear the status bar animation
        document.querySelector("#selectedTileStats[data-show]").removeAttribute("data-show");
        // Remove the handlers originally added to the model (to avoid keeping this element rooted to the model)
        modelEvent.target.off("activated", onMazeTileActivated);
        modelEvent.target.off("deactivated", onMazeTileDeactivated);
    }

    // User selects a maze tile element--tell the model which one.
    function onSelectMazeTile(e) {
        // To avoid complex de-registration calculations, each maze tile only registers for it's view updates
        // immediately before telling the model that it becomes active. On de-activation, which is guaranteed
        // to happen immedialy before a level is changed causing teardown of the maze tiles, the maze can un-register
        // itself.
        var mazeModelOb = mazeapp.levels[mazeapp.currentLevel].mazes[e.target.__mazeIndex];
        mazeModelOb.on("activated", onMazeTileActivated, e.target.parentNode.parentNode /* .gametile */);
        mazeModelOb.on("deactivated", onMazeTileDeactivated, e.target.parentNode.parentNode /* .gametile */);

        // Update the model to refer to this maze as the active maze
        mazeapp.activeMaze = e.target.__mazeIndex;
        e.stopPropagation();
    }

    ///////////////////////////////////////////////////////
    // MODEL INTEGRATION/STIMULUS
    ///////////////////////////////////////////////////////

    var levelSelectRootEl = document.querySelector("#levelSelectPanel2");
    var statsRootEl = document.querySelector("#selectedTileStats");

    mazeapp.on("levelselectshow", function onLevelSelectShow() {
        this.removeAttribute("hidden");
        // Kick-off the process of building the mazes for the selected level
        onSelectLevelAndAsyncConstructMazeTiles.call(document.querySelectorAll(".levelTileDimensions")[mazeapp.currentLevel]);
    }, levelSelectRootEl);

    mazeapp.on("levelselecthide", function onLevelSelectHide() {
        this.setAttribute("hidden", "");
        // Clear-out the mazes on view-hide (to re-show next visibility cycle)
        onUnselectLevel.call(document.querySelectorAll(".levelTileDimensions")[mazeapp.currentLevel]);
    }, levelSelectRootEl);

    mazeapp.on("appreinit", function onResetTearDownAndClearData(modelEvent) {
        onUnselectLevel.call(this[mazeapp.currentLevel]);
    }, document.querySelectorAll(".levelTileDimensions")); // Shove a StaticNodeList as the 'this' reference element :)

    // This section requires the levels array to be ready.
    mazeapp.on("levelsloaded", function initLevelScreen() {
        // Manage the click handlers (and locked status of the same) on 
        // the static level selection element blocks
        document.querySelectorAll(".levelTileDimensions .levelTileAction").forEach(function (levelActivateEl, i) {
            if (!mazeapp.levels[i].defaultLocked || mazeapp.payingCustomer) { // Paying customers always get all the levels.
                levelActivateEl.onclick = (function closureForCurrentLevel(myLevelIndex) { // If this is re-played by a level-reset, there is no leak because the event handler is replaced by the new one, allowing GC of the old handler.
                    return function onActivateLevel(e) {
                        mazeapp.currentLevel = myLevelIndex;
                    };
                })(i); // Generate an instance of the click action handler for each level in the view
            }
            else { // Not a paying customer and locked by default...
                levelActivateEl.onclick = null; // Clear this out (if it was set previously by unlocking and then the app data was reset, including the payingcustomer bit--which shouldn't occur in production code)
                levelActivateEl.parentNode.setAttribute("data-locked", ""); // If it doesn't get a handler, then it's locked
                // and it's previous level (if any) should not link to it...
                if (i > 0)
                    document.querySelectorAll(".levelTileDimensions .levelNav")[(i * 2) - 1].classList.remove("selectNextLevel"); // i*2 accounts for 2 levelNav's per level, and -1 gets me to the previous' "next level" indicator.
            }
        });

        // When the model says that a level is selected, show the appropriate mazes
        document.querySelectorAll(".levelTileDimensions").forEach(function (levelEl, i) {
            mazeapp.levels[i].on("selected", onSelectLevelAndAsyncConstructMazeTiles, levelEl);
            mazeapp.levels[i].on("unselected", onUnselectLevel, levelEl);
        });

        // Adjust the count of solved maze tiles (on the level elements) when the count changes
        document.querySelectorAll(".levelTileDimensions .levelTileInfo > span:first-child").forEach(function (solvedCountElem, i) {
            // First, update the solved Maze Count on the level elements based on the recently-loaded model
            if (mazeapp.levels[i].solvedMazeCount > 0)
                solvedCountElem.innerHTML = mazeapp.levels[i].solvedMazeCount;
            // Second, respond to any future dynamic changes...
            mazeapp.levels[i].on("solvedmazecountchange", function onadjustsolvedmazecount(levelModel) {
                // Update the specific level indicator which displays the total number of mazes complete
                this.innerHTML = levelModel.target.solvedMazeCount;
            }, solvedCountElem);
        });

        // If the solved maze count drops to zero, then remove the data-partialsolved attribute on the related level element.
        document.querySelectorAll(".levelTileDimensions .levelDescription").forEach(function (levelElem, i) {
            // First, update the data-partialsolved attribute on the level element appropriately from the freshly loaded level data
            if (mazeapp.levels[i].solvedMazeCount > 0)
                levelElem.setAttribute("data-partialsolved", "");
            else // Reset may leave previously partially-solved status present in the UI, even though the new data model has it zero-d out.
                levelElem.removeAttribute("data-partialsolved");
            // Second, repond to any dynamic changes...
            mazeapp.levels[i].on("solvedmazecountchange", function onShowOrHidePartialSolvedMazes(levelModel) {
                if (levelModel.target.solvedMazeCount === 1) // It must have just previously been zero...
                    this.setAttribute("data-partialsolved", "");
            }, levelElem);
        });
    });
});

mazeapp.on("unlock", function onunlock() {
    // This is only ever fired once, but I don't know if it will be before of after the levels have been initialized.
    // Add handlers to the remaining defaultLocked levels (making them available to click on)
    if (mazeapp.levels.length > 0) {
        // Add the handlers to the levels that didn't have it before.
        document.querySelectorAll(".levelTileDimensions .levelTileAction").forEach(function (levelActivateEl, i) {
            if (mazeapp.levels[i].defaultLocked) { // Unlock the locked levels...
                levelActivateEl.onclick = (function closureForCurrentLevel(myLevelIndex) { // If this is re-played by a level-reset, there is no leak because the event handler is replaced by the new one, allowing GC of the old handler.
                    return function onActivateLevel(e) {
                        mazeapp.currentLevel = myLevelIndex;
                    };
                })(i); // Generate an instance of the click action handler for each level in the view
                // Restore the "selectNextLevel" removed by default if this comes after levelsloaded
                if (i > 0)
                    document.querySelectorAll(".levelTileDimensions .levelNav")[(i*2)-1].classList.add("selectNextLevel");
            }
        });
    }
    // otherwise, when the levels are loaded, the handlers will be added based on the updated state of the payingCustomer flag.

    // Remove the indicators from all other existing locked levels + appbar purchase handler
    document.querySelectorAll("[data-locked]").forEach(function (target) {
        target.removeAttribute("data-locked");
    });
});

