"use strict";

// JSBinaryUtil static global object with two methods:
// * parse ( format, data )
// * serialize (format, data )

// Uses 'Set' from ES6 (add/has/clear APIs)
// Polyfill here for Set (if necessary)

if (!window.Set) {
   (function SetPolyfillScope() {
      function scan(instance, forThing) {
         // Brute-force (read: 'easy') linear scan
         for (var i = 0, len = instance.__size; i < len; i++) {
            if (instance["__" + i] === forThing)
               return forThing;
         }
         return null;
      }
      var SetPrototype = Object.defineProperties({}, {
         add: {
            enumerable: true,
            writable: true,
            configurable: true,
            value: function Set_add (x) {
               if (scan(this, x) == null) {
                  Object.defineProperty(this, "__" + this.__size, { configurable: true, value: x });
                  this.__size++;
               }
            }
         },
         has: {
            enumerable: true,
            writable: true,
            configurable: true,
            value: function Set_has (y) {
               if (scan(this, y) == null)
                  return false;
               return true;
            }
         },
         clear: {
            enumerable: true,
            writable: true,
            configurable: true,
            value: function Set_clear () {
               for (var i = 0, len = this.__size; i < len; i++) {
                  delete this["__" + i];
                  this.__size = 0;
               }
            }
         },
         size: {
            enumerable: true,
            configurable: true,
            get: function Set_size_getter () {
               return this.__size;
            }
         }
      });
      Object.defineProperty(window, "Set", {
         writable: true,
         configurable: true,
         value: function SetConstructor() {
            return Object.create(SetPrototype, {
               __size: {  
                  writable: true,
                  value: 0
               }
            });
         }
      });
   })();
}


(function SetupBinarySerializer() {
   var nonCyclicalFormat = new Set();
   
   var SimpleNumberFormat = { "int8":1,"uint8":1,"int16":2,"uint16":2,"int32":4,"uint32":4,"float32":4,"float64":8 };
   var SimpleNullableNumberFormat = { "int8?":1,"uint8?":1,"int16?":1,"uint16?":1,"int32?":1,"uint32?":1,"float32?":1,"float64?":1 };
   var SimpleBooleanFormat = { "bool":1 };
   var SimpleNullableBooleanFormat = { "bool?":1 };
   var SimpleStringFormat = { "ascii":1 };
   var SimpleNullableStringFormat = { "ascii?":1 };
   var UserDefinedFormat = {};
   
   // Formats are strings, objects or arrays (nothing else)
   function getFormatType(format, isPreValidated) {
      var type;
      if (typeof format == "string") {
         if (SimpleNumberFormat[format])
            type = "number";
         else if (SimpleBooleanFormat[format])
            type = "bool";
         else if (SimpleStringFormat[format])
            type = "string";
         else if (SimpleNullableNumberFormat[format])
            type = "number?";
         else if (SimpleNullableBooleanFormat[format])
            type = "bool?";
         else if (SimpleNullableStringFormat[format])
            type = "string?";
         else if (UserDefinedFormat[format])
            type = "user";
         else
            throw new TypeError("Unknown format provided");
      }
      else if ((typeof format == "object") && (Array.isArray(format))) {
         if (!isPreValidated) {
            if (typeof format.length != "number")
               throw new TypeError("Array format must have a length value");
            if ((format.length < 0) && (format.length > 2))
               throw new TypeError("Array format must have at most one inner format defined");
         }
         type = "list" + appendNullableFormatFlag(format, format[1], isPreValidated);
      }
      else if ((typeof format == "object") && (format != null)) {
         if (!isPreValidated) {
            // Note: potential problem if format.defineAs has the value undefined (and the object's defineAs key is defined) - ignoring this case.
            var keyExclusionCount = (typeof format.defineAs != "undefined") ? 1 : 0;
            if (Array.isArray(format.keyOrder)) {
               keyExclusionCount++;
               if (format.keyOrder.length != (Object.keys(format).length - keyExclusionCount))
                  throw new TypeError("Object format's 'keyOrder' list does not match the number of enumerable keys on the format");
               // Check that all keynames are accounted for in the keyOrder array
               for (var a = 0, len = format.keyOrder.length; a < len; a++) {
                  if (typeof format[format.keyOrder[a]] == "undefined")
                     throw new TypeError("Mismatch between object format's 'keyOrder' list and object's property keys");              
               }
            }
            else if (Object.keys(format).length > (keyExclusionCount + 1))
               throw new TypeError("Object formats with more than one property require a 'keyOrder' property with an array value to list the processing order of the key names");
         }
         type = "object" + appendNullableFormatFlag(format, format.defineAs, isPreValidated);
      }
      else
         throw new TypeError("Unknown format provided: expected a string, array or object");
      if (!isPreValidated && ((type == "list") || (type == "object") || (type == "list?") || (type == "object?"))) {
         if (nonCyclicalFormat.has(format))
            throw new TypeError("Cyclical formats cannot be processed");
         nonCyclicalFormat.add(format);
      }
      return type;
   }
   
   function appendNullableFormatFlag(format, objectFlag, preValidated) {
      // objectFlag is "?", "user-type", or "user-type?" (first and last are nullable object types)
      var objectFlagType = typeof objectFlag;
      if (objectFlagType == "string") {
         if (objectFlag == "?")
            return "?";
         if (!preValidated) {
            // The user-defined format can't match any of the existing simple formats
            if (SimpleNumberFormat[objectFlag] || SimpleBooleanFormat[objectFlag] ||
                SimpleStringFormat[objectFlag] || SimpleNullableNumberFormat[objectFlag] ||
                SimpleNullableBooleanFormat[objectFlag] || SimpleNullableStringFormat[objectFlag] ||
                UserDefinedFormat[objectFlag])
                throw new TypeError("Array/Object's format definition value cannot be an existing format");
            UserDefinedFormat[objectFlag] = format;
            // Note: "def" and "def?" are considered completely separate definitions.
         }
         if (/\?$/.test(objectFlag))
            return "?";
         return "";
      }
      else if (objectFlagType == "undefined")
         return "";
      else
         throw new TypeError("Array or object definition value must be a string");
   }
   
   function prepare(userFormat, userData, sizeCollector, valueCollector, preValidated) {
      switch (getFormatType(userFormat, preValidated)) {
         case "number": return recordNumber(userFormat, userData, false, sizeCollector, valueCollector);
         case "number?": return recordNumber(userFormat.substring(0, userFormat.length - 1), userData, true, sizeCollector, valueCollector);
         case "bool": return recordBool(userData, false, sizeCollector, valueCollector);
         case "bool?": return recordBool(userData, true, sizeCollector, valueCollector);
         case "string": return recordString(userFormat, userData, false, sizeCollector, valueCollector);
         case "string?": return recordString(userFormat, userData, true, sizeCollector, valueCollector);
         case "list": return recordList(userFormat, userData, false, sizeCollector, valueCollector, preValidated);
         case "list?": return recordList(userFormat, userData, true, sizeCollector, valueCollector, preValidated);
         case "object": return recordObject(userFormat, userData, false, sizeCollector, valueCollector, preValidated);
         case "object?": return recordObject(userFormat, userData, true, sizeCollector, valueCollector, preValidated);
         case "user": return prepare(UserDefinedFormat[userFormat], userData, sizeCollector, valueCollector, true);
         default: throw new TypeError("Unhandled format type encountered");
      }
   }
   
   function recordNumber(format, userData, isNullable, sizeCollector, valueCollector) {
      if (isNullable && recordNullablePrereqs(userData, sizeCollector, valueCollector))
         return 1;
      if (typeof userData != "number")
         throw new Error("Value '" + userData + "' is not a numerical type (or not nullable)");
      // Check for overflow of a value into a format...
      if (((format != "float32") && (format != "float64")) && ((userData >= typeMaxSize[format]) || (userData < typeMinSize[format])))
         throw new Error("Numerical value: " + userData + " falls outside the range of an " + format + " slot");
      //else
      //   console.warn("Use of float32 types can result in unexpected loss of precise decimal values when round-tripping data");
      sizeCollector.push(format);
      valueCollector.push(userData)
      return (isNullable ? 1 : 0) + SimpleNumberFormat[format];
   }
   
   function recordNullablePrereqs(userData, sizeCollector, valueCollector) {
      // It costs an extra byte to be nullable...
      sizeCollector.push("uint8");
      if (userData === null) {
         valueCollector.push(Flag.nullableIsNull)
         return true; // Meaning the caller should return 1.
      }
      valueCollector.push(Flag.nullableNotNull);
      return false;
   }
   
   var Flag = { nullableIsNull: 1, nullableNotNull: 2,
      // for booleans (pack values into the flags to conserve a byte)
      booleanTrue: 3, booleanFalse: 4
   };
   
   var typeMaxSize = {
      int8:    128,
      uint8:   256,
      int16:   32768,
      uint16:  65536,
      int32:   2147483648,
      uint32:  4294967296,
   };
   
   var typeMinSize = {
      int8:    -128,
      uint8:   0,
      int16:   -32768,
      uint16:  0,
      int32:   -2147483648,
      uint32:  0,
   };
   
   function recordBool(userData, isNullable, sizeCollector, valueCollector) {
      // booleans only use one byte, even for nullable types
      sizeCollector.push("uint8");
      if (isNullable && (userData === null)) {
         valueCollector.push(Flag.nullableIsNull);
         return 1;
      }
      if (typeof userData != "boolean")
         throw new Error("Value '" + userData + "' is not a boolean type (or not nullable)");
      valueCollector.push(userData ? Flag.booleanTrue : Flag.booleanFalse);
      return 1;
   }
   
   function recordString(format, userData, isNullable, sizeCollector, valueCollector) {
      if (isNullable && recordNullablePrereqs(userData, sizeCollector, valueCollector))
         return 1;
      if (typeof userData != "string")
         throw new Error("Value '" + userData + "' is not a string type (or not nullable)");
      var userDataLength = userData.length; // One read only
      var lengthBytes = recordSequencePrereqs(userDataLength, sizeCollector, valueCollector);
      // character bytes
      if ((format == "ascii") || (format == "ascii?")) {
         // single byte character values only
         for (var a = 0; a < userDataLength; a++) {
            var code = userData[a].charCodeAt(0);       
            if (code >= typeMaxSize["uint8"])
               throw new Error("Character encountered outside the range of allowable ASCII range: '" + userData[a] + "'");
            sizeCollector.push("uint8");
            valueCollector.push(code);
         }
         return (isNullable ? 1 : 0) + lengthBytes + userDataLength;
      }
      throw new TypeError("String format not implemented");
   }
   
   function recordSequencePrereqs(arrayLength, sizeCollector, valueCollector) {
      // length-width byte (how many bytes does it take to store the length)
      sizeCollector.push("uint8");
      var lengthType = lengthValueToType(arrayLength);
      var lengthByteCount = SimpleNumberFormat[lengthType];
      valueCollector.push(lengthByteCount); // 1,2,4, or 8
      // actual length
      sizeCollector.push(lengthType);
      valueCollector.push(arrayLength);
      return lengthByteCount + 1;
   }
   
   function lengthValueToType(length) {
      if (length < typeMaxSize["uint8"])
         return "uint8";
      if (length < typeMaxSize["uint16"])
         return "uint16";
      if (length < typeMaxSize["uint32"])
         return "uint32";
      return "float64";
   }
   
   function recordList(format, userData, isNullable, sizeCollector, valueCollector, isFormatPreValidated) {
      if (isNullable && recordNullablePrereqs(userData, sizeCollector, valueCollector))
         return 1;
      if (!userData || (typeof userData != "object"))
         throw new Error("Expected an array or array-like object--a suitable object was not provided (or not nullable)");
      var userDataLength = getProperty("length", userData);
      if (typeof userDataLength != "number")
         throw new Error("Array or array-like object did not include a 'length' numerical value");
      if (userDataLength < 0)
         throw new Error("'length' must be 0 or greater. Got: '"+userDataLength+"'");
      var lengthByteCount = (isNullable ? 1 : 0) + recordSequencePrereqs(userDataLength, sizeCollector, valueCollector);
      if ((userDataLength > 0) && (format.length == 0))
         throw new Error("Array format must have an inner format defined when the provided array data is non-empty");
      for (var a = 0, innerFormat = format[0]; a < userDataLength; a++) // May not record anything if length is 0
         lengthByteCount += prepare(innerFormat, userData[a], sizeCollector, valueCollector, (a > 0) || isFormatPreValidated);
      return lengthByteCount;
   }
   
   function getProperty(propertyName, userData) {
      var res = userData[propertyName]; // 1 and only 1 get request for the value;
      if (typeof res == "undefined")
         throw new TypeError("Expected property named '" + propertyName + "' was not found on '" + userData + "'");
      return res;
   }
   
   function recordObject(format, userData, isNullable, sizeCollector, valueCollector, isFormatPreValidated) {
      if (isNullable && recordNullablePrereqs(userData, sizeCollector, valueCollector))
         return 1;
      if (!userData || (typeof userData != "object"))
         throw new Error("Value '" + userData + "' is not an object (or not nullable)");
      var propByteSum = isNullable ? 1 : 0; // If I got here while nullable, I wrote an extra byte previously.
      if (format.keyOrder) {
         for (var a = 0, len = format.keyOrder.length; a < len; a++)
            propByteSum += prepare(format[format.keyOrder[a]], getProperty(format.keyOrder[a], userData), sizeCollector, valueCollector, isFormatPreValidated);
      }
      else { // There is zero, one, or two keys...(empty, single key, single key + defineAs key)
         for (var a = 0, keyList = Object.keys(format), len = keyList.length; a < len; a++) {
            if (keyList[a] != "defineAs")
               propByteSum += prepare(format[keyList[a]], getProperty(keyList[a], userData), sizeCollector, valueCollector, isFormatPreValidated);
         }
      }
      return propByteSum;
   }
   
   function fillDataView(dataView, typesArray, valuesArray) {
      var bytePos = 0;
      for (var a = 0, len = typesArray.length; a < len; a++) {
         var type = typesArray[a];
         switch (type) {
            case "int8"   : dataView.setInt8(bytePos, valuesArray[a]); break;
            case "uint8"  : dataView.setUint8(bytePos, valuesArray[a]); break;
            case "int16"  : dataView.setInt16(bytePos, valuesArray[a]); break;
            case "uint16" : dataView.setUint16(bytePos, valuesArray[a]); break;
            case "int32"  : dataView.setInt32(bytePos, valuesArray[a]); break;
            case "uint32" : dataView.setUint32(bytePos, valuesArray[a]); break;
            case "float32": dataView.setFloat32(bytePos, valuesArray[a]); break;
            case "float64": dataView.setFloat64(bytePos, valuesArray[a]); break;
            default: throw new TypeError("Ooops. Unspecified/unknown type: " + type);
         }
         bytePos += SimpleNumberFormat[type];
      }
      if (bytePos != dataView.byteLength)
         throw new TypeError("Miscalculated arraybuffer size!");
   }
   
   /// (above) Serializer
   /// (below) Parser
   
   var globalReadPosition = 0;
   
   function read(userFormat, dataView, preValidated) {
      switch (getFormatType(userFormat, preValidated)) {
         case "number": return readNumber(userFormat, dataView, false);
         case "number?": return readNumber(userFormat.substring(0, userFormat.length - 1), dataView, true);
         case "bool": return readBool(dataView, false);
         case "bool?": return readBool(dataView, true);
         case "string": return readString(userFormat, dataView, false);
         case "string?": return readString(userFormat, dataView, true);
         case "list": return readList(userFormat, dataView, false, preValidated);
         case "list?": return readList(userFormat, dataView, true, preValidated);
         case "object": return readObject(userFormat, dataView, false, preValidated);
         case "object?": return readObject(userFormat, dataView, true, preValidated);
         case "user": return read(UserDefinedFormat[userFormat], dataView, true);
         default: throw new TypeError("Unhandled format type encountered");
      }
   }
   
   function readNumber(format, dataView, isNullable) {
      if (isNullable && (Flag.nullableIsNull == getValue("uint8", dataView)))
         return null;
      return getValue(format, dataView);
   }
   
   function getValue(type, dataView) {
      var v;
      switch (type) {
         case "int8"   : v = dataView.getInt8(globalReadPosition); break;
         case "uint8"  : v = dataView.getUint8(globalReadPosition); break;
         case "int16"  : v = dataView.getInt16(globalReadPosition); break;
         case "uint16" : v = dataView.getUint16(globalReadPosition); break;
         case "int32"  : v = dataView.getInt32(globalReadPosition); break;
         case "uint32" : v = dataView.getUint32(globalReadPosition); break;
         case "float32": v = dataView.getFloat32(globalReadPosition); break;
         case "float64": v = dataView.getFloat64(globalReadPosition); break;
      }
      globalReadPosition += SimpleNumberFormat[type];
      return v;
   }
   
   function readString(format, dataView, isNullable) {
      if (isNullable && (Flag.nullableIsNull == getValue("uint8", dataView)))
         return null;
      var str = "";
      if ((format == "ascii") || (format == "ascii?")) {
         for (var a = 0, len = getSequenceLength(dataView); a < len; a++) {
            str += String.fromCharCode(getValue("uint8", dataView));
         }
      }
      return str;
   }
   
   function getSequenceLength(dataView) {
      var type;
      switch (getValue("uint8", dataView)) {
         case 1: type = "uint8"; break;
         case 2: type = "uint16"; break;
         case 4: type = "uint32"; break;
         case 8: type = "float64"; break;
         default: throw new Error("Unexpected byte encountered in byte stream (expected sequence width flag)");
      }
      return getValue(type, dataView);
   }
   
   function readBool(dataView, isNullable) {
      switch (getValue("uint8", dataView)) {
         case Flag.nullableIsNull: return null;
         case Flag.booleanTrue: return true;
         case Flag.booleanFalse: return false;
         default: throw new Error("Unexpected byte encountered in byte stream (expected boolean flag)");
      }
   }
   
   function readList(format, dataView, isNullable, isFormatPreValidated) {
      if (isNullable && (Flag.nullableIsNull == getValue("uint8", dataView)))
         return null;
      var array = [];
      for (var a = 0, len = getSequenceLength(dataView); a < len; a++)
         array.push(read(format[0], dataView, (a > 0) || isFormatPreValidated));
      return array;
   }
   
   function readObject(format, dataView, isNullable, isFormatPreValidated) {
      if (isNullable && (Flag.nullableIsNull == getValue("uint8", dataView)))
         return null;
      var object = {};
      if (format.keyOrder) {
         for (var a = 0, len = format.keyOrder.length; a < len; a++) {
            var keyName = format.keyOrder[a];
            object[keyName] = read(format[keyName], dataView, isFormatPreValidated);
         }
      }
      else { // 0, 1, or 2 keys (may include defineAs)...
         for (var a = 0, keyList = Object.keys(format), len = keyList.length; a < len; a++) {
            if (keyList[a] != "defineAs")
               object[keyList[a]] = read(format[keyList[a]], dataView, isFormatPreValidated);
         }
      }
      return object;
   }
   
   Object.defineProperty(window, "JSBinaryUtil", {
      writable: true,
      configurable: true,
      value: Object.create(Object.prototype, {
         /* serialize
          * Turns JS object trees (no graphs with cycles) into a binary structure according to a provided format
          * returns an ArrayBuffer with the data.
          * param1 required: a format
          * param2 required: JavaScript object graph to serialize
          */
         serialize: {
            enumerable: true,
            writable: true,
            configurable: true,
            value: function(format, jsData) {
               try {
                  var sizes = [], values = [];
                  var totalBytesToAllocate = prepare(format, jsData, sizes, values);
                  var b = new ArrayBuffer(totalBytesToAllocate);
                  fillDataView(new DataView(b), sizes, values);
                  nonCyclicalFormat.clear(); // Clear when done (don't leak any references)
                  UserDefinedFormat = {}; // Clear any user-defined formats on function-exit
                  return b;
               }
               catch (error) {
                  // For any thrown error, make sure the set is cleared...
                  nonCyclicalFormat.clear(); // Clear when done (don't leak any references)
                  UserDefinedFormat = {}; // Clear any user-defined formats on exit
                  // Re-throw
                  throw error;
               }
            }
         },
         /* parse
          * Turns binary data into a JS object tree according to the provided format array
          * returns a JS object graph
          */
         parse: {
            enumerable: true,
            writable: true,
            configurable: true,
            value: function(format, binaryData) {
               if (ArrayBuffer.isView(binaryData))
                  binaryData = binaryData.buffer; // Get the raw buffer from the view
               if (!(binaryData instanceof ArrayBuffer))
                  throw TypeError("Second parameter must be binary data of some form");
               try {
                  globalReadPosition = 0;
                  var value = read(format, new DataView(binaryData));
                  // Make sure I read it all...
                  if (globalReadPosition != binaryData.byteLength)
                     throw Error("Failed to read all of the available binary data");
                  nonCyclicalFormat.clear(); // Clear this when done. Parsing doesn't use the nonCyclicalJSData memory, so no need to clear it.
                  UserDefinedFormat = {}; // Clear any user-defined formats on exit
                  return value;
               }
               catch (error) {
                  // Always clear this in case of error to avoid leaking any objects
                  nonCyclicalFormat.clear();
                  UserDefinedFormat = {}; // Clear any user-defined formats on exit
                  throw error; // Re-throw
               }
            }
         }       
      })
   });
})();


/** NEW FORMAT
  A format is either a string, an array, or an object
  A string is a "simple format"
  An array indicates a repeating form of what is contained in it (only one format may be defined in an array format)
  An object indicates a group of formats where each format is associated with a name, and an order is required where
    more than one format is present.

  Simple Formats:
  * numbers: "uint8", "int8", "uint16", "int16", "uint32", "int32", "float32", "float64"
  * strings: "ascii"
  * boolean: "bool"
  * if the simple format can be nullable: append a "?" to the end of the simple format string
  * Simple formats may be used as the only value provided as a single format, the only value of an array, or 
    the value of an object property.
  
  Array Formats:
  * Must have at most a single contained element which is the format of the array. Serialized arrays must have 
    consistent elements that match the format, or the serializer will reject them (arrays of multiple types are
    not supported).
  * Example: ["uint8?"] - an array of nullable uint8 number values
  * Example: ["bool"] - an array of boolean values
  * Example: [{}] - an array of objects (of which no properties will be serialized on the objects)
  * if the array format should be nullable, add a second array element to the format with the string
    value "?" to indicate nullable.
  
  Object Formats:
  * Are an object literal whose enumerable properties are the set of object keys that will be serialized and 
    the property values are the format that the property contains (either another object, array, or simple format.
    To be a well-formed object format (where at least two keys will be processed) it MUST contain a property 
    named "keyOrder" (case-sensitive). This property must be an array of strings whose
    values represent the order in which the keys of the object will be processed. All named format properties must
    be present in the keyOrder list.
  * Example: {name:"bool"} - serialize a property named "name" on an object, and its simple format is a boolean
  * Example: {prop:["ascii?"]} - the property "prop" is an array format containing nullable strings
  * Example: {x:{y:"uint32"}} - "x" is an object format containing a property "y" to serialize as a simple "uint32" number
  * Example: {x:"uint8",y:"uint8",keyOrder:["x","y"]} - serialize the value of "x" followed by "y"
  * if the object format should be nullable, add an addition key to the object called "defineAs" with the string
    value "?" to indicate that the object is nullable.
  
  Custom Formats:
  * Object and array formats can be reused by defining them with a simple string name, and then referencing that string
    anywhere a format can be used. This is done for object formats by using the 'defineAs' property name with string 
    value which will be the name to use in subsequent references. For Array formats, the custom name should be included
    as the second array element in the format definition.
  * Example: {name:"ascii?",defineAs:"nameOb"} - then later referencing this object using: "nameOb" as the format.
  * The custom formats can be made nullable by appending a "?" to the end of the new name. Note that a format defined as
    "a" and one defined as "a?" are completely unique formats (and not related to each other).
  
*/

// Serializer binary layout:
// * nullable types add an extra leading uint8 byte to 
//   indicate whether the value is null/undefined or not. If so, 
//   the actual value is not stored. (Booleans do not require an extra byte)
//   [nullable-flags][optional-value]
// * array types store an extra leading uint8 byte to indicate 
//   the type of the following "length" byte (dynamically 
//   assigned based on the actual length contents). Following
//   this byte is the length value stored as either a uint8,
//   uint16, uint32, or float64.
//   [nullable-flags-if-indicated][length-flags][array length][element-0 .. element-N]
// * string types store an extra leading uint8 byte similar
//   to arrays, along with the length of the string
//   [nullable-flags-if-indicated][length-flags][string length][char-0 .. char-N]
// * object types just store the values in the provided sequence (no preamble)

/*

(function JSBinaryUtilTesting() {
    var tests = [
       { // 0
           control: 33,
           format: "uint8",
           expect: true
       },{
           control: 4523123.3213,
           format: "float64",
           expect: true
       },{ // 2
           control: "hello",
           format: "ascii",
           expect: true
       },{
           control: true,
           format: "bool",
           expect: true
       },{ // 4
           control: -23,
           format: "int16",
           expect: true
       },{
           control: -5,
           format: "uint8",
           expect: false // negative control value for type
       },{ // 6
           control: 999999999,
           format: "uint16",
           expect: false // control value overflow
       },{
           control: false,
           format: "uint32",
           expect: false // type mismatch
       },{ // 8
           control: 2,
           format: "ascii",
           expect: false // type mismatch
       },{
           control: null,
           format: "uint32?",
           expect: true
       },{ // 10
           control: null,
           format: "uint32",
           expect: false // non-nullable type
       },{
           control: null,
           format: "bool?",
           expect: true
       },{ // 12
           control: null,
           format: "bool",
           expect: false // non-nullable type
       },{
           control: { version: 5 },
           format: { version: "uint8" },
           expect: true
       },{ // 14
           control: { a: "a", b: "b" },
           format: { b: "ascii", a: "ascii", keyOrder:["a","b"]},
           expect: true
       },{
           control: { a: "a", b: "b" },
           format: { a: "ascii", b: "ascii" },
           expect: false // no keyOrder!
       },{ // 16
           control: {},
           format: {},
           expect: true
       },{
           control: [],
           format: [],
           expect: true
       },{ // 18
           control: [{}],
           format: [{}],
           expect: true
       },{
           control: [15.5, 78322],
           format: ["float32"],
           expect: true
       },{ // 20
           control: [0, null, 2, -3333],
           format: ["int16?"],
           expect: true
       },{
           control: [0, -1],
           format: ["uint8?"],
           expect: false // negative value in array range
       },{ // 22
           control: [{prop:"yes"}],
           format: [{prop:"ascii"}],
           expect: true
       },{
           control: [{prop:"yes"}],
           format: [{prop:"ascii"}],
           expect: true
       },{ // 24
           control: [{prop:null,prop2:null}],
           format: [{keyOrder:["prop","prop2"],prop2:"float64?",prop:"ascii?"}],
           expect: true
       },{
           control: [
              {p:[5.21, 6.2]},
              {p:[2.0, 2.35]}],
           format: [{p:["float64"]}],
           expect: true
       },{ // 26
           control: [
              {
                  col: [
                     {a:88,b:255,g:0,r:0},
                     {a:1001.5,b:0,g:0,r:0}],
                  name: "v1",
                  row: [
                     {a:0.5,b:0,g:0,r:255},
                     {a:0.9973816275596619,b:0,g:255,r:0}]},
            
              {
                  col: [
                     {a:982922.1875,b:255,g:0,r:254},
                     {a:6,b:0,g:0,r:255}],
                  name: "null",
                  row: [
                     {a:255.5,b:0,g:0,r:0},
                     {a:-15,b:0,g:255,r:1}]}],
           format: [{
               row:[{g:"uint8",r:"uint8",a:"float32",b:"uint8",keyOrder:["r","g","b","a"]}],
               col:[{b:"uint8",g:"uint8",a:"float32",r:"uint8",keyOrder:["r","g","b","a"]}],
               name:"ascii",
               keyOrder:["name","row","col"]}],
           expect: true
       },{
           control: [
              {
                  col: [
                     {r:88,b:255,g:0,a:0},
                     {a:1001.5,b:0,g:0,r:0}],
                  name: "v1",
                  row: [
                     {a:0.5,b:0,g:0,r:255},
                     {a:0.9973816275596619,b:0,g:255,r:0}]},
            
              {
                  col: [
                     {a:982922.1875,b:255,g:0,r:254},
                     {a:6,b:0,g:0,r:255}],
                  name: "null",
                  row: [
                     {a:255.5,b:0,g:0,r:0},
                     {a:-15,b:0,g:255,r:1}]}],
           format: [{
               row:[{g:"uint8",r:"uint8",a:"float32",b:"uint8",keyOrder:["r","g","b","a"],defineAs:"rgba"}],
               col:["rgba"],
               name:"ascii",
               keyOrder:["name","row","col"]}],
           expect: true
       },{ // 28
           control: {
               r1: { p:233.1, q: 33.1, x: 2, y: 3, r: 354.222001, f: [1.1, 2.3, 5.5]}},
           format: {
               r1: { p:"float64", q: "float64", x: "int8", y: "int8", r: "float64", f: ["float64"], keyOrder: ["p","q","x","y","r"]}},
           expect: false // missing "f" in keyOrder
       },{
           control: {
               r1: { p:233.1, q: 33.1, x: 1, y: 1, r: 351.222001, f: [1.1, 2.1, 5.5]},
               r2: { p:233.2, q: 33.2, x: 2, y: 2, r: 352.222002, f: [2.1, 2.2, 6.6]},
               r3: { p:233.3, q: 33.3, x: 3, y: 3, r: 353.222003, f: [3.1, 2.3, 7.7]},
               r4: { p:233.4, q: 33.4, x: 4, y: 4, r: 354.222004, f: [4.1, 2.4, 8.8]},
               r5: { p:233.5, q: 33.5, x: 5, y: 5, r: 355.222005, f: [5.1, 2.5, 9.9]}},
           format: {
               keyOrder:["r1","r2","r3","r4","r5"],
               r2: "rowspec",
               r3: "rowspec",
               r1: { p:"float64", q: "float64", x: "int8", y: "int8", r: "float64", f: ["float64"], keyOrder: ["p","q","x","y","r","f"], defineAs:"rowspec"},
               r4: "rowspec",
               r5: "rowspec"},
           expect: true
       },{ // 30
           control: {
               first: { name: "Mando" },
               last: { name: "M." }},
           format: {
               keyOrder: ["first","last"],
               last: { name: "ascii", defineAs: "name" },
               first: "name"},
           expect: false // defineAs is encountered too late.
       },{
           control: {
               first: { name: "Mando" },
               last: { name: "M." }},
           format: {
               keyOrder: ["first","last"],
               first: { name: "ascii", defineAs: "name" },
               last: "name"},
           expect: true
       },{ // 32
           control: {
               p: { art: "paint" }},
           format: {
               p: { art: "ascii", defineAs: "int8" }},
           expect: false // defineAs re-defines existing well-defined formats!
       },{
           control: {
               p: { art: "paint" },
               q: { type: "thing" }},
           format: {
               keyOrder:["p","q"],
               p: { art: "ascii", defineAs: "thing1" },
               q: { type: "ascii?", defineAs: "thing1" }},
           expect: false // tries to re-defines a user-defined format name.
       },{ // 34
           control: {
               a: {},
               b: {},
               c: null
           },
           format: {
               a: { defineAs: "ob" },
               b: { defineAs: "ob2?" },
               c: "ob2?",
               keyOrder: ["a","b","c"]
           },
           expect: true
       },{
           control: {
               a: null,
               b: [ {}, null ]
           },
           format: {
               keyOrder: ["a","b"],
               a: [{ defineAs: "?" }, "nullArray?"],
               b: "nullArray?"
           },
           expect: true
       },{ // 36
           control: {
               a: {},
               b: {},
               c: null
           },
           format: {
               a: { defineAs: "ob" },
               b: { defineAs: "ob?" },
               c: "ob?",
               keyOrder: ["a","b","c"]
           },
           expect: true
       }
    ];


    function cmp(x,y) {
        var typeX = typeof x;
        var typeY = typeof y;
        if (typeX != typeY)
            return false;
            // Types are the same...
        else if (typeX == "object") {
            if ((x === y) && (x === null))
                return true;
            else if ((x === null) || (y === null))
                return false;
                // Are they both (this conditional) or one (the following) an array?
            else if (Array.isArray(x) && Array.isArray(y)) {
                if (x.length != y.length)
                    return false;
                for (var a = 0, len = Math.min(x.length, y.length); a < len; a++) {
                    if (!cmp(x[a],y[a]))
                        return false;
                }
                return true;
            }
            else if (Array.isArray(x) || Array.isArray(y))
                return false;
            // They must be standard objects
            var keysX = Object.keys(x);
            var keysY = Object.keys(y);
            if (keysX.length != keysY.length)
                return false;
            for (var a = 0, len = keysX.length; a < len; a++) {
                var aFound = false;
                for (var b = 0; (b < len) && !aFound; b++) {
                    if (keysX[a] == keysY[b]) {
                        if (!cmp(x[keysX[a]], y[keysY[b]]))
                            return false;
                        aFound = true;
                    }
                }
                if (!aFound)
                    return false;
            }
            return true;
        }
        else
            return x == y;
    }

    var doTesting = false;
    var showSuccessfullyRunTests = false;
    if (doTesting) {
        console.log("Running tests...");
        tests.forEach(function (test, i) {
            try {
                if (test.expect != cmp(JSBinaryUtil.parse(test.format, JSBinaryUtil.serialize(test.format, test.control)), test.control))
                    console.error("Test " + i + " failed to compare equally");
                else if (showSuccessfullyRunTests)
                    console.log("Test " + i + " passed (object serialized to binary and restored back to object without loss)");
            } catch (ex) {
                if (test.expect)
                    console.error("Test " + i + " threw exception: " + ex);
                else if (showSuccessfullyRunTests)
                    console.log("Test " + i + " passed (expected exception: " + ex + ")");
            }
        });
        console.log("Tests complete");
    }

})();

*/