﻿"use strict";

(function ModelActorScope() {

    // Cell navigation:
    // * Keyboard assist (logical movement):
    //   <up>/<left>/<right>/<down> these keys move you along a path that most closely matches the logical direction indicated. Where there is ambiguity, after pressing
    //                              the key, an angle is shown and the key must be repeated to confirm the direction, keys to the CCW/CW from the key will cycle through 
    //                              alternate angles, again with the original angle key used to confirm the choice. To cancel and make an alternate choice, use the
    //                              direction key opposite the intended direction to "back out"
    //                              and a repeat of the original angle will confirm the choice.



    // Actors:
    // mouse
    // firefly (mobile light, - mostly dark
    // fog (mobile game obscuring) - mostly light
    // snake (mobile blocker)
    // non-mobile
    // static fog
    // light
    // fan (static fog dispersal)

    // Actors:
    // ------------
    // * location (cell where it is now, read/write - if written, re-sets the history
    // * assertiveness (read/write) - changes the length/algorithm for determining the visible path.
    // * goal (an actor or Item or null)
    // * agility (read/write)
    // * size (read/write)
    // * class (actor classification)
    // * Cell move() ACTION - returns the next cell along this actor's goal
    // PRIVATE:
    // * visiblePath - how far along to the goal this actor can see (based on assertiveness). When exhausted, visible path is recomputed.
    // * pathIndex - where the actor is along it's visible path.
    // * history (cells where they've been - limited in some actors, 100% preserved for the mouse (list of Edges)
    //   (body length comes out of the history for a snake)


})();